FROM continuumio/miniconda3
MAINTAINER HAL24K "docker@hal24k.com"

# Create a working directory
RUN mkdir water_situational_awareness
WORKDIR water_situational_awareness

# Setup HAL24K pip server creds
ARG PIP_TRUSTED_HOST=pipserver.dimension.ws
ARG PIP_EXTRA_INDEX_URL=https://hal_muser:Mkw5VxNDNxkOMYrIDqdAy645mLGvnqxW@pip.dimension.ws:443/

# Copy install dependencies and create conda environmemnt
COPY environment.yml .

RUN conda env create -f environment.yml
RUN echo "source activate app_env" > ~/.bashrc \
    && conda clean -afy
ENV PATH /opt/conda/envs/app_env/bin:$PATH

# Copy app files to image
COPY ./src ./src
WORKDIR src
ENV WRKDIR /water_situational_awareness/src

# Add dimension creds
RUN rm -f conf/mongo_conf.txt \
    && touch conf/mongo_conf.txt \
    && echo "[mongoDB credentials]" > conf/mongo_conf.txt \
    && echo "username : " >> conf/mongo_conf.txt \
    && echo "password : " >> conf/mongo_conf.txt \
    && echo "database : cd_waterdemo" >> conf/mongo_conf.txt \
    && echo "uri : mongodb+srv://cd_waterdemo:jA0VmwEW86MAVb8MhiHiJokvvJuYUFp9@atlas-prod-pl-0.hldmf.mongodb.net" >> conf/mongo_conf.txt



ENV URL_PREFIX='/'
ENV PYTHONPATH=/src:/src/yorkshirewater-deploy-temp
ENV SERVER_HOST='0.0.0.0'
ENV SERVER_PORT=80
#EXPOSE 8000

RUN ls -lcrth && which python && echo $PYTHON_PATH

ADD start.sh /
RUN chmod +x /start.sh

CMD ["/start.sh"]
