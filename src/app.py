"""
Copyright HAL24K 2020
app entry point, runs server

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY=== 


"""
from visualization.index import app
from argparse import ArgumentParser

water_app = app
server = water_app.server

if __name__ == "__main__":
    argparser = ArgumentParser()
    argparser.add_argument("-local_mode", action="store_true",
                           default=False, required=False)
    argparser.add_argument("-remote_debugger", action="store_true",
                           required=False, default=True)

    args = argparser.parse_args()

    water_app.run_server(debug=True)
