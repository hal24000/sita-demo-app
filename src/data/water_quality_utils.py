"""Utility functions to support water quality calculations
Copyright: HAL24K 2020
Authors: Adam Hill - adam.hill@hal24k.com

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===


"""

import numpy as np
import pandas as pd
import logging
import plotly
import plotly.graph_objects as go
import plotly.express as px
from .collections_object import ProjectCollections as prj_collections
from data.project_loggers import dash_logger

logger = logging.getLogger(dash_logger)

# Master lookup references for utility functions
# Master WQ metrics dataframe
metric_config = prj_collections.metric_config

# Dictionary lookup for site references
metric_lookup = {
        site: metric_config.query(f"location == '{site}'")
                  .loc[:, ["tag_name", "functional_area"]]
            .to_dict(orient="records")
        for site in metric_config["location"].unique()
}

# Dictionary lookup for individual WQ sensor tags
tag_lookup = metric_config.set_index("tag_name").T.to_dict()


# noinspection PyRedundantParentheses
def get_site_wq_metrics(site: str, selected_areas: tuple = ("all")) -> list:
    """Function that fetches all relevant WQ tags for a given site location. Optionally it can retrieve for a subset
    of functional areas

    Args:
        site, str: reference of the location of the WTW
        selected_areas, tuple: a tuple of strings that are known functional areas; defaults to "all"

    Returns:
        list: a list of strings that correspond to the tag_names of the WQ sensors
    """
    tags = metric_lookup.get(site, [])
    if "all" in selected_areas:
        tag_list = [tag.get("tag_name") for tag in tags]
    else:
        tag_list = [
                tag.get("tag_name")
                for tag in tags
                if tag.get("functional_area") in selected_areas
        ]
    return tag_list


def load_wq_data(
        site: str, date, time, lookback_days: int, metrics: tuple
) -> pd.DataFrame:
    """Function to load the Water Quality data for a given time range at a given site

    Args:
        site, str:
        date, str:
        time, str:
        lookback_days, int:
        metrics, tuple:

    Returns:
        DataFrame:
    """
    end_date = pd.to_datetime(
            f"{date} {time}", format=prj_collections.dashboard_datetime_format
    )
    start_date = end_date - pd.Timedelta(days=lookback_days)

    sensor_list = get_site_wq_metrics(site=site, selected_areas=metrics)

    if not sensor_list:
        return pd.DataFrame()

    query = {
            "tag_name":              {"$in": sensor_list},
            "measurement_timestamp": {"$gte": start_date, "$lte": end_date},
    }
    project = {
            "_id":                   0,
            "measurement_timestamp": 1,
            "measurement_value":     1,
            "tag_name":              1,
    }

    dataframe = pd.DataFrame(prj_collections.series_coll.find(query, project))
    dataframe.loc[:, "measurement_timestamp"] = dataframe['measurement_timestamp'].dt.tz_localize("UTC")

    return dataframe


def resample_wq_data(wq_df: pd.DataFrame):
    """Function that resamples a water quality group dataframe

    Args:
        wq_df: a dataframe that is extracted from a group-by on tag_name and hence has df.name == tag_name

    Returns:

    """

    def _resample(dataframe: pd.DataFrame):
        """A utility function that when applied to a dataframe applies the correct resampling and aggregation
        functions based upon the master metrics record

        Args:
            dataframe:

        Returns:

        """
        name = dataframe.name
        resample_period = tag_lookup.get(name).get("risk_scoring_period")
        agg_method = tag_lookup.get(name).get("risk_scoring_aggregation")

        res = (
                dataframe.set_index("measurement_timestamp")
                    .sort_index()
                    .resample(f"{resample_period}s", label="left")
                    .agg(agg_method).round(2)
                    .ffill()
        )
        return res

    resampled_series = wq_df.groupby("tag_name").apply(_resample)
    return resampled_series


def process_resampled_wq_data(tag_wq_df: pd.DataFrame):
    """Function that enriches the resampled water quality data with the rate of change thresholds and calculates
    the changes over the time-series.

    Args:
        tag_wq_df:

    Returns:

    """
    tag_wq_df = tag_wq_df.sort_index().reset_index()

    # Correct for any missing values and calculate the difference between measurements
    tag_wq_df.loc[:, "measurement_value"] = tag_wq_df.loc[
                                            :, "measurement_value"
                                            ].fillna(method="ffill")
    tag_wq_df.loc[:, "Delta"] = tag_wq_df.loc[:, "measurement_value"].diff()

    # Convert to local UK time
    tag_wq_df.loc[:, "measurement_timestamp"] = tag_wq_df['measurement_timestamp'].dt.tz_convert(
        prj_collections.tz_london)

    # Merge in metric information about rate of change measures
    tag_wq_df = pd.merge(
            tag_wq_df,
            metric_config[["metric", "tag_name", "cutoff_drop", "cutoff_rise"]],
            on="tag_name",
            how="left",
    )

    # Calculate whether there has been an unacceptable rate of change
    tag_wq_df["nominal_ROC"] = (
            tag_wq_df[["cutoff_drop", "cutoff_rise"]]
                .replace([-np.inf, np.inf], 0)
                .abs()
                .max(axis=1)
    )

    # Categorise the changes
    intervals = (-np.inf, np.max([tag_wq_df['cutoff_drop'].iloc[0,], -9999]),
                 np.min([tag_wq_df['cutoff_rise'].iloc[0,], 9999]), np.inf)
    labels = ["drop", "normal", "rise"]
    tag_wq_df["status"] = pd.cut(tag_wq_df.loc[:, 'Delta'], intervals, right=True, labels=labels)
    tag_wq_df["Delta"] = tag_wq_df["Delta"].map(lambda x: 0 if np.abs(x) < 1e-8 else x)
    return tag_wq_df.reset_index(drop=True)


def count_unchanged_periods(dataframe: pd.DataFrame, status_column: str = 'status',
                            count_column: str = 'periods_of_unchanged_status') -> pd.DataFrame:
    """
    In a time series data counts the number of times a status has been unchanged.

    Args:
        dataframe (pd.DataFrame): input dataframe, assumed ordered by time
        status_column (str): name of the column with status (default: 'status')
        count_column (str): name of the new column (default: 'periods_of_unchanged_status')

    Returns:
        pd.DataFrame
    """
    # Create local copy of the dataframe
    dataframe = dataframe.copy()

    # Convert status column into categorical columns with boolean values
    categorical_df = pd.get_dummies(dataframe[status_column])
    column_names = tuple(categorical_df.columns)

    # For each status create a groupID for periods of time when that status has not changed
    group_ids = (categorical_df == 0).cumsum() \
        .rename(columns={col: f"{col}_group" for col in column_names})
    combined_df = pd.concat([group_ids, categorical_df], axis=1)
    for column in column_names:
        categorical_df[column] = combined_df.groupby(f"{column}_group")[column].cumsum()

    dataframe.loc[:, count_column] = categorical_df.max(axis=1).values
    return dataframe


def get_latest_status(dataframe: pd.DataFrame, filter_normal=True, min_occurrence=2):
    """

    Args:
        dataframe:
        filter_normal:
        min_occurrence:

    Returns:

    """
    if filter_normal:
        if dataframe.loc[dataframe['status'] != "normal", "periods_of_unchanged_status"].max() >= min_occurrence:
            return dataframe.sort_values("measurement_timestamp", ascending=False).head(1)
        else:
            return pd.DataFrame()
    else:
        return dataframe.sort_values("measurement_timestamp", ascending=False).head(1)


def convert_to_table_format(dataframe: pd.DataFrame):
    """

    Args:
        dataframe ():

    Returns:

    """
    metadata_cols = ['functional_area', "measurement_unit"]
    column_renaming = {
            "functional_area":             "Func. Area",
            "metric":                      "Metric",
            "measurement_timestamp":       "Interval Start Time",
            "measurement_unit":            "UOM",
            "measurement_value":           "Mean Reading",
            "nominal_ROC":                 "Threshold",
            "Delta":                       "ROC",
            "status":                      "ROC Status",
            "periods_of_unchanged_status": "#Periods",
    }

    dataframe = dataframe.copy()

    # Tidy up the datetime presentation
    dataframe.loc[:, 'measurement_timestamp'] = dataframe['measurement_timestamp'].map(
        lambda x: x.strftime('%d/%m/%Y %H:%M'))

    # Add some extra data and rename columns
    metadata_cols = dataframe['tag_name'].apply(
            lambda tag: pd.Series({key: tag_lookup.get(tag).get(key) for key in metadata_cols}))
    dataframe = dataframe.merge(metadata_cols, left_index=True, right_index=True)
    dataframe = dataframe.rename(columns=column_renaming)
    return dataframe[column_renaming.values()]


def wq_tag_processor(wq_dataframe: pd.DataFrame, filter_normal: bool = True, min_occurrence: int = 2):
    """

    Args:
        wq_dataframe:
        filter_normal:
        min_occurrence:

    Returns:

    """
    result = wq_dataframe.pipe(process_resampled_wq_data) \
        .pipe(count_unchanged_periods) \
        .pipe(get_latest_status, filter_normal=filter_normal, min_occurrence=min_occurrence)

    return result


def get_wq_summary_data(site, date=None, time=None):
    """Function to return a table and the corresponding tags of the water quality sensors
    current status for those that have had abnormal rate-of-change in the previous 24 hours.

    Args:
        site:
        date:
        time:

    Returns:

    """
    if date is None:
        date = pd.Timestamp.utcnow().strftime("%d/%m/%Y")
    if time is None:
        time = pd.Timestamp.utcnow().strftime("%H:%M")

    all_columns = ['Func. Area', 'Metric', 'Interval Start Time', 'UOM', 'Mean Reading',
                   'Threshold', 'ROC', 'ROC Status', '#Periods']
    wq_dataframe = load_wq_data(site, date, time, lookback_days=1, metrics=("all",))
    if wq_dataframe.shape[0] < 1:
        return make_empty_table_dataframe(columns=all_columns), tuple()

    wq_dataframe = resample_wq_data(wq_dataframe)
    wq_grouped = wq_dataframe.groupby("tag_name")
    wq_processed = wq_grouped.apply(wq_tag_processor, filter_normal=True, min_occurrence=2) \
        .reset_index(drop=True)

    if wq_processed.shape[0] < 1:
        return make_empty_table_dataframe(columns=all_columns), tuple()

    alert_tags = tuple(wq_processed['tag_name'].unique())

    clean_table = wq_processed.pipe(convert_to_table_format) \
        .sort_values(by=["Func. Area"])
    clean_table = clean_table.round({'Reading': 3, 'ROC': 3})
    return clean_table, alert_tags


def get_wq_plot(datetime: str, alert_tags: tuple, lookback_days: int = 1):
    """

    Args:
        datetime ():
        alert_tags ():
        lookback_days ():

    Returns:

    """
    end_date = pd.to_datetime(datetime, format=prj_collections.dashboard_datetime_format).tz_localize("UTC")
    start_date = end_date - pd.Timedelta(days=lookback_days)

    sensors = alert_tags

    if not sensors:
        wq_dataframe = pd.DataFrame()

    query = {
            "tag_name":              {"$in": sensors},
            "measurement_timestamp": {"$gte": start_date, "$lte": end_date},
    }
    project = {
            "_id":                   0,
            "measurement_timestamp": 1,
            "measurement_value":     1,
            "tag_name":              1,
    }

    wq_dataframe = pd.DataFrame(prj_collections.series_coll.find(query, project))
    if wq_dataframe.empty:
        fig = make_empty_plot(annotation='There are no alerting tags.')
        return fig

    wq_dataframe = resample_wq_data(wq_dataframe).reset_index()

    # Convert the timestamps to correct timezone
    wq_dataframe.loc[:, "measurement_timestamp"] = wq_dataframe['measurement_timestamp'].dt.tz_localize("UTC")
    wq_dataframe.loc[:, "measurement_timestamp"] = wq_dataframe['measurement_timestamp'].dt.tz_convert(
        prj_collections.tz_london)
    logger.info(wq_dataframe.tail(3))

    metadata_cols = ['functional_area', "measurement_unit", "metric"]
    metadata_cols = wq_dataframe['tag_name'].apply(
            lambda tag: pd.Series({key: tag_lookup.get(tag).get(key) for key in metadata_cols}))
    wq_dataframe = wq_dataframe.merge(metadata_cols, left_index=True, right_index=True)
    fig = generate_wq_linegraph(wq_dataframe,
                                min_time=start_date.tz_convert(prj_collections.tz_london),
                                max_time=end_date.tz_convert(prj_collections.tz_london))
    fig.update_xaxes(
            dtick=3 * 60 * 60 * 1000,
            tickformat="%H:%M\n%d\n%b"
    )
    return fig


def generate_wq_linegraph(dataframe, min_time=None, max_time=None):
    """"""
    fig = plotly.subplots.make_subplots(rows=1,
                                        cols=1,
                                        print_grid=True,
                                        horizontal_spacing=0.15,
                                        vertical_spacing=0.12,
                                        specs=[[{'secondary_y': True}]]
                                        )

    if dataframe.shape[0] > 0:
        if min_time is None:
            min_time = dataframe['measurement_timestamp'].min()
        if max_time is None:
            max_time = dataframe['measurement_timestamp'].max()

        dataframe = dataframe.sort_values(by="measurement_timestamp")

        colors = px.colors.qualitative.Dark24
        uom = list(dataframe['measurement_unit'].value_counts().sort_values(ascending=False).index)
        primary_y_units = uom[0]
        secondary_y_units = ''
        if len(uom) > 1:
            secondary_y_units = uom[1]

        for z, (metric, color) in enumerate(zip(dataframe['metric'].unique(), colors)):
            # if z == 0: primary_y_units = dataframe['measurement_unit'].iloc[0]
            # if z == 1: secondary_y_units = dataframe['measurement_unit'].iloc[0]
            # if primary_y_units == secondary_y_units: secondary_y_units=''

            data_to_plot = dataframe[dataframe['metric'] == metric]
            if data_to_plot['measurement_unit'].iloc[0] == secondary_y_units:
                secondary_y = True
            else:
                secondary_y = False

            trace = go.Scatter(x=data_to_plot["measurement_timestamp"],
                               y=data_to_plot['measurement_value'],
                               name=metric, line=dict(color=color))
            fig.add_trace(trace, secondary_y=secondary_y)

        fig.update_xaxes(range=[min_time, max_time])
        fig.update_yaxes(title_text=f"<b>{primary_y_units}</b>", secondary_y=False)
        fig.update_yaxes(title_text=f"<b>{secondary_y_units}</b>", secondary_y=True)

        fig.update_layout(
                xaxis_tickformat='%d-%m-%Y %H:%M',
                legend=dict(
                        orientation="h",
                        yanchor="bottom",
                        y=1.02,
                        xanchor="right",
                        x=1
                ))
        fig.update_layout(margin={'t': 60, 'b': 10, 'l': 10, 'r': 10})

    return fig


def make_empty_plot(annotation: str = '', layout_update: dict = None):
    """
    Create an empty plotly plot.

    Args:
        annotation (str): add an optional text to the plot.
    """

    fig = px.scatter()
    fig.update_layout(annotations=[
            dict(
                    x=0.5,
                    y=0.5,
                    xref='paper',
                    yref='paper',
                    showarrow=False,
                    text=annotation

            )],
            xaxis={
                    'title':          "",
                    'showgrid':       False,
                    'ticks':          '',
                    'showticklabels': False
            },
            yaxis={
                    "title":          "",
                    'showgrid':       False,
                    'ticks':          '',
                    'showticklabels': False
            },
            title='',
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            margin=dict(
                    l=2,
                    r=2,
                    b=2,
                    t=2,
            ),

    )

    if layout_update is not None:
        fig.update_layout(**layout_update)

    return fig


def make_empty_table_dataframe(columns: list, no_rows: int = 1) -> pd.DataFrame:
    """
    Create a dataframe with set columns and empty string data.

    Args:
        columns (list): list of column titles
        no_rows (int): number of empty rows to create (default: 1)

    Returns:
        pd.DataFrame
    """

    return pd.DataFrame(no_rows * [dict(zip(columns, [''] * len(columns)))], columns=columns)
