"""
Utility functions for calculating duty cycles of pumps
Author: Adam Hill, HAL24K 2020

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""

from pymongo import UpdateOne
import pandas as pd
import plotly.express as px
import logging

from .collections_object import ProjectCollections as prj_collections
from .load_data import colors_rag as rag
from .water_quality_utils import count_unchanged_periods
from .summary_utils import (
    get_equipment_dataframe,
    get_pump_data,
    find_pump_run_signal_sensor,
)
from .project_loggers import dash_logger

logger = logging.getLogger(dash_logger)


def get_dutycycle_pump_sensors(site: str, pump_filter=True) -> dict:
    """

    Args:
        site:
        pump_filter:

    Returns:

    """
    tag_subcat_filters = ("Speed", "RunStatus")
    runtime_sensors = get_equipment_dataframe(
        site=site, equip="Pump", tag_subcat_filter=tag_subcat_filters
    )
    if pump_filter:
        runtime_sensors = runtime_sensors.pipe(find_pump_run_signal_sensor)

    sensor_dict = {}
    for subcat in tag_subcat_filters:
        sensor_dict[subcat] = list(
            runtime_sensors.query("tag_subcategory == @subcat")["tag_name"].unique()
        )

    return sensor_dict


def calculate_daily_pump_duty_stats(
    site=None, startDate=None, endDate=None, pump_filter=True
):
    """

    Args:
        site:
        startDate:
        endDate:
        pump_filter:

    Returns:

    """
    assert site is not None, "Error - a valid site name must be provided"

    # Set up the time range
    if endDate is None:
        endDate = pd.Timestamp.utcnow().floor("D")
    else:
        endDate = pd.to_datetime(endDate)

    if startDate is None:
        startDate = endDate - pd.Timedelta("7d")
    else:
        startDate = pd.to_datetime(startDate)

    sensor_dict = get_dutycycle_pump_sensors(site=site, pump_filter=pump_filter)

    daily_pump_stats = mongo_daily_pump_duty_query(
        sensor_dict, startDate, endDate, threshold=5, duty_frac=0.66
    )
    return daily_pump_stats


# def mongo_daily_speed_duty_query(sensor_list: list, startDate, endDate, threshold=5, duty_frac=0.66) -> pd.DataFrame:
#     """
#
#     Args:
#         sensor_list:
#         startDate:
#         endDate:
#         threshold:
#         duty_frac:
#
#     Returns:
#
#     """
#     if threshold is None:
#         threshold = 5
#
#
#     match = {"tag_name": {"$in": sensor_list},
#              "measurement_timestamp": {
#                  "$gte": startDate,
#                  "$lte": endDate
#              }
#              }
#
#     addfields = {"date":
#         {
#             "year": {"$year": "$measurement_timestamp"},
#             "month": {"$month": "$measurement_timestamp"},
#             "day": {"$dayOfMonth": "$measurement_timestamp"}
#         }
#     }
#
#     group = {
#         "_id": {"date": "$date", "tag_name": "$tag_name"},
#         "count": {"$sum": 1},
#         "count_gt_thresh": {"$sum": {"$cond": [{'$gt': ['$measurement_value', threshold]}, 1, 0]}},
#         "count_eq_thresh": {"$sum": {"$cond": [{'$lte': ['$measurement_value', threshold]}, 1, 0]}},
#         "tag_name": {"$first": "$tag_name"},
#         "date": {"$first": "$date"}
#     }
#
#     project = {
#         "status_datetime": {"$dateFromParts": {'year': "$date.year", 'month': "$date.month", 'day': "$date.day"}},
#         "total_readings": "$count",
#         "percentage_gt_thresh": {"$divide": ["$count_gt_thresh", "$count"]},
#         "on_duty": {"$cond": [{"$gte": [{"$divide": ["$count_gt_thresh", "$count"]}, duty_frac]}, 1, 0]},
#         'tag_name': "$tag_name",
#         "_id": 0
#     }
#
#     sort = {
#         "status_datetime": 1
#     }
#
#     pipeline = [{"$match": match}, {"$addFields": addfields}, {"$group": group}, {"$project": project}, {"$sort": sort}]
#
#     sensor_collection = prj_collections.series_coll
#     result_df = pd.DataFrame(sensor_collection.aggregate(pipeline))
#
#     return result_df
#
#
# def calc_daily_speed_duty_cycles(sensor_list: list, startDate, endDate, threshold=5, duty_frac=0.66) -> pd.DataFrame:
#     """
#
#     Args:
#         sensor_list:
#         startDate:
#         endDate:
#         threshold:
#         duty_frac:
#
#     Returns:
#
#     """
#     duty_cycle_df = mongo_daily_speed_duty_query(sensor_list=sensor_list,
#                                                  startDate=startDate,
#                                                  endDate=endDate,
#                                                  threshold=threshold,
#                                                  duty_frac=duty_frac)
#     if duty_cycle_df.shape[0] == 0:
#         columns = ["status_datetime", "total_readings", "percentage_gt_thresh", "on_duty", "tag_name"]
#         duty_cycle_df = pd.DataFrame(columns=columns)
#     else:
#         duty_cycle_df.loc[:, "threshold_value"] = threshold
#         duty_cycle_df.loc[:, "on_duty_frac"] = duty_frac
#         duty_cycle_df.loc[:, "calculated_at"] = pd.Timestamp.utcnow()
#
#     return duty_cycle_df


def mongo_daily_speed_duty_query(
    sensor_list: list, start, end, duty_frac=0.66
) -> pd.DataFrame:
    """

    Args:
        sensor_list:
        start:
        end:
        duty_frac:

    Returns:

    """

    # Duty Cycle Running events mongo query
    match1 = {
        "tag_name": {"$in": sensor_list},
        "measurement_value": {"$in": ["RUNNING", "Running", "ON"]},
        "$or": [
            {"occurrence_end_time": {"$gte": start, "$lte": end}},
            {
                "$and": [
                    {"occurrence_start_time": {"$lte": start}},
                    {"occurrence_end_time": {"$gte": end}},
                ]
            },
            {
                "$and": [
                    {"occurrence_start_time": {"$gte": start}},
                    {"occurrence_end_time": {"$lte": end}},
                ]
            },
            {
                "$and": [
                    {"occurrence_start_time": {"$gte": start, "$lte": end}},
                    {
                        "$or": [
                            {"occurrence_last_time": {"$gte": end}},
                            {"occurrence_end_time": {"$gte": end}},
                        ]
                    },
                ]
            },
        ],
    }

    project1 = {
        "tag_name": "$tag_name",
        "measurement_value": "$measurement_value",
        "threshold_value": "$threshold_value",
        "occurrence_start_time": {"$max": ["$occurrence_start_time", start]},
        "occurrence_end_time": {"$min": ["$occurrence_end_time", end]},
        "occurrence_duration": {
            "$divide": [
                {
                    "$subtract": [
                        {"$min": ["$occurrence_end_time", end]},
                        {"$max": ["$occurrence_start_time", start]},
                    ]
                },
                1000,
            ]
        },
    }

    group1 = {
        "_id": {"tag_name": "$tag_name"},
        "measurement_value": {"$first": "$measurement_value"},
        "threshold_value": {"$first": "$threshold_value"},
        "total_secs_at_value": {"$sum": "$occurrence_duration"},
    }

    project2 = {
        "tag_name": "$_id.tag_name",
        "status_datetime": start,
        "measurement_value": "$measurement_value",
        "threshold_value": "$threshold_value",
        "percentage_gt_thresh": {"$divide": ["$total_secs_at_value", 86400]},
        "on_duty": {
            "$cond": [
                {"$gte": [{"$divide": ["$total_secs_at_value", 86400]}, duty_frac]},
                1,
                0,
            ]
        },
        "_id": 0,
    }

    sort = {"tag_name": 1, "status_datetime": 1}

    pipeline = [
        {"$match": match1},
        {"$project": project1},
        {"$group": group1},
        {"$project": project2},
        {"$sort": sort},
    ]

    alarm_event_coll = prj_collections.speed_events_coll
    cursor = alarm_event_coll.aggregate(pipeline)

    # Make the dataframe
    duty_cycle_df = pd.DataFrame(cursor)

    return duty_cycle_df


def calc_1day_speed_duty_cycles(sensor_list: list, start, duty_frac=0.66):
    """

    Args:
        sensor_list:
        start:
        duty_frac:

    Returns:

    """

    # Set times
    start = start.floor("d")
    end = start + pd.Timedelta("23h59m59s")

    # Get meta data for the site tags
    sensor_meta = pd.DataFrame(
        [{"tag_name": sensor, "measurement_value": "ON"} for sensor in sensor_list]
    )

    # Get duty cycles and merge with sensor meta data
    duty_cycle_df = mongo_daily_speed_duty_query(
        sensor_list, start, end, duty_frac=duty_frac
    )

    if duty_cycle_df.shape[0] == 0:
        columns = [
            "status_datetime",
            "measurement_value",
            "percentage_gt_thresh",
            "on_duty",
            "tag_name",
            "threshold_value",
        ]
        duty_cycle_df = pd.DataFrame(columns=columns)
    else:
        duty_cycle_df = pd.merge(
            sensor_meta, duty_cycle_df, how="left", on=["tag_name", "measurement_value"]
        )

        # Fill in 0 and the date for tags that had no data that day
        duty_cycle_df.loc[:, ["percentage_gt_thresh", "on_duty"]] = (
            duty_cycle_df.loc[:, ["percentage_gt_thresh", "on_duty"]].fillna(0).round(2)
        )

        duty_cycle_df.loc[:, ["status_datetime", "threshold_value"]] = (
            duty_cycle_df.loc[:, ["status_datetime", "threshold_value"]]
            .fillna(method="ffill")
            .fillna(method="bfill")
        )

        # Store the data for the chosen duty fraction and time of calculation
        duty_cycle_df.loc[:, "on_duty_frac"] = duty_frac
        duty_cycle_df.loc[:, "calculated_at"] = pd.Timestamp.utcnow()

    return duty_cycle_df


def calc_daily_speed_duty_cycles(sensor_list: list, startDate, endDate, duty_frac=0.66):
    """

    Args:
        sensor_list:
        startDate:
        endDate:
        duty_frac:

    Returns:

    """
    all_days = []
    for date in pd.date_range(startDate.floor("d"), endDate.floor("d"), freq="1d"):
        daily_df = calc_1day_speed_duty_cycles(
            sensor_list=sensor_list, start=date, duty_frac=duty_frac
        )
        all_days.append(daily_df)

    final_daily_df = pd.concat(all_days).sort_values(by=["status_datetime", "tag_name"])
    return final_daily_df


def mongo_daily_pump_duty_query(
    sensor_dict: dict, startDate, endDate, threshold=5, duty_frac=0.66
) -> pd.DataFrame:
    """

    Args:
        sensor_dict:
        startDate:
        endDate:
        threshold:
        duty_frac:

    Returns:

    """
    digital_sensor_list = []
    analog_sensor_list = []

    for signal_type in ["RunStatus"]:
        digital_sensor_list += sensor_dict.get(signal_type, [])

    for signal_type in ["Speed"]:
        analog_sensor_list += sensor_dict.get(signal_type, [])

    digital_pump_duty_cycle_df = calc_daily_runstatus_duty_cycles(
        sensor_list=digital_sensor_list,
        startDate=startDate,
        endDate=endDate,
        duty_frac=duty_frac,
    )

    # Update speed "events" collection first
    speed_events_df = calculate_speed_occurrence_events(
        tag_list=analog_sensor_list, start=startDate, end=endDate, threshold=threshold
    )
    update_db = save_speed_events(speed_events_df)
    logger.info(
        f"[{pd.Timestamp.utcnow}] - Updated the speed events collection with {update_db.modified_count} mods"
    )

    # Calc daily speed duty cycles
    analog_pump_duty_cycle_df = calc_daily_speed_duty_cycles(
        sensor_list=analog_sensor_list,
        startDate=startDate,
        endDate=endDate,
        duty_frac=duty_frac,
    )

    all_daily_duty_cycles_df = (
        pd.concat([digital_pump_duty_cycle_df, analog_pump_duty_cycle_df])
        .sort_values(by=["status_datetime", "tag_name"])
        .drop_duplicates(subset=["status_datetime", "tag_name"], keep="last")
        .reset_index(drop=True)
    )

    return all_daily_duty_cycles_df


def save_daily_pump_duty(dataframe: pd.DataFrame):
    """

    Args:
        dataframe:

    Returns:

    """
    collection = prj_collections.pump_duty_coll
    operations = []
    for record in dataframe.to_dict(orient="records"):
        match = {
            "status_datetime": record.pop("status_datetime"),
            "tag_name": record.pop("tag_name"),
        }
        vals = record
        operations.append(
            UpdateOne(match, {"$set": vals, "$inc": {"calc_count": 1}}, upsert=True)
        )
    result = collection.bulk_write(operations)
    return result


def save_speed_events(dataframe: pd.DataFrame):
    """

    Args:
        dataframe:

    Returns:

    """
    collection = prj_collections.speed_events_coll
    operations = []
    for record in dataframe.to_dict(orient="records"):
        match = {
            "status_datetime": record.get("status_datetime"),
            "tag_name": record.get("tag_name"),
        }
        vals = record
        operations.append(
            UpdateOne(match, {"$set": vals, "$inc": {"calc_count": 1}}, upsert=True)
        )
    result = collection.bulk_write(operations)
    return result


def get_daily_pump_duty_db(
    sensor_list: list, startDate=None, endDate=None
) -> pd.DataFrame:
    """

    Args:
        sensor_list:
        startDate:
        endDate:

    Returns:

    """
    collection = prj_collections.pump_duty_coll

    datetime_range = {}
    if startDate is not None:
        datetime_range["$gte"] = pd.to_datetime(startDate.floor("D"))
    if endDate is not None:
        datetime_range["$lt"] = pd.to_datetime(endDate.floor("D"))

    query = {"tag_name": {"$in": sensor_list}}
    if datetime_range:
        query["status_datetime"] = datetime_range

    result_df = pd.DataFrame(collection.find(query, {"_id": 0}))
    return result_df.sort_values(by=["status_datetime", "tag_name"])


def last_pump_status(dataframe: pd.DataFrame) -> pd.DataFrame:
    """

    Args:
        dataframe:

    Returns:

    """
    pump_status_runtimes = dataframe.sort_values(
        by="status_datetime", ascending=True
    ).pipe(count_unchanged_periods, "on_duty")
    return pump_status_runtimes.tail(1)


def fill_data_gap(
    historic_df: pd.DataFrame,
    site: str,
    reference_datetime=None,
    update_db: bool = True,
) -> pd.DataFrame:
    """

    Args:
        historic_df:
        site:
        reference_datetime:
        update_db:

    Returns:

    """
    if reference_datetime is None:
        reference_datetime = pd.Timestamp.utcnow()
    time_gap = check_data_gap_in_pump_history(historic_df, reference_datetime)
    if time_gap > pd.Timedelta("1d"):
        reference_day = reference_datetime.floor("D")
        missing_data = calculate_daily_pump_duty_stats(
            site=site,
            startDate=reference_day - time_gap,
            endDate=reference_day - pd.Timedelta("1ns"),
        )
        if update_db:
            update_results = save_daily_pump_duty(missing_data)
            logger.info(
                f"[{pd.Timestamp.utcnow()}] - Updating the historic pump duty cycle collection: {update_results}"
            )
        historic_df = (historic_df.append(missing_data)).sort_values(
            by=["tag_name", "status_datetime"]
        )
        historic_df = historic_df.drop_duplicates().reset_index(drop=True)
    return historic_df


def check_data_gap_in_pump_history(historic_df: pd.DataFrame, reference_datetime):
    """

    Args:
        historic_df:

    Returns:

    """

    # Find the last update to each tag
    most_recent_tag_update = historic_df.groupby("tag_name").apply(
        lambda x: x.sort_values(by="status_datetime").tail(1)
    )

    # Add UTC timezones to the datetimes
    most_recent_tag_update.loc[:, "status_datetime"] = most_recent_tag_update[
        "status_datetime"
    ].dt.tz_localize("UTC")
    most_recent_tag_update.loc[:, "calculated_at"] = most_recent_tag_update[
        "calculated_at"
    ].dt.tz_localize("UTC")

    # Check if the calculation has been repeated excessively
    calc_check = most_recent_tag_update.calc_count < 8

    # Check if we've attempted this calculation in the recent past
    calc_time_check = (
        pd.Timestamp.utcnow() - most_recent_tag_update.calculated_at
    ) > pd.Timedelta("6h")

    # If we haven't calculated it recently and we haven't done it repeatedly then keep the record in
    # the search for the biggest timelag in our duty calculations
    minimum_tag_timestamp = most_recent_tag_update.loc[
        calc_check & calc_time_check, "status_datetime"
    ].min()

    reference_day = reference_datetime.floor("D")
    return reference_day - minimum_tag_timestamp


def get_pump_duty_status(site: str, reference_datetime=None, pump_filter=True) -> pd.DataFrame:
    """

    Args:
        site:
        reference_datetime:
        pump_filter:

    Returns:

    """
    if reference_datetime is None:
        reference_datetime = pd.Timestamp.utcnow()
    else:
        reference_datetime = reference_datetime.tz_localize("UTC")
    sensor_dict = get_dutycycle_pump_sensors(site=site, pump_filter=pump_filter)
    sensor_list = [sensor for sublist in sensor_dict.values() for sensor in sublist]

    historic_daily_data = get_daily_pump_duty_db(
        sensor_list=sensor_list, endDate=reference_datetime
    )

    # Check there is no missing history
    historic_daily_data = historic_daily_data.pipe(
        fill_data_gap, site=site, reference_datetime=reference_datetime, update_db=True
    )

    # Check the latest results from today and combine with history
    # today_data = calculate_daily_pump_duty_stats(site=site, startDate=reference_datetime.floor('D'), endDate=reference_datetime)
    combined_data = historic_daily_data.sort_values(by="status_datetime").reset_index(
        drop=True
    )

    # Derive the duty cycles
    pump_latest_duty_cycles = combined_data.groupby("tag_name").apply(last_pump_status)
    # pump_latest_duty_cycles = pump_latest_duty_cycles[pump_latest_duty_cycles['on_duty'] == 1]
    # Set "off" pumps to have a "periods_of_unchanged_status" to 0
    off_duty_pumps = pump_latest_duty_cycles["on_duty"] == 0
    pump_latest_duty_cycles.loc[off_duty_pumps, "periods_of_unchanged_status"] = 0
    pump_latest_duty_cycles["status_datetime"] = pump_latest_duty_cycles[
        "status_datetime"
    ].dt.tz_localize("UTC")

    # Filter out status that are not linked to the current time
    pump_latest_duty_cycles = pump_latest_duty_cycles.loc[
        :, ["status_datetime", "tag_name", "on_duty", "periods_of_unchanged_status"]
    ]
    current_status = pump_latest_duty_cycles[
        "status_datetime"
    ] >= reference_datetime.floor("D") - pd.Timedelta("7d")
    pump_latest_duty_cycles = pump_latest_duty_cycles[current_status].reset_index(
        drop=True
    )

    # Enrich the latest duty cycles with extra tag information
    pump_latest_duty_cycles = pump_latest_duty_cycles.pipe(
        merge_meta_data,
        ["tag_name", "tag_short_description", "tag_subcategory", "priority"],
    )

    return pump_latest_duty_cycles


def enrich_tag_information(tag_list: list, metadata_list: list) -> pd.DataFrame:
    """"""
    site_tag_coll = prj_collections.site_tag_config_coll
    query = {"tag_name": {"$in": tag_list}}
    project = {key: 1 for key in metadata_list}
    project["_id"] = 0
    project["tag_name"] = 1
    meta_data_df = site_tag_coll.find(query, project)
    return meta_data_df


def merge_meta_data(base_df: pd.DataFrame, metadata_list: list) -> pd.DataFrame:
    """"""
    tag_names = list(base_df["tag_name"].unique())
    metadata_df = pd.DataFrame(enrich_tag_information(tag_names, metadata_list))
    base_df = base_df.merge(metadata_df, how="left", on="tag_name")
    return base_df


def gen_pump_dutycycle_chart(pump_dutycycle_df: pd.DataFrame):
    """"""
    pump_dutycycle_df = pump_dutycycle_df.sort_values(
        by=["priority", "periods_of_unchanged_status"], ascending=[True, False]
    ).reset_index(drop=True)

    gt_0_status = pump_dutycycle_df.query("periods_of_unchanged_status > 0")
    eq_0_status = pump_dutycycle_df.query("periods_of_unchanged_status == 0")
    pump_dutycycle_df = pd.concat(
        [gt_0_status, eq_0_status], ignore_index=True
    ).reset_index(drop=True)

    top_ten_pumps = pump_dutycycle_df.iloc[0:10]

    args = {
        "hover_name": "tag_short_description",
        "hover_data": ["periods_of_unchanged_status"],
        "orientation": "h",
        "color": "periods_of_unchanged_status",
        "range_color": [0.0, 40],
        "color_continuous_scale": [
            (0.00, rag.get("green")),
            (0.75, rag.get("green")),
            (0.75, rag.get("amber")),
            (0.8749, rag.get("amber")),
            (0.8749, rag.get("red")),
            (1.0, rag.get("red")),
        ],
        "labels": {
            "periods_of_unchanged_status": "# Days on duty",
            "tag_short_description": "Pump Description",
        },
        "template": "plotly_white",
    }

    layout_update = {
        "hovermode": "closest",
        "yaxis": {
            "visible": True,
            # 'automargin':True,
            "autorange": "reversed",
            "showticklabels": True,
            "fixedrange": True,
            "showgrid": False,
        },
        "xaxis": {
            "showgrid": True,
            # 'automargin':True,
            "fixedrange": True,
            "range": [0, 50],
        },
        "font": {
            "family": "Lato, Courier New, monospace",
            "size": 10,
            "color": "Black",
        },
        # 'autosize': True,
        "margin": dict(l=200, r=0, t=25, b=0),
        "paper_bgcolor": "white",
        "plot_bgcolor": "white",
        "showlegend": False,
    }

    print(args)
    bar_chart_figure_pumps = px.bar(
        top_ten_pumps,
        x="periods_of_unchanged_status",
        y="tag_short_description",
        **args,
    )
    bar_chart_figure_pumps.update_xaxes(
        tick0=0, dtick=10, showgrid=True, gridwidth=1.0, gridcolor="#CCCCCC"
    )
    bar_chart_figure_pumps.update_layout(coloraxis_showscale=False, **layout_update)
    bar_chart_figure_pumps.update_layout(
        hoverlabel=dict(bgcolor="#eeeeee", font_color="black")
    )
    bar_chart_figure_pumps = bar_chart_figure_pumps.update_traces(
        hovertemplate="<b>%{y}</b> <br><br> # Days on duty = %{x}"
    )

    if "colorscale" not in bar_chart_figure_pumps.__dict__.get("_layout", {}).get(
        "coloraxis", {}
    ):
        print("NEW FIX")
        bar_chart_figure_pumps.__dict__["_layout"]["coloraxis"] = {
            "colorbar": {"title": {"text": "# Days on duty"}},
            "colorscale": [
                [0.0, "#2dc937"],
                [0.75, "#2dc937"],
                [0.75, "#FDB515"],
                [0.8749, "#FDB515"],
                [0.8749, "#E82C2E"],
                [1.0, "#E82C2E"],
            ],
            "cmin": 0.0,
            "cmax": 40,
        }
    print(bar_chart_figure_pumps.__dict__["_layout"]["coloraxis"])

    config = {
        "modeBarButtonsToRemove": [
            "zoom2d",
            "pan2d",
            "select2d",
            "lasso2d",
            "zoomIn2d",
            "zoomOut2d",
        ],
        "displaylogo": False,
    }

    return bar_chart_figure_pumps, config


def mongo_daily_runstatus_duty_query(sensor_list: list, start, end, duty_frac=0.66) -> pd.DataFrame:
    """

    Args:
        sensor_list:
        start:
        end:
        duty_frac:

    Returns:

    """
    alarm_event_coll = prj_collections.events_coll

    # Duty Cycle Running events mongo query
    match1 = {
        "tag_name": {"$in": sensor_list},
        "measurement_value": {"$in": ["RUNNING", "Running", "ON"]},
        "$or": [
            {"occurrence_end_time": {"$gte": start, "$lte": end}},
            {
                "$and": [
                    {"occurrence_start_time": {"$lte": start}},
                    {"$or": [
                        {"occurrence_last_time": {"$gte": end}},
                        {"occurrence_end_time": {"$gte": end}}
                    ]},
                ]
            },
            {
                "$and": [
                    {"occurrence_start_time": {"$gte": start}},
                    {"occurrence_end_time": {"$lte": end}},
                ]
            },
            {
                "$and": [
                    {"occurrence_start_time": {"$gte": start, "$lte": end}},
                    {
                        "$or": [
                            {"occurrence_last_time": {"$gte": end}},
                            {"occurrence_end_time": {"$gte": end}},
                        ]
                    },
                ]
            },
        ],
    }

    project1 = {
        "tag_name": "$tag_name",
        "measurement_value": "$measurement_value",
        "occurrence_start_time": {"$max": ["$occurrence_start_time", start]},
        "occurrence_end_time": {"$min": ["$occurrence_end_time", end]},
        "occurrence_duration": {
            "$divide": [
                {
                    "$subtract": [
                        {"$min": ["$occurrence_end_time", end]},
                        {"$max": ["$occurrence_start_time", start]},
                    ]
                },
                1000,
            ]
        },
    }

    group1 = {
        "_id": {"tag_name": "$tag_name"},
        "measurement_value": {"$first": "$measurement_value"},
        "total_secs_at_value": {"$sum": "$occurrence_duration"},
    }

    project2 = {
        "tag_name": "$_id.tag_name",
        "status_datetime": start,
        "measurement_value": "$measurement_value",
        "percentage_day_at_value": {"$divide": ["$total_secs_at_value", 86400]},
        "on_duty": {
            "$cond": [
                {"$gte": [{"$divide": ["$total_secs_at_value", 86400]}, duty_frac]},
                1,
                0,
            ]
        },
        "_id": 0,
    }

    sort = {"tag_name": 1, "status_datetime": 1}

    pipeline = [
        {"$match": match1},
        {"$project": project1},
        {"$group": group1},
        {"$project": project2},
        {"$sort": sort},
    ]

    cursor = alarm_event_coll.aggregate(pipeline)

    # Make the dataframe
    duty_cycle_df = pd.DataFrame(cursor)

    return duty_cycle_df


def calc_1day_runstatus_duty_cycles(sensor_list: list, start, duty_frac=0.66):
    """

    Args:
        sensor_list:
        start:
        duty_frac:

    Returns:

    """
    site_tag_coll = prj_collections.site_tag_config_coll

    # Set times
    start = start.floor("d")
    end = start + pd.Timedelta("23h59m59s")

    # Get meta data for the site tags
    sensor_meta = pd.DataFrame(
        site_tag_coll.find(
            {
                "tag_name": {"$in": sensor_list},
                "high": {"$in": ["RUNNING", "Running", "ON"]},
            },
            {"_id": 0, "tag_name": 1, "high": "$high"},
        )
    )

    # Get duty cycles and merge with sensor meta data
    duty_cycle_df = mongo_daily_runstatus_duty_query(
        sensor_list, start, end, duty_frac=duty_frac
    )

    if duty_cycle_df.shape[0] == 0:
        columns = [
            "status_datetime",
            "measurement_value",
            "percentage_day_at_value",
            "on_duty",
            "tag_name",
        ]
        duty_cycle_df = pd.DataFrame(columns=columns)
    else:
        duty_cycle_df = pd.merge(sensor_meta, duty_cycle_df, how="left", on="tag_name")

        # Fill the measurement_value that was used in the filter
        duty_cycle_df.loc[:, "measurement_value"] = duty_cycle_df.loc[
            :, "measurement_value"
        ].fillna(duty_cycle_df["high"])
        duty_cycle_df = duty_cycle_df.drop(["high"], axis=1)

        # Fill in 0 and the date for tags that had no data that day
        duty_cycle_df.loc[:, ["percentage_day_at_value", "on_duty"]] = (
            duty_cycle_df.loc[:, ["percentage_day_at_value", "on_duty"]]
            .fillna(0)
            .round(2)
        )
        duty_cycle_df.loc[:, "status_datetime"] = (
            duty_cycle_df.loc[:, "status_datetime"]
            .fillna(method="ffill")
            .fillna(method="bfill")
        )

        # Store the data for the chosen duty fraction and time of calculation
        duty_cycle_df.loc[:, "on_duty_frac"] = duty_frac
        duty_cycle_df.loc[:, "calculated_at"] = pd.Timestamp.utcnow()

    return duty_cycle_df


def calc_daily_runstatus_duty_cycles(sensor_list: list, startDate, endDate, duty_frac=0.66):
    """

    Args:
        sensor_list:
        startDate:
        endDate:
        duty_frac:

    Returns:

    """
    all_days = []
    for date in pd.date_range(startDate.floor("d"), endDate.floor("d"), freq="1d"):
        daily_df = calc_1day_runstatus_duty_cycles(
            sensor_list=sensor_list, start=date, duty_frac=duty_frac
        )
        all_days.append(daily_df)

    final_daily_df = pd.concat(all_days).sort_values(by=["status_datetime", "tag_name"])
    return final_daily_df


"""
NEW CODE FOR TRACKING SPEED PUMPS AS RUNNING STATUS
"""


def pump_signal_digitise(df, column, threshold=0):
    """

    Args:
        df:
        column:
        threshold:

    Returns:

    """

    def _digitise(input_value):
        if input_value > threshold:
            return True
        else:
            return False

    df["STATUS"] = pd.to_numeric(df[column], errors="coerce").map(_digitise)

    return df


def pump_status_differential(df):
    """

    Args:
        df:

    Returns:

    """
    df.sort_index()
    df["STATUS_DIFF"] = df["STATUS"].map(int).diff()
    return df


def pump_status_groups(dataframe):
    """

    Args:
        dataframe:

    Returns:

    """
    dataframe["GB_OFF_VAR"] = dataframe["STATUS"].cumsum()
    dataframe["GB_ON_VAR"] = (~dataframe["STATUS"]).cumsum()
    dataframe["TIME_DELTA"] = (dataframe["measurement_timestamp"].diff()).map(
        lambda x: x.total_seconds()
    )
    return dataframe


def status_duration_calc(dataframe, status_type):
    """

    Args:
        dataframe:
        status_type:

    Returns:

    """
    column_lookup = {"ON": "GB_ON_VAR", "OFF": "GB_OFF_VAR"}
    assert (
        status_type in column_lookup.keys()
    ), f'Choose an appropriate status, "ON" or "OFF": {status_type} not recognised'

    intervals = dataframe.groupby(by=column_lookup.get(status_type)).agg(
        occurrence_start_time=("measurement_timestamp", "min"),
        occurrence_end_time=("measurement_timestamp", "max"),
        occurrence_counter=("measurement_timestamp", "count"),
    )

    intervals["occurrence_duration"] = (
        intervals["occurrence_end_time"] - intervals["occurrence_start_time"]
    ).map(lambda x: x.total_seconds())
    intervals = intervals[intervals["occurrence_duration"] > 0]

    return intervals


def pump_runtime_calcs(dataframe):
    """

    Args:
        dataframe:

    Returns:

    """
    on_intervals = dataframe.pipe(status_duration_calc, status_type="ON")
    on_intervals["measurement_value"] = "ON"
    off_intervals = dataframe.pipe(status_duration_calc, status_type="OFF")
    off_intervals["measurement_value"] = "OFF"

    duty_cycles = (
        pd.concat([on_intervals, off_intervals], ignore_index=True)
        .sort_values(by=["occurrence_start_time"])
        .reset_index(drop=True)
    )
    duty_cycles.loc[:, "tag_name"] = dataframe["tag_name"].unique()[0]

    return duty_cycles


def switch_off_long_gaps(dataframe, time_limit=3 * 60 * 60):
    """

    Args:
        dataframe:
        time_limit:

    Returns:

    """
    dataframe["TIME_DELTA"] = (dataframe["measurement_timestamp"].diff()).map(
        lambda x: x.total_seconds()
    )
    long_gap_df = dataframe.reset_index(drop=True).query("TIME_DELTA >= @time_limit")

    new_data = []
    for i, row in long_gap_df.iterrows():
        record = {}
        if row["STATUS"] != dataframe.iloc[i - 1]["STATUS"]:
            record["tag_name"] = row["tag_name"]
            record["measurement_timestamp"] = (
                row["measurement_timestamp"]
                - pd.Timedelta(f"{int(row['TIME_DELTA'])}s")
                + pd.Timedelta(f"{time_limit / 2}s")
            )
            record["measurement_value"] = -99
            record["STATUS"] = False
            new_data.append(record)

    if new_data:
        new_df = pd.DataFrame(new_data)
        dataframe = (
            pd.concat([dataframe.reset_index(drop=True), new_df], ignore_index=True)
            .sort_values(by="measurement_timestamp")
            .set_index("measurement_timestamp", drop=False)
        )
        dataframe["TIME_DELTA"] = (dataframe["measurement_timestamp"].diff()).map(
            lambda x: x.total_seconds()
        )

    return dataframe


def calculate_speed_occurrence_events(tag_list: list, start, end, threshold=5) -> pd.DataFrame:
    """

    Args:
        tag_list:
        start:
        end:
        threshold:

    Returns:

    """
    date_range = pd.date_range(start, end, freq="30d")
    all_events = []
    for tag in tag_list:
        chunked_events = []
        for _, date in enumerate(date_range):
            start_time = date
            stop_time = date + pd.Timedelta("30d")
            if stop_time > end:
                stop_time = end

            pump_sensor_data = get_pump_data(
                [tag], start_time=start_time, stop_time=stop_time
            )
            if pump_sensor_data.shape[0] == 0:
                continue
            try:
                result = (
                    pump_sensor_data.sort_index()
                    .pipe(
                        pump_signal_digitise,
                        column="measurement_value",
                        threshold=threshold,
                    )
                    .pipe(pump_status_differential)
                    .pipe(switch_off_long_gaps)
                    .pipe(pump_status_groups)
                    .pipe(pump_runtime_calcs)
                )
                result.loc[:, "threshold_value"] = 5

                chunked_events.append(result)
            except ValueError:
                print(f"Skipping over: {tag} - {start_time}:{stop_time}")

        if chunked_events:
            tag_events = (
                pd.concat(chunked_events, ignore_index=True)
                .sort_values(by="occurrence_start_time")
                .reset_index(drop=True)
            )
            all_events.append(tag_events)

    if all_events:
        final_df = pd.concat(all_events, ignore_index=True).reset_index(drop=True)
    else:
        final_df = pd.DataFrame()
    return final_df
