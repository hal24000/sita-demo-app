"""
Copyright HAL24K 2020
Pandas cacher object and utility functions, it uses brain plasma to store pandas dataframes using a hash key.
It includes a cleanup routine, although that needs to be reworked @todo

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===


"""
import pandas as pd
import time
import gc
import threading
from brain_plasma import Brain
import hashlib
from data.project_loggers import dash_logger
import logging
from data.collections_object import ProjectCollections
import json
import copy
from datetime import datetime, timedelta

logger = logging.getLogger(dash_logger)


def hash_query(query):
    """
    Changes the date components of the query to strings so that a hash of the query can then be computed

    Args:
        query (dict): a dictionary holding a mongo query

    Returns:
        hashed_query (str):

    """
    query_to_hash = copy.deepcopy(query)
    for timestamp_column in ['measurement_timestamp', 'bsc_start', 'actual_release']:
        if timestamp_column in query:
            if "$lte" in query[timestamp_column]:
                ts_tmp = query[timestamp_column]['$lte']
                end_date_string = datetime.strftime(ts_tmp, '%Y-%m-%d %H:%M')
                query_to_hash[timestamp_column]['$lte'] = end_date_string
            if "$gte" in query[timestamp_column]:
                ts_tmp = query[timestamp_column]['$lte']
                start_date_string = datetime.strftime(ts_tmp, '%Y-%m-%d %H:%M')
                query_to_hash[timestamp_column]['$gte'] = start_date_string

    hashed_query = hashlib.sha1(json.dumps(query_to_hash).encode()).hexdigest()
    return hashed_query


class BrainHolder(object):
    def __init__(self, client_brains, namespace,
                 keep_period=1060, cleanupfreq=120):
        """
        The Brainholder object uses the Brain() from plasma_brain. A pyarrow database.
        It's meant to be used for caching and sharing data between plotly dash callbacks.

        keep_period is in minutes
        cleanup freq in seconds
        """
        # super(PandasCacher, cls).__init__()
        self.keep_period = keep_period
        self.cleanup_freq = cleanupfreq
        self.namespace = namespace
        self.clients = {}
        for client_brain in client_brains:
            brain = Brain(namespace=namespace)
            self.clients[client_brain] = brain
        self.stop = False

        if "cleanup" in client_brains:
            thread = threading.Thread(target=self.cleanup, args=())
            thread.daemon = True  # Daemonize thread
            thread.start()

    def cleanup(self):
        while not self.stop:
            hash_keys = self.clients['cleanup'].names(namespace=self.namespace)
            remove_hashes = []
            for hash_key in hash_keys:
                time_alive = datetime.now() - self.clients['cleanup'][hash_key]['time_created']
                time_alive = time_alive.total_seconds()
                if time_alive >= self.keep_period:
                    remove_hashes.append(hash_key)

            if len(remove_hashes) > 0:
                for key in remove_hashes:
                    del self.clients['cleanup'][key]
            gc.collect()
            time.sleep(self.cleanup_freq)

    def get(self, client_id, key):
        """
        fetches data for the given client id and hash key
        Args:
            client_id (str): the id of the client in the object client list
            key (str): the hash code under which data has been previously stored

        Returns:
            data (pd.DataFrame): the resulting data

        """
        try:
            data = self.clients[client_id][key]['frame']
        except:
            data = pd.DataFrame()
        return data

    def delete(self, client_id, key):
        """
        Deletes the data for the given client and hash key
        Args:
            client_id (str): the client id for which to delete an entry
            key (str): the hash code under which data is stored

        Returns:

        """
        success = False
        try:
            del (self.clients[client_id][key])
            success = True
        except KeyError:
            pass
        return success

    def store(self, client_id, key, frame, time_created):
        """
        stores data for the given client id and hash key
        Args:
            client_id (str): the client in which to store the dataframe
            key (str): the hash key under which to store the dataframe
            frame (pandas.DataFrame): the dataframe to store
            time_created (datetime): the timestamp of the time of creation

        Returns:

        """
        self.clients[client_id][key] = {'frame': frame, 'time_created': time_created}
