"""
Helper functions for risk calculation
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""

import numpy as np
import pandas as pd


DEFAULT_COLUMN_NAMES = {'tag_name': 'tag_name',
                        'lolo_used': 'lolo_used',
                        'lo_used': 'lo_used',
                        'hi_used': 'hi_used',
                        'hihi_used': 'hihi_used',
                        'roc_used': 'rate_of_change_used',
                        'impact': 'impact'}

RAG_SEVERITY = {'abs_red': 0.8,
                'abs_amber': 0.5,
                'abs_green': 0.,
                'roc_red': np.nan,
                'roc_amber': 0.8,
                'roc_green': 0.}


def calculate_sensor_risk(site_tag_config: pd.DataFrame, data_df: pd.DataFrame) -> pd.DataFrame:
    """
    Calculate the risk values for the individual sensors.

    Args:
        site_tag_config (pd.DataFrame): sensor metadata, corresponding to the site_tag_config collection
        data_df (pd.DataFrame): timestamp-indexed, one column per sensor, resampled as needed sensor data

    Returns:
        enriched site_tag_config by, most notably, column 'risk'
    """

    output_df = site_tag_config.copy()

    output_df = calculate_mean_sd_risks(site_tag_config=output_df, data_df=data_df)
    output_df = calculate_roc_risks(site_tag_config=output_df, data_df=data_df)

    # TODO: make sure this is fine
    output_df['risk_msd'] = output_df[['risk_mean_sd', 'risk_roc']].max(axis=1)

    return output_df


def calculate_mean_sd_risks(site_tag_config: pd.DataFrame,
                            data_df: pd.DataFrame,
                            column_names:dict=DEFAULT_COLUMN_NAMES,
                            severity: dict=RAG_SEVERITY) -> pd.DataFrame:
    """
    Calculate risks based on mean and sd (or median and iqr).

    Args:
        site_tag_config (pd.DataFrame): sensor metadata, corresponding to the site_tag_config collection
        data_df (pd.DataFrame): timestamp-indexed, one column per sensor, resampled as needed sensor data
        column_names (dict): mapping of the column names from site tag config
        severity (dict): values for the various RAG severities

    Returns:
        enriched site_tag_config by, most notably, column 'risk_mean_sd'
    """

    out_df = site_tag_config.copy()

    # only interested in the last measurements
    last_measurements = data_df.iloc[-1].to_dict()
    out_df['last_measurement'] = out_df[column_names.get('tag_name', 'tag_name')].map(last_measurements)

    out_df['severity'] = np.nan
    out_df.loc[(out_df['last_measurement'] >= out_df[column_names.get('lo_used', 'lo_used')])
               & (out_df['last_measurement'] <= out_df[column_names.get('hi_used', 'hi_used')]),
               'severity'] = severity.get('abs_green', 0.)
    out_df.loc[(out_df['last_measurement'] > out_df[column_names.get('hi_used', 'hi_used')])
               | (out_df['last_measurement'] < out_df[column_names.get('lo_used', 'lo_used')]),
               'severity'] = severity.get('abs_amber', 0.5)
    out_df.loc[(out_df['last_measurement'] > out_df[column_names.get('hihi_used', 'hihi_used')])
               | (out_df['last_measurement'] < out_df[column_names.get('lolo_used', 'lolo_used')]),
               'severity'] = severity.get('abs_red', 0.8)

    out_df['risk_mean_sd'] = out_df['severity'] * out_df[column_names.get('impact', 'impact')]

    out_df.drop(['severity', 'last_measurement'], axis=1, inplace=True)

    return out_df


def calculate_roc_risks(site_tag_config: pd.DataFrame,
                        data_df: pd.DataFrame,
                        column_names:dict=DEFAULT_COLUMN_NAMES,
                        severity: dict=RAG_SEVERITY) -> pd.DataFrame:
    """
    Calculate risks based on the ROC.

    Args:
        site_tag_config (pd.DataFrame): sensor metadata, corresponding to the site_tag_config collection
        data_df (pd.DataFrame): timestamp-indexed, one column per sensor, resampled as needed sensor data
        column_names (dict): mapping of the column names from site tag config
        severity (dict): values for the various RAG severities

    Returns:
        enriched site_tag_config by, most notably, column 'risk_mean_sd'

    """

    out_df = site_tag_config.copy()
    # get the most recent difference
    last_roc = data_df.diff().iloc[-1].to_dict()

    out_df['last_roc'] = out_df[column_names.get('tag_name', 'tag_name')].map(last_roc)

    out_df['severity'] = np.nan
    out_df.loc[(out_df['last_roc'].map(lambda x: np.abs(x)) <= out_df[column_names.get('roc_used', 'roc_used')]),
               'severity'] = severity.get('roc_green', 0.)
    out_df.loc[(out_df['last_roc'].map(lambda x: np.abs(x)) > out_df[column_names.get('roc_used', 'roc_used')]),
               'severity'] = severity.get('roc_amber', 0.8)

    out_df['risk_roc'] = out_df['severity'] * out_df[column_names.get('impact', 'impact')]

    out_df.drop(['severity', 'last_roc'], axis=1, inplace=True)

    return out_df
