"""
Copyright HAL24K 2020

rgf threshold functions and thresholds for the rgf page

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""

from data.load_data import colors_risk
import numpy as np
from datetime import timedelta
from data.summary_utils import query_tags_conditional
from data.rgf_collections import RGFCollections
import logging
from data.rgf_gac_utils import (
    get_extra_row_formatting,
    variance_within_perc,
    abs_value_thresh,
    abs_value_thresh_based_another,
    string_thresh,
    string_thresh_general,
    roc_thresh_func)

logger = logging.getLogger('rgf_thresholds')


def backwash_flow_tresholds(val, installation):
    thresholds = {
            'GOODW': 702
    }

    if installation in thresholds:
        if np.abs(val - thresholds[installation]) / thresholds[installation] > 0.05:
            color = colors_risk['amber']
        else:
            color = colors_risk['neutral']
    else:
        color = colors_risk['neutral']

    return color


def turbidity_thresholds(installation):
    thresholds = {
            'GOODW':     0.1,
            'HUBY':      0.1,
            'ELVINGTON': 0.1,
    }

    return thresholds[installation]


def inlet_level_thresholds(val, plant):
    amber_thresholds = {
            'GOODW':     90,
            'HUBY':      0.7,
            'ELVINGTON': 3
    }

    if val >= amber_thresholds[plant]:
        return colors_risk['amber']
    else:
        return colors_risk['neutral']


oos_thresholds = {'GOODW':     {'red':   {'upper': 999, 'lower': 1},
                                'amber': {'upper': 1, 'lower': 1}},
                  'ELVINGTON': {'amber': {'upper': 3, 'lower': 3},
                                'red':   {'upper': 999, 'lower': 3}},
                  'HUBY':      {'amber': {'upper': 1, 'lower': 1},
                                'red':   {'upper': 10, 'lower': 1}},
                  }
queue_excess_thresholds = {
        'GOODW':     {'amber': {'lower': 0, 'upper': 2},
                      'red':   {'lower': 2, 'upper': np.inf}},
        'HUBY':      {'amber': {'lower': 0, 'upper': 2},
                      'red':   {'lower': 2, 'upper': np.inf}},
        'ELVINGTON': {'amber': {'lower': 0, 'upper': 3},
                      'red':   {'lower': 3, 'upper': np.inf}}
}

uv254_roc_threshold = {
        'GOODW':     {"amber": {"lower": 0.5, "upper": np.inf}},
        'ELVINGTON': {"amber": {"lower": 0.5, "upper": np.inf}}

}
pflow_thresholds = {
        'ELVINGTON': {'amber': {'upper': 65, 'lower': 60},
                      'red':   {'upper': 60, 'lower': -np.inf}}
}
pflow_counter_thresholds = {
        'ELVINGTON': {'amber': {'upper': 450, 'lower': 350},
                      'red':   {'upper': np.inf, 'lower': 450}}
}

outflow_thresholds = {
        'ELVINGTON': {'amber': {'upper': 150, 'lower': 140},
                      'red':   {'upper': 140, 'lower': -np.inf}}
}


def get_queue_excess_thresholds(n_in_excess, site):
    thresholds = queue_excess_thresholds[site]

    return_color = colors_risk['neutral']

    if n_in_excess > thresholds['amber']['lower'] and n_in_excess <= thresholds['amber']['upper']:
        return_color = colors_risk['amber']
    elif n_in_excess > thresholds['red']['lower'] and n_in_excess <= thresholds['red']['upper']:
        return_color = colors_risk['red']

    return return_color


def get_flow_risk_color(site, val):
    thresholds = outflow_thresholds[site]
    return_color = colors_risk['neutral']
    if val < thresholds['amber']['upper'] and val >= thresholds['amber']['lower']:
        return_color = colors_risk['amber']
    elif val < thresholds['red']['upper'] and val >= thresholds['red']['lower']:
        return_color = colors_risk['red']
    return return_color


def get_pflow_risk_color(site, val):
    thresholds = pflow_thresholds[site]
    return_color = colors_risk['neutral']
    if val < thresholds['amber']['upper'] and val >= thresholds['amber']['lower']:
        return_color = colors_risk['amber']
    elif val < thresholds['red']['upper'] and val >= thresholds['red']['lower']:
        return_color = colors_risk['red']
    return return_color


def get_pflow_counter_risk_color(site, val):
    thresholds = pflow_counter_thresholds[site]
    return_color = colors_risk['neutral']
    if val < thresholds['amber']['upper'] and val >= thresholds['amber']['lower']:
        return_color = colors_risk['amber']
    elif val < thresholds['red']['upper'] and val >= thresholds['red']['lower']:
        return_color = colors_risk['red']
    return return_color


def get_uv254_risk_color(site, color_code):
    return colors_risk[color_code]


def backwash_pressure_thresholds(val, installation):
    # huby: "Agree rules with Steve - 0.80-0.85: Amber >0.85 Red"
    # elvington: Drain Down - Thern commence washing, Risk of lifting/Damaging Filter Bed > 0.8 Amber

    huby_thresholds = {
            'red':   {'lower': 0.85, "upper": np.inf},
            'amber': {'lower': 0.8, 'upper': 0.85}
    }

    elvington_thresholds = {
            'red':   {'lower': np.inf, "upper": np.inf},
            'amber': {'lower': 0.8, 'upper': np.inf}
    }

    thresholds = {
            'HUBY':      huby_thresholds,
            'ELVINGTON': elvington_thresholds,
    }

    thresh = thresholds[installation]
    color = colors_risk['neutral']
    if val > thresh['amber']['lower'] and val <= thresh['amber']['upper']:
        color = colors_risk['amber']
    elif val > thresh['red']['lower'] and val <= thresh['red']['upper']:
        color = colors_risk['red']

    return color


def backwash_thresholds(nbackwashing):
    ##TODO:Not in the traceability matrix v10
    return colors_risk['neutral']


def individual_thresholds(installation, df, roc_df, current_datetime):
    # for acomb
    # backwash_filtering_steps = np.arange(3, 13).tolist()
    # backwash_filtering_steps += [0] #fail status

    acomb_outser_vals = RGFCollections.OOS_VALS['GOODW']
    acomb_backwashing_vals = RGFCollections.BACKWASHING_VALS['GOODW']

    # BEGIN GOODW FUNCTIONS===============================
    service_status_args_acomb = dict(col='Service Status',
                                     values=['WASHING',
                                             'IN SERVICE',
                                             'OUT OF SERVICE'],
                                     colors=['darkgreen', 'green', 'red'],
                                     func=string_thresh_general)

    """Variance from Set Point or average of other RGFs 'In service' with 1% Tolerance"""

    flow_args_acomb = dict(col='Outlet Flow',
                           threshold=0.05,
                           func=variance_within_perc,
                           in_service_col='Service Status',
                           out_service_val=["OUT OF SERVICE", 'WASHING'])

    outlet_valve_pos_acomb = dict(col='Outlet Valve Pos',
                                  threshold=0.05,
                                  func=variance_within_perc,
                                  in_service_col='Service Status',
                                  out_service_val=["OUT OF SERVICE", 'WASHING'])

    """Straight Amber if value of 'IN EXCESS'
    GOODWDA:RGFF_AV_RGF1EXTBQ
    """
    values = ['OK', 'IN EXCESS']
    colors = ['green', 'red']
    bw_excess_queue_stat_args_acomb = dict(col='BW Excess Queue Stat',
                                           values=values,
                                           colors=colors,
                                           func=string_thresh_general)

    """
    RGF_DW_RGF1TIFWBWS
    >= Set Point  - Use RGF_DW_RGF1_EXCESSTIME (Excess Time Filtering > 0: Amber, > 2
    """

    thresh = {
            'amber': {'lower': 0, 'upper': 120},
            'red':   {'lower': 120, 'upper': 10000000}
    }
    filtering_hours_args_acomb = dict(func=abs_value_thresh_based_another,
                                      col_style='Filtering Hours',
                                      in_service_col='Service Status',
                                      out_service_vals=['WASHING', 'OUT OF SERVICE'],
                                      col_based='Excess Time (Mins)',
                                      thresh=thresh,
                                      )

    """> 2 Hrs: Amber, > 3 Red """
    red_abs_threshold = {'lower': 3, 'upper': 10000000}
    amber_abs_threshold = {'lower': 1.99, 'upper': 3}

    bw_excess_q_hrs_args_acomb = dict(col='BW Excess Queue Hrs',
                                      func=abs_value_thresh,
                                      in_service_col='Service Status',
                                      out_service_vals=['WASHING', 'OUT OF SERVICE'],
                                      red_abs_threshold=red_abs_threshold,
                                      amber_abs_threshold=amber_abs_threshold,
                                      )

    """Any Value > 0: Amber, Value > 2"""
    red_abs_threshold = {'lower': 120, 'upper': 10000000}
    amber_abs_threshold = {'lower': 0, 'upper': 120}
    args_excess_time_in_service_acomb = dict(col='Excess Time (Mins)',
                                             func=abs_value_thresh,
                                             in_service_col='Service Status',
                                             out_service_vals=['WASHING', 'OUT OF SERVICE'],
                                             red_abs_threshold=red_abs_threshold,
                                             amber_abs_threshold=amber_abs_threshold
                                             )

    """
    RGF_A_RGF1LOWFLOWA
    RGF Oulet Flow Status Ok/Low
    Green if 'OK' Else Red
    """
    lowflow_args_acomb = dict(col="BW Pressure Exception(Last 24H)",
                              func=string_thresh,
                              val_ok='OK')

    # "BW Status"
    bw_status_args_acomb = dict(col="BW Status",
                                func=string_thresh,
                                val_ok='OK')
    """
    Level equal to others with 5% Tolerance
    TODO: whats the color?
    RGF_DW_LT04101SCD
    """
    level_args_acomb = dict(col='Level',
                            func=variance_within_perc,
                            threshold=0.05,
                            in_service_col='Service Status',
                            out_service_val=['OUT OF SERVICE', 'WASHING'])

    """
    TODO: francis needs to add this
    RGF_DW_PT04102SCD
    Rate of Change Amber on 0.15 rise, Only alert on two consecutive rises
    """

    outlet_pressure_args_acomb = dict(col='Outlet Pressure',
                                      func=roc_thresh_func,
                                      in_service_col='Service Status',
                                      out_service_vals=['WASHING', 'OUT OF SERVICE'],
                                      roc_thresh=0.15,
                                      abs_diff_flag=False)

    """
    RGF_A_PS04104H
    Green if 'OK' Else Red
    """

    outlet_pressure_stat_args_acomb = dict(col='BW Pressure Status',
                                           func=string_thresh,
                                           val_ok='OK')

    # outlet flow stat
    outlet_flow_stat_args_acomb = dict(col='Outlet Flow Stat',
                                       func=string_thresh,
                                       val_ok='OK')

    """
    RGF_DW_AT24101SCD
    Significant Rise 0.1 over 15min Avg - Only alert on two consecutive rises
    """
    turbidity_args_acomb = dict(col='Turbidity',
                                func=roc_thresh_func,
                                in_service_col='Service Status',
                                out_service_vals=['WASHING', 'OUT OF SERVICE'],
                                roc_thresh=0.1,
                                abs_diff_flag=False)

    # turbidity_stat_args_acomb = dict(col='Turbidity status',
    #                                   val_ok ='ΟΚ',
    #                                   func=string_thresh)

    # END GOODW FUNCTIONS=================================
    # BEGIN HUBY FUNCTIONS================================
    # TODO:HUBY FUNCS

    """
    Tells us if 'In Service' or 'Out of Service'
    HUBYWTS1:RGF1_OOS_INSERV_SP
    Green if 'IN SERVICE' Else Red
    """

    service_stat_args_huby = dict(col='Service Status',
                                  values=['IN SERVICE', 'WASHING', 'OUT OF SERVICE'],
                                  colors=['green', 'darkgreen', 'red'],
                                  func=string_thresh_general)

    """
    < Average of 'In Service' RGF Flows allowing 5% Tolerance each way: Amber
    HUBYWTS1:RGF1_FLOW_SI
    """
    # flow

    flow_args_huby = dict(col='Outlet Flow', func=variance_within_perc,
                          in_service_col="Service Status",
                          out_service_val=["OUT OF SERVICE", 'WASHING'],
                          threshold=0.05)

    """
    < Average of 'In Service' RGF levels allowing 5% Tolerance each way: Amber
    HUBYWTS1:RGF1_LEVEL_SI
    """
    level_args_huby = dict(col="Level",
                           in_service_col="Service Status", func=variance_within_perc,
                           out_service_val=["OUT OF SERVICE", 'WASHING'], threshold=0.05)

    """
    HUBYWTS1:RGF1_TB_SI
    I would be looking at 0.25 (Maybe 0.175) Amber and 0.40 Red, Normal Range < 0.1
    """
    threshold_red = {
            'upper': 10000000,
            'lower': 0.4
    }
    threshold_amber = {
            'upper': 0.4,
            'lower': 0.25,
    }

    turbidity_args_huby = dict(col="Turbidity", red_abs_threshold=threshold_red,
                               amber_abs_threshold=threshold_amber,
                               in_service_col='Service Status',
                               out_service_vals=['WASHING', 'OUT OF SERVICE'],
                               func=abs_value_thresh)

    """
    RGF1 Differential Pressure
    Profile and implement Rate of Change
    HUBYWTS1:RGF1_DP_SI
    TODO: rate of change calculation and then use
    """

    """
    RGF1 Differential Pressure
    Green if 'OK' Else 'RED'
    HUBYWTS1:RGF1_DP_OOR
    """
    pressure_oor_args_huby = dict(col="Pressure Status", val_ok="OK", func=string_thresh)

    """
    RGF1 fault status
    Green if 'IN SERVICE' Else 'RED'
    HUBYWTS1:RGF1_FAULT
    """
    fail_status_args_huby = dict(col="RGF Fail Status", val_ok="OK", func=string_thresh)

    """
    RGF1 Flow Fault Status
    Green if 'OK' Else 'RED'
    HUBYWTS1:RGF1_FAULT
    """
    flow_status_args_huby = dict(col="Flow Fault Stat", val_ok="OK", func=string_thresh)

    """
    RGF1 service time
    >27 Hours: Amber, > 32 Hours: Red
    HUBYWTS1:RGF1_SERVICE_TIME_SI
    """
    threshold_red = {'upper': 10000000, 'lower': 32}
    threshold_amber = {'lower': 27, 'upper': 32}

    filtering_hours_args_huby = dict(col='Filtering Hours',
                                     func=abs_value_thresh,
                                     in_service_col='Service Status',
                                     out_service_vals=['WASHING', 'OUT OF SERVICE'],
                                     red_abs_threshold=threshold_red,
                                     amber_abs_threshold=threshold_amber
                                     )

    """
    TODO: Francis needs to add this not emitting
    RGF1 Flow Fault Status
    Active when Backwashing, Inactive if not
    note: not defined as of matrix v11
    HUBYWTS1:RGF1_WASHING
    """

    """
    HUBYWTS1:TA_RGF1_TB_FAILED
    Green if 'OK' Else 'RED'
    """

    turbidity_fail_stat_huby_args = dict(col='Turbidity OOR',
                                         func=string_thresh,
                                         val_ok='OK')

    # Headloss
    threshold_red = {'lower': 0, 'upper': 10000000}
    threshold_amber = {'lower': 10000000, 'upper': 10000000}
    headloss_args_acomb = dict(col='Headloss', func=abs_value_thresh,
                               red_abs_threshold=threshold_red,
                               in_service_col='Service Status',
                               out_service_vals=['WASHING', 'OUT OF SERVICE'],
                               amber_abs_threshold=threshold_amber
                               )

    # END HUBY FUNCTIONS==================================

    # BEGIN ELVINGTON FUNCTIONS===========================
    # TODO: ELVINGTON funcs

    def elv_filter_bedcond(**kwargs):
        """
        WARNING: THIS FUNCTION IS A SPECIAL CASE... 1h rocs,
         figure out how to generalize later

        FILTER 1 BED CONDITION
        ERGF0107
        Amber >=1.5 or Rate of Change > 0.08 over Hour, Red >= 1.6
        """
        # query this weirdo by itself and anything else thats not 2 15min periods

        df = kwargs['df']
        col = 'Bed Cond'
        current_datetime = kwargs['current_datetime']
        installation = kwargs['installation']

        latest_values = df[col]
        out_service_vals = kwargs['out_service_vals']
        in_service_cols = kwargs['in_service_col']

        individual_tags_df = RGFCollections.individual_tag_df
        original_description = RGFCollections.col_renames_inverted[installation][col]

        rel_tags = individual_tags_df.loc[
            (individual_tags_df['tag_short_description'] == original_description.lower()) &
            (individual_tags_df.site_name == installation), 'tag_name'].tolist()
        data_1h_ago = query_tags_conditional(tag_list=rel_tags,
                                             reference_time=current_datetime - timedelta(hours=1),
                                             time_span=20 / 60 / 24,
                                             latest_only=True)
        rocs = latest_values - data_1h_ago['measurement_value']

        format_rows_amber_roc = rocs[(rocs > 0.08) &
                                     (~df[in_service_cols].isin(out_service_vals))].index.values

        # return data_query_dict, header_query_dict
        data_query_dict = get_extra_row_formatting(row_indeces=format_rows_amber_roc,
                                                   column_id=col,
                                                   color='white',
                                                   backgroundColor=colors_risk['amber'])

        format_rows_amber = df.loc[(df[col] >= 1.5) &
                                   (~df[in_service_cols].isin(out_service_vals))].index.values
        data_query_dict += get_extra_row_formatting(row_indeces=format_rows_amber,
                                                    column_id=col,
                                                    color='white',
                                                    backgroundColor=colors_risk['amber'])

        format_rows_red = df.loc[(df[col] >= 1.6) &
                                 (~df[in_service_cols].isin(out_service_vals))].index.values
        data_query_dict += get_extra_row_formatting(row_indeces=format_rows_red,
                                                    column_id=col,
                                                    color='white',
                                                    backgroundColor=colors_risk['red'])

        header_query_dict = {}
        return data_query_dict, header_query_dict

    """
    RGF FILTER 1 IN/OUT SERVICE
    ERGF01IOSR
    Green if 'IN SERVICE' Else Red
    """

    service_stat_args_elv = dict(col='Service Status',
                                 values=['IN SERVICE', 'WASHING', 'OUT OF SERVICE'],
                                 colors=['green', 'darkgreen', 'red'],
                                 func=string_thresh_general)

    """
    FILTER 1 LEVEL
    ERGF0105
    level(m)
    > 1.85: Amber, > 1.9: Red
    """

    red_thresholds = {'upper': 10000000,
                      'lower': 1.9}
    amber_thresholds = {'upper': 1.9,
                        'lower': 1.85}

    level_args_elv = dict(col="Level",
                          func=abs_value_thresh,
                          in_service_col='Service Status',
                          out_service_vals=['WASHING', 'OUT OF SERVICE'],
                          red_abs_threshold=red_thresholds,
                          amber_abs_threshold=amber_thresholds
                          )

    """
    FILTER 1 OUTLET FLOW
    ERGF0102
    < Average of 'In Service' Flow allowing 5% Tolerance each way: Amber
    """

    outlet_flow_args_elv = dict(col='Outlet Flow',
                                func=variance_within_perc,
                                threshold=0.05,
                                in_service_col='Service Status',
                                out_service_val=['OUT OF SERVICE', 'WASHING'])

    """
    FILTER 1 TIME IN SERVICE
    ERGF01TIS
    >= Wash Interval Elvington: 48 Hours Amber
    """
    red_thresholds = {'upper': 10000000,
                      'lower': 10000000}
    amber_thresholds = {'upper': 10000000,
                        'lower': 48}

    filtering_hours_args_elv = dict(col='Filtering Hours',
                                    func=abs_value_thresh,
                                    in_service_col='Service Status',
                                    out_service_vals=['WASHING', 'OUT OF SERVICE'],
                                    red_abs_threshold=red_thresholds,
                                    amber_abs_threshold=amber_thresholds)

    """
    FILTER 1 TOTAL LOH
    ERGF0103
    Use for Acomb and Huby
    TODO: what does the above mean mean?
    """

    """
    FILTER 1 TURBIDITY
    ERGF0101
    >.1 Amber, > .2 Red, Consider Rate of Change
    also use roc thresh as of v 11
    """

    turbidity_args_elv = dict(col='Turbidity',
                              roc_thresh=0.1,
                              in_service_col='Service Status',
                              out_service_vals=['WASHING', 'OUT OF SERVICE'],
                              func=roc_thresh_func,
                              abs_diff_flag=False,
                              )

    """
    ELVGTNS1:ERGF01IW
    Green if 'OPEN', else Red
    """

    washing_stat_args_elv = dict(col='Washing Status',
                                 val_ok='OPEN',
                                 func=string_thresh)

    """
    FILTER 1 TURB INST NO FLOW
    ERGF0167_3
    Green if 'OK', else Red
    """

    turbidity_no_flow_args_elv = dict(col='Turbidity No Flow',
                                      val_ok="OK",
                                      func=string_thresh)

    """
    FILT01 TURBIDITY METER FAILED
    ERGF01TMF
    Amber if not 'OK'
    """
    turbidity_meter_stat_args_elv = dict(col='Turbidity Meter Status',
                                         val_ok='OK',
                                         color='amber',
                                         func=string_thresh)

    "BW Queue Alarm"
    bw_queue_alarm_args_elv = dict(col='BW Queue Alarm',
                                   val_ok='OK',
                                   color='red',
                                   func=string_thresh)

    """
    FILT01 TAKEN OUT OF SERVICE BY HEADLOSS
    ERGF01TOOSBH
    Amber if not 'OK'
    """

    headloss_oos_stat_args_elv = dict(col='OOS Headloss',
                                      func=string_thresh,
                                      color='amber',
                                      val_ok='OK')

    """
    FILT01 TAKEN OUT OF SERVICE BY HI TURB
    ERGF01TOOSHT
    Amber if not 'OK'
    """
    hiturb_oos_stat_args_elv = dict(col='OOS Turbidity',
                                    func=string_thresh,
                                    color='amber',
                                    val_ok='OK')

    "BW Queue Time"
    red_thresholds = {'upper': 10000000,
                      'lower': 10000000}
    amber_thresholds = {'upper': 10000000,
                        'lower': 19.99}

    queue_time_args_elv = dict(col='BW Queue Time',
                               func=abs_value_thresh,
                               in_service_col='Service Status',
                               out_service_vals=['WASHING', 'OUT OF SERVICE'],
                               red_abs_threshold=red_thresholds,
                               amber_abs_threshold=amber_thresholds)

    # END ELVINGTON FUNCTIONS=============================

    thresh_funcs = {
            'GOODW':     {
                    # 'backwash fail step no':    0,
                    # 'backwash step no':         0,
                    "BW Pressure Exception(Last 24H)": lowflow_args_acomb,
                    'BW Status':                       bw_status_args_acomb,
                    'BW Excess Queue Stat':            bw_excess_queue_stat_args_acomb,  # done
                    'Excess Time (Mins)':              args_excess_time_in_service_acomb,  # done
                    # 'Turbidity Low Stat': filter_water_turb_flow, #done, #descoped
                    'Outlet Flow':                     flow_args_acomb,  # done
                    # 'headloss status':          0,
                    'Outlet Pressure':                 outlet_pressure_args_acomb,  # done
                    'Level':                           level_args_acomb,  # done
                    'Outlet Flow Stat':                outlet_flow_stat_args_acomb,  # done
                    'BW Pressure Status':              outlet_pressure_stat_args_acomb,  # done
                    # 'BW Pressure Status': filter_outlet_pressure,  # done

                    'Filtering Hours':                 filtering_hours_args_acomb,  # done
                    'Service Status':                  service_status_args_acomb,  # done
                    'BW Excess Queue Hrs':             bw_excess_q_hrs_args_acomb,  # done
                    "Headloss":                        headloss_args_acomb,
                    # 'Turbidity High Stat':    filter_turbidity_status_high, #done#descoped
                    'Turbidity':                       turbidity_args_acomb,  # done
                    'Outlet Valve Pos':                outlet_valve_pos_acomb,
            },
            'HUBY':      {
                    "Service Status":   service_stat_args_huby,
                    'Outlet Flow':      flow_args_huby,
                    'Level':            level_args_huby,
                    'Turbidity':        turbidity_args_huby,
                    'Outlet Pressure':  outlet_pressure_args_acomb,
                    'Pressure Status':  pressure_oor_args_huby,
                    'RGF Fail Status':  fail_status_args_huby,
                    'Flow Fault Stat':  flow_status_args_huby,
                    # 'flow out of range status':0,
                    'Filtering Hours':  filtering_hours_args_huby,
                    # 'wash queue position':0,
                    # 'queued':0,
                    # 'turbidity oor stat':0,
                    # 'BW Status':huby_filter_washing_stat,#TODO
                    "Turbidity OOR":    turbidity_fail_stat_huby_args,
                    'Outlet Valve Pos': outlet_valve_pos_acomb,
            },
            'ELVINGTON': {
                    # bed cond is a special case, direct function call
                    'Bed Cond':               elv_filter_bedcond,
                    'Service Status':         service_stat_args_elv,
                    'Level':                  level_args_elv,
                    'Outlet Flow':            outlet_flow_args_elv,
                    'Filtering Hours':        filtering_hours_args_elv,
                    # 'time in washing':0,
                    'Turbidity':              turbidity_args_elv,
                    # "Washing Status":washing_stat_args_elv,
                    # 'Headloss' :elv_filter_totalloh
                    # 'BW StepNo':0,
                    "BW Queue Time":          queue_time_args_elv,
                    # 'Turb Inst Failed':elv_filter_turbinstfailed,
                    # 'turb inst warning':elv_filter_turbinstwarning,
                    'Turbidity No Flow':      turbidity_no_flow_args_elv,
                    'BW Queue Alarm':         bw_queue_alarm_args_elv,
                    'Turbidity Meter Status': turbidity_meter_stat_args_elv,
                    'OOS Headloss':           headloss_oos_stat_args_elv,
                    'OOS Turbidity':          hiturb_oos_stat_args_elv
            }
    }
    style_cond_queries = []
    style_cond_queries_header = []
    columns = df.columns

    # temporary hack for development, once francis has made this emit again, remove
    # if installation == 'ELVINGTON':
    #     df['in service status'] = ['IN SERVICE' for i in range(len(df))]

    if installation in thresh_funcs:
        for tag in columns:
            if tag in thresh_funcs[installation]:
                try:

                    args = thresh_funcs[installation][tag]

                    if not callable(args):

                        args.update(dict(df=df, col=tag,
                                         installation=installation,
                                         current_datetime=current_datetime,
                                         roc_df=roc_df))
                        func = args['func']
                        style_cond_query, style_cond_query_header = func(**args)
                    else:
                        func = args
                        passed_args = dict(df=df, col=tag,
                                           installation=installation,
                                           out_service_vals=['WASHING', 'OUT OF SERVICE'],
                                           in_service_col='Service Status',
                                           current_datetime=current_datetime,
                                           roc_df=roc_df)

                        style_cond_query, style_cond_query_header = func(**passed_args)

                    style_cond_queries += style_cond_query
                    style_cond_queries_header += style_cond_query_header
                except Exception as e:
                    logger.warning('rgf:individual_thresholds:couldnt format {}'.format(tag))
                    logger.warning(e)

    return style_cond_queries, style_cond_queries_header


service_hour_thresholds = {
        'GOODW':     25,  # (26-1)
        'HUBY':      26,  # (27-1)
        "ELVINGTON": 47  # (48-1)
}
