"""
Copyright HAL24K 2020
Utility functions for dataframes

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""

import pandas as pd
from difflib import SequenceMatcher
import operator


def convert_timezone(datetime_obj, tzfrom, tzto):
    """
    Converts a datetime object from one timezone to another
    Args:
        datetime_obj (datetime.datetime): the datetime object to be converted from
        tzfrom (datetime.timezone): the source timezone that the datetime obj is in
        tzto (datetime.timezone): the target timezone to convert to

    Returns:

    """
    localized_timestamp = tzfrom.localize(datetime_obj)
    new_timezone_timestamp = localized_timestamp.astimezone(tzto).replace(tzinfo=None)
    return new_timezone_timestamp


def get_greatest_common_str(names):
    """
    Gets the greatest common string out of a list of strings
    Args:
        names (list(str): a list of strings

    Returns:
        max_occurring_substring (str): the most frequently occurring substring in the source strings

    """
    substring_counts = {}
    for i in range(0, len(names)):
        for j in range(i + 1, len(names)):
            string1 = names[i]
            string2 = names[j]
            match = SequenceMatcher(None, string1, string2).find_longest_match(0, len(string1), 0, len(string2))
            matching_substring = string1[match.a:match.a + match.size]
            if (matching_substring not in substring_counts):
                substring_counts[matching_substring] = 1
            else:
                substring_counts[matching_substring] += 1

    max_occurring_substring = max(substring_counts.items(), key=operator.itemgetter(1))[0]
    return max_occurring_substring


def sort_categorical_column(data: pd.DataFrame, column: str, ordered_values: list = None) -> pd.DataFrame:
    """
    Sort a categorical column in a dataframe into a specified order.

    Args:
        data (pd.DataFrame): input data
        column (str): name of the categorical column
        ordered_values (list): order to be followed

    Returns:
        ordered dataframe
    """

    if ordered_values is None:
        # do not order
        return data

    sorter = dict(zip(ordered_values, range(len(ordered_values))))

    internal_data = data.copy()
    internal_data['rank_{}'.format(column)] = internal_data[column].map(sorter)
    internal_data.sort_values(by='rank_{}'.format(column), inplace=True)
    internal_data.drop('rank_{}'.format(column), axis=1, inplace=True)

    return internal_data
