"""
load data script, includes static data, maybe they should be elsewhere, such as colors and thresholds

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===
"""

import numpy as np
import pandas as pd
from collections import OrderedDict

from .collections_object import ProjectCollections as prj_collections

colors_buttons = {
        'amber':'#FDB515',
        'red':'#E82C2E',
        'neutral':'#FAFAF9 ' #off-white
}

#for the map page
colors_risk_map ={
        'amber':'#FDB515',
        'red':'#E82C2E',
        'neutral':'#00AE84 ' #green
}

colors_risk ={
        'green':'#00AE84',
        'amber':'#FDB515',
        'red':'#E82C2E',
        'darkgreen':'#008261',
        'neutral':'#FAFAF9' #off-white
}

colors_rag ={
        'amber':'#FDB515',
        'red':'#E82C2E',
        'green':'#2dc937'
}

risk_thresholds = OrderedDict({
        (0,0.4):'neutral',
        (0.4,0.7):'amber',
        (0.7,np.inf): 'red'
})

button_thresholds_daily = OrderedDict({
        (0,0):colors_buttons['neutral'],
        (np.inf,np.inf): colors_buttons['amber'],
        (0,np.inf): colors_buttons['red'],

})
button_thresholds_weekly = OrderedDict({
        (0,np.inf):colors_buttons['neutral'],
        (np.inf,np.inf): colors_buttons['amber'],
        (np.inf,np.inf): colors_buttons['red'],

})
button_thresholds_monthly= OrderedDict({
        (0,np.inf):colors_buttons['neutral'],
        (np.inf,np.inf): colors_buttons['amber'],
        (np.inf,np.inf): colors_buttons['red'],

})

button_thresholds = {
        'daily':button_thresholds_daily,
        'weekly':button_thresholds_weekly,
        'monthly':button_thresholds_monthly
}

limit_df = {
    'Raw Water pH': [6.5, 6.7, 9., 9.5],
    'Clarified Water Turbidity': [-np.inf, -np.inf, 4.5, 5.5],
    'Dosed Raw Water pH (TVAL)': [5.9, 6., 6.8, 6.9],
    'Comb RGF Filtered Water UV254': [-np.inf, -np.inf, 6., 8.],
    'RGF Filtered Water Turbidity': [-np.inf, -np.inf, 0.2, 0.3],
    'Post Bisul Cl Level (TVAL)': [0.35, 0.6, 0.8, 0.9], # not sure what these should be
    'Cont Tank Inlet Free Cl (mg/l)': [0.8, 1., 1.35, 1.9], # what should these be?
    'Comb GAC Filtered Turbidity': [-np.inf, -np.inf, 0.2, 0.4], # unsure about the levels
    'Pre GAC pH (TVAL)': [6.8, 7., 7.9, 8.],
    'Treat Water Alum µg/l': [-np.inf, -np.inf, np.inf, np.inf],
    'Treat Water Cl (Total) (mg/l)': [0.01, 0.02, 0.2, 0.3], # check levels
    'Treat Water Final pH': [6.8, 7., 7.8, 8.2],
    'Treated Water Turbidity': [-np.inf, -np.inf, 0.2, 0.4], # check these levels
}
limit_df = pd.DataFrame(limit_df).T
limit_df.columns = ['LR', 'LA', 'HA', 'HR']

limit_df['LR'] = limit_df['LR'].replace(-np.inf, -99999999)
limit_df['LA'] = limit_df['LA'].replace(-np.inf, -9999999)
limit_df['HA'] = limit_df['HA'].replace(-np.inf,  9999999)
limit_df['HR'] = limit_df['HR'].replace(-np.inf,  99999999)

def load_data(collection, query={}, projection={'_id':0}) -> pd.DataFrame:
    """
    Simple function for loading mongo data into a dataframe.

    Args:
        collection: mongoDB collection
        query: dictionary-like object for query
        projection: dictionary-like object with projection

    Returns:
        pd.DataFrame
    """

    cursor = collection.find(query, projection)
    return pd.DataFrame(list(cursor))
