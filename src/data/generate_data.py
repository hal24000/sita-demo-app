"""
Generate a dataframe with random data of running pumps.
"""
"""
Copyright HAL24K 2020
Utility functions for dataframes

generate data script, contains random data generation code, only used in the early stages of the project
left here in case they are useful for testing new plots

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===


"""

import numpy as np
import pandas as pd
import datetime
from data.collections_object import ProjectCollections as prj_collections

sample_assets = [
        'Acid Dosing Pump {}',
        'Alum Dosing Pump {}',
        'DAF Recycle Pump {}',
        'Clean Backw Pump {}',
        'Centrifuge Pump {}',
]

sample_functional_areas = [
        'Clarification',
        'Disinfection',
        'Raw water intake',
        'Lime dosing',
]


def generate_exc_avail(n_exceptions, total=50):
    exc_df = []
    for i in range(n_exceptions):
        exc_df.append({
                'FunctionalArea':    np.random.choice(sample_functional_areas),
                'AssetDescription':  np.random.choice(sample_assets).format(np.random.randint(1, 30)),
                'DaysUnavailable':   np.random.randint(1, 30),
                'NumberUnavailable': np.random.randint(1, 10)

        })

    for i in range(total - n_exceptions):
        exc_df.append({
                'FunctionalArea':    np.random.choice(sample_functional_areas),
                'AssetDescription':  np.random.choice(sample_assets).format(np.random.randint(1, 30)),
                'DaysUnavailable':   np.nan,
                'NumberUnavailable': 0

        })

    exc_df = pd.DataFrame(exc_df)
    exc_df.append(exc_df.sum().rename('Total'))
    return exc_df


def generate_exc_failure(total=108):
    exc_df = []
    for i in range(total):
        exc_df.append({
                'FunctionalArea':        np.random.choice(sample_functional_areas),
                'AssetDescription':      np.random.choice(sample_assets).format(np.random.randint(1, 30)),
                'MostRecentFailureDate': datetime.datetime(year=2020, month=6,
                                                           day=np.random.randint(1, 10),
                                                           hour=np.random.randint(0, 23),
                                                           minute=np.random.randint(0, 59)),
                'MostRecentFailureOK':   datetime.datetime(year=2020, month=6,
                                                           day=np.random.randint(1, 10),
                                                           hour=np.random.randint(0, 23),
                                                           minute=np.random.randint(0, 59)),

        })

    exc_df = pd.DataFrame(exc_df)
    return exc_df


def generate_issues_data(n_issues):
    issues_df = []
    for i in range(n_issues):
        issues_df.append(
                {'AssetDescription':    np.random.choice(sample_assets).format(np.random.randint(0, 10)),
                 'DataIssues':          np.random.randint(0, 10),
                 'DataIssuesLastWeek':  np.random.randint(0, 10),
                 'DataIssuesLastMonth': np.random.randint(0, 10)}
        )
    issues_df = pd.DataFrame(issues_df)
    return issues_df


def generate_water_quality_readings_tabledf(n, metric_name):
    df = []
    for i in range(n):
        variance = np.random.rand()
        df.append({'Date':        datetime.datetime(year=2019, month=1, day=1),
                   'Metric':      metric_name,
                   'Reading':     np.random.rand() * 10,
                   'PrevReading': np.random.rand() * 10,
                   'Variance':    variance,
                   'ABSVariance': variance,
                   'lolo':        "",
                   "lo":          "",
                   'HiHi':        "",
                   "CheckStatus": np.random.choice(['Normal', 'Not Normal'])
                   })

    df = pd.DataFrame(df)
    df.append(df.sum().rename('Total'))
    return df


def generate_issues_comms(n_issues):
    issues_df = []
    for i in range(n_issues):
        issues_df.append(
                {'AssetDescription':     np.random.choice(sample_assets).format(np.random.randint(0, 10)),
                 'CommsIssues':          np.random.randint(0, 10),
                 'CommsIssuesLastWeek':  np.random.randint(0, 10),
                 'CommsIssuesLastMonth': np.random.randint(0, 10)}
        )
    issues_df = pd.DataFrame(issues_df)

    return issues_df


def generate_map_data():
    bot_left = (53.9751506, -1.3619189)
    top_right = (54.0462501, -0.8233207)

    avail_installations = prj_collections.avail_installations
    installation_df = []
    for i, name in enumerate(avail_installations):
        rand_lat = np.random.rand() * (top_right[0] - bot_left[0]) + bot_left[0]
        rand_lon = np.random.rand() * (top_right[1] - bot_left[1]) + bot_left[1]
        installation_df.append({"lat":  rand_lat, "lon": rand_lon,
                                "name": name,
                                'risk': np.random.rand()})

    installation_df = pd.DataFrame(installation_df)
    return installation_df


def generate_pump_data(no_pumps: int = 10,
                       max_running_days: int = 50,
                       seed: int = None) -> pd.DataFrame:
    """
    Generate a fake record of the number of days of pumps running.

    Args:
        no_pumps (int): number of pumps in the system (default: 50)
        max_running_days (int): maximum days of a pump running (default: 50)
        seed (int): random seed for reproducibility (default: None)

    Returns:
        pd.DataFrame
    """

    if seed is not None:
        np.random.seed(seed=seed)

    out_df = pd.DataFrame({"device":       ["pump_{0:02d}".format(i) for i in range(1,
                                                                                    no_pumps + 1)],
                           "days_running": np.random.randint(0,
                                                             max_running_days,
                                                             size=no_pumps)})
    out_df.sort_values(by="days_running", ascending=False, inplace=True)

    return out_df


def generate_time_series(no_devices: int = 5, start_time: pd.Timestamp = None,
                         end_time: pd.Timestamp = None, frequency: str = '1T') -> pd.DataFrame:
    """
    Generate fake time series data.

    Args:
        no_devices (int): for how many devices to generate the data
        start_time (pd.Timestamp): at which point to start with the data (if None, make it NOW)
        end_time (pd.Timestamp): at which point to finish the data (if None, make it 12 hours from start)
        frequency (str): what cadense should the data have (default: '1T')

    Returns:
        pd.DataFrame
    """

    if start_time is None:
        start_time = pd.Timestamp.now()
    elif isinstance(start_time, str):
        try:
            start_time = pd.Timestamp(start_time)
        except ValueError:
            start_time = pd.Timestamp.now()
    else:
        start_time = pd.Timestamp.now()

    if end_time is None:
        end_time = start_time + pd.Timedelta(hours=12)
    elif isinstance(end_time, str):
        try:
            end_time = pd.Timestamp(end_time)
        except ValueError:
            end_time = start_time + pd.Timedelta(hours=12)
    else:
        end_time = start_time + pd.Timedelta(hours=12)

    out_df = pd.DataFrame(index=pd.date_range(start_time, end_time, freq=frequency))

    for i in range(no_devices):
        out_df["machine{}".format(i + 1)] = np.random.normal(loc=i, size=len(out_df))

    return out_df


def generate_days_elapsed_no_maint(n):
    df = []

    for i in range(n):
        fa_ = np.random.choice(prj_collections.assets_df['functional_area'].unique())
        pa_ = np.random.choice(prj_collections.assets_df['process_area'].unique())
        asset = np.random.choice(sample_assets).format(np.random.randint(0, 15))
        days = np.random.randint(0, 40)
        df.append({'Functional Area': fa_, 'Process Area': pa_, 'Asset': asset, 'Days': days})

    df = pd.DataFrame(df)
    return df


def generate_wq_exceptions_summary(n):
    wq_names = [
            'Treat water CI (Total)(mg/l)',
            'Pre GAC pH(TVAL)',
            'Post Bisul CI Level(TVAL)',
            'Treat Water Final ph Metric',
            'Cent Tank Inlet Free CI',
            'RGF Filtered Water Turbidity',
            'Comb GAC Filtered Turbidity'
    ]
    issue_types = ['Issue-HR', 'Issue-LA', 'Issue-LR', 'Issue-HA']
    df = []
    for i, var in enumerate(wq_names[:n]):
        for issue_type in issue_types:
            df.append({
                    'var':        var,
                    'issue_type': issue_type,
                    'n_issues':   np.random.randint((1000)),
            })

    df = pd.DataFrame(df)
    return df


def generate_melted_time_series(time_series: pd.DataFrame) -> pd.DataFrame:
    """
    Change the output of generate_time_series() into a table for dahs output.

    Args:
        time_series (pd.DataFrame): output from generate_time_series

    Returns:
        pd.DataFrame
    """

    means = time_series.mean().to_dict()
    stds = time_series.std().to_dict()

    out_df = pd.melt(time_series.reset_index(), id_vars='index')
    out_df.rename(columns={'index':    'Date',
                           'variable': 'Metric',
                           'value':    'Reading'}, inplace=True)

    out_df.sort_values(by=['Date', 'Metric'], ascending=False, inplace=True)

    out_df['Date'] = out_df['Date'].map(lambda x: x.strftime('%d%b %H:%M'))

    out_df['Lo'] = np.nan
    out_df['Hi'] = np.nan

    for k in means:
        out_df.loc[out_df['Metric'] == k, 'Lo'] = means[k] - 1 * stds[k]
        out_df.loc[out_df['Metric'] == k, 'Hi'] = means[k] + 1 * stds[k]

    for col in ['Reading', 'Lo', 'Hi']:
        out_df[col] = out_df[col].round(2)

    out_df['Check'] = (out_df['Reading'] < out_df['Lo']) | (out_df['Reading'] > out_df['Hi'])

    return out_df


def generate_flow_exception_data(no_assets: int = 10,
                                 max_exceptions: int = 10,
                                 all_zero: bool = False) -> pd.DataFrame:
    """
    Genrate input data for the flow exceptions.

    Args:
        no_assets (int): number of assets monitored (default: 10)
        max_exceptions (int): the maximum number of excpetions available per asset (default: 10)
        all_zero (bool): if true, make all exceptions zero

    Returns
        pd.DataFrame

    TODO:
        figure out how the data will actually look 
    """

    out_df = pd.DataFrame({'Asset':                     ['machine{}'.format(i + 1) for i in range(no_assets)],
                           'No. of low flow instances': 0 if all_zero else \
                                                            np.random.randint(0, max_exceptions, size=no_assets),
                           'Total assets':              max_exceptions})

    return out_df


def generate_stock_level_drops(no_assets: int = 5) -> pd.DataFrame:
    """
    Generate random percentages.

    Args:
        no_assets (int): numer of assets for which to report.

    Returns:
        pd.DataFrame
    """

    out_df = pd.DataFrame({'Asset':                         ['machine{}'.format(i + 1) for i in range(no_assets)],
                           'Fraction of stock level drops': np.random.randint(0, 100, size=no_assets)})

    out_df.sort_values(inplace=True, by='Fraction of stock level drops', ascending=False)

    return out_df
