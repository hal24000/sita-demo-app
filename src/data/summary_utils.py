"""
Summary Dashboard utilities
Copyright 2020 HAL24K

includes functions built for the summary page, however some of them are used elsewhere as well

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===


"""
from datetime import timedelta
import pathlib
import pandas as pd
from pandas.api.types import CategoricalDtype
import pytz
from data.load_data import button_thresholds
from bson.codec_options import CodecOptions
import logging
from datetime import datetime
from enum import Enum
from data.collections_object import CollaborateDataConnect, ProjectCollections
from data.project_loggers import dash_logger

logger = logging.getLogger(dash_logger)


uri = 'mongodb+srv://cd_waterdemo:jA0VmwEW86MAVb8MhiHiJokvvJuYUFp9@atlas-prod-pl-0.hldmf.mongodb.net'
dbname = 'cd_waterdemo'
dim_sdk = CollaborateDataConnect(uri = uri, dbname = dbname)
db = dim_sdk.db

tz_london = ProjectCollections.tz_london
tz_utc = ProjectCollections.tz_utc


class MessageStatus(Enum):
    DATA_ERROR = 1
    CRITERIA_NOT_MET = 2
    CRITERIA_MET = 3


def get_equipment_list(site, equip, tag_subcat_filter=None,
                       functional_area=None):
    """

    Args:
        site ():
        equip ():
        tag_subcat_filter ():
        functional_area ():

    Returns:

    """
    collection = db['site_tag_config']
    query = {
            "site_name": site,
    }
    if tag_subcat_filter is not None:
        if isinstance(tag_subcat_filter, tuple):
            query["tag_subcategory"] = {"$in": tag_subcat_filter}
        elif isinstance(tag_subcat_filter, str):
            query["tag_subcategory"] = tag_subcat_filter
    if equip is not None:
        if isinstance(equip, tuple):
            query["tag_category"] = {"$in": equip}
        elif isinstance(equip, str):
            query["tag_category"] = equip
    if functional_area is not None:
        if isinstance(functional_area, tuple):
            query["functional_area"] = {"$in": functional_area}
        elif isinstance(functional_area, str):
            query["functional_area"] = functional_area

    result = [item.get("tag_name") for item in list(collection.find(query, {"_id": 0, 'tag_name': 1}))]
    return result


def get_pump_list(site, tag_subcat_filter=None):
    """

    Args:
        site ():
        tag_subcat_filter ():

    Returns:

    """
    pump_list = get_equipment_list(site=site, equip="Pump", tag_subcat_filter=tag_subcat_filter)
    return pump_list


def get_otherEquip_list(site, tag_subcat_filter=None):
    """

    Args:
        site ():
        tag_subcat_filter ():

    Returns:

    """
    equip_list = get_equipment_list(site=site, equip="OtherEquip", tag_subcat_filter=tag_subcat_filter)
    return equip_list


def count_equip_status(site,
                       reference_time=None, time_span=None,
                       equip_type=None, equip_list=None,
                       tag_subcat_filter=None, search_terms=None):
    """

    Args:
        site:
        reference_time:
        time_span:
        equip_type:
        equip_list:
        tag_subcat_filter:
        search_terms:

    Returns:

    """
    assert search_terms is not None, "A list of valid search values is required"
    assert equip_list is not None or equip_type is not None, "Either equip_type or equip_list must be supplied"
    if equip_type is not None:
        assert equip_type in ["Pump", "OtherEquip"], "Only 'Pump' or 'OtherEqip' are acceptable inputs to equip_type"

    # If we don't receive a reference time set things to be "live" with no lookback time
    if reference_time is None:
        reference_time = pd.Timestamp.utcnow()
    else:
        reference_time = datetime.strptime(reference_time, ProjectCollections.dashboard_datetime_format)

    site_tag_config = ProjectCollections.site_tag_config

    # In case pump list needs to be produced on the fly
    if equip_list is None:
        equip_list = get_equipment_list(site=site, equip=equip_type,
                                        tag_subcat_filter=tag_subcat_filter)

    # Establishing the query for the mongo match
    base_query = {
            "tag_name":          {"$in": equip_list},
            "measurement_value": {"$in": search_terms}
    }

    if time_span is None:
        data_collection = ProjectCollections.series_and_alarms_coll_latest
    else:
        data_collection = ProjectCollections.get_mongo_collection(collection='alarms',
                                                                  lookback=time_span)

        time_delta = pd.Timedelta(f'{time_span}d')
        lookback_time = reference_time - time_delta
        base_query.update(
                {
                        "measurement_timestamp": {
                                "$gte": lookback_time,
                                "$lte": reference_time
                        }
                }
        )

    status_count = data_collection.count_documents(base_query)
    return status_count


def count_alarm_occurrences(site: str,
                            reference_time=None, time_span=None, live=False,
                            equip_type=None, equip_list=None,
                            tag_subcat_filter=None, search_terms=None):
    """Function to query the alarm occurrence collections and identify how many "active" (as defined by the
    search_terms) alarms there are at a given time or time window

    Args:
        site:
        reference_time:
        time_span:
        live:
        equip_type:
        equip_list:
        tag_subcat_filter:
        search_terms:

    Returns:
        int
    """
    assert search_terms is not None, "A list of valid search values is required"
    assert equip_list is not None or equip_type is not None, "Either equip_type or equip_list must be supplied"
    if equip_type is not None:
        assert equip_type in ["Pump", "OtherEquip"], "Only 'Pump' or 'OtherEqip' are acceptable inputs to equip_type"

    # If we don't receive a reference time set things to be "live" with no lookback time
    if reference_time is None:
        reference_time = pd.Timestamp.utcnow()
    else:
        # reference_time = pd.to_datetime(reference_time, utc=True)
        reference_time = datetime.strptime(reference_time, ProjectCollections.dashboard_datetime_format)

    # In case pump list needs to be produced on the fly
    if equip_list is None:
        equip_list = get_equipment_list(site=site, equip=equip_type,
                                        tag_subcat_filter=tag_subcat_filter)

    # Stuff
    base_query = {
            "tag_name":          {"$in": equip_list},
            "measurement_value": {"$in": search_terms}
    }

    if (time_span is None) and live:
        data_collection = db['alarm_occurrences_latest']
    elif time_span is None:
        data_collection = db['alarm_occurrences']
        base_query.update(
                {"$or": [
                        {"occurrence_start_time": {"$lte": reference_time},
                         "occurrence_end_time":   {"$gte": reference_time}
                         },
                        {"occurrence_start_time": {"$lte": reference_time},
                         "occurrence_end_time":   None
                         }
                ]
                }
        )
    else:
        data_collection = db['alarm_occurrences']
        time_delta = pd.Timedelta(f'{time_span}d')
        lookback_time = reference_time - time_delta
        base_query.update(
                {"$or": [
                        {"occurrence_start_time": {"$gte": lookback_time,
                                                   "$lte": reference_time
                                                   }
                         },
                        {"occurrence_end_time": {"$gte": lookback_time,
                                                 "$lte": reference_time
                                                 }
                         },
                        {"occurrence_start_time": {"$lte": lookback_time},
                         "occurrence_end_time":   {"$gte": reference_time}
                         },
                        {"occurrence_start_time": {"$lte": lookback_time},
                         "occurrence_end_time":   None
                         }
                ]
                }
        )

    status_count = data_collection.count_documents(base_query)
    return status_count


def count_equip_failures(site, equip_type=None, reference_time=None, time_span=None, live=False, equip_list=None):
    """

    Args:
        site ():
        equip_type ():
        reference_time ():
        time_span ():
        live ():
        equip_list ():

    Returns:

    """
    assert equip_list is not None or equip_type is not None, "Either equip_type or equip_list must be supplied"
    if equip_type is not None:
        assert equip_type in ["Pump", "OtherEquip"], "Only 'Pump' or 'OtherEqip' are acceptable inputs to equip_type"

    tag_subcat_filter = ("Status", "Fail Status")
    search_terms = ["Failed", "FAILED"]
    failure_count = count_alarm_occurrences(site=site,
                                            reference_time=reference_time, time_span=time_span, live=live,
                                            equip_list=equip_list, equip_type=equip_type,
                                            tag_subcat_filter=tag_subcat_filter,
                                            search_terms=search_terms)
    return failure_count


def count_equip_unavailable(site, equip_type=None, reference_time=None, time_span=None, live=False, equip_list=None):
    """

    Args:
        site ():
        equip_type ():
        reference_time ():
        time_span ():
        live ():
        equip_list ():

    Returns:

    """
    assert equip_list is not None or equip_type is not None, "Either equip_type or equip_list must be supplied"
    if equip_type is not None:
        assert equip_type in ["Pump", "OtherEquip"], "Only 'Pump' or 'OtherEqip' are acceptable inputs to equip_type"

    tag_subcat_filter = "AvailStatus"
    search_terms = ["UNAVAILABLE", "UNAVAIL", "Unavail", "Unavailable"]
    unavailable_count = count_alarm_occurrences(site=site,
                                                reference_time=reference_time, time_span=time_span, live=live,
                                                equip_list=equip_list, equip_type=equip_type,
                                                tag_subcat_filter=tag_subcat_filter,
                                                search_terms=search_terms)
    return unavailable_count


def count_comms_failures(site, reference_time=None, time_span=None, live=True):
    """

    Args:
        site ():
        reference_time ():
        time_span ():
        live ():

    Returns:

    """
    equip_type = "Pump"
    tag_subcat_filter = "CommsStatus"
    search_terms = ["FAILED", "Failed", "COMMS FAILED"]
    failure_count = count_alarm_occurrences(site=site,
                                            reference_time=reference_time, time_span=time_span, live=live,
                                            equip_type=equip_type,
                                            tag_subcat_filter=tag_subcat_filter,
                                            search_terms=search_terms)
    return failure_count


def count_data_failures(site, reference_time=None, time_span=None, live=True):
    """

    Args:
        site ():
        reference_time ():
        time_span ():
        live ():

    Returns:

    """
    equip_type = "Pump"
    search_terms = ["No DATA", "NO DATA", "No Data"]
    failure_count = count_alarm_occurrences(site=site,
                                            reference_time=reference_time, time_span=time_span, live=live,
                                            equip_type=equip_type,
                                            search_terms=search_terms)
    return failure_count


def get_temp_sensor_list(site, temperature_sensor=None):
    """

    Args:
        site ():
        temperature_sensor ():

    Returns:

    """
    assert temperature_sensor in ["MOTOR",
                                  "BEARING"], "temperature_sensor not recognised; 'MOTOR' or 'BEARING' are accepted"

    if temperature_sensor == "MOTOR":
        regex = ".*MOT$"
    elif temperature_sensor == "BEARING":
        regex = ".*BOT$"

    collection = db['site_tag_config']
    query = {
            "site_name": site,
            "tag_name":  {"$regex": regex, "$options": "i"}
    }

    result = [item.get("tag_name") for item in list(collection.find(query, {"_id": 0, 'tag_name': 1}))]
    return result


def count_temp_sensor_status(site,
                             reference_time=None, time_span=None, live=False,
                             temperature_sensor=None, sensor_list=None,
                             search_terms=None):
    """

    Args:
        site ():
        reference_time ():
        time_span ():
        live ():
        temperature_sensor ():
        sensor_list ():
        search_terms ():

    Returns:

    """
    assert search_terms is not None, "A list of valid search values is required"
    assert temperature_sensor is not None or sensor_list is not None, "Either temperature_sensor or sensor_list must be supplied"
    if temperature_sensor is not None:
        assert temperature_sensor in ["MOTOR",
                                      "BEARING"], "Only 'MOTOR' or 'BEARING' are acceptable inputs to temperature_sensor"

    # If we don't receive a reference time set things to be "live" with no lookback time
    if reference_time is None:
        reference_time = pd.Timestamp.utcnow()
    else:
        # reference_time = pd.to_datetime(reference_time)
        reference_time = datetime.strptime(reference_time, ProjectCollections.dashboard_datetime_format)

    # In case pump list needs to be produced on the fly
    if sensor_list is None:
        sensor_list = get_temp_sensor_list(site=site, temperature_sensor=temperature_sensor)

    # Mongo query
    base_query = {
            # "site_name": site,
            "tag_name":          {"$in": sensor_list},
            "measurement_value": {"$in": search_terms}
    }

    if (time_span is None) and live:
        data_collection = ProjectCollections.series_and_alarms_coll_latest
    elif time_span is None:
        data_collection = ProjectCollections.series_and_alarms_coll
    else:
        data_collection = ProjectCollections.get_mongo_collection(lookback=time_span, name='alarms')

        time_delta = pd.Timedelta(f'{time_span}d')
        lookback_time = reference_time - time_delta
        base_query.update(
                {
                        "measurement_timestamp": {
                                "$gte": lookback_time,
                                "$lte": reference_time
                        }
                }
        )

    status_count = data_collection.count_documents(base_query)
    return status_count


def count_temperature_failures(site, temperature_sensor=None, reference_time=None, time_span=None, live=False,
                               sensor_list=None):
    """

    Args:
        site ():
        temperature_sensor ():
        reference_time ():
        time_span ():
        live ():
        sensor_list ():

    Returns:

    """
    assert sensor_list is not None or temperature_sensor is not None, "Either temperature_sensor or sensor_list must be supplied"
    if temperature_sensor is not None:
        assert temperature_sensor in ["MOTOR",
                                      "BEARING"], "Only 'MOTOR' or 'BEARING' are acceptable inputs to temperature_sensor"

    search_terms = [f'{temperature_sensor} OVR TEMP']
    excess_temperature_count = count_temp_sensor_status(site=site,
                                                        reference_time=reference_time, time_span=time_span, live=live,
                                                        sensor_list=sensor_list, temperature_sensor=temperature_sensor,
                                                        search_terms=search_terms)
    return excess_temperature_count


def count_maintenance_status(site,
                             reference_time=None, time_span=None,
                             status=None):
    """

    Args:
        site ():
        reference_time ():
        time_span ():
        status ():

    Returns:

    """
    assert status in ["Cancelled", "In-Progress",
                      "Completed"], "Invalid status type; choose from 'Cancelled', 'In-Progress', 'Completed'"

    # If we don't receive a reference time set things to be "live" with no lookback time
    if reference_time is None:
        reference_time = pd.Timestamp.utcnow()
    else:
        # reference_time = pd.to_datetime(reference_time)
        reference_time = datetime.strptime(reference_time, ProjectCollections.dashboard_datetime_format)

    if time_span is None:
        time_span = 365

    # Primary query
    base_query = {
            "functional_location_description": {"$regex": f"{site}.*", "$options": "i"},
            "bsc_start":                       {"$gte": reference_time - pd.Timedelta(f"{time_span}d"),
                                                "$lte": reference_time},
            "completion":                      status
    }

    # Setup the data collection and make it timezone aware
    codec_options = CodecOptions(tz_aware=True, tzinfo=pytz.timezone('UTC'))
    data_collection = db["maintenance_workorders"].with_options(codec_options=codec_options)

    # status_count = data_collection.count_documents(base_query) # Original method
    # This approach should prevent double counting when the same work order appears multiple times
    status_count = len(data_collection.distinct("order_number", base_query))
    return status_count


def count_delayed_maintenance(site, reference_time=None, time_span=None):
    """

    Args:
        site ():
        reference_time ():
        time_span ():

    Returns:

    """

    # If we don't receive a reference time set things to be "live" with no lookback time
    if reference_time is None:
        reference_time = pd.Timestamp.utcnow()
    else:
        # reference_time = pd.to_datetime(reference_time)
        reference_time = datetime.strptime(reference_time, ProjectCollections.dashboard_datetime_format)

    if time_span is None:
        time_span = 365

    # Primary query
    base_query = {
            "functional_location_description": {"$regex": f"{site}.*", "$options": "i"},
            "bsc_start":                       {"$gte": reference_time - pd.Timedelta(f"{time_span}d"),
                                                "$lte": reference_time},
            "completion":                      "In-Progress"
    }
    projection = {"_id": 0, "bsc_start": 1, "planned_work_amount": 1, "priority": 1}

    # Setup the data collection and make it timezone aware
    codec_options = CodecOptions(tz_aware=True, tzinfo=pytz.timezone('UTC'))
    data_collection = db["maintenance_workorders"].with_options(codec_options=codec_options)

    inprogress_df = pd.DataFrame(list(data_collection.find(base_query)))
    if len(inprogress_df) == 0:
        return 0
    # this column is actually minutes
    # overdue is quite badly defined by time it takes to complete the work
    inprogress_df['planned_work_amount'] = inprogress_df['planned_work_amount'].map(float)
    inprogress_df["DUE"] = inprogress_df["actual_release"] + inprogress_df.apply(
            lambda x: pd.Timedelta('{}{}'.format(x['actual_work_amount'],
                                                 ProjectCollections.work_amount_unit_map.get(x['work_amount_unit'],
                                                                                             'T'))),
            axis=1)
    inprogress_df["LATE"] = inprogress_df["DUE"].map(lambda x: x.ceil("D") < reference_time)

    # Get the number of overdue workorders
    number_workorders_overdue = inprogress_df.loc[inprogress_df["LATE"], "order_number"].nunique()

    return number_workorders_overdue


def safe_float_convert(x):
    try:
        float(x)
        return True  # numeric, success!
    except ValueError:
        return False  # not numeric
    except TypeError:
        return False  # null type


def query_last_in_period(tags, reference_time, lookback=None, search_terms=None,
                         collection='sensors', retry=True):
    """
    Retrieves the last entry for a search term in a specific time period
    """

    if lookback is None:
        lookback_in_days = (25 / 60) / 24
        lookback_time = reference_time - timedelta(minutes=25)
        data_collection = ProjectCollections.get_mongo_collection(lookback=lookback_in_days, collection=collection)
    else:
        lookback_in_days = (lookback / 60) / 24
        lookback_time = reference_time - timedelta(minutes=lookback)
        data_collection = ProjectCollections.get_mongo_collection(lookback=lookback_in_days, collection=collection)

    query_match = {'$match': {"tag_name":              {'$in': tags},
                              'measurement_timestamp': {"$lte": reference_time,
                                                        "$gte": lookback_time},
                              }
                   }

    if search_terms is not None:
        query_match.update({'measurement_value': {'$in': search_terms}})

    query_group = {'$group': {'_id':                   "$tag_name",
                              'measurement_value':     {'$last': "$measurement_value"},
                              'measurement_timestamp': {'$last': '$measurement_timestamp'},
                              'measurement_unit':      {'$last': '$measurement_unit'}

                              }
                   }

    query_sort = {"$sort": {'measurement_timestamp': 1}}
    query1 = [query_match, query_sort, query_group]
    res = pd.DataFrame(data_collection.aggregate(query1))
    if len(res) == 0 and retry:
        if collection == 'sensors':
            collection = 'alarms'
        else:
            collection = 'sensors'

        res = query_last_in_period(tags, reference_time,
                                   retry=False,
                                   collection=collection)

        return res

    res.rename({"_id": 'tag_name'}, axis=1, inplace=True)
    return res


def get_significant_level_drops(site, reference_time=None):
    """
    returns the tanks with significant stock level drops
    A significant stock level drop is defined as a change of 10% relative difference in 24 hours.
    E.g. if the previous value was 10 24 hours ago, and 8 now.
    Return those and their drop %
    Args:
        site (str): the relevant site
        reference_time (datetime|str): reference time to query for

    Returns:

    """
    # curr_datetime = pd.to_datetime(reference_time)
    # td = timedelta(hours=lookback)

    site_tag_config = ProjectCollections.site_tag_config
    stock_level_tags_df = ProjectCollections.stock_level_tags_df
    stock_level_tags = list(stock_level_tags_df.loc[stock_level_tags_df.site_name == site, 'tag_name'])
    # dont fetch these columns
    skip_ = {'fabricated_key': 0, "_id": 0}
    # query 1 , fetch the latest values
    if reference_time is None:
        query1 = {"tag_name": {'$in': stock_level_tags}}
        res = pd.DataFrame(list(ProjectCollections.series_and_alarms_coll_latest.find(query1, skip_)))
    else:
        res = query_tags_conditional(tag_list=stock_level_tags,
                                     reference_time=reference_time,
                                     latest_only=True)

    if len(res) == 0:
        return pd.DataFrame(), MessageStatus.DATA_ERROR

    res = pd.merge(res, site_tag_config[['tag_name', 'lo_used', 'tag_short_description']],
                   on='tag_name', how='left')

    logger.info('stock level data:')
    logger.info(res[['tag_name', 'measurement_value', 'lo_used']])

    significant_drops = res.loc[res.measurement_value < res.lo_used]

    if len(significant_drops) == 0:
        return pd.DataFrame(), MessageStatus.CRITERIA_NOT_MET

    return significant_drops, MessageStatus.CRITERIA_MET


def get_color_button(n, period):
    thresholds_dict = button_thresholds.get(period)
    for key, value in thresholds_dict.items():
        if n > key[0] and n <= key[1]:
            return value


def get_equipment_list_local(site, equip_type, functional_areas):
    """

    Args:
        site ():
        equip_type ():
        functional_areas ():

    Returns:

    """
    site_conf = ProjectCollections.site_tag_config
    relevant_equipment = list(site_conf.loc[(site_conf.site_name == site) &
                                            (site_conf.tag_category.isin(equip_type)) &
                                            (site_conf.functional_area.isin(functional_areas)), 'tag_name'].unique())
    return relevant_equipment


def df_utc_to_london(df, column='measurement_timestamp'):
    """

    Args:
        df ():
        column ():

    Returns:

    """
    df[column] = df[column].dt.tz_localize(tz_utc)
    df[column] = df[column].dt.tz_convert(tz=tz_london)
    df[column] = df[column].dt.tz_localize(tz=None)

    return df


def query_max_value(tag_list, reference_time, lookback, value_cap=None, collection='sensors'):
    """

    Args:
        tag_list ():
        reference_time ():
        lookback ():
        value_cap ():
        collection ():

    Returns:

    """
    # lookback in days is
    lookback_d = lookback / 24

    reference_time_tded = reference_time - timedelta(hours=lookback)

    data_collection = ProjectCollections.get_mongo_collection(lookback=lookback_d, collection=collection)

    match_q = {'$match': {
            'tag_name':              "",
            "measurement_timestamp": {'$gte': reference_time_tded,
                                      "$lte": reference_time}
    }}

    if value_cap is not None:
        match_q['$match'].update({'measurement_value': {"$lte": value_cap}})

    results = []
    if not isinstance(tag_list, list):
        tag_list = [tag_list]
    for tag in tag_list:
        match_q['$match']['tag_name'] = tag
        res = pd.DataFrame(list(data_collection.aggregate([match_q,
                                                           {'$sort': {'measurement_value': -1}},
                                                           {'$limit': 1}])))

        results.append(res)

    res = pd.concat(results)
    return res


def query_tags_conditional(tag_list=None,
                           tag_values=None,
                           reference_time=None,
                           time_span=None,
                           collection='sensors',
                           latest_only=False,
                           retry=True):
    """
    returns results based on search criteria. E.g. Availstatus tag_subcat and 'Unavailable' in search terms.
    equip_type =  'PUMPS'
    etc.
    time span is in days

    looks in the latest collection (no historical data)

    Args:
        tag_list (list): the tag list to look for
        tag_values (list): optional, values the tags must contain
        reference_time (datetime|str):
        time_span (float): time lookback in days, can be 0.5 for 12 hours etc
        collection (str): one of alarms | sensors
        latest_only (bool): if true, when more than 1 result is fetched, it only returns the latest
        retry (bool): if true, tries the other collection as well, e.g. if looking in alarms will look in sensors

    Returns:
        res

    """


    if tag_list is not None:
        if not isinstance(tag_list, list):
            tag_list = [tag_list]
        query = {
                'tag_name': {'$in': tag_list},
        }
    else:
        raise (ValueError)

    if tag_values is not None:
        if not isinstance(tag_values, list):
            tag_values = [tag_values]
        # tag_values = [tag_values]
        query.update(measurement_value={"$in": tag_values})

    if reference_time is None:
        reference_time = pd.Timestamp.utcnow()
    else:
        # reference_time = pd.to_datetime(reference_time)
        if isinstance(reference_time, str):
            reference_time = datetime.strptime(reference_time, ProjectCollections.dashboard_datetime_format)

    if time_span is None:
        time_span_auto = (30 / 60) / 24
        lookback_time = reference_time - timedelta(minutes=30)
        data_collection = ProjectCollections.get_mongo_collection(lookback=time_span_auto, collection=collection)
        query.update({"measurement_timestamp": {"$gte": lookback_time, "$lte": reference_time}})
    else:
        data_collection = ProjectCollections.get_mongo_collection(lookback=time_span, collection=collection)
        lookback_time = reference_time - pd.Timedelta(days=time_span)
        query.update({"measurement_timestamp": {"$gte": lookback_time, "$lte": reference_time}})

    if time_span is None or latest_only:  # latest reults assumed when timespan not set
        res = []
        for tag in tag_list:
            query['tag_name']['$in'] = [tag]
            res_temp = pd.DataFrame(list(data_collection.find(query,
                                                              {'_id':            0,
                                                               "fabricated_key": 0}).sort([("measurement_timestamp",
                                                                                            -1)]).limit(1)))
            if len(res_temp) > 0:
                res.append(res_temp)

        if len(res) > 0:
            res = pd.concat(res)
            # reset index because all have index 0, since they were queried sequentially and concatenated
            res.reset_index(inplace=True, drop=True)
        else:
            res = pd.DataFrame()

    else:
        res = pd.DataFrame(list(data_collection.find(query,
                                                     {'_id': 0, "fabricated_key": 0}) \
                                .sort([("measurement_timestamp", -1)])))

    if len(res) == 0 and retry:
        if collection == "sensors":
            collection = 'alarms'
        else:
            collection = 'sensors'
        return query_tags_conditional(tag_list=tag_list,
                                      tag_values=tag_values,
                                      reference_time=reference_time,
                                      time_span=time_span,
                                      collection=collection,
                                      latest_only=latest_only,
                                      retry=False)
    elif len(res) == 0:
        return res

    if collection == 'sensors':
        try:
            res['measurement_value'] = res['measurement_value'].astype(float)
        except:
            pass
    return res


def get_equipment_dataframe(site, equip, tag_subcat_filter=None,
                            functional_area=None):
    """

    Args:
        site ():
        equip ():
        tag_subcat_filter ():
        functional_area ():

    Returns:

    """
    timestart = datetime.now()
    collection = db['site_tag_config']
    query = {
            "site_name": site,
    }
    if tag_subcat_filter is not None:
        if isinstance(tag_subcat_filter, tuple):
            query["tag_subcategory"] = {"$in": tag_subcat_filter}
        elif isinstance(tag_subcat_filter, str):
            query["tag_subcategory"] = tag_subcat_filter
    if equip is not None:
        if isinstance(equip, tuple):
            query["tag_category"] = {"$in": equip}
        elif isinstance(equip, str):
            query["tag_category"] = equip
    if functional_area is not None:
        if isinstance(functional_area, tuple):
            query["functional_area"] = {"$in": functional_area}
        elif isinstance(functional_area, str):
            query["functional_area"] = functional_area

    result = pd.DataFrame(list(collection.find(query, {"_id": 0})))

    timetaken = datetime.now() - timestart
    logger.info("Get equipment query took: {}".format(timetaken))
    return result


def find_pump_run_signal_sensor(df):
    """

    Args:
        df ():

    Returns:

    """
    # cat_type = CategoricalDtype(categories=["RunStatus", "RunningStatus", "Power", "Speed"], ordered=True)
    cat_type = CategoricalDtype(categories=["Speed"], ordered=True)
    df["RunSensor"] = df['tag_subcategory'].astype(cat_type)
    df = df.sort_values(by="RunSensor").groupby("common_reference").head(1)
    return df


def get_pump_data(tag_id, start_time=None, stop_time=None):
    """

    Args:
        tag_id ():
        start_time ():
        stop_time ():

    Returns:

    """
    assert (isinstance(tag_id, str) or isinstance(tag_id, list)), "tag_id must be a string or a list of strings"
    if stop_time is None:
        stop_time = pd.Timestamp.utcnow()
    if start_time is None:
        start_time = stop_time - pd.Timedelta("60d")

    # collection = db["alarms_and_sensor_values"]
    lookback = stop_time - start_time
    collection = ProjectCollections.get_mongo_collection(collection='sensors', lookback=lookback.days)

    match = {"measurement_timestamp": {
            "$gte": start_time,
            "$lte": stop_time
    }
    }
    if isinstance(tag_id, list):
        match["tag_name"] = {"$in": tag_id}
    else:
        match["tag_name"] = tag_id

    # Select fields to return from the query
    projection = {"_id": 0, "measurement_timestamp": 1, "measurement_value": 1, "tag_name": 1}

    sensor_df = pd.DataFrame(list(collection.find(match, projection)))
    if sensor_df.shape[0] > 0:
        sensor_df = sensor_df.sort_values(by="measurement_timestamp").set_index("measurement_timestamp", drop=False)
    return sensor_df


def pump_signal_digitise(df, column, threshold=0):
    """

    Args:
        df ():
        column ():
        threshold ():

    Returns:

    """

    def _digitise(input_value):
        if input_value > threshold:
            return True
        else:
            return False

    df['STATUS'] = pd.to_numeric(df[column], errors='coerce').map(_digitise)

    return df


def pump_status_differential(df):
    """

    Args:
        df ():

    Returns:

    """
    df.sort_index()
    df['STATUS_DIFF'] = df["STATUS"].map(int).diff()
    return df


def pump_status_groups(df):
    """"""
    df["GB_OFF_VAR"] = df["STATUS"].cumsum()
    df["GB_ON_VAR"] = (~df["STATUS"]).cumsum()
    df["TIME_DELTA"] = (df["measurement_timestamp"].diff()).map(lambda x: x.total_seconds())
    return df


def status_duration_calc(df, status_type):
    """

    Args:
        df ():
        status_type ():

    Returns:

    """
    column_lookup = {
            "ON":  "GB_ON_VAR",
            "OFF": "GB_OFF_VAR"
    }
    assert status_type in column_lookup.keys(), f'Choose an appropriate status, "ON" or "OFF": {status_type} not recognised'

    intervals = df.groupby(by=column_lookup.get(status_type)) \
        .agg(min_time=('measurement_timestamp', 'min'),
             max_time=('measurement_timestamp', 'max'))

    intervals["DURATION"] = (intervals["max_time"] - intervals["min_time"]).map(lambda x: x.total_seconds() / 3600)
    intervals = intervals[intervals["DURATION"] > 0]

    return intervals


def pump_runtime_calcs(df):
    """"""
    on_intervals = df.pipe(status_duration_calc, status_type="ON")
    off_intervals = df.pipe(status_duration_calc, status_type="OFF")

    return on_intervals, off_intervals


def most_recent_cycle_duration(cycle_interval_df):
    """

    Args:
        cycle_interval_df ():

    Returns:

    """
    cycle_interval_df = cycle_interval_df.sort_values(by=["min_time", "max_time"], ascending=False)
    if len(cycle_interval_df) > 0:
        latest_cycle = cycle_interval_df.head(1).to_dict(orient="records")[0]
    else:
        latest_cycle = {
                "DURATION": 0,
                "min_time": pd.to_datetime("1999-01-24"),
                "max_time": pd.to_datetime("1999-01-24")
        }
    return latest_cycle


def get_pump_run_time(site, tag_id, start_time=None, stop_time=None):
    """

    Args:
        site ():
        tag_id ():
        start_time ():
        stop_time ():

    Returns:

    """
    latest_cycle = {"tag_name": tag_id,
                    "DURATION": 0,
                    "min_time": pd.to_datetime("2001-01-24"),
                    "max_time": pd.to_datetime("2001-01-24")
                    }

    pump_sensor_data = get_pump_data(tag_id=tag_id, start_time=start_time, stop_time=stop_time)
    if pump_sensor_data.shape[0] > 0:
        on_df, off_df = pump_sensor_data.sort_index() \
            .pipe(pump_signal_digitise, column='measurement_value', threshold=0) \
            .pipe(pump_status_differential) \
            .pipe(pump_status_groups) \
            .pipe(pump_runtime_calcs)

        latest_on_cycle = on_df.pipe(most_recent_cycle_duration)
        latest_off_cycle = off_df.pipe(most_recent_cycle_duration)

        if len(latest_off_cycle) > 0:
            latest_cycle.update(min_time=latest_off_cycle.get('max_time'), max_time=latest_off_cycle.get('max_time'))

        if latest_on_cycle.get('max_time', pd.to_datetime("1999-12-31")) > latest_cycle.get("max_time"):
            latest_cycle.update(latest_on_cycle)

    return latest_cycle


def calculate_pump_runtimes(tag_id, dataframe):
    """

    Args:
        tag_id ():
        dataframe ():

    Returns:

    """
    latest_cycle = {"tag_name": tag_id,
                    "DURATION": 0,
                    "min_time": pd.to_datetime("2001-01-24"),
                    "max_time": pd.to_datetime("2001-01-24")
                    }

    pump_sensor_data = dataframe
    if pump_sensor_data.shape[0] > 0:
        pump_sensor_data = pump_sensor_data.sort_index(ascending=False) \
            .pipe(pump_signal_digitise, column='measurement_value', threshold=0)
        min_time = pump_sensor_data['STATUS'].ne(True).idxmax()
        max_time = pump_sensor_data.iloc[0].name
        duration = (max_time - min_time).total_seconds() / (60. * 60)
        latest_cycle.update(min_time=min_time, max_time=max_time, DURATION=duration)

    return latest_cycle


def get_site_pump_runtimes(site, reference_time=None, time_span=None):
    """

    Args:
        site ():
        reference_time ():
        time_span ():

    Returns:

    """
    tStart = pd.Timestamp.utcnow()
    logger.info(f'[{tStart}] PUMP runtime-calcs started')
    if reference_time is None:
        reference_time = pd.Timestamp.utcnow()
    else:
        reference_time = datetime.strptime(reference_time, ProjectCollections.dashboard_datetime_format)

    if time_span is None:
        time_span = 60
    time_delta = pd.Timedelta(f'{time_span}d')

    runtime_sensors = get_equipment_dataframe(site=site, equip='Pump', tag_subcat_filter=("Power", "Speed")). \
        pipe(find_pump_run_signal_sensor)

    pump_sensor_data = get_pump_data(list(runtime_sensors['tag_name']), stop_time=reference_time,
                                     start_time=reference_time - time_delta)

    pump_runtimes = []
    for tag_id, dataframe in pump_sensor_data.groupby("tag_name"):
        latest_run = calculate_pump_runtimes(tag_id, dataframe)
        pump_runtimes.append(latest_run)

    # Convert to dataframe and merge with sensor information
    runtime_sensors = runtime_sensors[['tag_name', 'tag_short_description', 'functional_area', 'tag_subcategory']]
    runtime_df = pd.DataFrame(pump_runtimes)
    runtime_df = pd.merge(runtime_df, runtime_sensors, left_on="tag_name", right_on="tag_name")

    # Convert duration to days and sort from largest to smallest
    runtime_df["DURATION"] = runtime_df["DURATION"] / 24.
    runtime_df = runtime_df.sort_values(by=['DURATION', 'functional_area'], ascending=(False, True))
    runtime_df = runtime_df.rename(columns={
            "tag_short_description": "Description",
            "DURATION":              "Duration (days)",
            "min_time":              "Cycle Start",
            "max_time":              "Last Measured",
            "functional_area":       "Functional Area",
            "tag_subcategory":       "Subcategory",
            "tag_name":              "Tag Name"
    }).reset_index(drop=True)

    tStop = pd.Timestamp.utcnow()
    logger.info(f'[{tStop}] PUMP runtime-calcs completed')
    logger.info(f'[{tStop - tStart}] Time taken')
    return runtime_df


def query_fail_unavail_new(equipment_list,
                           current_datetime,
                           search_terms_fail,
                           live,
                           failures):
    """

    Args:
        equipment_list ():
        current_datetime ():
        search_terms_fail ():
        live ():
        failures ():

    Returns:

    """
    query = {
            'tag_name':          {"$in": equipment_list},
            'measurement_value': {'$in': search_terms_fail},
    }

    if live:
        events_coll = ProjectCollections.fast_events_coll_latest
    else:
        events_coll = ProjectCollections.fast_events_coll
        query_update = {"$or": [
                {"occurrence_start_time": {"$lte": current_datetime},
                 "occurrence_end_time":   {"$gte": current_datetime}
                 },
                {"occurrence_start_time": {"$lte": current_datetime},
                 "occurrence_end_time":   None
                 }
        ]
        }

        query.update(query_update)

    if failures:
        keyword = "Failed"
    else:
        keyword = "Unavailable"
    site_tag_config = ProjectCollections.site_tag_config

    res = pd.DataFrame(list(events_coll.find(query, {"_id": 0})))

    if len(res) == 0:
        return pd.DataFrame()

    # logger.warning("query fail unavail res: {}".format( res))
    df_merged_with_assets = pd.merge(res,
                                     site_tag_config[['tag_name', "tag_short_description", 'tag_description',
                                                      'functional_area']],
                                     on='tag_name',
                                     how='left')

    # logger.warning("query fail unavail res: {}".format( res))
    # df_merged_with_assets['end_start_time'] =  pd.to_datetime(df_merged_with_assets['occurence_end_time'])
    df_merged_with_assets['occurrence_start_time'] = pd.to_datetime(df_merged_with_assets['occurrence_start_time'])
    df_merged_with_assets['occurrence_end_time'] = pd.to_datetime(df_merged_with_assets['occurrence_end_time'])
    temp_end_times = df_merged_with_assets['occurrence_end_time'].fillna(datetime.now())

    df_merged_with_assets[f'Days {keyword}'] = (temp_end_times - df_merged_with_assets['occurrence_start_time']).dt.days

    df_merged_with_assets = df_utc_to_london(df_merged_with_assets, 'occurrence_start_time')
    df_merged_with_assets = df_utc_to_london(df_merged_with_assets, 'occurrence_end_time')

    # Functional Area, Full Description,Unavailable Date, UOM, Available Date, Days Unavailable
    df_merged_with_assets.rename({'occurrence_start_time': f"{keyword} Date",
                                  'occurrence_end_time':   'Available Date',
                                  # 'occurrence_last_time': 'Still unavailable by',
                                  'functional_area':       'Functional Area',
                                  'tag_description':       'Full Description'}, axis=1, inplace=True)
    # df_merged_with_assets['Available Date'] = df_merged_with_assets['Unavailable Date'] - timedelta(minutes=15)
    df_merged_with_assets['Available Date'] = df_merged_with_assets['Available Date'].map \
        (lambda x: x.strftime(ProjectCollections.dashboard_datetime_format) if pd.notna(x) else "")
    df_merged_with_assets[f'{keyword} Date'] = df_merged_with_assets[f'{keyword} Date'].map \
        (lambda x: x.strftime(ProjectCollections.dashboard_datetime_format) if pd.notna(x) else "")

    df_merged_with_assets.drop(['is_alarm', 'occurrence_counter', 'occurrence_duration',
                                "occurrence_last_time"], axis=1, inplace=True)

    df_merged_with_assets = df_merged_with_assets[["Functional Area",
                                                   "Full Description",
                                                   f"{keyword} Date",
                                                   "Available Date",
                                                   f"Days {keyword}",
                                                   "tag_short_description"]]
    return df_merged_with_assets


def get_site_throughput_exceptions_nos(datetime: pd.Timestamp, plant: str) -> int:
    """
    Count the flow throughput exceptions by:
    - selecting all tags in the "flow" category
    - finding all their ongoing alarms
    - counting those without "ok" in their value

    Args:
        datetime (pd.Timestamp): current datetime
        plant (str): name of the plant

    Returns:
        count of the alarms

    TODO: refactor, might be combined with the nav_header risk calculation
    """

    # get list of all relevant tags
    site_tag_conf = ProjectCollections.site_tag_config
    site_df = site_tag_conf[site_tag_conf['site_name'] == plant]
    f_tags = list(site_df.loc[(site_df['tag_category'] == 'Flow')
                              & (site_df['tag_subcategory'] == 'Site Throughput')
                              & (site_df['measurement_unit'] != 'Text'), 'tag_name'].unique())
    sf_tags = list(site_df.loc[(site_df['tag_category'] == 'Flow')
                               & (site_df['tag_subcategory'] == 'Sample Flow')
                               & (site_df['measurement_unit'] != 'Text'), 'tag_name'].unique())

    # query the database for the currently ongoing events
    query = {'occurrence_start_time': {'$lt': datetime, '$gt': datetime - pd.Timedelta(days=10)},
             '$or':                   [{'occurrence_end_time': {'$type': 10}},  # type 10 is type null
                                       {'occurrence_end_time': {'$gte': datetime}}],
             'tag_name':              {'$in': f_tags}
             }
    projection = {'_id': 0, 'tag_name': 1, 'measurement_value': 1}

    cursor = ProjectCollections.fast_events_coll.find(query, projection)
    alarm_df = pd.DataFrame(list(cursor))

    try:
        f_counts = alarm_df['measurement_value'].map(lambda x: 'ok' not in x.lower()).sum()
    except KeyError:
        f_counts = 0

    # sample flow calculation
    try:
        sub_df = alarm_df[alarm_df['tag_name'].map(lambda x: x in sf_tags)]
        sf_counts = sub_df['measurement_value'].map(lambda x: 'ok' not in x.lower()).sum()
    except KeyError:
        sf_counts = 0

    return f_counts, sf_counts
