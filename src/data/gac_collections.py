"""
Copyright HAL24K 2020
Utility functions for dataframes

GAC collections object, stores gac relevant options and data

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""

from data.collections_object import ProjectCollections
from data.rgf_gac_utils import get_individual_tag_df, filter_description, get_greatest_common_str
import pandas as pd
import numpy as np
import logging

logger = logging.getLogger('gac_collections')


class GACCollections():
    DECIM_S = 2
    DECIM_L = 3
    site_tag_config = ProjectCollections.site_tag_config

    sites = ['GOODW', 'HUBY', 'ELVINGTON']

    col_renames = {
            'GOODW':     {
                    'Service Status':          'Service Status',
                    'Backwash Step No':        'BW StepNo',
                    'Level':                   'Level',
                    'Outlet Flow':             'Outlet Flow',
                    'Flow Status':             'Flow Status',
                    'Hours Filtering':         'Filtering Hours',
                    'Excess Hours':            'Excess Time(Mins)',
                    'Hours In Backwash Queue': 'BW Queue Hrs',
                    'Bw Excess Queue Stat':    'BW Excess Queue Hrs',
                    'Bw Fail Step':            'BW Fail Step',
                    'Pressure Diff':           'Pressure Diff',
                    'Pressure':                'Pressure Status',
                    'Headloss Status':         'Headloss',
                    'Outlet Valve Pos':        'Outlet Valve Pos'

            },
            'HUBY':      {
                    "Service Status":      "Service Status",
                    'BW StepNo':           'BW StepNo',
                    'Level Si':            'Level',
                    'Outlet Flow':         'Outlet Flow',
                    'Flow Fault Status':   'Flow Status',
                    'Service Hours':       'Filtering Hours',
                    'Underbed Press Stat': 'Pressure Diff',
                    'Diff Press Stat':     'Pressure Status',
                    'Outlet Valve Pos':    "Outlet Valve Pos",

            },
            'ELVINGTON': {
                    'Inservicestatus':      'Service Status',
                    'Adsorber  Bwash Step': 'BW StepNo',
                    'Level (%)':            'Level',
                    'O/L Flow':             'Outlet Flow',
                    'Inservicehrs':         'Filtering Hours',
                    'Pressure High':        'Pressure Status',
                    'Headloss(M)':          'Headloss',
                    'O/L Flow Total':       'Total Flow Since Regen',
                    'Bed Volumes Treated':  'Bed Volumes Treated',

            }

    }

    OOS_VALS = {
            'GOODW':     ['0', 0, 16, '16'],
            'HUBY':      ['OUT OF SERVICE'],  # huby gets the values from postprocess_df for the table
            'ELVINGTON': ["OFF"]
    }

    individual_regex_acomb = [
            "(.*RGF_DW_GAC)[1-9]+_BWSTEPNUM$",
            "(.*RGF_DW_GAC)[1-9]+TIFWBWS$",
            "(.*RGF_DW_GAC)[1-9]+_EXCESSTIME$",
            "(.*RGF_DW_V07)[1-9]+05POS$",
            "(.*RGF_A_GAC)[1-9]+TFT$",
            "(.*RGF_A_GAC)[1-9]+EXTBQ$",
            "(.*RGF_DW_GAC)[1-9]+TIQWBWS$",
            "(.*RGF_DW_GAC)[1-9]+_BWFAILSTEP$",
            "(.*RGF_A_PS07)[1-9]+04H$",
            "(.*RGF_A_GAC)[1-9]+BWFA$",
            "(.*RGF_DW_FT07)[1-9]+03SCD$",
            "(.*RGF_A_GAC)[1-9]+FLOWDEV$",
            "(.*RGF_A_GAC)[1-9]+LOWFLOWA$",
            "(.*RGF_DW_LT07)[1-9]+01SCD$",
            '(.*RGF_DW_PT07)[1-9]+02SCD$',
            "(.*RGF_A_GAC)[1-9]+_HDLSS_HT$",

    ]

    # missing service status
    individual_regex_huby = ["(.*GAC)[A-Z]_UNDERBED_PRESSURE_HIGH$",
                             "(.*GAC)[A-Z]_HI_DP$",
                             "(.*GAC)[A-Z]_LEVEL_SI$",
                             '(.*GAC)[A-Z]_OL_VLV_POS_SI$',
                             "(.*GAC)[A-Z]_FLOW_FAULT$",
                             "(.*GAC)[A-Z]_AUTO$",
                             # '(.*GAC)[1-9]+_BWSTEPNUM'
                             '(.*GAC)[A-Z]_WASHING$',
                             '(.*GAC)[A-Z]_QUEUED$',
                             "(.*GAC)[A-Z]_SERVICE_TIME_SI$",
                             "(.*GAC)[A-Z]_OL_FLOW_SI$",
                             ]

    # elvington has a tough regex on tag_name, use category instead...
    # NOTE: have to skip doing the regex later when making the individual DF
    individual_tags_elvington = {
            'service status':     site_tag_config.loc[
                                      (site_tag_config.tag_description.str.contains(
                                          'GAC Adsorber (?!00)\d+ InServiceStatus$',
                                          regex=True))].sort_values(
                    'tag_name'),
            'bw stepno':          site_tag_config.loc[
                                      (site_tag_config.tag_description.str.contains(
                                              'Adsorber (?!00)\d+ Backwash Step$', regex=True))].sort_values(
                    'tag_name'),
            'level':              site_tag_config.loc[
                                      (site_tag_config.tag_description.str.contains(
                                              'Adsorber (?!00)\d+ Level\(\%\)$', regex=True))].sort_values(
                    'tag_name'),
            'flow':               site_tag_config.loc[
                                      (site_tag_config.tag_description.str.contains(
                                              'GAC Adsorber (?!00)\d+ O/L Flow$', regex=True))].sort_values(
                    'tag_name'),

            'filtering_hours':    site_tag_config.loc[
                                      (site_tag_config.tag_description.str.contains(
                                              'GAC Adsorber (?!00)\d+ InServiceHrs$', regex=True))].sort_values(
                    'tag_name'),

            'pressure_high_stat': site_tag_config.loc[
                                      (site_tag_config.tag_description.str.contains(
                                              'GAC Adsorber (?!00)\d+ Pressure High$', regex=True))].sort_values(
                    'tag_name'),
            'headloss':           site_tag_config.loc[
                                      (site_tag_config.tag_description.str.contains(
                                              'GAC Adsorber (?!00)\d+ Headloss\(m\)$'))].sort_values(
                    'tag_name'),
            'flow_total':         site_tag_config.loc[
                                      (site_tag_config.tag_description.str.contains(
                                              'GAC Adsorber (?!00)\d+ O/L Flow Total$', regex=True))].sort_values(
                    'tag_name'),

    }

    ## GROUP STATUS TAGS
    bw_status = {
            'GOODW':     site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                                 .contains("(.*RGF_DW_GAC)[1-9]+_BWSTEPNUM$", regex=True),
                                             "tag_name"].tolist(),
            'HUBY':      "HUBYWTS1:GAC_CURRENT_WASHING",
            'ELVINGTON': individual_tags_elvington['bw stepno']['tag_name'].tolist()
    }

    bw_due_tags = {
            'GOODW':     site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                                 .contains("(.*RGF_DW_GAC)[1-9]+_EXCESSTIME$", regex=True),
                                             "tag_name"].tolist(),
            'HUBY':      site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                                 .contains("(.*GAC).+_SERVICE_TIME_SI$", regex=True),
                                             "tag_name"].tolist(),
            'ELVINGTON': individual_tags_elvington['filtering_hours']['tag_name'].tolist()

    }

    bw_req_tags = {
            'GOODW':     site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                                 .contains("(.*RGF_A_GAC)[1-9]+_HDLSS_HT$", regex=True),
                                             "tag_name"].tolist(),
            'ELVINGTON': individual_tags_elvington['headloss']['tag_name'].tolist()
    }

    # GROUP PERFORMANCE TAGS + stream tags
    uv254_tags = {}

    outlet_flow_tags = {
            # ACOMLDS1: CHEM_DW_FT07X03SUMMLD
            'GOODW':     'GOODWDA:CHEM_DA_FT_07X03SUMMLD',
            # not req for huby yet the tag is..
            'HUBY':      'HUBYWTS1:COMBINED_GAC_FLOW_SI',
            'ELVINGTON': 'ELVGTNS1:GR_GAC_FLOW'
    }

    bedcond_tags = {}

    inlet_channel_tags_a = {
            # RGF_DW_LT07001SCD
            'ELVINGTON': 'ELVGTNS1:EOZ022',
            'GOODW':     "GOODWDA:RGFF_DA_LV_07001_SCD_1",
            "HUBY":      "HUBYWTS1:GAC_INLET_CHANNELREMOTE_SP_SI"

    }

    # na for huby or acomb
    inlet_channel_tags_b = {

    }

    turbidity_tags_a = {
            # CHEM_DW_AT39101SCD
            'ELVINGTON': 'ELVGTNS1:REG26_DW_EOZ133SCD',
            'GOODW':     'GOODWDA:CHEM_DA_AT_39101_SCD_1',
            'HUBY':      "HUBYWTS1:GAC_DW_QIT890SCD"
    }

    # na for huby, acomb
    turbidity_tags_b = {
    }

    # tbd huby and acomb
    backwash_pressure_tags_a = {

    }

    # na for acomb/huby
    backwash_pressure_tags_b = {

    }

    backwash_flow = {
            "HUBY":      "HUBYWTS1:GAC_BACKWASH_FLOW_SI",
            'ELVINGTON': 'ELVGTNS1:D_FTC200FLW'

    }

    # INDIVIDUAL UNIT TAGS

    oos_tags = {
            # RGF_A_GAC1OUTSER
            # 'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
            #                                  .contains("(.*RGF_A_GAC)[1-9]+OUTSER", regex=True),
            #                              "tag_name"].tolist(),

            # use stepnum for acomb
            'GOODW':     site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                                 .contains("(.*RGF_DW_GAC)[1-9]+_BWSTEPNUM$", regex=True),
                                             "tag_name"].sort_values().tolist(),
            "HUBY":      site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                                 .contains("(.*GAC)[A-Z]_AUTO$", regex=True),
                                             "tag_name"].sort_values().tolist(),

            'ELVINGTON': individual_tags_elvington['service status']['tag_name'].tolist()

    }

    time_in_filt_tags = {
            # RGF_DW_GAC1TIFWBWS
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_DW_GAC)[1-9]+TIFWBWS$", regex=True),
                                         "tag_name"].tolist(),

            "HUBY":  site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*GAC).+_SERVICE_TIME_SI$", regex=True),
                                         "tag_name"].tolist(),
            # 'HUBYWTS1:GACA_SERVICE_TIME_SI'
    }
    excess_h_in_filt_tags = {
            # RGF_DW_GAC1_EXCESSTIME
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_DW_GAC)[1-9]+_EXCESSTIME$", regex=True),
                                         "tag_name"].tolist(),

    }
    excess_time_in_filt_status = {
            # ACOMLDS1:RGF_A_GAC1TFT
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_A_GAC)[1-9]+TFT$", regex=True),
                                         "tag_name"].tolist(),

    }

    excess_time_in_backwash_queue_status = {
            # RGF_DW_GAC1TIQWBWS
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_A_GAC)[1-9]+EXTBQ$", regex=True),
                                         "tag_name"].tolist(),

    }

    backwash_failstep_num = {
            # RGF_DW_GAC1_BWFAILSTEP
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_DW_GAC)[1-9]+_BWFAILSTEP$", regex=True),
                                         "tag_name"].tolist(),

    }

    backwash_status = {
            # RGF_A_GAC1BWFA
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_A_GAC)[1-9]+BWFA$", regex=True),
                                         "tag_name"].tolist(),

    }

    flow = {
            # RGF_DW_FT07103SCD
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_DW_FT07)[1-9]+03SCD$", regex=True),
                                         "tag_name"].tolist(),
            "HUBY":  site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*GAC).+_OL_FLOW_SI$", regex=True),
                                         "tag_name"].tolist()

    }

    flow_deviation = {
            # RGF_A_GAC1FLOWDEV
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_A_GAC)[1-9]+FLOWDEV$", regex=True),
                                         "tag_name"].tolist(),

    }

    flow_status = {
            # RGF_A_GAC1LOWFLOWA
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_A_GAC)[1-9]+LOWFLOWA$", regex=True),
                                         "tag_name"].tolist(),

            'HUBY':  site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*GAC).+_FLOW_FAULT$", regex=True),
                                         "tag_name"].tolist(),
            # "HUBYWTS1:GACA_FLOW_FAULT",
    }

    backwash_step_num = {
            # RGF_DW_RGF1_BWSTEPNUM
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_DW_GAC)[1-9]+_BWSTEPNUM$", regex=True),
                                         "tag_name"].tolist(),

    }

    level = {
            # RGF_DW_LT07101SCD
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_DW_LT07)[1-9]+01SCD$", regex=True),
                                         "tag_name"].tolist(),
            "HUBY":  site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*GAC).+_LEVEL_SI$", regex=True),
                                         "tag_name"].tolist(),
            # 'HUBYWTS1:GACA_LEVEL_SI'

    }

    pressure_diff = {
            # RGF_DW_PT07102SCD
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_DW_PT07)[1-9]+02SCD$", regex=True),
                                         "tag_name"].tolist(),

    }

    presure_diff_status = {
            'HUBY': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                            .contains("(.*GAC)._HI_DP$", regex=True),
                                        "tag_name"].tolist(),
            # 'HUBYWTS1:GACA_HI_DP'
    }

    head_loss = {
            # RGF_A_GAC1_HDLSS_HT
            'GOODW': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                             .contains("(.*RGF_A_GAC)[1-9]+_HDLSS_HT$", regex=True),
                                         "tag_name"].tolist(),

    }
    underbed_pressure = {
            'HUBY': site_tag_config.loc[site_tag_config.tag_name.str.upper().str \
                                            .contains("(.*GAC)._UNDERBED_PRESSURE_HIGH$", regex=True),
                                        "tag_name"].tolist(),
            # "HUBYWTS1:GACA_UNDERBED_PRESSURE_HIGH"
    }

    pflow_tags = {}

    pcounter_tags = {}

    individual_tag_df_elv = []
    for cat, tag_df in individual_tags_elvington.items():

        # most_common_str = tag_df['tag_name']
        if len(tag_df) == 0:
            logger.warning('individual_tag_df_elvington, couldnt find tags for {}'.format(cat))
            continue
        tag_df['GAC'] = np.arange(1, len(tag_df) + 1)
        short_descriptions = tag_df['tag_short_description'].values

        tag_df['tag_short_description_old'] = tag_df['tag_short_description']

        common_strs = tag_df['tag_short_description'].apply(lambda x: filter_description(x)).values

        common_str = get_greatest_common_str(common_strs)
        tag_df['tag_short_description'] = common_str

        tag_df = tag_df[
            ['GAC', "tag_name", "tag_short_description_old", "site_name", "tag_short_description", "is_alarm"]]
        individual_tag_df_elv.append(tag_df)
    individual_tag_df_elv = pd.concat(individual_tag_df_elv)
    # only do GOODW and HUBY like this, Elvington already done manually above
    individual_tag_df = get_individual_tag_df(['GOODW', 'HUBY'],
                                              [individual_regex_acomb, individual_regex_huby],
                                              asset='GAC')
    individual_tag_df = pd.concat([individual_tag_df, individual_tag_df_elv])

    num_gacs = individual_tag_df.groupby(['GAC', 'site_name']).sum().reset_index()
    num_gacs['count'] = 1
    num_gacs = num_gacs.groupby("site_name")['count'].sum()
