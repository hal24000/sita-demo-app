"""
water quality exceptions page
Copyright 2020 HAL24K
===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from visualization.app_def import app
from data.collections_object import ProjectCollections as prj_collections
from visualization.plot_drawing.plotly_plots import draw_bar_chart, \
    draw_pie_chart
from data.generate_data import generate_time_series
from visualization.pages import water_quality_filters
import plotly.express as px
import pandas as pd
import logging
from data.project_loggers import dash_logger

from data.pandas_cacher import BrainHolder

logger = logging.getLogger(dash_logger)

time_series = generate_time_series(no_devices=1, frequency='15T')

wq_store_reader = BrainHolder(client_brains=['wq-exc-barplot'],
                              namespace="water-quality")
ts_args = {
        'draw_boundaries': {col: [time_series[col].min(), time_series[col].max()] for col in time_series.columns}
}

exceptions_content = html.Div(
        className="water-quality__exceptions",
        children=[
                dcc.Store(id='wq-exc-lookbackstore', storage_type="memory", data=0),
                html.Div(
                        className="water-quality__exceptions--g1 w-8 bg-white mr-16 h-with-filter-1-row-of-1",
                        children=[
                                html.Div(
                                        className="water-quality__exceptions--title title",
                                        children=["Water quality exceptions by metric"],
                                ),
                                html.Div(
                                        className="water-quality__exceptions--sub-title",
                                        id="wq-exc-barplot-subtitle",
                                        children=[""]
                                ),
                                html.Div(
                                        className="water-quality__exceptions--body h-with-filter-1-row-of-1__body",
                                        children=[
                                                dcc.Loading(
                                                        id='loading-wq-exc-barplot',
                                                        type=prj_collections.loading_animation,
                                                        children=[
                                                                dcc.Graph(id="wq-exc-barplot",
                                                                          config={'displayModeBar': False})
                                                                # , figure=wq_exc_barplot)
                                                        ]
                                                ),
                                        ],
                                )
                        ],
                ),
                html.Div(
                        className="water-quality__exceptions--g2 w-4 bg-white h-with-filter-1-row-of-1",
                        children=[
                                html.Div(
                                        className="water-quality__exceptions--title",
                                        children=["Water quality exceptions by type"],
                                ),
                                html.Div(
                                        className="water-quality__exceptions--sub-title",
                                        id="wq-exc-pieplot-subtitle",
                                        children=[""]
                                ),
                                html.Div(
                                        className="water-quality__exceptions--body h-with-filter-1-row-of-1__body",
                                        children=[
                                                dcc.Loading(
                                                        id='loading-wq-exc-pieplot',
                                                        type=prj_collections.loading_animation,
                                                        children=[
                                                                dcc.Graph(id="wq-exc-pieplot",
                                                                          config={'displayModeBar': False})
                                                                # , figure=wq_exc_pie_fig)
                                                        ]
                                                ),
                                        ],
                                )
                        ],
                ),
        ],
)

content = html.Div(
        className="water-quality-page__body",
        children=[
                # Row 1: Filters and other controls
                water_quality_filters.filters_content,
                # Row 2: Graph Contents
                html.Div(
                        className="water-quality__graphs",
                        id="wq-graph-contents-exc",
                        children=exceptions_content
                ),
        ],
)


@app.callback(
        Output(component_id='wq-exc-barplot-subtitle', component_property='children'),
        [Input(component_id='nav-history-days', component_property='data')]
)
def update_exc_barplot_title(lookback_days: int):
    if lookback_days is None:
        return ''
    else:
        return 'Lookback period: {} days'.format(lookback_days)


@app.callback(
        Output(component_id='wq-exc-pieplot-subtitle', component_property='children'),
        [Input(component_id='nav-history-days', component_property='data')]
)
def update_exc_pieplot_title(lookback_days):
    if lookback_days is None:
        return ''
    else:
        return 'Lookback period: {} days'.format(lookback_days)


@app.callback(output=[Output("wq-exc-barplot", "figure"),
                      Output("wq-exc-pieplot", "figure")],
              inputs=[Input("wq-amber-tablestore", "data"),
                      Input("wq-dd-metrics", "value")])
def update_exception_charts(input_data, chosen_metrics):
    """
    Draw the charts with the metrics.
    """
    input_df = wq_store_reader.get('wq-exc-barplot', input_data)
    if input_df.empty:
        # if empty before filtering
        return px.bar(), px.pie()

    input_df = input_df[input_df['tag_name'].map(lambda x: x in chosen_metrics)]
    if input_df.empty:
        # empty after filtering
        return px.bar(), px.pie()

    input_df.rename(columns={'low_amber':  'Falling',
                             'high_amber': 'Rising'}, inplace=True)

    updated_barplot = draw_bar_chart(input_df, x='metric', y=['Falling', 'Rising'],
                                     args=dict(color_discrete_map={'Falling': 'blue',
                                                                   'Rising':  'orange'},
                                               labels={'metric': 'Metric'},
                                               height=603)
                                     )

    updated_barplot.update_layout(legend={'traceorder': 'reversed'},
                                  legend_title_text='High ROC Type')
    updated_barplot.update_yaxes(title_text='Counts')
    updated_barplot.update_traces(
            hovertemplate='High ROC Type: Falling<br>Metric: %{x}<br>Occurences: %{y}<extra></extra>',
            selector={'name': 'Falling'})
    updated_barplot.update_traces(
            hovertemplate='High ROC Type: Rising<br>Metric: %{x}<br>Occurences: %{y}<extra></extra>',
            selector={'name': 'Rising'})

    cumulative_ambers = pd.DataFrame(input_df[['Falling', 'Rising']].sum()).reset_index()
    cumulative_ambers.columns = ['Issue Type', 'Issue Count']

    updated_pie_chart = draw_pie_chart(cumulative_ambers, x='Issue Type', y='Issue Count',
                                       args=dict(color=['Falling', 'Rising'],
                                                 color_discrete_map={'Falling': 'blue',
                                                                     'Rising':  'orange'},
                                                 hole=0.))
    updated_pie_chart.update_layout(legend_title_text='High ROC Type',
                                    margin=dict(l=0, r=0, t=50, b=0, pad=0))

    updated_pie_chart.update_traces(
            hovertemplate='High ROC Type: %{label}<br>Occurences: %{value}<extra></extra>'
    )

    return updated_barplot, updated_pie_chart
