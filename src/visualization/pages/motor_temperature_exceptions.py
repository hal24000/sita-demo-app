"""
Motor over temperature exceptions page (8.6)
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from datetime import datetime
from visualization.app_def import app
from data.summary_utils import df_utc_to_london
from data.collections_object import ProjectCollections
from visualization.plot_drawing.plotly_plots import make_table, draw_bar_chart, make_proper_datetime_formatting
import numpy as np
import pandas as pd
import plotly.graph_objects as go

content = html.Div(
        className="motor-temperature-exceptions__page",
        children=[
                # First Row: Filter
                html.Div(
                        className="motor-temperature-exceptions__r1 w-12 h-filter mb-16",
                        children=[
                                html.Div(
                                        className="motor-temperature-exceptions__r1--filter h-filter w-12 p-4 bg-white",
                                        children=[
                                                html.Div(
                                                        className="motor-temperature-exceptions__title title",
                                                        children=["Functional area"],
                                                ),
                                                html.Div(
                                                        className="motor-temperature-exceptions__body",
                                                        children=[
                                                                dcc.Dropdown(
                                                                        id="motor-ot-fa-dd",
                                                                        className="motor-temperature-exceptions__r1--functional-filter",
                                                                        multi=True
                                                                ),
                                                        ],
                                                )
                                        ],
                                ),
                        ],
                ),
                dcc.Loading(
                        id='loading-motor-ot-table-store',
                        type=ProjectCollections.loading_animation,
                        children=[
                                dcc.Store(id='motor-ot-table-store'),
                        ]
                ),
                # Second Row:
                html.Div(
                        className="motor-temperature-exceptions__r2 h-with-filter-1-row-of-2 w-12 bg-white mb-16",
                        children=[
                                html.Div(
                                        className="motor-temperature-exceptions__title title m-4",
                                        children=["Equipment over temperature"],
                                ),
                                html.Div(
                                        className="motor-temperature-exceptions__body",
                                        children=[
                                                dcc.Loading(
                                                        id='loading-motor-ot-datatable-div',
                                                        type=ProjectCollections.loading_animation,
                                                        children=[
                                                                html.Div(
                                                                        className="motor-temperature-exceptions__r2--body",
                                                                        id='motor-ot-datatable-div',
                                                                )]),
                                        ],
                                ),
                        ],
                ),
                # Third Row:
                html.Div(
                        className="motor-temperature-exceptions__r3 h-with-filter-1-row-of-2 w-12 bg-white",
                        children=[
                                html.Div(
                                        className="motor-temperature-exceptions__title title m-4",
                                        children=["Alarms"],
                                ),
                                html.Div(
                                        className="motor-temperature-exceptions__body",
                                        children=[
                                                html.Div(
                                                        className="motor-temperature-exceptions__r3--body",
                                                        children=[
                                                                dcc.Loading(
                                                                        id='loading-motor-ot-table-barchart',
                                                                        type=ProjectCollections.loading_animation,
                                                                        children=[
                                                                                dcc.Graph(id='motor-ot-table-barchart')
                                                                        ]),
                                                        ]
                                                ),
                                        ],
                                ),
                        ],
                ),
        ]
)


@app.callback(output=[Output('motor-ot-fa-dd', 'options'),
                      Output('motor-ot-fa-dd', 'value')],
              inputs=[Input('nav-selected-installation', 'data')])
def update_motor_ot_fa_dd(selected_installation):
    conf = ProjectCollections.site_tag_config
    fas = list(conf.loc[conf.site_name == selected_installation, 'functional_area'].unique())

    fas = np.sort(fas)
    options = [
            {'label': val, 'value': val} for val in fas
    ]
    return options, fas


@app.callback(inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data'),
                      Input('motor-ot-fa-dd', 'value')],
              state=[State("nav-history-days", "data")],
              output=[Output('motor-ot-table-barchart', 'figure'),
                      Output('motor-ot-table-store', 'data')])
def update_motor_ot_devices(site, reference_time, functional_areas, lookback_time):
    ot_tags_df = ProjectCollections.motor_ot_tags_df
    ot_tags = list(ot_tags_df.loc[(ot_tags_df.site_name == site) &
                                  (ot_tags_df.functional_area.isin(functional_areas)), 'tag_name'])
    site_tag_config = ProjectCollections.site_tag_config
    # Mongo query
    # reference_time = pd.to_datetime(reference_time)
    reference_time = datetime.strptime(reference_time, ProjectCollections.dashboard_datetime_format)

    regex = ".*OVR TEMP$"
    values = ['MOTOR OVR TEMP', 'MOTOR OVER TEMP', "OVER TEMP", 'OVR TEMP']
    base_query = {
            "tag_name":          {"$in": ot_tags},
            "measurement_value": {"$in": values},
    }

    data_collection = ProjectCollections.get_mongo_collection(lookback=lookback_time,
                                                              collection='alarms')

    time_delta = pd.Timedelta(f'{lookback_time}d')
    lookback_time = reference_time - time_delta
    base_query.update(
            {
                    "measurement_timestamp": {
                            "$gte": lookback_time,
                            "$lte": reference_time
                    }
            }
    )
    res = pd.DataFrame(list(data_collection.find(base_query, {'_id': 0, 'fabricated_key': 0})))

    if len(res) == 0:
        return go.Figure(layout={'title': 'No data found for specified input combination'}), []

    unique_tags = res.tag_name.unique()

    unique_devices = site_tag_config.loc[site_tag_config.tag_name.isin(unique_tags), ['tag_name',
                                                                                      'tag_short_description',
                                                                                      'common_reference']]

    res = pd.merge(res, unique_devices, how='left', on='tag_name')

    # res_temp = res.groupby('common_reference')
    res_counts = res.groupby('tag_short_description')['low'].count().reset_index()
    res_counts.rename({'low': 'num alarms'}, axis=1, inplace=True)

    barv = draw_bar_chart(df=res_counts, x='tag_short_description',
                          y='num alarms')

    # table = make_table(res,)
    res = df_utc_to_london(res)
    res = make_proper_datetime_formatting(res)

    return barv, res.to_dict('records')


@app.callback(
        output=Output('motor-ot-datatable', 'data'),
        inputs=[Input('motor-ot-datatable', "page_current"),
                Input('motor-ot-datatable', "page_size")],
        state=[State('motor-ot-table-store', 'data')])
def motor_ot_navigate_readings_table(page_current, page_size, current_data):
    return current_data[
           page_current * page_size:(page_current + 1) * page_size
           ]


@app.callback(output=Output('motor-ot-datatable-div', 'children'),
              inputs=[Input('motor-ot-table-store', 'data')])
def update_motor_ot_datatable(data):
    data = pd.DataFrame(data)
    table = make_table(df=data,
                       page_size=6,
                       id='motor-ot-datatable',
                       title=None)
    return table
