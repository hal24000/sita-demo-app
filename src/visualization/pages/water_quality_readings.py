"""
water quality readings, section 1.11 from H1.2 document
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===
"""


import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from visualization.app_def import app
from visualization.plot_drawing.plotly_plots import make_table
from data.collections_object import ProjectCollections as prj_collections
import numpy as np
from visualization.pages import water_quality_filters
import plotly.express as px
import plotly.graph_objects as go
from data.pandas_cacher import BrainHolder
import pandas as pd

# TODO: unhardcode colours
DEFAULT_COLORS=['rgb(31, 119, 180)', 'rgb(255, 127, 14)',
                'rgb(44, 160, 44)', 'rgb(214, 39, 40)',
                'rgb(148, 103, 189)', 'rgb(140, 86, 75)',
                'rgb(227, 119, 194)', 'rgb(127, 127, 127)',
                'rgb(188, 189, 34)', 'rgb(23, 190, 207)']

wq_store_reader = BrainHolder(client_brains=['wq-ts-graph',
                                       'wq-readings-table','wq-readings-dash-table'],
                              namespace="water-quality")



readings_content = html.Div(
    className="water-quality__readings",
    children=[
        html.Div(
            className="water-quality__readings--g1 w-12 bg-white mb-16 h-with-filter-1-row-of-2",
            children=[
                html.Div(
                        className="water-quality__readings--title title",
                        children=["Water quality plot"],
                ),
                html.Div(
                    className="water-quality__readings--sub-title",
                    id="wq-readings-graph-subtitle",
                    children=[""]
                ),
                html.Div(
                    className="water-quality__readings--body h-with-filter-1-row-of-2__body",
                    children=[
                        dcc.Loading(
                            id='loading-wq-ts-graph',
                            type=prj_collections.loading_animation,
                            children=[
                                dcc.Graph(id="wq-ts-graph", className="water-quality__readings--graph h-with-filter-1-row-of-2__body")
                            ]
                        ),
                    ],
                )
            ],
        ),
        html.Div(
            className="water-quality__readings--g2 w-12 bg-white h-with-filter-1-row-of-2",
            children=[
                html.Div(
                        className="water-quality__readings--title",
                        children=["Water quality readings"],
                ),
                html.Div(
                    className="water-quality__readings--sub-title",
                    id="wq-readings-table-subtitle",
                    children=[""]
                ),
                html.Div(
                    className="h-with-filter-1-row-of-2__body",
                    children=[
                        dcc.Loading(
                            id='loading-wq-readings-table',
                            type=prj_collections.loading_animation,
                            children=[
                                html.Div(id='wq-readings-table', className="water-quality__readings--graph h-with-filter-1-row-of-2__body")
                            ]
                        ),
                    ],
                )
            ],
        ),
    ],
)


content = html.Div(
    className="water-quality-page__body",
    children=[
        # Row 1: Filters and other controls
        water_quality_filters.filters_content,
        # Row 2: Graph Contents
        html.Div(
            className="water-quality__graphs",
            id="wq-graph-contents-readings",
            children=readings_content
        ),
    ],
)

@app.callback(
    Output(component_id='wq-readings-graph-subtitle', component_property='children'),
    [Input(component_id='nav-history-days', component_property='data')]
)
def update_readings_graph_title(lookback_days: int):
    if lookback_days is None:
        return ''
    else:
        return 'Lookback period: {} days'.format(lookback_days)

@app.callback(
    Output(component_id='wq-readings-table-subtitle', component_property='children'),
    [Input(component_id='nav-history-days', component_property='data')]
)
def update_readings_table_title(lookback_days):
    if lookback_days is None:
        return ''
    else:
        return 'Lookback period: {} days'.format(lookback_days)

@app.callback(output=Output("wq-ts-graph", "figure"),
              inputs=[Input("wq-readings-tablestore", "data"),
                      Input("wq-dd-metrics", "value"),
                      Input("nav-plant-dd", "value")])
def update_readings_chart(input_data, chosen_metrics, location):
    data  = wq_store_reader.get('wq-ts-graph',input_data)

    if data.empty:
        return px.line()

    data['datetime'] = pd.to_datetime(data['datetime'], format=prj_collections.dashboard_datetime_format)

    fig = go.Figure()
    color_index = 0

    for metric in chosen_metrics:
        color = DEFAULT_COLORS[color_index]

        # create limits
        metric_df = data[data['tag_name'] == metric].copy()

        if metric_df.empty:
            continue

        metric_df.sort_values(by='datetime', inplace=True)
        metric_df.reset_index(inplace=True, drop=True)

        metric_df['prev_reading'] = metric_df['prev_reading'].ffill()
        metric_df['reading'] = metric_df['reading'].ffill()

        # add the single future measurement
        # TODO: parametrize aggregation period
        future_dict = metric_df.iloc[-1].to_dict()
        future_dict["datetime"] = future_dict["datetime"] + pd.Timedelta(minutes=15)
        future_dict["prev_reading"] = future_dict["reading"]
        future_dict["reading"] = np.nan
        future_dict["difference"] = np.nan

        metric_df.loc[len(metric_df)] = future_dict

        if metric_df.empty:
            # TODO: alert about missing data
            continue

        lower_limit = metric_df['cutoff_drop'].unique()[0]
        upper_limit = metric_df['cutoff_rise'].unique()[0]

        if np.isfinite(lower_limit) and np.isfinite(upper_limit):
            # create an area
            metric_df['lower_limit'] = metric_df['prev_reading'] + metric_df['cutoff_drop']
            metric_df['upper_limit'] = metric_df['prev_reading'] + metric_df['cutoff_rise']

            fig.add_trace(go.Scatter(x=(metric_df['datetime'].tolist()
                                        + [metric_df['datetime'].tolist()[-1] + pd.Timedelta(minutes=15)] * 2
                                        + metric_df['datetime'].tolist()[::-1]),
                                     y=np.concatenate([metric_df['upper_limit'],
                                                       np.array([metric_df['upper_limit'].iloc[-1] + upper_limit]),
                                                       np.array([metric_df['lower_limit'].iloc[-1] + lower_limit]),
                                                       metric_df['lower_limit'].iloc[::-1]]),
                                     name='', showlegend=False, fill='toself', hoverinfo="none",
                                     legendgroup=metric_df['metric'].unique()[0],
                                     line=dict(color=color.replace('rgb', 'rgba').replace(')',',0.5)'))))
        elif np.isfinite(upper_limit):
            # create filled area
            metric_df['upper_limit'] = metric_df['prev_reading'] + metric_df['cutoff_rise']
            fig.add_trace(go.Scatter(x=metric_df['datetime'].tolist()
                                       + [metric_df['datetime'].tolist()[-1] + pd.Timedelta(minutes=15)],
                                     y=metric_df['upper_limit'].tolist() + [metric_df['upper_limit'].tolist()[-1]
                                                                            + upper_limit],
                                     mode='none', fill='tozeroy', name='', showlegend=False,
                                     legendgroup=metric_df['metric'].unique()[0],
                                     hoverinfo="none",
                                     fillcolor=color.replace('rgb', 'rgba').replace(')',',0.5)')))
        else:
            # TODO: implement lower limit only
            continue

        fig.add_trace(go.Scatter(x=metric_df['datetime'], y=metric_df['reading'],
                                 mode='lines', line_color=color,
                                 name=metric_df['metric'].unique()[0],
                                 legendgroup=metric_df['metric'].unique()[0],
                                 showlegend=True))

        # go to the next colour and roll over if needed
        color_index += 1
        color_index = color_index % len(DEFAULT_COLORS)

    fig.update_layout(
        margin = dict(l=0, r=0, t=20, b=0, pad=0),
        hovermode='x',
        legend_title_text='WQ metrics at {}'.format(location.lower().capitalize()),
        xaxis_tickformat='%d %b %Y %H:%M'
    )
    return fig


@app.callback(output=Output("wq-readings-table","children"),
              inputs=[Input("wq-readings-tablestore", "data"),
                      Input("wq-dd-metrics", "value")])
def create_wq_readings_data_table(input_data, metrics):
    internal_data  = wq_store_reader.get('wq-readings-table',input_data)

    if internal_data.empty:
        return list()

    internal_data['datetime'] = pd.to_datetime(internal_data['datetime'],
                                               format=prj_collections.dashboard_datetime_format)
    internal_data = internal_data[internal_data['tag_name'].map(lambda x: x in metrics)]
    internal_data = internal_data[['datetime', 'metric', 'reading', 'prev_reading', 'difference',
                 'nominal_ROC', 'status', 'periods_of_unchanged_status']]

    table_readings = make_table(internal_data,6,"wq-readings-dash-table","WQ Reading Data",
                                numeric_columns=["reading", "prev_reading", "difference", "nominal_ROC"],
                                decimals_displayed=3)

    return table_readings


@app.callback(
    output=Output('wq-readings-dash-table', 'data'),
    inputs=[Input('wq-readings-dash-table', "page_current"),
            Input('wq-readings-dash-table', "page_size"),
            Input("wq-dd-metrics", "value")],
    state=[State("wq-readings-tablestore", "data"),
           ])
def wq_navigate_readings_table(page_current,page_size, metrics, current_data):
    data  = wq_store_reader.get('wq-readings-dash-table',current_data)

    if data.empty:
        return list()

    data = data[data['tag_name'].map(lambda x: x in metrics)]
    data = data[['datetime', 'metric', 'reading', 'prev_reading', 'difference',
                 'nominal_ROC', 'status', 'periods_of_unchanged_status']]
    data['datetime'] = pd.to_datetime(data['datetime'],
                                      format=prj_collections.dashboard_datetime_format)
    data.sort_values(by=['datetime', 'metric'], ascending=False, inplace=True)
    data['datetime'] = data['datetime'].map(lambda x: x.strftime(prj_collections.dashboard_datetime_format))

    return data.iloc[
        page_current*page_size:(page_current+ 1)*page_size
    ].to_dict(orient='records')

