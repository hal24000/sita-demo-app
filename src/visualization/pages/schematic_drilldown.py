"""
OSM/Discharge Schematic Drilldown
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import dash_core_components as dcc
import dash_html_components as html
from visualization.plot_drawing.draw_schematic import draw_schematic_svg
from dash.dependencies import Input, Output, State
from visualization.app_def import app
from data.summary_utils import query_tags_conditional, df_utc_to_london
from data.collections_object import ProjectCollections
from visualization.plot_drawing.plotly_plots import make_table, make_proper_datetime_formatting
import pandas as pd
from datetime import datetime
from data.pandas_cacher import BrainHolder
from data.load_data import risk_thresholds, colors_risk
import logging
from data.project_loggers import dash_logger
logger = logging.getLogger(dash_logger)

risk_store_reader = BrainHolder(client_brains=['drilldown', 'brkdn_store', 'flow'],
                                namespace="risk-nav")

styledata_cond = []
for i,(k,v) in enumerate(risk_thresholds.items()):
    if i == 0:
        styledata_cond.append(
            {'if': {
                'filter_query': '{{Risk Score}} < {}'.format(k[1]),
                'column_id': 'Risk Score'
            },
                'backgroundColor': colors_risk[v]
            }
        )
    elif i == len(risk_thresholds.items()) - 1:
        styledata_cond.append(
            {'if': {
                'filter_query': '{{Risk Score}} >= {}'.format(k[0]),
                'column_id': 'Risk Score'
            },
                'backgroundColor': colors_risk[v]
            }
        )
    else:
        styledata_cond.append(
            {'if': {
                'filter_query': '{{Risk Score}} >= {} && {{Risk Score}} < {}'.format(k[0], k[1]),
                'column_id': 'Risk Score'
            },
                'backgroundColor': colors_risk[v]
            }
        )

content = html.Div(
    className="schematic-drilldown__page",
    children=[
        dcc.Loading(
            id="loading-osm-brkdn-datastore",
            type=ProjectCollections.loading_animation,
            children=[
                dcc.Store(id='osm-brkdn-alarms-datastore'),
                dcc.Store(id='osm-brkdn-series-datastore'),
            ]
        ),
        # First Column: Schematic - Water treatment process and aggregated risk
        html.Div(
            className="schematic-drilldown__c1 w-4 h-nofilter-1-row-of-1 bg-white mr-16",
            children=[
                html.Div(
                    className="schematic-drilldown__title title mb-4",
                    children=["Water treatment process and aggregated risk"],
                ),
                html.Div(
                    className="schematic-drilldown__body h-nofilter-1-row-of-1__body",
                    children=[
                        dcc.Loading(
                            id='loading-osm-brkdn-scchematic',
                            type=ProjectCollections.loading_animation,
                            children=[
                                dcc.Graph(
                                    className="schematic-drilldown__c1--schematic",
                                    id="osm-brkdn-schematic",
                                    config={'displayModeBar': False},
                                    figure={
                                        'layout': {
                                            'autosize': True,
                                        }
                                    }
                                )
                            ]
                        ),
                    ],
                )
            ],
        ),

        # Second Column: Root Cause Table
        html.Div(
            className="schematic-drilldown__c2 w-8",
            children=[
                html.Div(
                    className="schematic-drilldown__c2--table h-nofilter-1-row-of-2 bg-white mb-16",
                    children=[
                        html.Div(
                                className="schematic-drilldown__title title",
                                id='osm-brkdn-alarms-title',
                                children=["Alarms"],
                        ),
                        html.Div(
                            className="schematic-drilldown__sub-title",
                            id="osm-brkdn-alarms-subtitle",
                            children=[""]
                        ),
                        html.Div(
                            className="schematic-drilldown__body h-nofilter-1-row-of-2__body",
                            children=[
                                dcc.Loading(
                                    id='loading-osm-brkdn-alarms-tablediv',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        html.Div(
                                            id='osm-brkdn-alarms-tablediv',
                                            className="schematic-drilldown__c2--alarms-table",
                                        )
                                    ]
                                ),
                            ],
                        ),
                    ],
                ),
                html.Div(
                    className="schematic-drilldown__c2--table h-nofilter-1-row-of-2 bg-white",
                    children=[
                        html.Div(
                                className="schematic-drilldown__title title",
                                id='osm-brkdn-series-title',
                                children=["Values"],
                        ),
                        html.Div(
                            className="schematic-drilldown__sub-title",
                            id="osm-brkdn-series-subtitle",
                            children=[""]
                        ),
                        html.Div(
                            className="schematic-drilldown__body h-nofilter-1-row-of-2__body",
                            children=[
                                dcc.Loading(
                                    id='loading-osm-brkdn-values-tablediv',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        html.Div(
                                            id= 'osm-brkdn-values-tablediv',
                                            className="schematic-drilldown__c2--values-table",
                                        )
                                    ]
                                ),
                            ],
                        ),
                    ],
                ),
            ]
        ),
    ]
)


@app.callback(output=Output("osm-brkdn-schematic", "figure"),
              inputs=[Input("nav-selected-installation", "data"),
                      Input("nav-risk-query", 'data'),
                      Input("nav-flow-query", "data")])
def osm_brkdn_update_schematic(selected_plant, hashed_query_risk, hashed_query_flow):
    risk_df = risk_store_reader.get('drilldown', hashed_query_risk)
    risk_df = risk_df[risk_df['site_name'] == selected_plant]
    risk_dict = risk_df.groupby('site_specific_schematic')['risk'].max().to_dict()

    flows = risk_store_reader.get('flow', hashed_query_flow)

    figure = draw_schematic_svg(selected_plant, plant_risks=risk_dict, max_allowed_x = 550, max_allowed_y =769, flow_data=flows)
    return figure


@app.callback(output=[Output('osm-brkdn-alarms-datastore', 'data'),
                      Output('osm-brkdn-series-datastore', 'data'),
                      Output('osm-brkdn-alarms-subtitle', 'children'),
                      Output('osm-brkdn-series-subtitle', 'children')],
              inputs=[Input('nav-datetime', 'data'),
                      Input('osm-brkdn-schematic', 'clickData'),
                      Input('nav-selected-installation', 'data')],
              state=[State("nav-risk-query", 'data'), State("nav-data_filters", 'data')])
def osm_brkdn_update_datastores(current_datetime, clickdata, selected_installation, hashed_query, data_filter):
    logger.info(clickdata)
    # current_datetime= pd.to_datetime(current_datetime)
    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    # relevant_tags
    risk_df = risk_store_reader.get('brkdn_store', hashed_query)

    site_tag_config = pd.DataFrame(ProjectCollections.site_tag_config_coll.find({}, {"_id": 0}))

    if clickdata is None:
        if data_filter.get("tag_category") is not None:
            logger.info(f"Schematic Drilldown: Data Filter is set - {data_filter}")
            site_tag_coll = ProjectCollections.site_tag_config_coll
            tag_cat = data_filter.get("tag_category", "MISSING")
            tag_subcat = data_filter.get("tag_subcategory", "MISSING")
            query = {"site_name":       selected_installation,
                     "tag_category":    tag_cat,
                     "tag_subcategory": tag_subcat
                     }
            cursor = site_tag_coll.find(query, {"_id": 0})
            relevant_tags_df = pd.DataFrame(cursor)

            alarms_title = f"{tag_subcat} Exceptions"
            series_title = f"{tag_subcat} Exceptions"
        else:
            return [], [], html.H1("Please select a process area on the schematic"), ""
    else:
        # return empty tables if new installation is chosen
        alarms_title = "Process Area: {}"
        series_title = 'Process Area: {}'

        if selected_installation != clickdata['points'][0]['customdata']['plant']:
            return [], [], html.H1("Please select a process area on the schematic"), ""

        process_areas = clickdata['points'][0]['customdata']['process_areas']

        alarms_title = alarms_title.format(process_areas)
        series_title = series_title.format(process_areas)

        process_areas = process_areas.split(",")
        # remove trailing spaces
        process_areas = [i.strip() for i in process_areas]

        logger.info('Selected process areas:')
        logger.info(process_areas)

        relevant_tags_df = site_tag_config.loc[site_tag_config.process_area.isin(process_areas) &
                                               (site_tag_config.site_name == selected_installation)]

    if relevant_tags_df.empty:
        return dict(), dict(), alarms_title, series_title

    # standard_lookback = timedelta(minutes=15)

    # alarms_results = query_tags_conditional(list(relevant_tags_df['tag_name'].unique()),
    #                                         reference_time=current_datetime,
    #                                         latest_only=True,
    #                                         time_span=0.03,
    #                                         collection='alarms',
    #                                         retry=False
    #                                         )

    alarm_query = {'tag_name': {'$in': list(relevant_tags_df['tag_name'].unique())},
                   'occurrence_start_time': {'$lt': current_datetime},
                   '$or': [{'occurrence_end_time': {'$type': 10}},  # type 10 is type null
                           {'occurrence_end_time': {'$gte': current_datetime}}]
                   }
    projection = {'_id': 0}
    alarms_results = pd.DataFrame(list(ProjectCollections.events_coll.find(alarm_query, projection)))
    alarms_results.rename(columns={'occurrence_start_time': 'measurement_timestamp'}, inplace=True)

    if len(alarms_results) > 0:
        # enrich alarm data
        alarms_results = pd.merge(alarms_results, site_tag_config[['process_area', 'tag_name',
                                                                   'tag_short_description', 'priority']],
                                  on='tag_name', how='left')
        alarms_results = pd.merge(alarms_results, risk_df[['tag_name', 'risk']],
                                  on='tag_name', how='left')

        alarm_relevant_columns = ['process_area', 'tag_short_description', 'measurement_timestamp',
                                  'measurement_value', 'risk', 'priority']
        alarms_results = alarms_results[alarm_relevant_columns]
        alarms_results['risk'] = alarms_results['risk'].fillna(0).round(3)

        alarms_results = df_utc_to_london(alarms_results)
        alarms_results.sort_values(by='risk', ascending=False, inplace=True)
        alarms_results = make_proper_datetime_formatting(alarms_results)
    else:
        alarms_results = pd.DataFrame()

    series_results = query_tags_conditional(list(relevant_tags_df['tag_name'].unique()),
                                            reference_time=current_datetime,
                                            time_span=0.03,
                                            latest_only=True,
                                            collection='sensors',
                                            retry=False
                                            )
    if len(series_results) > 0:
        # enrich sensor data
        series_results = pd.merge(series_results, site_tag_config[['tag_name', 'tag_short_description', 'priority']],
                                  on='tag_name', how='left')
        series_results = pd.merge(series_results, risk_df[['tag_name', 'risk', 'lolo_used', 'lo_used',
                                                           'hi_used', 'hihi_used', 'rate_of_change_used']],
                                  on='tag_name', how='left')

        # create the threshold flag
        series_results = pd.merge(series_results, risk_df[['tag_name', 'hihi_calculated']],
                                  on='tag_name', how='left')
        series_results['threshold'] = 'calculated'
        series_results.loc[series_results['hihi_calculated'] != series_results['hihi_used'],
                           'threshold'] = 'overridden'
        series_results.drop(['hihi_calculated'], axis=1, inplace=True)

        series_relevant_columns = ['process_area', 'tag_short_description', 'measurement_timestamp',
                                   'measurement_value', 'measurement_unit', 'risk', 'threshold', 'lolo_used', 'lo_used',
                                   'hi_used', 'hihi_used', 'rate_of_change_used', 'priority']

        series_results = series_results[series_relevant_columns]
        series_results['risk'] = series_results['risk'].fillna(0).round(3)

        for col in ['measurement_value', 'lolo_used', 'lo_used', 'hi_used', 'hihi_used',
                    'rate_of_change_used']:
            # round numerical columns
            try:
                series_results[col] = series_results[col].round(3)
            except TypeError:
                logger.info(f"Schematic Drilldown: WARNING no numeric data found in target column - {col}")

        series_results = df_utc_to_london(series_results)
        series_results.sort_values(by=['risk', 'process_area', 'priority'],
                                   ascending=[False, True, True], inplace=True)
        series_results = make_proper_datetime_formatting(series_results)
    else:
        series_results = pd.DataFrame()

    try:
        alarms_results = alarms_results[alarms_results['priority'] != 999]
    except KeyError:
        logger.info('No priority column in alarms.')
        pass

    try:
        series_results = series_results[series_results['priority'] != 999]
    except KeyError:
        logger.info('No priority column in series.')
        pass

    # TODO: Unhardcode this.
    column_name_mapping = {'process_area':          "Process Area",
                           'tag_short_description': "Short Description",
                           'measurement_timestamp': "Timestamp",
                           'measurement_value':     "Reading",
                           'risk':                  'Risk Score',
                           'measurement_unit':      "UOM",
                           'threshold':             "Threshold",
                           'lolo_used':             "LoLo",
                           'lo_used':               "Lo",
                           'hi_used':               "Hi",
                           'hihi_used':             "HiHi",
                           'rate_of_change_used':   "ROC",
                           'priority':              'Priority'}

    for df in alarms_results, series_results:
        df.rename(columns=column_name_mapping, inplace=True)

    return alarms_results.to_dict('records'), series_results.to_dict('records'), alarms_title, series_title


@app.callback(output=(Output("osm-brkdn-values-tablediv", "children")),
              inputs=[Input("osm-brkdn-series-datastore", "data")])
def osm_brkdn_update_series_table(data):
    df = pd.DataFrame(data)

    if df.empty:
        return html.H1("No data available."),

    table = make_table(df=df, page_size=9,
                       id='osm-brkdn-series-datatable',
                       title="",
                       styledata_cond=styledata_cond,
                       page_action='native',
                       )
    return table


@app.callback(output=(Output("osm-brkdn-alarms-tablediv", "children")),
              inputs=[Input("osm-brkdn-alarms-datastore", "data")])
def osm_brkdn_update_alarms_table(data):
    df = pd.DataFrame(data)

    if df.empty:
        return html.H1("No data available."),

    table = make_table(df=df, page_size=9,
                       id='osm-brkdn-alarms-datatable',
                       title="",
                       styledata_cond=styledata_cond,
                       page_action='native')
    return table
