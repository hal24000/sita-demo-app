"""
includes sections 1.3, 1.4,, 1.5 and 1.6 from H1.2 document.
Use a navigation in the page to choose which types of exceptions to show. A dropdown possibly.
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from visualization.app_def import app
from visualization.plot_drawing.plotly_plots import make_table, draw_pie_chart_donut, \
    draw_bar_chart, make_proper_datetime_formatting
from data.summary_utils import (query_fail_unavail_new)
from data.collections_object import ProjectCollections
import numpy as np
import pandas as pd
import logging
import hashlib
from datetime import datetime
import plotly.graph_objects as go
from data.project_loggers import dash_logger
from data.pandas_cacher import BrainHolder

caching_object_avail = BrainHolder(client_brains=['exc-avail-tablestore',
                                                  'exc-avail-datatable',
                                                  'exc-avail-barchart',
                                                  'exc-avail-piechart',
                                                  'exc-avail-equipmentstore',
                                                  'cleanup'],
                                   namespace='equipment-avail')

logger = logging.getLogger(dash_logger)

content = html.Div(
        className="equipment-availability-exceptions__page",
        children=[
                # Row 1: Filters
                html.Div(
                        className="equipment-availability-exceptions__row-1 mb-16",
                        children=[
                                html.Div(
                                        className="equipment-availability-exceptions__r1 h-filter w-12 d-flex justify-content-between",
                                        children=[
                                                html.Div(
                                                        className="equipment-availability-exceptions__r1--filter-1 h-filter w-6 p-4 bg-white mr-16",
                                                        children=[
                                                                html.Div(
                                                                        className="equipment-availability-exceptions__r1--filter-title title",
                                                                        children=["Functional area"],
                                                                ),
                                                                html.Div(
                                                                        className="equipment-availability-exceptions__r1--filter-body p-10",
                                                                        children=[
                                                                                dcc.Dropdown(id="exc-avail-fa-dd",
                                                                                             multi=True),
                                                                        ],
                                                                )
                                                        ],
                                                ),
                                                html.Div(
                                                        className="equipment-availability-exceptions__r1--filter-2 h-filter w-6 p-4 bg-white",
                                                        children=[
                                                                html.Div(
                                                                        className="equipment-availability-exceptions__r1--filter-title title",
                                                                        children=["Equipment type"],
                                                                ),
                                                                html.Div(
                                                                        className="equipment-availability-exceptions__r1--filter-body p-10",
                                                                        children=[
                                                                                dcc.Dropdown(id='exc-avail-device-dd',
                                                                                             multi=True),
                                                                        ],
                                                                )
                                                        ],
                                                ),
                                        ],
                                ),
                        ],
                ),
                dcc.Loading(
                        id="loading-exc-avail-memory",
                        type=ProjectCollections.loading_animation,
                        children=[
                                dcc.Store(storage_type='memory', id='exc-avail-tablestore'),
                                dcc.Store(storage_type='memory', id='exc-avail-equipmentstore'),
                        ],
                ),
                # Row 2: Equipment and Failed assets against available charts
                html.Div(
                        className="equipment-availability-exceptions__row-2 mb-16",
                        children=[
                                html.Div(
                                        className="equipment-availability-exceptions__g1 w-8 h-with-filter-1-row-of-2 bg-white mr-16",
                                        children=[
                                                html.Div(
                                                        className="equipment-availability-exceptions__title title m-4",
                                                        children=["Equipment"],
                                                ),
                                                html.Div(
                                                        className="equipment-availability-exceptions__body",
                                                        children=[
                                                                dcc.Loading(
                                                                        id="loading-exc-avail-datatable",
                                                                        type=ProjectCollections.loading_animation,
                                                                        children=[
                                                                                html.Div(
                                                                                        id="exc-avail-datatable",
                                                                                        children=[],
                                                                                        className="equipment-availability-exceptions__g1--body h-with-filter-1-row-of-2__body",
                                                                                )]),
                                                        ],
                                                )
                                        ]
                                ),
                                html.Div(
                                        className="equipment-availability-exceptions__g4 h-with-filter-1-row-of-2 w-4 bg-white",
                                        children=[
                                                html.Div(
                                                        className="equipment-availability-exceptions__title title m-4",
                                                        children=["Unavailable assets against available"],
                                                ),
                                                html.Div(
                                                        className="equipment-availability-exceptions__body",
                                                        children=[
                                                                dcc.Loading(
                                                                        id="loading-exc-avail-piechart",
                                                                        type=ProjectCollections.loading_animation,
                                                                        children=[
                                                                                dcc.Graph(
                                                                                        id="exc-avail-piechart",
                                                                                        className="equipment-availability-exceptions__g4--body h-with-filter-1-row-of-2__body",
                                                                                )]),
                                                        ],
                                                ),
                                        ],
                                ),
                        ]
                ),

                # Row 3: Days Unavailable chart
                html.Div(
                        className="equipment-availability-exceptions__row-3",
                        children=[
                                html.Div(
                                        className="equipment-availability-exceptions__g5 h-with-filter-1-row-of-2 w-12 bg-white",
                                        children=[
                                                html.Div(
                                                        className="equipment-availability-exceptions__title title m-4",
                                                        children=["Days unavailability"],
                                                ),
                                                html.Div(
                                                        className="equipment-availability-exceptions__body",
                                                        children=[
                                                                dcc.Loading(
                                                                        id='loading-exc-avail-barchart',
                                                                        type=ProjectCollections.loading_animation,
                                                                        children=[
                                                                                dcc.Graph(
                                                                                        id="exc-avail-barchart",
                                                                                        className="equipment-availability-exceptions__g5--body h-with-filter-1-row-of-2__body",
                                                                                )]),
                                                        ],
                                                ),
                                        ],
                                ),
                        ]
                ),
        ]
)


@app.callback(
        output=Output('exc-avail-datatable-el', 'data'),
        inputs=[Input('exc-avail-datatable-el', "page_current"),
                Input('exc-avail-datatable-el', "page_size")],
        state=[State('exc-avail-tablestore', 'data')])
def exc_avail_navigate_readings_table(page_current, page_size, current_data):
    """
    navigates the shown table, updates the data when page change is triggered
    Args:
        page_current (int): current page shown of the data
        page_size (int): page size of the table
        current_data (list): current data in the table

    Returns:
        current_df (list): a list of records of the new data to show

    """
    current_df = caching_object_avail.get('exc-avail-tablestore', current_data)
    return current_df.iloc[
           page_current * page_size:(page_current + 1) * page_size
           ].to_dict(orient='records')


@app.callback(output=[Output('exc-avail-fa-dd', "value"),
                      Output('exc-avail-fa-dd', 'options')],
              inputs=[Input('nav-selected-installation', 'data')]
              )
def update_exc_avail_fa_dd(site):
    """
    updates the available functional areas upon site change
    Args:
        site (str): currently selected installation

    Returns:
        fas (list): currently selected functional areas
        options (dict): label value dict with possible functional area options

    """
    conf = ProjectCollections.site_tag_config
    fas = list(conf.loc[conf.site_name == site, 'functional_area'].unique())

    fas = np.sort(fas)
    options = [
            {'label': val, 'value': val} for val in fas
    ]
    return fas, options


@app.callback(output=[Output('exc-avail-device-dd', "value"),
                      Output('exc-avail-device-dd', 'options'), ],
              inputs=[Input('nav-selected-installation', 'data')],
              state=[State("nav-data_filters", 'data')]
              )
def update_exc_avail_devices_dd(site, data_filters):
    """
    updates the available devices dropdown item
    Args:
        site (str):
        data_filters ():

    Returns:
        default_value (str): the default selected value
        options (list): list of possible values
    """
    devices = ["Pump", "OtherEquip"]
    options = [{'label': val, 'value': val} for val in devices]
    if data_filters is not None:
        equipType = data_filters.get("equipType")
    else:
        equipType = None

    if equipType is None:
        default_value = ['Pump']
    else:
        default_value = equipType

    return default_value, options


@app.callback(output=Output('exc-avail-datatable', "children"),
              inputs=[
                      Input('exc-avail-tablestore', 'data'),
              ])
def update_avail_exc_datatable(input_data):
    """
    updates the table holder div with a new availability exception table
    Args:
        input_data (str): a hash key to address the relevant data with in the caching object

    Returns:
        table_readings (dash_tables.DataTable): the created datatable
    """
    df_store = caching_object_avail.get('exc-avail-datatable', input_data)
    # df_store = pd.DataFrame(df_store)
    if len(df_store) == 0:
        return make_table(pd.DataFrame(), 6, 'exc-avail-datatable-el', 'No data found')

    try:
        df_store.drop(['tag_short_description'], axis=1, inplace=True)
    except:
        pass

    style_data = [
    ]
    # columns should be
    # Functional area, Full Description,Unavailable Date, UOM, Available Date, Days Unavailable

    table_readings = make_table(df_store, 6, 'exc-avail-datatable-el', "Availability exceptions",
                                styledata_cond=style_data,
                                numeric_columns=['Days Unavailable'], decimals_displayed=3)

    return table_readings


@app.callback(output=Output('exc-avail-barchart', "figure"),
              inputs=[
                      Input('exc-avail-tablestore', 'data'),
              ])
def update_avail_exc_bar_chart(current_data):
    """

    Args:
        current_data (str): the hash key to query plasma store for the relevant data

    Returns:
        bar_chart_avail (px.bar): a bar chart containing the availability exceptions

    """
    df_store = caching_object_avail.get("exc-avail-barchart", current_data)
    if len(df_store) == 0:
        return go.Figure(layout={'title': 'No unavailable equipment found'})

    df_store = df_store.loc[df_store['Days Unavailable'] >= 0]

    bar_chart_avail = draw_bar_chart(df_store, 'tag_short_description',
                                     'Days Unavailable')

    return bar_chart_avail


@app.callback(output=Output('exc-avail-piechart', "figure"),
              inputs=[
                      Input('exc-avail-tablestore', 'data'),

              ],
              state=[State('exc-avail-equipmentstore', 'data')])
def update_avail_exc_piechart(input_data, relevant_equipment):
    """

    Args:
        input_data (str): the hash key to query plasma store with
        relevant_equipment (list):

    Returns:
        pie_chart_exc_avail (go.Figure): the resulting pie chart
    """
    relevant_equipment = caching_object_avail.get('exc-avail-piechart', relevant_equipment)
    df_store = caching_object_avail.get('exc-avail-piechart', input_data)
    if len(df_store) == 0:
        return go.Figure(layout={'title': 'No data found'})
    num_unavail = len(df_store.loc[df_store['Days Unavailable'] >= 0])

    pie_chart_exc_avail = draw_pie_chart_donut([num_unavail, len(relevant_equipment)],
                                               ['Unavailable Equipment', 'Total Equipment'],
                                               args={'title': ""}, pct=False)
    return pie_chart_exc_avail


@app.callback(
        output=[Output('exc-avail-tablestore', 'data'),
                Output('exc-avail-equipmentstore', 'data')],
        inputs=[
                Input("nav-datetime", 'data'),
                Input('exc-avail-fa-dd', 'value'),
                Input('exc-avail-device-dd', 'value'),
        ],
        state=[State('nav-selected-installation', 'data'),
               State('nav-live-switch', 'on')]
)
def exc_avail_update_datastore(current_datetime,
                               selected_func_areas,
                               selected_devices,
                               selected_installation,
                               live
                               ):
    """

    Args:
        current_datetime (str): current dashboard datetime in string format
        selected_func_areas (list): list of selected functional areas
        selected_devices (list): list of selected device types
        selected_installation (str): currently selected isntallation
        live (bool): is the dashboard live?

    Returns:
        hash_code_table (str): dataframe hash key
        hash_code_eq (str): equipment_list hash key

    """
    site_conf = ProjectCollections.site_tag_config
    tag_subcat_filter = ["availstatus"]
    search_terms_fail = ["UNAVAILABLE", "UNAVAIL", "Unavail", "Unavailable"]

    selected_devices = tuple(selected_devices)
    selected_func_areas = tuple(selected_func_areas)

    equipment_list = list(site_conf.loc[(site_conf.site_name == selected_installation) &
                                        (site_conf.tag_category.isin(selected_devices)) &
                                        (site_conf.tag_subcategory.str.lower().isin(tag_subcat_filter)) &
                                        (site_conf.functional_area.isin(selected_func_areas)), 'tag_name'].unique())

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)
    logger.warning('Availability exceptions, querying with time: {}'.format(current_datetime))
    ts_now_str = datetime.strftime(current_datetime, '%Y-%m-%d %H:%M')

    hash_code_table = hashlib.sha1(ts_now_str.encode()).hexdigest()
    hash_code_eq = hashlib.sha1(str(equipment_list).encode()).hexdigest()

    if len(selected_func_areas) > 0 and len(selected_devices) > 0:
        df_merged = query_fail_unavail_new(equipment_list=equipment_list,
                                           search_terms_fail=search_terms_fail,
                                           current_datetime=current_datetime,
                                           live=live,
                                           failures=False)

        if len(df_merged) == 0:
            caching_object_avail.store('exc-avail-tablestore', hash_code_table, df_merged,
                                       time_created=current_datetime)
            caching_object_avail.store('exc-avail-equipmentstore', hash_code_eq, equipment_list,
                                       time_created=current_datetime)

            return hash_code_table, hash_code_eq

        df_merged.sort_values(by='Days Unavailable', ascending=False, inplace=True)


    else:
        df_merged = pd.DataFrame()
    try:
        df_merged.drop(["fabricated_key", 'site_name'], axis=1, inplace=True)
    except:
        pass

    df_merged = make_proper_datetime_formatting(df_merged)

    for col in df_merged.columns:
        df_merged[col] = df_merged[col].astype(str)

    if 'Days Unavailable' in df_merged.columns:
        df_merged['Days Unavailable'] = df_merged['Days Unavailable'].astype(int)

    caching_object_avail.store('exc-avail-tablestore', hash_code_table, df_merged, time_created=current_datetime)
    caching_object_avail.store('exc-avail-equipmentstore', hash_code_eq, equipment_list, time_created=current_datetime)

    return hash_code_table, hash_code_eq
