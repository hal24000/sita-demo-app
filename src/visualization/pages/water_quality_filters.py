"""
water quality readings, section 1.11 from H1.2 document
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===


"""

import numpy as np
import pandas as pd
from data.pandas_cacher import BrainHolder, hash_query
import dash_core_components as dcc
import dash_html_components as html
from visualization.app_def import app
from data.collections_object import ProjectCollections as prj_collections
from data.summary_utils import df_utc_to_london
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
import hashlib
from datetime import datetime
from data.load_data import load_data
import logging
from data.project_loggers import dash_logger

logger = logging.getLogger(dash_logger)
tz_utc = prj_collections.tz_utc

metric_config = prj_collections.metric_config

wq_store_cacher = BrainHolder(client_brains=['cleanup', 'wq-readings-tablestore',
                                             'wq-amber-tablestore', 'wq-exc-tablestore'],
                              namespace="water-quality")

filters_content = html.Div(
        className="water-quality-filters mb-16",
        children=[
                html.Div(
                        className="water-quality-filters--g1 w-12 h-filter p-4 bg-white",
                        children=[
                                dcc.Loading(
                                        id='loading-wq-exc-memory',
                                        type=prj_collections.loading_animation,
                                        children=[
                                                dcc.Store(id="wq-exc-tablestore", storage_type="memory"),
                                                dcc.Store(id="wq-amber-tablestore", storage_type="memory"),
                                                dcc.Store(id="wq-readings-tablestore", storage_type="memory"),
                                        ]
                                ),
                                html.Div(
                                        className="water-quality-filters__title title mb-8",
                                        children=["Metrics"],
                                ),
                                html.Div(
                                        className="water-quality-filters__body",
                                        children=[
                                                dcc.Checklist(
                                                        className="water-quality-filters__func-area",
                                                        id='wq-chk-func_areas',
                                                        # options=[
                                                        #     {'label': 'all', 'value': 'all'}
                                                        # ] + func_areas,
                                                        # value=['all'],
                                                        labelStyle={'display': 'inline-block'},
                                                        inputClassName="water-quality-filters__func-area--input mb-4"

                                                ),
                                                dcc.Dropdown(
                                                        id="wq-dd-metrics",
                                                        multi=True
                                                ),
                                        ],
                                )
                        ],
                ),
        ],
)


@app.callback(output=Output("wq-exc-tablestore", "data"),
              inputs=[Input('nav-datetime', 'data'),
                      Input("wq-dd-metrics", "options")],
              state=[State("nav-history-days", "data")]
              )
def load_wq_data(set_datetime, metrics: list, lookback_days: int):
    if lookback_days is None:
        logger.info(
            f"[WQ Filters - {pd.Timestamp.utcnow()}] WQ Filter history is out of range {lookback_days}, defaulting to 30")
        lookback_days = 7
    end_date = datetime.strptime(set_datetime, prj_collections.dashboard_datetime_format)
    start_date = end_date - pd.Timedelta(days=lookback_days)

    logger.info(f"WQ Filter querying data for: {lookback_days} days starting from {start_date} to {end_date}")

    # load raw data
    sensor_list = [metric['value'] for metric in metrics]
    if not sensor_list:
        return dict()

    query = {
            'tag_name':              {'$in': sensor_list},
            'measurement_timestamp': {'$gte': start_date, '$lte': end_date}
    }

    wq_data = load_data(collection=prj_collections.series_coll, query=query,
                        projection={'_id':                   0,
                                    'measurement_timestamp': 1,
                                    'measurement_value':     1,
                                    'tag_name':              1})

    if wq_data.empty:
        return dict()

    wq_data = df_utc_to_london(wq_data, 'measurement_timestamp')

    wq_data = pd.pivot_table(wq_data, index='measurement_timestamp',
                             columns='tag_name', values='measurement_value')

    # TODO: Allow for different aggregation periods per column
    # pick the most frequent aggregation period
    resampling_period = metric_config['risk_scoring_period'].value_counts().index[0]
    aggregation_operations = dict(zip(metric_config.loc[metric_config['tag_name'].map(lambda x: x in sensor_list),
                                                        'tag_name'].tolist(),
                                      metric_config.loc[metric_config['tag_name'].map(lambda x: x in sensor_list),
                                                        'risk_scoring_aggregation'].tolist()))

    # remove aggregation for nonexistent columns
    to_drop = set(aggregation_operations.keys()) - set(wq_data.columns)
    for key in to_drop:
        del aggregation_operations[key]

    wq_data = wq_data.resample('{}s'.format(resampling_period), label='right').agg(aggregation_operations)

    # fill in the missing time stamps
    full_index = pd.date_range(start=wq_data.index.min(), end=wq_data.index.max(),
                               freq='{}s'.format(resampling_period))
    wq_data = wq_data.reindex(full_index)
    wq_data.sort_index(inplace=True)

    wq_data.index = wq_data.index.strftime(prj_collections.dashboard_datetime_format)
    hashed_query = hash_query(query)
    # end_date_str = end_date.strftime(prj_collections.dashboard_datetime_format)
    wq_store_cacher.store("wq-exc-tablestore", hashed_query, wq_data, end_date)
    return hashed_query


@app.callback(output=Output("wq-amber-tablestore", "data"),
              inputs=[Input("wq-exc-tablestore", "data")],
              state=[State('links-dropdown', "value")]
              )
def calculate_amber_numbers(input_data, dash_location):
    """
    Assuming that the data have been resampled as appropriate, ie. to 15minute averages
    """

    if dash_location != 'water-quality-exceptions':
        return dict()

    # input_df = pd.DataFrame(input_data)
    input_df = wq_store_cacher.get('wq-amber-tablestore', input_data)
    if input_df.empty:
        return dict()

    # here we do not care about the timestamps so remove them
    input_df.reset_index(inplace=True, drop=True)

    diff_df = input_df - input_df.shift()

    amber_df = pd.DataFrame(index=diff_df.index)
    final_df = pd.DataFrame(index=diff_df.index)
    out_df = []
    out_index = []

    for column in diff_df.columns:
        periods_to_amber = metric_config.loc[metric_config['tag_name'] == column,
                                             'threshold_number_of_periods'].values[0]
        cutoff_lo = metric_config.loc[metric_config['tag_name'] == column, 'cutoff_drop'].values[0]
        cutoff_hi = metric_config.loc[metric_config['tag_name'] == column, 'cutoff_rise'].values[0]

        amber_df[column] = diff_df[column].map(
                lambda x: 'rise' if x > cutoff_hi else 'drop' if x < cutoff_lo else 'normal'
        )

        hlp_df = count_unchanged_periods(df=amber_df,
                                         status_column=column,
                                         count_column='same_status')

        final_df[column] = 'normal'
        final_df.loc[(hlp_df['same_status'] >= periods_to_amber)
                     & (hlp_df[column] == 'rise'), column] = 'high_amber'
        final_df.loc[(hlp_df['same_status'] >= periods_to_amber)
                     & (hlp_df[column] == 'drop'), column] = 'low_amber'

        # final_df.loc[(amber_df[column] == 'rise') & (amber_df[column].shift() == 'rise'), column] = 'high_amber'
        # final_df.loc[(amber_df[column] == 'drop') & (amber_df[column].shift() == 'drop'), column] = 'low_amber'

        out_df.append(final_df[column].value_counts().to_dict())
        out_index.append(column)

    out_df = pd.DataFrame(out_df, index=out_index)
    out_df.index.name = 'tag_name'

    target_columns = ['normal', 'low_amber', 'high_amber']
    for column in target_columns:
        if column not in out_df.columns:
            out_df[column] = 0

    out_df.fillna(0, inplace=True)

    for column in ['metric', 'functional_area']:
        out_df[column] = out_df.index.map(dict(zip(metric_config['tag_name'].values,
                                                   metric_config[column].values)))

    # sort data
    out_df.sort_values(by=['functional_area', 'metric'], inplace=True)

    ts_now = datetime.now()
    ts_now_str = datetime.strftime(ts_now, '%Y-%m-%d %H:%M')

    ts_now_hash = hashlib.sha1(ts_now_str.encode()).hexdigest()

    wq_store_cacher.store(client_id="wq-amber-tablestore",
                          key=ts_now_hash,
                          frame=out_df.reset_index(),
                          time_created=ts_now)
    # print(ts_now_hash)
    return ts_now_hash


@app.callback(output=Output("wq-readings-tablestore", "data"),
              inputs=[Input("wq-exc-tablestore", "data")],
              state=[State("links-dropdown", "value")]
              )
def prepare_readings_table(input_data, dash_location):
    """
    Assuming that the data have been resampled as appropriate, ie. to 15minute averages
    """

    if dash_location != 'water-quality-readings':
        return dict()

    # input_df = pd.DataFrame(input_data)
    input_df = wq_store_cacher.get('wq-readings-tablestore', input_data)
    if input_df.empty:
        return dict()

    input_df.index = pd.to_datetime(input_df.index, format=prj_collections.dashboard_datetime_format)
    prev_df = input_df.shift()
    diff_df = input_df - prev_df

    # update the datetimes
    # input_df.index = pd.to_datetime(input_df.index, format=prj_collections.dashboard_datetime_format)
    # diff_df.index = pd.to_datetime(diff_df.index, format=prj_collections.dashboard_datetime_format)

    input_df.reset_index(inplace=True)
    prev_df.reset_index(inplace=True)
    diff_df.reset_index(inplace=True)

    input_df.rename(columns={'index': 'datetime'}, inplace=True)
    prev_df.rename(columns={'index': 'datetime'}, inplace=True)
    diff_df.rename(columns={'index': 'datetime'}, inplace=True)

    input_df = input_df.melt(id_vars=['datetime'], var_name='tag_name', value_name='reading')
    prev_df = prev_df.melt(id_vars=['datetime'], var_name='tag_name', value_name='prev_reading')
    diff_df = diff_df.melt(id_vars=['datetime'], var_name='tag_name', value_name='difference')

    # final_df = pd.merge(input_df, diff_df, on=['datetime', 'tag_name'])
    final_df = input_df.copy()
    final_df['prev_reading'] = prev_df['prev_reading'].values
    final_df['difference'] = diff_df['difference'].values

    # final_df['prev_reading'] = final_df['reading']  - final_df['difference']
    final_df = pd.merge(final_df, metric_config[['metric', 'tag_name', 'cutoff_drop', 'cutoff_rise']],
                        on='tag_name', how='left')
    final_df['nominal_ROC'] = final_df[['cutoff_drop', 'cutoff_rise']].replace([-np.inf, np.inf], 0).abs().max(axis=1)
    final_df['status'] = final_df.apply(lambda x: 'rise' if x['difference'] > x['cutoff_rise'] \
        else 'drop' if x['difference'] < x['cutoff_drop'] \
        else 'normal', axis=1)

    # hide rounding errors
    final_df['difference'] = final_df['difference'].map(lambda x: 0 if np.abs(x) < 1e-8 else x)

    final_df['periods_of_unchanged_status'] = 0

    # count the number of times
    for metric, grp in final_df.groupby('metric'):
        hlp_df = grp.copy()

        hlp_df = count_unchanged_periods(df=hlp_df, status_column='status',
                                         count_column='periods_of_unchanged_status')

        final_df.loc[grp.index, 'periods_of_unchanged_status'] = hlp_df['periods_of_unchanged_status']

    final_df['periods_of_unchanged_status'] = final_df['periods_of_unchanged_status'].astype(int)
    final_df['datetime'] = final_df['datetime'].map(lambda x: x.strftime(prj_collections.dashboard_datetime_format))

    ts_now = datetime.now()
    ts_now_str = datetime.strftime(ts_now, '%Y-%m-%d %H:%M')

    ts_now_hash = hashlib.sha1(ts_now_str.encode()).hexdigest()

    wq_store_cacher.store("wq-readings-tablestore", ts_now_hash, final_df, ts_now)

    return ts_now_hash


@app.callback(output=[Output('wq-chk-func_areas', "options"),
                      Output("wq-chk-func_areas", "value")],
              inputs=[Input("nav-selected-installation", 'data'),
                      Input("links-dropdown", "value")],
              state=[State("nav-data_filters", 'data')]
              )
def update_wq_fa_dd(selected_inst, page, data_filter):
    plant_df = metric_config[metric_config['location'] == selected_inst].copy()

    options = sorted(plant_df['functional_area'].unique())
    options = [{'label': label, 'value': label} for label in options]

    options = [{"label": "all", "value": "all"}] + options

    if page == 'water-quality-exceptions':
        # show all metrics by default on the WQ exceptions page
        values = ["all"]
    elif page == 'water-quality-readings':
        # show the first single metric of WQ readings
        values = [options[1]["value"]]
    else:
        values = ["all"]

    if data_filter is not None and data_filter.get('wq_alert_tags') is not None:
        values = []

    return options, values


@app.callback(output=Output("wq-dd-metrics", "options"),
              inputs=[Input("nav-selected-installation", "data")]
              )
def update_wq_met_dd_options(selected_installation):
    plant_df = metric_config[metric_config['location'] == selected_installation].copy()
    if plant_df.empty:
        return list()

    plant_df.sort_values(by=['functional_area', 'metric'], inplace=True)

    options = []
    for _, row in plant_df.iterrows():
        options.append({'label': row['metric'], 'value': row['tag_name']})

    return options


@app.callback(output=Output("nav-data_filters", 'clear_data'),
              inputs=[Input('wq-dd-metrics', "value")],
              state=[Output("nav-data_filters", 'data')]
              )
def clear_nav_data(wq_metrics, nav_data):
    """"""
    if nav_data is None:
        raise PreventUpdate

    return True


@app.callback(output=Output('wq-dd-metrics', "value"),
              inputs=[Input("nav-selected-installation", 'data'),
                      Input("wq-chk-func_areas", "value")],
              state=[State("nav-data_filters", 'data')]
              )
def update_wq_met_values(selected_installation, selected_areas, data_filter):
    plant_df = metric_config[metric_config['location'] == selected_installation].copy()
    selected_options = list()
    if plant_df.empty:
        return selected_options

    plant_df.sort_values(by=['functional_area', 'metric'], inplace=True)

    # Get water quality alert tags if they are set
    if data_filter is not None:
        wq_alert_tags = data_filter.get("wq_alert_tags")
    else:
        wq_alert_tags = None

    if "all" in selected_areas:
        selected_options = plant_df['tag_name'].values
    elif wq_alert_tags is not None:
        selected_options = wq_alert_tags
    else:
        values = plant_df.loc[plant_df['functional_area'].map(lambda x: x in selected_areas), 'tag_name'].values
        selected_options = list(values)

    return selected_options


def count_unchanged_periods(df: pd.DataFrame, status_column: str = 'status',
                            count_column: str = 'periods_of_unchanged_status') -> pd.DataFrame:
    """
    In a time series data counts the number of times a status has been unchanged.

    Args:
        df (pd.DataFrame): input dataframe, assumed ordered by time
        status_column (str): name of the column with status (default: 'status')
        count_column (str): name of the new column (default: 'periods_of_unchanged_status')

    Returns:
        pd.DataFrame

    TODO: remove potential column name collisions
    """

    internal_df = df.copy()

    # find where the status changes
    internal_df['change'] = internal_df[status_column] != internal_df[status_column].shift()
    internal_df['change'] = internal_df['change'].fillna(False)

    # initial status counting
    internal_df[count_column] = 1
    internal_df[count_column] = internal_df[count_column].cumsum()

    # identify points of resetting the counter
    internal_df['subtraction'] = np.nan
    internal_df.loc[internal_df['change'],
                    'subtraction'] = internal_df.loc[internal_df['change'],
                                                     count_column] - 1
    internal_df['subtraction'] = internal_df['subtraction'].ffill()
    internal_df['subtraction'] = internal_df['subtraction'].fillna(0)

    internal_df[count_column] = internal_df[count_column] - internal_df['subtraction']
    internal_df.drop(['subtraction', 'change'], axis=1, inplace=True)

    return internal_df
