"""
filtration-gac page
Includes the callbacks and plotly dash layout for the gac page
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from visualization.app_def import app
from data.collections_object import ProjectCollections
from visualization.plot_drawing.plotly_plots import make_table
from data.summary_utils import (query_tags_conditional,
                                query_max_value,
                                query_last_in_period)
from data.rgf_gac_utils import (get_value_diff_periods,
                                get_service_status,
                                thresh_func,
                                update_datastore_df,
                                thresh_roc_cons)
from data.gac_collections import GACCollections
from data.gac_thresholds import (oos_threshold,
                                 inlet_lvl_thresh,
                                 exc_queue_threshold,
                                 turbidity_thresh,
                                 bw_press_thresh,
                                 bw_flow_thresh,
                                 individual_thresholds
                                 )
from data.gac_functions import (preproc_bw_funcs,
                                preproc_outflow_funcs,
                                assign_filtering_funcs,
                                preproc_bwreq_funcs,
                                preproc_bwdue_funcs,
                                preproc_bwpressure_funcs,
                                preproc_inletlevel_funcs,
                                preproc_bwflow_funcs,
                                postprocess_df_funcs,
                                preproc_bw_excqueue_funcs
                                )
from data.load_data import colors_risk
import numpy as np
import pandas as pd
from datetime import datetime
import logging
from data.project_loggers import dash_logger
from data.pandas_cacher import BrainHolder
import hashlib

logger = logging.getLogger(dash_logger)

DECIM_S= GACCollections.DECIM_S
DECIM_L = GACCollections.DECIM_L
site_tag_config = ProjectCollections.site_tag_config

filtration_cacher = BrainHolder(client_brains=['filt-gac-datastore',
                                               'filt-gac-datastore-rocs',
                                               'filt-gac-datatable',
                                               'filt-gac-tablediv'],
                                namespace='filtration-gac',
                                keep_period=1060)

content = html.Div(
    className="filtration-rgf__page",
    children=[
        # First Row: Filters
        dcc.Loading(id='filt-gac-datastore-roc-loading', children=[dcc.Store(id='filt-gac-datastore-rocs')]),
        dcc.Loading(id='filt-gac-datastore-loading', children =[dcc.Store(id='filt-gac-datastore')] ),
        html.Div(
            className="filtration-rgf__row-1 w-12 mb-16 h-filter",
            children=[
                html.Div(
                    className="filtration-rgf__g1 w-5 bg-white p-4 mr-16 h-filter",
                    children=[
                        html.Div(
                            className="filtration-rgf__title title",
                            children=["Status"],
                        ),
                        html.Div(
                            className="filtration-rgf__body filtration-gac__g1--body",
                            children=[
                                dcc.Loading(id='gac-loadingstat-oos',
                                            children=html.Div(
                                                            id='filtration-gac__g1--b1',
                                                            className="button-f mr-16",
                                                            children=[
                                                                html.Div("Out of service", className="button-f__title"),
                                                                html.Div("", className="button-f__value",
                                                                         id="filtration-gac__g1--b1-value")
                                                            ],),

                                ),
                                dcc.Loading(id='gac-loadingstat-backwashing',
                                            children=html.Div(
                                                    id='filtration-gac__g1--b2',
                                                    className="button-f mr-16",
                                                    children=[
                                                        html.Div("Backwashing", className="button-f__title"),
                                                        html.Div("", className="button-f__value",
                                                                 id="filtration-gac__g1--b2-value")
                                                    ])
                                ),
                            dcc.Loading(id='gac-loadingstat-backwashdue',
                                            children=html.Div(
                                                    id='filtration-gac__g1--b3',
                                                    className="button-f mr-16",
                                                    children=[
                                                        html.Div("Backwash due", className="button-f__title"),
                                                        html.Div("", className="button-f__value",
                                                                 id="filtration-gac__g1--b3-value")
                                                    ])
                                ),
                                dcc.Loading(id='gac-loadingstat-bwreq',
                                            children=html.Div(
                                                    id='filtration-gac__g1--b4',
                                                    className="button-f mr-16",
                                                    children=[
                                                        html.Div("Backwash required", className="button-f__title"),
                                                        html.Div("", className="button-f__value",
                                                                 id="filtration-gac__g1--b4-value")
                                                    ])
                                ),
                                dcc.Loading(id='gac-loadingstat-filterbed', children=html.Div(
                                                                        id='filtration-gac__g1--b5',
                                                                        className="button-f mr-16",
                                                                        children=[
                                                                            html.Div("Filter bed condition change", className="button-f__title"),
                                                                            html.Div("", className="button-f__value", id="filtration-gac__g1--b5-value")
                                                                        ])
                                ),
                                dcc.Loading(id='gac-loadingstat-excqueue',
                                            children=html.Div(
                                                            id='filtration-gac__g1--b6',
                                                            className="button-f mr-16",
                                                            children=[
                                                                html.Div("Excess que for backwash",
                                                                         className="button-f__title"),
                                                                html.Div("", className="button-f__value",
                                                                         id="filtration-gac__g1--b6-value")
                                                            ])
                                ),
                            ],
                        )
                    ]
                ),
                html.Div(
                    className="filtration-rgf__g2 w-7 bg-white p-4 h-filter",
                    children=[
                        html.Div(
                            className="filtration-rgf__title title",
                            children=["Performance"],
                        ),
                        html.Div(
                            className="filtration-rgf__body filtration-gac__g2--body",
                            children=[
                                dcc.Loading(id='gac-loadingstat-uv254',
                                            children=html.Div(
                                                        id='filtration-gac__g2--b1',
                                                        className="button-f mr-16",
                                                        children=[
                                                            html.Div("UV254", className="button-f__title"),
                                                            html.Div("", className="button-f__value", id="filtration-gac__g2--b1-value")
                                                        ])
                                ),
                                dcc.Loading(id='gac-loadingstat-pflow',
                                            children=html.Div(
                                                            id='filtration-gac__g2--b2',
                                                            className="button-f mr-16",
                                                            children=[
                                                                html.Div("Particle flow", className="button-f__title"),
                                                                html.Div("", className="button-f__value",
                                                                         id="filtration-gac__g2--b2-value")
                                                            ])
                                ),
                                dcc.Loading(id='gac-loadingstat-pcounter',
                                            children=html.Div(
                                                                id='filtration-gac__g2--b3',
                                                                className="button-f mr-16",
                                                                children=[
                                                                    html.Div("Particle counter",
                                                                             className="button-f__title"),
                                                                    html.Div("", className="button-f__value",
                                                                             id="filtration-gac__g2--b3-value")
                                                                ])
                                ),
                                dcc.Loading(id='gac-loadingstat-outflow',
                                            children=html.Div(
                                                        id='filtration-gac__g2--b4',
                                                        className="button-f mr-16",
                                                        children=[
                                                            html.Div("Outlet flow", className="button-f__title"),
                                                            html.Div("", className="button-f__value", id="filtration-gac__g2--b4-value")
                                                        ])
                                ),
                                html.Div(
                                    id='filtration-gac__g2--b5',
                                    className="button-f__double mr-16",
                                    children=[
                                        dcc.Loading(id='gac-loadingstat-inletlvla',
                                                    children=html.Div(
                                                                    id='filtration-gac__g2--b5t',
                                                                    className="button-h__double",
                                                                    children=[
                                                                        html.Div("Inlet level A", className="button-h__title"),
                                                                        html.Div("", className="button-h__value",
                                                                                 id="filtration-gac__g2--b5t-value")
                                                                    ])
                                        ),
                                        dcc.Loading(id='gac-loadingstat-inletlvlb',
                                                    children=html.Div(
                                                                id='filtration-gac__g2--b5b',
                                                                className="button-h__double",
                                                                children=[
                                                                    html.Div("Inlet level B",
                                                                             className="button-h__title"),
                                                                    html.Div("", className="button-h__value",
                                                                             id="filtration-gac__g2--b5b-value")
                                                                ])
                                        ),
                                    ],
                                ),
                                html.Div(
                                    id='filtration-gac__g2--b6',
                                    className="button-f__double mr-16",
                                    children=[
                                        dcc.Loading(id='gac-loadingstat-turbida',
                                                    children=html.Div(
                                                                id='filtration-gac__g2--b6t',
                                                                className="button-h__double",
                                                                children=[
                                                                    html.Div("TurbidityA", className="button-h__title"),
                                                                    html.Div("", className="button-h__value",
                                                                             id="filtration-gac__g2--b6t-value")
                                                                ])
                                        ),
                                        dcc.Loading(id='gac-loadingstat-turbidb',
                                                    children=html.Div(
                                                                    id='filtration-gac__g2--b6b',
                                                                    className="button-h__double",
                                                                    children=[
                                                                        html.Div("TurbidityB", className="button-h__title"),
                                                                        html.Div("", className="button-h__value",
                                                                                 id="filtration-gac__g2--b6b-value")
                                                                    ])
                                        ),
                                    ],
                                ),
                                html.Div(
                                    id='filtration-gac__g2--b7',
                                    className="button-f__double mr-16",
                                    children=[
                                        dcc.Loading(id='gac-loadingstat-bwpressure',
                                                    children=html.Div(
                                                                id='filtration-gac__g2--b7t',
                                                                className="button-h__double",
                                                                children=[
                                                                    html.Div("Backwash pressureA (24h max)",
                                                                             className="button-h__title"),
                                                                    html.Div("", className="button-h__value",
                                                                             id="filtration-gac__g2--b7t-value")
                                                                ])
                                        ),
                                        dcc.Loading(id='gac-loadingstat-bwpressureb',
                                                    children=html.Div(
                                                                id='filtration-gac__g2--b7b',
                                                                className="button-h__double",
                                                                children=[
                                                                    html.Div("Backwash pressureB (24h max)",
                                                                             className="button-h__title"),
                                                                    html.Div("", className="button-h__value",
                                                                             id="filtration-gac__g2--b7b-value")
                                                                ])
                                        ),
                                    ],
                                ),
                                dcc.Loading(id='gac-loadingstat-bflow',
                                            children=html.Div(
                                                            id='filtration-gac__g2--b8',
                                                            className="button-f mr-16",
                                                            children=[
                                                                html.Div("Backwash Flow",
                                                                         className="button-f__title"),
                                                                html.Div("", className="button-f__value",
                                                                         id="filtration-gac__g2--b8-value")
                                                            ])
                                ),
                            ],
                        )
                    ]
                ),
            ],
        ),

        # Second Row: Table
        html.Div(
            className="filtration-rgf__row-2 w-12 h-with-filter-1-row-of-1",
            children=[
                html.Div(
                    className="filtration-rgf__g3 w-12 bg-white h-with-filter-1-row-of-1",
                    children=[
                        html.Div(
                            className="filtration-rgf__title title p-4",
                            children=["GACs with current performance and status details"],
                        ),
                        dcc.Loading( id='filt-gac-loading-tablediv',
                            children= [html.Div(
                                className="filtration-rgf__body h-with-filter-1-row-of-1__body",
                                id='filt-gac-tablediv',

                                children=[
                                # Graph or table please use the same class name, thanks
                                # html.Div(
                                #     id = 'filt-gac-tablediv',
                                #     # className="filtration-gac__g3--body h-with-filter-1-row-of-1__body",
                                # ),
                                ],
                            )]
                        )
                    ]
                ),
            ]
        ),
    ]
)


@app.callback(output=[Output('filtration-gac__g1--b1-value', 'children'),
                      Output('filtration-gac__g1--b1', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_update_oos(selected_installation, current_datetime):
    """
    updates the out of service status button
    GOODW logic: Total compliment of RGF Filters 'OUT OF SERVICE' (OOS)/Full compliment
        * tag_name: RGF_A_RGF1OUTSER
    HUBY logic: Total compliment of RGF Filters 'OUT OF SERVICE' (OOS)/Full compliment
        * tag_name: RGF1_OOS_INSERV_SP
    ELVINGTON logic: Total compliment of RGF Filters OOS/Full compliment (sum Tags:ERGFnnIOSR = 'OUT OF SERVICE'
        * tag_name : ERGF01IOSR

    """

    # try and get the tags from the project collections
    oos_tags = GACCollections.oos_tags.get(selected_installation, None)

    # if this installation has no tags then return empty
    if oos_tags is None:
        return "", {'background-color': 'grey'}

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    n_assets = GACCollections.num_gacs[selected_installation]
    oos_vals = GACCollections.OOS_VALS[selected_installation]
    # oos_res = query_last_in_period(tags=oos_tags,
    #                                  reference_time= current_datetime,
    #                                  collection=collection)
    # if len(oos_res) == 0:
    #     return "no data", {"background-color":'grey'}

    try:
        service_df = get_service_status(oos_tags=GACCollections.oos_tags[selected_installation],
                                        reference_time=current_datetime,
                                        service_func=assign_filtering_funcs[selected_installation])
        n_unavail = (service_df == 'oos').sum()

    except Exception as e:
        return "no data", {'background-color': 'grey'}

    if selected_installation in oos_threshold:
        color = thresh_func(val=n_unavail,
                            thresh=oos_threshold[selected_installation])
    else:
        color = colors_risk['neutral']

    return_str = "{}/{}".format(n_unavail, n_assets)
    style = {'background-color': color}

    return return_str, style


@app.callback(output=[Output("filtration-gac__g1--b2-value", 'children'),
                      Output('filtration-gac__g1--b2', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_update_backwashing(selected_installation, current_datetime):
    """
    updates the backwashing button

    ACCOMB logic: tags in state 2 are queued to bashwash, 3-12  => bacwashing currently
        * tag_name: RGF_DW_RGF1_BWSTEPNUM
        GACCollections.backwashing_tags
    HUBY logic: only 1 should be backwashing, raw value of current backwashing
        * tag_name: HUBYWTS1:RGF_CURRENT_WASHING
    ELVINGTON logic: individual rgf backwash status
        * tag_name:ERGF01CBS
        #NOTE: currently unused, sensor isnt being reported. Francis has requested

    """
    # individual_tag_df = GACCollections.individual_tag_df
    # individual_tag_df_site = individual_tag_df.loc[individual_tag_df]
    backwash_tags = GACCollections.bw_status.get(selected_installation, None)
    n_assets = GACCollections.num_gacs[selected_installation]

    # currently no color for acomb, elvington or huby according to doc matrix v10
    if backwash_tags is None:
        return "", {'background-color': 'grey'}

    backwash_vals_df = query_tags_conditional(tag_list=backwash_tags, latest_only=True, reference_time=current_datetime,
                                              collection='sensors')

    if len(backwash_vals_df) == 0:
        return "no data", {'background-color': 'grey'}

    if selected_installation in preproc_bw_funcs:
        backwash_val = preproc_bw_funcs[selected_installation](backwash_vals_df)
    else:
        backwash_val = backwash_vals_df['measurement_value'].squeeze()



    color = colors_risk['neutral']
    return_str = "{}/{}".format(int(backwash_val), n_assets)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-gac__g1--b3-value', 'children'),
                      Output('filtration-gac__g1--b3', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_update_backwash_due(selected_installation, current_datetime):
    """
    updates the backwash due button

    GOODW logic: servicehours >= thresholds -1h => servicehours >= 26-1
        * tag_name GOODWDA:RGFF_DA_RGF_1_HIF
    HUBY logic: service hours >= thresholds -h => servicehours >= 78- 1
        *  Unit Service Hours(Tag:HUBYWTS1:GACA_SERVICE_TIME_SI)  > 78 - 1
    ELVINGTON logic: service hours >= thresholds -1h => servicehours >= 48-1
        * tag_name ELVGTNS1:ERGF01TIS


    """
    # backwash_rgf_df = GACCollections.backwash_rgf_df
    bw_due_tags = GACCollections.bw_due_tags.get(selected_installation, None)
    n_assets = GACCollections.num_gacs[selected_installation]

    if bw_due_tags is None:
        return "", {'background-color': 'grey'}

    # NOTE no thresholds needed for any site according to matrix v10
    bw_due_df = query_tags_conditional(bw_due_tags,
                                       collection='sensors',
                                       latest_only=True,
                                       reference_time=current_datetime)
    if len(bw_due_df) == 0:
        return "no data", {'background-color': 'grey'}

    try:
        service_df = get_service_status(oos_tags=GACCollections.oos_tags[selected_installation],
                                        reference_time=current_datetime,
                                        service_func=assign_filtering_funcs[selected_installation])

        bw_due_df = bw_due_df.sort_values(by='tag_name').reset_index(drop=True)
        bw_due_df = pd.merge(bw_due_df, service_df, how='left', left_index=True, right_index=True)

    except Exception as e:
        bw_due_df['service_status'] = 'filtering'
        logger.warning('filt_gac_update_backwash_due: failed checking the filtering status')

    if selected_installation in preproc_bwdue_funcs:
        b_due = preproc_bwdue_funcs[selected_installation](bw_due_df)
    else:
        b_due = bw_due_df['measurement_value'].values.squeeze()

    return_str = "{}/{}".format(b_due, n_assets)

    return return_str, {}


@app.callback(output=[Output('filtration-gac__g1--b4-value', 'children'),
                      Output('filtration-gac__g1--b4', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_backwash_required(selected_installation, current_datetime):
    """
    updates the backwash required button

    GOODW rules: Look at headloss alarm tags with a High alarm value
        * tag_name: GOODWDA:RGFF_AV_RGF1HDLSSH
    HUBY rules:  Look at differential pressure 95% percentile >  => needs backwashing
        * tag_name: HUBYWTS1:RGF1_DP_SI
    ELVINGTON rules: filterbed cond >= 1.6
        * tag_name: ELVGTNS1:ERGF0107
    """

    n_assets = GACCollections.num_gacs[selected_installation]
    # if selected_installation not in button_availabilities['backwashrequired']:
    if selected_installation not in GACCollections.bw_req_tags:
        return "", {'background-color': 'grey'}

    # NOTE no thresholds required for any site according to matrix v10
    bwreq_tags = GACCollections.bw_req_tags[selected_installation]
    bw_req_df = query_tags_conditional(bwreq_tags,
                                       collection='alarms',
                                       latest_only=True,
                                       reference_time=current_datetime)

    if len(bw_req_df) == 0:
        return "no data", {'background-color': 'grey'}

    try:
        service_df = get_service_status(oos_tags=GACCollections.oos_tags[selected_installation],
                                        reference_time=current_datetime,
                                        service_func=assign_filtering_funcs[selected_installation])

        bw_req_df = bw_req_df.sort_values(by='tag_name').reset_index(drop=True)
        bw_req_df = pd.merge(bw_req_df, service_df, how='left', left_index=True, right_index=True)

    except Exception as e:
        bw_req_df['service_status'] = 'filtering'
        logger.warning('filt_gac_backwash_required: failed checking the filtering status')

    if selected_installation in preproc_bwreq_funcs:
        bw_req_df = preproc_bwreq_funcs[selected_installation](bw_req_df)

    bw_req = len(bw_req_df)
    return_str = "{}/{}".format(bw_req, n_assets)
    return return_str, {}


@app.callback(output=[Output('filtration-gac__g1--b5-value', 'children'),
                      Output('filtration-gac__g1--b5', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_filterbed_condition(selected_installation, current_datetime):
    """
    updates the backwash filterbed condition change button
    Filter Bed condition change. Number of RGFs in rapidly changing. Cat:RGF.
    SubCat: BedCond. Want to count based on ROC over an hour. Change     >=1.5 in an hour.
    No tag for Acomb or Huby. No alert colour.
    """
    # NA FOR GOODW OR HUBY
    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)
    tags = GACCollections.bedcond_tags.get(selected_installation, None)

    return "", {'background-color': 'grey'}


@app.callback(output=[Output('filtration-gac__g1--b6-value', 'children'),
                      Output('filtration-gac__g1--b6', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_backwash_excessqueue(selected_installation, current_datetime):
    """
    updates the excess queue for backwash button
    Excess queue for backwash. Number of in alarm state for Backwash. Cat:RGF. SubCat: BackWashQueueExcess.
     Want to all RGFs with Text “ALARM”. No tag for Huby.
     Acomb might refer to value of “IN EXCESS” for Subcategory BackWashQueue – Check.
      Elvington Colour: >3 Red, =3 Amber. No colour rule for Acom?
    """
    exc_queue_tags = GACCollections.excess_time_in_backwash_queue_status.get(selected_installation, None)

    if exc_queue_tags is None:
        return "", {"background-color": 'grey'}

    is_alarm = site_tag_config.loc[site_tag_config.tag_name.isin(exc_queue_tags), 'is_alarm'].unique()

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)
    if is_alarm:
        collection = 'alarms'
    else:
        collection = 'sensors'

    res = query_tags_conditional(tag_list=exc_queue_tags,
                                 reference_time=current_datetime,
                                 collection=collection,
                                 latest_only=True)

    if len(res) == 0:
        return "no data", {'background-color': 'grey'}

    try:
        service_df = get_service_status(oos_tags=GACCollections.oos_tags[selected_installation],
                                        reference_time=current_datetime,
                                        service_func=assign_filtering_funcs[selected_installation])

        res = res.sort_values(by='tag_name').reset_index(drop=True)
        res = pd.merge(res, service_df, how='left', left_index=True, right_index=True)

    except Exception as e:
        res['service_status'] = 'filtering'
        logger.warning('filt_gac_backwash_excessqueue: failed checking the filtering status')

    n_assets = len(exc_queue_tags)

    n_excess = preproc_bw_excqueue_funcs[selected_installation](res)
    return_str = "{}/{}".format(n_excess, n_assets)
    thresh = exc_queue_threshold.get(selected_installation, None)
    if thresh is not None:
        color = thresh_func(n_excess, thresh)
    else:
        color = colors_risk['neutral']

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-gac__g2--b1-value', 'children'),
                      Output('filtration-gac__g2--b1', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_uv254(selected_installation, current_datetime):
    """
    updates the uv254 button
    NA for acomb or huby
    """
    return "", {'background-color': 'grey'}


@app.callback(output=[Output('filtration-gac__g2--b2-value', 'children'),
                      Output('filtration-gac__g2--b2', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_particleflow(selected_installation, current_datetime):
    """
    updates the particle flow button
    ELVINGTON ONLY
    """

    # pflow_tags =GACCollections.pflow_tags.get(selected_installation)
    # if pflow_tags == 0:
    return "", {'background-color': 'grey'}


@app.callback(output=[Output('filtration-gac__g2--b3-value', 'children'),
                      Output('filtration-gac__g2--b3', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_particlecounter(selected_installation, current_datetime):
    """
    updates the particle counter button
    """

    # NO PCOUNTER TAGS FOR GOODW OR HUBY
    return "", {'background-color': 'grey'}


@app.callback(output=[Output('filtration-gac__g2--b4-value', 'children'),
                      Output('filtration-gac__g2--b4', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_outlet_flow(selected_installation, current_datetime):
    """
    updates the outlet flow button
    """

    outlet_flow_tags = GACCollections.outlet_flow_tags.get(selected_installation, None)

    if outlet_flow_tags is None:
        return "", {'background-color': 'grey'}

    val_df = query_tags_conditional(tag_list=outlet_flow_tags,
                                    reference_time=current_datetime,
                                    collection='sensors',
                                    latest_only=True)
    if len(val_df) == 0:
        return "no data", {'background-color': 'grey'}

    val = val_df['measurement_value'].values.squeeze()

    if selected_installation in preproc_outflow_funcs:
        val = preproc_outflow_funcs[selected_installation](val)
    return_str = "{} tcmd".format(np.round(val, DECIM_S))
    return return_str, {'background-color': 'white'}


@app.callback(output=[Output('filtration-gac__g2--b5t-value', 'children'),
                      Output('filtration-gac__g2--b5t', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_intlevel_a(selected_installation, current_datetime):
    """
    updates the int level A stream button

    elvington has two
    """

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    int_level_tag = GACCollections.inlet_channel_tags_a.get(selected_installation, None)

    if int_level_tag is None:
        return "", {'background-color': 'grey'}

    val_df = query_tags_conditional(tag_list=int_level_tag,
                                    reference_time=current_datetime,
                                    latest_only=True,
                                    collection='sensors')
    if len(val_df) == 0:
        return "data error", {'background-color': 'grey'}

    val = val_df['measurement_value'].values.squeeze()

    if selected_installation in preproc_inletlevel_funcs:
        val = preproc_inletlevel_funcs[selected_installation](val)

    thresh = inlet_lvl_thresh.get(selected_installation, None)

    if thresh is not None:
        color = thresh_func(val, thresh)
    else:
        color = colors_risk['neutral']

    uom = val_df['measurement_unit'].values.squeeze()

    return_str = "{} {}".format(np.round(val, DECIM_S), uom)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-gac__g2--b6t-value', 'children'),
                      Output('filtration-gac__g2--b6t', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_turbidity_a(selected_installation, current_datetime):
    """
    updates the turbidity stream a button
    """

    site_tag_config = GACCollections.site_tag_config

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    turbidity_tag = GACCollections.turbidity_tags_a.get(selected_installation, None)

    if turbidity_tag is None:
        return "", {'background-color': "grey"}

    uom = site_tag_config.loc[site_tag_config.tag_name == turbidity_tag, 'measurement_unit'].unique()[0]

    diff_res = get_value_diff_periods(tags=turbidity_tag,
                                      reference_time=current_datetime,
                                      periods=3)
    if len(diff_res) == 0:
        return "no data", {'background-color': 'grey'}

    thresholds = turbidity_thresh.get(selected_installation, None)
    if thresholds is not None:
        color = thresh_roc_cons(diff_res['{}_diff'.format(turbidity_tag)].values, thresholds)
    else:
        color = colors_risk['neutral']

    val = diff_res.iloc[-1][turbidity_tag]

    return_str = "{} {}".format(np.round(val, DECIM_S), uom)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-gac__g2--b7t-value', 'children'),
                      Output('filtration-gac__g2--b7t', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_backwash_pressure_a(selected_installation, current_datetime):
    """
    updates the backwash pressure stream a button
    """

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    backwash_pressure_a_tags = GACCollections.backwash_pressure_tags_a.get(selected_installation, None)

    if backwash_pressure_a_tags is None:
        return "", {'background-color': 'grey'}

    val_df = query_max_value(tag_list=backwash_pressure_a_tags,
                             reference_time=current_datetime,
                             value_cap=1.,
                             lookback=24,
                             collection='sensors')

    if len(val_df) == 0:
        return "data error", {'background-color': 'grey'}
    max_df = val_df.loc[val_df.measurement_value == val_df.measurement_value.max()]
    ts = max_df['measurement_timestamp'].dt.time.values[0].strftime("%H:%M")

    site_tag_config = GACCollections.site_tag_config
    uom = site_tag_config.loc[site_tag_config.tag_name.isin(backwash_pressure_a_tags),
                              'measurement_unit'].unique()[0].lower()
    val = val_df['measurement_value'].values.squeeze()

    return_str = "{} {}\n{}".format(np.round(val, DECIM_S), uom, ts)
    color = bw_press_thresh.get(selected_installation, colors_risk['neutral'])(val)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-gac__g2--b5b-value', 'children'),
                      Output('filtration-gac__g2--b5b', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_intlevel_b(selected_installation, current_datetime):
    """
    updates the intlevel b button
    """

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    int_level_tag = GACCollections.inlet_channel_tags_b.get(selected_installation, None)

    if int_level_tag is None:
        return "", {'background-color': 'grey'}

    val_df = query_tags_conditional(tag_list=int_level_tag,
                                    reference_time=current_datetime,
                                    latest_only=True,
                                    collection='sensors')
    if len(val_df) == 0:
        return "data error", {'background-color': 'grey'}

    val = val_df['measurement_value'].values.squeeze()

    uom = val_df['measurement_unit'].values.squeeze()

    color = inlet_lvl_thresh.get(selected_installation, colors_risk['neutral'])(val)

    return_str = "{} {}".format(np.round(val, DECIM_S), uom)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-gac__g2--b6b-value', 'children'),
                      Output('filtration-gac__g2--b6b', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_turbidity_b(selected_installation, current_datetime):
    """
    updates the turbidity b button
    """
    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    turbidity_tags = GACCollections.turbidity_tags_b.get(selected_installation, None)

    if turbidity_tags is None:
        return "", {'background-color': 'grey'}

    uom = site_tag_config.loc[site_tag_config.tag_name.isin(turbidity_tags), 'measurement_unit'].unique()[0]

    diff_res = get_value_diff_periods(tags=turbidity_tags,
                                      reference_time=current_datetime,
                                      periods=3)
    if len(diff_res) == 0:
        return "data error", {'background-color': 'grey'}

    color = turbidity_thresh.get(selected_installation, colors_risk['neutral'])(diff_res)

    val = diff_res.iloc[-1][turbidity_tags].values.squeeze()
    return_str = "{} {}".format(np.round(val, DECIM_S), uom)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-gac__g2--b7b-value', 'children'),
                      Output('filtration-gac__g2--b7b', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_backwash_pressure_b(selected_installation, current_datetime):
    """
    updates the backwash pressure stream b button
    """
    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    backwash_pressure_b_tags = GACCollections.backwash_pressure_tags_b.get(selected_installation, None)

    if backwash_pressure_b_tags is None:
        return "", {'background-color': 'grey'}

    val_df = query_max_value(tag_list=backwash_pressure_b_tags,
                             reference_time=current_datetime,
                             lookback=24,
                             collection='sensors')

    if len(val_df) == 0:
        return "data error", {'background-color': 'grey'}

    val = preproc_bwpressure_funcs.get(selected_installation,
                                       val_df['measurement_value'])(val_df)

    site_tag_config = GACCollections.site_tag_config
    uom = site_tag_config.loc[site_tag_config.tag_name.isin(backwash_pressure_b_tags),
                              'measurement_unit'].unique()[0].lower()

    color = bw_press_thresh.get(selected_installation, colors_risk['neutral'])(val)

    return_str = "{} {}".format(np.round(val, DECIM_S), uom)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-gac__g2--b8-value', 'children'),
                      Output('filtration-gac__g2--b8', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_gac_backwash_flow(selected_installation, current_datetime):
    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    backwash_flow_tags = GACCollections.backwash_flow.get(selected_installation, None)

    if backwash_flow_tags is None:
        return "", {'background-color': 'grey'}

    val_df = query_max_value(tag_list=backwash_flow_tags,
                             reference_time=current_datetime,
                             # value_cap=1.,
                             lookback=24,
                             collection='sensors')

    if len(val_df) == 0:
        return "data error", {'background-color': 'grey'}
    max_df = val_df.loc[val_df.measurement_value == val_df.measurement_value.max()]
    ts = max_df['measurement_timestamp'].dt.time.values[0].strftime("%H:%M")
    val = max_df['measurement_value'].values.squeeze()

    if selected_installation in preproc_bwflow_funcs:
        val = preproc_bwflow_funcs[selected_installation](val)

    uom = max_df['measurement_unit'].values.squeeze()

    thresh = bw_flow_thresh.get(selected_installation)
    if thresh is not None:
        color = thresh_func(val, thresh)
    else:
        color = colors_risk['neutral']

    return_str = "{} {}\n @{}".format(np.round(val, DECIM_S), uom, ts)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filt-gac-datastore', 'data'),
                      Output('filt-gac-datastore-rocs', 'data')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')])
def filt_gac_update_datastore(selected_installation, current_datetime):
    """
    One row per RGF found for the site. Sequence by RGF number.
    Show one column for all possible RGF value (take superset from all sites) and populate where we can. Each cell coloured according to provided rules.

    """

    # need to fetch all the RGF assets for the selected installation

    gac_tags = GACCollections.individual_tag_df
    relevant_tags = gac_tags.loc[gac_tags.site_name == selected_installation]
    relevant_tags.set_index('GAC', inplace=True)
    new_df = []
    rocs_dfs = []
    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    new_df, rocs_df = update_datastore_df(collection_obj=GACCollections,
                                          asset_type='GAC',
                                          selected_installation=selected_installation,
                                          reference_time=current_datetime,
                                          postprocess_df_funcs=postprocess_df_funcs)

    ts_now_str = datetime.strftime(current_datetime, '%Y-%m-%d %H:%M')

    hash_str_data = ts_now_str + selected_installation + 'data'
    hash_str_rocs = ts_now_str + selected_installation + 'rocs'

    hash_code_data = hashlib.sha1(hash_str_data.encode()).hexdigest()
    hash_code_rocs = hashlib.sha1(hash_str_rocs.encode()).hexdigest()

    filtration_cacher.store('filt-gac-datastore-rocs', hash_code_rocs, rocs_df, current_datetime)
    filtration_cacher.store('filt-gac-datastore', hash_code_data, new_df, current_datetime)

    return hash_code_data, hash_code_rocs


@app.callback(
        output=Output('filt-gac-datatable', 'data'),
        inputs=[Input('filt-gac-datatable', "page_current"),
                Input('filt-gac-datatable', "page_size")],
        state=[State('filt-gac-datastore', 'data')])
def filt_gac_navigate_readings_table(page_current, page_size, current_data):
    data = filtration_cacher.get('filt-gac-datatable', current_data)

    if data.empty:
        return list()

    return data.iloc[
           page_current * page_size:(page_current + 1) * page_size
           ].to_dict(orient='records')


@app.callback(output=Output('filt-gac-tablediv', 'children'),
              inputs=[Input('filt-gac-datastore', 'data'),
                      Input('filt-gac-datastore-rocs', 'data')],
              state=[State("nav-selected-installation", 'data'),
                     State('nav-datetime', 'data')]
              )
def update_filt_gac_datatable(input_key_data, input_key_rocs,
                              selected_installation, current_datetime):
    data = filtration_cacher.get('filt-gac-tablediv', input_key_data)
    rocs_data = filtration_cacher.get('filt-gac-tablediv', input_key_rocs)

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    if len(data) == 0:
        table = html.H2("No data found for specified time period")
    else:

        style_cond_queries, style_cond_header = individual_thresholds(installation=selected_installation,
                                                                      df=data,
                                                                      current_datetime=current_datetime,
                                                                      roc_df=rocs_data)

        table = make_table(df=data,
                           page_size=20,
                           style_cell={'font-size': '12px'},
                           id='filt-gac-datatable',
                           styledata_cond=style_cond_queries,
                           style_header_cond=style_cond_header,
                           title=None,
                           custom_height_multiplier=60)
    return table
