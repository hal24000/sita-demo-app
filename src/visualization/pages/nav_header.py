"""
nav header
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from visualization.app_def import app
import dash_daq as daq
from datetime import datetime
from data.data_utils import convert_timezone
from data.collections_object import ProjectCollections, DATETIME_OVERRIDE
from data.risk_calculation import calculate_sensor_risk
import numpy as np
import re
import logging
from data.project_loggers import dash_logger
from data.pandas_cacher import BrainHolder, hash_query

logger = logging.getLogger(dash_logger)
tz_london = ProjectCollections.tz_london
tz_utc = ProjectCollections.tz_utc

import pandas as pd

risk_store_cacher = BrainHolder(client_brains=['risks', 'flows'],
                                # keep_period=60,
                                namespace="risk-nav")

pages = {
    'map':                              'Site Risk Map',
    'summary':                          'Water Treatment Facility Summary',
    'availability-exceptions':          'Equipment Availability Exceptions',
    'failure-exceptions':               'Equipment Failure Exceptions',
    'bearing-temperature-exceptions':   'Bearing Temperature Exceptions',
    'motor-temperature-exceptions':     'Motor Temperature Exceptions',
    'plc-data':                         'PLC Data Issues',
    'plc-communication':                'PLC Communication Exceptions',
    'none-execution-maintenance-plans': 'Maintenance Exceptions',
    'water-quality-exceptions':         'Water Quality Exceptions',
    'water-quality-readings':           'Water Quality Readings',
    # 'filtration-rgf':                   'Filtration RGF',
    # 'filtration-gac':                   "Filtration GAC",
    'schematic-drilldown':              'Schematic Drilldown',
}

dropdown_options = [{'label': "{}".format(text), 'value':"{}".format(page)}
                    for page, text in pages.items()]

plants = ProjectCollections.avail_installations

content = html.Div(
    className="page__header",
    id='nav-header',
    children=[
        dcc.Store(id='nav-live-switch-prev', storage_type='memory', data=False),
        html.Div(
            className="nav__page-selector w-3",
            id="nav-page-dropdown-container",
            children=[
                dcc.Dropdown(
                    options=dropdown_options,
                    multi=False,
                    value="map",
                    id='links-dropdown',
                    clearable=False
                ),
            ],
        ),
        html.Div(
            className="nav__other-selector",
            children=[
                html.Div(
                    className="nav__live-switch d-inline-flex",
                    children=[
                        daq.BooleanSwitch(
                            id='nav-live-switch',
                            className="nav__live-switch--icon",
                            on=False,
                            label="Live",
                            labelPosition="left",
                            color="#5cb85c",
                            disabled=True
                        ),
                    ],
                ),
                html.Div(
                    className='nav__date-selector ml-16',
                    children=[
                        dcc.DatePickerSingle(
                            id='nav-datepicker',
                            className="nav__datepicker disabled",
                            min_date_allowed=datetime(2020, 1, 1),
                            max_date_allowed=datetime(2020, 5, 10),
                            display_format='DD / MM / YYYY',
                            date=DATETIME_OVERRIDE.date(),
                            style={},
                        ),
                        dcc.Store(id='nav-prev-interval', data=0),
                        dcc.Interval(
                                id='nav-interval',
                                interval= 300 * 1000,  # in milliseconds (300 seconds)
                                n_intervals=0
                        ),
                        dcc.Input(
                            id='nav-timepicker',
                            className="nav__timepicker ml-16 disabled",
                            style={"width": "90px"},
                            type='text',
                            pattern='^([01]\d|2[0-3]):([0-5]\d)$',
                            value=DATETIME_OVERRIDE.strftime(ProjectCollections.dashboard_time_format)
                        ),
                        html.Div(
                            className="nav__history-picker ml-16",
                            id="nav-historypicker",
                            children=[
                                html.Label(
                                    "History:",
                                    className="nav__history-picker--label mr-4",
                                ),
                                dcc.Input(
                                    className="nav__history-picker--body",
                                    id="nav-historypicker-input",
                                    type="number",
                                    value=10,
                                    min=1,
                                    max=30,
                                    step=1,
                                    debounce=True,
                                ),
                            ]
                        ),
                        html.Button(
                            id="nav-set-bttn",
                            className="nav__set--bttn",
                            value="SET",
                            children=["SET"]
                        ),
                    ],
                ),
            ],
        ),
        html.Div(
            className="nav__site-selector w-2",
            children=[
                dcc.Dropdown(
                    value=plants[0],
                    options=[{'value': name, 'label': name} for name in plants],
                    id='nav-plant-dd',
                    className="nav__input",
                    clearable=False
                ),
                dcc.Loading(id="loading-2",
                            children=[html.Div([html.Div(id="loading-output-2")])],
                            type="circle"),
                dcc.Store(id='nav-selected-installation', storage_type='memory', data=plants[0]),
                dcc.Store(id='nav-selected-page', storage_type='memory', data='map'),
                dcc.Store(id="nav-datetime", storage_type='memory'),
                dcc.Store(id='nav-date', storage_type='memory'), # store date-only so no update all the time when live
                dcc.Store(id="nav-history-days", storage_type='memory', data=7),
                dcc.Store(id="nav-flow-query", storage_type='memory'),
                dcc.Store(id="nav-risk-query", storage_type='memory'),
                dcc.Store(id="nav-data_filters", storage_type="local", data=None)
            ],
        ),
    ],
)


@app.callback(Output('nav-historypicker', 'style'), [Input('links-dropdown', 'value')])
def hide_graph(input):
    pages_using_history = [
            'bearing-temperature-exceptions',
            'motor-temperature-exceptions',
            'water-quality-exceptions',
            'water-quality-readings',
    ]
    if input in pages_using_history:
        return {'display': 'inline-block'}
    else:
        return {'display': 'none'}


@app.callback(Output('nav-selected-installation', 'data'),
              [Input('nav-plant-dd', 'value')])
def update_nav_plant(selected_plant):
    return selected_plant


@app.callback(output=[Output('url', 'pathname'),
                      Output('stored-session-url', 'data')],
              inputs=[Input('links-dropdown', 'value')])
def update_nav_page(selected_page):
    return selected_page, selected_page


@app.callback([Output('links-dropdown', 'value'),
               Output("nav-data_filters", 'data')
               ],
              [Input('summ-btngrp-1', 'n_clicks_timestamp'),
               Input('summ-btngrp-2', 'n_clicks_timestamp'),
               Input('summ-btngrp-3', 'n_clicks_timestamp'),
               Input('summ-btngrp-4', 'n_clicks_timestamp'),
               Input('summ-btngrp-5', 'n_clicks_timestamp'),
               Input('summ-btngrp-4', 'n_clicks_timestamp'),
               Input('summ-btngrp-7', 'n_clicks_timestamp'),
               Input('summ-btngrp-8', 'n_clicks_timestamp'),
               Input('summ-btngrp-9', 'n_clicks_timestamp'),
               Input('summ-btngrp-10', 'n_clicks_timestamp'),
               Input('summ-btngrp-11', 'n_clicks_timestamp'),
               Input('bttn-wq-excep', 'n_clicks_timestamp'),
               Input('bttn-wq-read', 'n_clicks_timestamp')
               ],
              [State('links-dropdown', 'value'),
               State('summ-wq-readings-ts-store', 'data')]
              )
def update_dd_selected(summ_btngrp_1,
                       summ_btngrp_2,
                       summ_btngrp_3,
                       summ_btngrp_4,
                       summ_btngrp_5,
                       summ_btngrp_6,
                       summ_btngrp_7,
                       summ_btngrp_8,
                       summ_btngrp_9,
                       summ_btngrp_10,
                       summ_btngrp_11,
                       bttn_wq_excep,
                       bttn_wq_read,
                       current_page_state,
                       wq_alert_state):
    """
    Updates the dropdown function by using the clicks on the alert buttons
    All of the alert buttons are used as inputs, e.g. any single key press will trigger this function
    the different number of clicks up to the point of the trigger are sorted and counted
    the highest click one was the

    Args:
        summ_btngrp_1 (timestamp): the time of the latest click on the pump failures button
        summ_btngrp_2 (timestamp): the time of the latest click on the pump availability button
        summ_btngrp_3 (timestamp): the time of the latest click on the eqip failures  button
        summ_btngrp_4 (timestamp): the time of the latest click on the equip failures button
        summ_btngrp_5 (timestamp): the time of the latest click on the motor temp button
        summ_btngrp_6 (timestamp): the time of the latest click on the bearing temp button
        summ_btngrp_7 (timestamp): the time of the latest click on the data issues button
        summ_btngrp_8 (timestamp): the time of the latest click on the comms issues button
        summ_btngrp_9 (timestamp): the time of the latest click on the maintenance button
        summ_btngrp_10 (timestamp): the time of the latest click on the Site Throughput Exceptions button
        summ_btngrp_11(timestamp): the time of the latest click on the Sampling Flow Exceptions button
        bttn_wq_excep (timestamp): the time of the latest click on the Water Quality exceptions button
        bttn_wq_read  (timestamp): the time of the latest click on the Water Quality readings button
        current_page_state (str): the current value of the dropdown (current page)
        wq_alert_state (dict): the current value of the detected water quality tags that are in alert

    Returns:
        page_set(str): the name of the selected page
        data_filters(dict): any specific data filters linked to the button that has been clicked
    """
    buttons = locals()
    current_page_state = buttons.pop("current_page_state")
    buttons = {k.replace("_", "-"): v for k, v in buttons.items()}

    for k, v in buttons.items():
        if v is None:
            buttons[k] = 0

    # Set the WQ filters
    wq_filters = wq_alert_state
    if wq_filters is not None:
        wq_filters['wq_alert_tags'] = wq_filters.pop('tags')

    pages_button_dict = {
            "summ-btngrp-1":  {"page":    'failure-exceptions',
                               "filters": {"equipType": ["Pump"]}
                               },
            "summ-btngrp-2":  {"page":    'availability-exceptions',
                               "filters": {"equipType": ["Pump"]}
                               },
            "summ-btngrp-3":  {"page":    'failure-exceptions',
                               "filters": {"equipType": ["OtherEquip"]}
                               },
            "summ-btngrp-4":  {"page":    'availability-exceptions',
                               "filters": {"equipType": ["OtherEquip"]}
                               },
            "summ-btngrp-5":  {"page": 'motor-temperature-exceptions'},
            "summ-btngrp-4":  {"page": 'bearing-temperature-exceptions'},
            "summ-btngrp-7":  {"page": 'data-issues'},
            "summ-btngrp-8":  {"page": 'comms-issues'},
            "summ-btngrp-9":  {"page": 'none-execution-maintenance-plans'},
            "summ-btngrp-10": {"page":    "schematic-drilldown",
                               "filters": {"tag_category": "Flow", "tag_subcategory": "Site Throughput"}
                               },
            "summ-btngrp-11": {"page":    "schematic-drilldown",
                               "filters": {"tag_category": "Flow", "tag_subcategory": "Sample Flow"}
                               },
            "bttn-wq-excep":  {"page":    "water-quality-exceptions",
                               "filters": wq_filters
                               },
            "bttn-wq-read":   {"page":    "water-quality-readings",
                               "filters": wq_filters
                               },
            "mappage-map":    {"page": "summary"},
    }

    button_triggered = [p for p in dash.callback_context.triggered][0]
    # Default to a blank filter set
    data_filters = {}
    if button_triggered.get('value') is None:
        page_set = current_page_state
    else:
        page_key = button_triggered.get('prop_id').split(".")[0].replace("_", "-")
        page_info = pages_button_dict.get(page_key)
        page_set = page_info.get("page")
        page_filters = page_info.get("filters")
        if page_filters is not None:
            data_filters.update(page_filters)

    return page_set, data_filters


@app.callback(output=Output("nav-datepicker", 'max_date_allowed'),
              inputs=[Input("nav-datetime", "data")])
def nav_update_max_datetime_allowed(current_datetime):
    """
    sets the maximum date allowed in the time picker
    """
    max_allowed = datetime(2020, 5, 10, tzinfo=tz_london)
    # logger.warning("nav_update_max_datetime_allowed:max allowed month: {m}, year {y}".format(m=max_allowed.month,y=max_allowed.year))
    return max_allowed


@app.callback(
        [Output("nav-datepicker", "date"), Output("nav-timepicker", "value"),
         Output("nav-datepicker", 'disabled'), Output("nav-timepicker", 'disabled'),
         Output("nav-set-bttn", "hidden"), Output('nav-live-switch-prev', 'data')],
        [Input("nav-live-switch", "on"), Input("nav-datetime", "data")],
        [State("nav-datetime", "data"),
         State('nav-live-switch-prev', 'data')]
)
def update_datetime_pickers(live, new_datetime, current_datetime, prev_live):
    """

    Args:
        live:
        new_datetime:
        current_datetime:

    Returns:

    """

    if live:
        datetime_used = datetime.utcnow()
        date_selector_disable = True
        time_selector_disable = True
        set_bttn_hide = True
    else:
        date_selector_disable = False
        time_selector_disable = False
        set_bttn_hide = False
        if new_datetime is None and current_datetime is not None:
            datetime_used = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)
            # .localize(tz_utc)
        elif new_datetime is not None:
            datetime_used = datetime.strptime(new_datetime, ProjectCollections.dashboard_datetime_format)
            # .astimezone(tz_utc)
        else:
           datetime_used = DATETIME_OVERRIDE

    datetime_used_london = convert_timezone(datetime_used, tz_utc, tz_london)

    date = datetime_used_london.date()
    time = datetime_used_london.strftime(ProjectCollections.dashboard_time_format)

    logger.debug(f"[{pd.Timestamp.utcnow()}] - Updating the date and time selectors to: {date} - {time}")
    return date, time, date_selector_disable, time_selector_disable, set_bttn_hide, live


@app.callback(output=Output('nav-interval', 'disabled'),
              inputs=[Input("nav-live-switch", 'on')])
def toggle_interval(switch_val):
    return not (switch_val)


@app.callback(output=Output("nav-prev-interval", 'data'),
              inputs=[Input('nav-interval', 'n_intervals')])
def update_prev_intervals(n_intervals):
    return n_intervals


@app.callback(output=Output('nav-history-days', 'data'),
              inputs=[Input('nav-historypicker-input', 'value')]
              )
def update_history_days(lookback_days):
    """"""
    if lookback_days is None:
        raise PreventUpdate
    logger.debug(f"[Setting lookback to {lookback_days}")
    return lookback_days


@app.callback(Output('nav-datetime', 'data'),
              [Input('nav-live-switch', 'on'),
               Input('nav-interval', 'n_intervals'),
               Input('nav-set-bttn', 'n_clicks_timestamp')],
              [State('nav-datepicker', 'date'),
               State('nav-timepicker', 'value'),
               State('nav-prev-interval', 'data'),
               State('nav-live-switch-prev', 'data')]
              )
def update_internal_datetime(live, n_intervals_current, set_bttn_clicks,
                             date, time, n_intervals_prev, prev_live):
    """

    Args:
        live:
        n_intervals_current:
        set_bttn_clicks:
        date:
        time:
        n_intervals_prev:

    Returns:

    """
    if prev_live and not live:
        raise PreventUpdate

    if re.match("^([01]\d|2[0-3]):([0-5]\d)$", time) is None:
        logger.info(f"[{pd.Timestamp.utcnow()}] - User input invalid time = {time}, setting time to 12:00")
        # time = "12:00"
        raise PreventUpdate

    if n_intervals_current is None:
        n_intervals_current = 0
    if n_intervals_prev is None:
        n_intervals_prev = 0

    if live:
        current_datetime = pd.Timestamp.utcnow()
    else:
        current_datetime = pd.to_datetime(f"{date} {time}", format="%Y-%m-%d %H:%M")
        current_datetime = current_datetime.tz_localize(tz_london).astimezone(tz_utc)

    logger.debug(f"[{pd.Timestamp.utcnow()}] - Triggered a change of datetime: Internal UTC = {current_datetime}")
    current_datetime_str = current_datetime.strftime(ProjectCollections.dashboard_datetime_format)
    return current_datetime_str


@app.callback(Output('nav-date', 'data'),
              Input('nav-datetime', 'data')
              )
def update_internal_date(active_datetime):
    """
    """
    current_datetime = pd.to_datetime(active_datetime, format=ProjectCollections.dashboard_datetime_format)
    current_date_str = current_datetime.strftime(ProjectCollections.dashboard_date_format)

    return current_date_str


@app.callback(output=Output('nav-risk-query', 'data'),
              inputs=[Input('nav-datetime', 'data')],
              state=[State('nav-risk-query', 'data')])
def calculate_risk_data(timestamp, current_key):
    """
    Calculate and store the risk data everytime the time changes.
    """

    start_time = pd.Timestamp.now()

    date_time = pd.to_datetime(timestamp, format=ProjectCollections.dashboard_datetime_format)
    tag_data = ProjectCollections.site_tag_config

    # get sensor tags - look at one of the USED columns for nonNaN values
    sensor_risk_tags = list(tag_data.loc[tag_data['lolo_used'].notna(), 'tag_name'].unique())

    if not sensor_risk_tags:
        sensor_risks = pd.DataFrame(columns=['tag_name', 'risk_msd'])
    else:
        # get sensor data
        query = {'tag_name':              {'$in': sensor_risk_tags},
                 'measurement_timestamp': {'$lte': date_time,
                                           '$gte': date_time - pd.Timedelta(minutes=30)}
                 }
        projection = {'_id': 0, 'measurement_timestamp': 1, 'tag_name': 1, 'measurement_value': 1}

        sensor_risks = pd.DataFrame(list(ProjectCollections.fast_series_coll.find(query, projection)))

        if sensor_risks.empty:
            sensor_risks = pd.DataFrame(columns=['tag_name', 'risk_msd'])

        else:
            sensor_risks = pd.pivot_table(sensor_risks,
                                          index='measurement_timestamp',
                                          columns='tag_name',
                                          values='measurement_value')
            sensor_risks = sensor_risks.resample('15T', label='right').mean()

            # only use the relevant tags for sensor risk calculation
            sensor_risks = calculate_sensor_risk(
                site_tag_config=tag_data[tag_data['tag_name'].map(lambda x: x in sensor_risk_tags)],
                data_df=sensor_risks)

    cur_time = pd.Timestamp.now()

    # get alarm tags
    alarm_severity_map = pd.DataFrame(list(ProjectCollections.alarm_severity_coll.find({}, {'_id': 0})))

    # make all alarm values uppercase
    alarm_severity_map['alarm_value'] = alarm_severity_map['alarm_value'].map(lambda x: x.upper())
    alarm_severity_map = alarm_severity_map.set_index('alarm_value')['severity'].to_dict()

    query = {'occurrence_start_time': {'$lt': date_time},
             '$or':                   [{'occurrence_end_time': {'$type': 10}},  # type 10 is type null
                                       {'occurrence_end_time': {'$gte': date_time}}]
             }
    projection = {'_id': 0, 'tag_name': 1, 'measurement_value': 1}

    current_alarms = pd.DataFrame(list(ProjectCollections.fast_events_coll.find(query, projection)))

    if current_alarms.empty:
        current_alarm_map = {'unknown_tag': np.nan}

    else:
        # use uppercase alarms to calculate the risks
        current_alarms['measurement_value_uppercase'] = current_alarms['measurement_value'].map(lambda x: x.upper())
        current_alarms['severity'] = current_alarms['measurement_value_uppercase'].map(alarm_severity_map)
        current_alarms.drop('measurement_value_uppercase', axis=1, inplace=True)

        current_alarm_map = current_alarms.set_index('tag_name')['severity'].to_dict()

    alarm_risks = tag_data.copy()
    alarm_risks['severity'] = alarm_risks['tag_name'].map(current_alarm_map)
    alarm_risks['risk_alarm'] = alarm_risks['impact'] * alarm_risks['severity']

    # merge and store everything

    # risk_df is the same as the site_tag_config at the moment
    risk_df = alarm_risks.drop('severity', axis=1).copy()
    risk_df = pd.merge(risk_df, sensor_risks[['risk_msd', 'tag_name']], on='tag_name', how='left')

    risk_df['risk'] = risk_df[['risk_msd', 'risk_alarm']].max(axis=1)

    new_query = {'measurement_timestamp': {'$lte': date_time,
                                           '$gte': date_time - pd.Timedelta(minutes=30)},
                 'type':                  'risk'
                 }

    hashed_query = hash_query(new_query)

    if current_key is not None:
        success_ = risk_store_cacher.delete(client_id='risks', key=current_key)
        logger.info("Risks calculation: Deletion of previous key status: {}".format(success_))

    risk_store_cacher.store(client_id="risks",
                            key=hashed_query,
                            frame=risk_df,
                            time_created=pd.Timestamp.now())

    logger.info('Risks calculated in {0:.2f}s'.format((pd.Timestamp.now() - start_time).total_seconds()))

    return hashed_query


@app.callback(output=Output('nav-flow-query', 'data'),
              inputs=[Input('nav-datetime', 'data')],
              state=[State('nav-flow-query', 'data')])
def calculate_flow_data(timestamp, current_key):
    """
    Calculate and store the flow data everytime the time changes.
    """

    start_time = pd.Timestamp.now()

    date_time = pd.to_datetime(timestamp, format=ProjectCollections.dashboard_datetime_format)
    flow_tag_df = ProjectCollections.flow_tag_df
    flow_tags = list(flow_tag_df['tag_name'].unique())

    if not flow_tags:
        flows = pd.DataFrame(columns=['tag_name', 'site_name', 'functional_area',
                                      'process_area', 'flow_value', 'site_specific_schematic'])
    else:
        flows = []
        projection = {'_id': 0, 'tag_name': 1, 'measurement_value': 1}

        for tag in flow_tags:
            # get sensor data - use 60 minutes since some sensors only send an info every 30 minutes
            query = {'tag_name':              tag,
                     'measurement_timestamp': {'$lte': date_time}
                     }

            value = list(ProjectCollections.fast_series_coll.find(query,
                                                                  projection).sort([('measurement_timestamp',
                                                                                     -1)]).limit(1))

            try:
                flows.append(value[0])
            except IndexError:
                flows.append({'tag_name':          tag,
                              'measurement_value': np.nan})

        flows = pd.DataFrame(flows)

        if flows.empty:
            flows = pd.DataFrame(columns=['tag_name', 'site_name', 'functional_area',
                                          'process_area', 'flow_value', 'site_specific_schematic'])
        else:
            flows.rename(columns={'measurement_value': 'flow_value'}, inplace=True)
            flows = pd.merge(flow_tag_df, flows, on='tag_name', how='left')

            # rescale
            flows['flow_value'] = flows['flow_value'] * flows['mul_fact']
            flows = flows[
                ['tag_name', 'site_name', 'functional_area', 'process_area', 'flow_value', 'site_specific_schematic']]

    new_query = {'measurement_timestamp': {'$lte': date_time},
                 'type':                  'flow'
                 }

    hashed_query = hash_query(new_query)

    if current_key is not None:
        success_ = risk_store_cacher.delete(client_id='flows', key=current_key)
        logger.info("Risks calculation: Deletion of previous key status: {}".format(success_))

    risk_store_cacher.store(client_id="flows",
                            key=hashed_query,
                            frame=flows,
                            time_created=pd.Timestamp.now())

    logger.info('Flows calculated in {0:.2f}s'.format((pd.Timestamp.now() - start_time).total_seconds()))

    return hashed_query
