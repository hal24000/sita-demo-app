"""
includes sections 1.3, 1.4,, 1.5 and 1.6 from H1.2 document.
Use a navigation in the page to choose which types of exceptions to show. A dropdown possibly.
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===


"""
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from visualization.app_def import app
from visualization.plot_drawing.plotly_plots import make_table, draw_pie_chart_donut,\
    draw_bar_chart, make_proper_datetime_formatting
from data.summary_utils import (query_fail_unavail_new)
from data.collections_object import ProjectCollections
import numpy as np
import pandas as pd
import logging
import plotly.graph_objects as go
from data.project_loggers import dash_logger
from datetime import datetime
from data.pandas_cacher import BrainHolder
import hashlib


# Setup app logging
logger = logging.getLogger(dash_logger)

caching_object_fail = BrainHolder(client_brains=['exc-failure-tablestore',
                                            'exc-failure-datatable',
                                            'exc-failure-barchart',
                                            'exc-failure-piechart',
                                            'exc-failure-equipmentstore',
                                            'cleanup'],
                                  namespace='equipment-fail')


content = html.Div(
    className="equipment-availability-exceptions__page",
    children=[
        # Row 1: Filters
        html.Div(
            className="equipment-availability-exceptions__row-1 h-filter w-12 mb-16",
            children=[
                html.Div(
                    className="equipment-availability-exceptions__r1--filter-1 h-filter w-6 p-4 bg-white mr-16",
                    children=[
                        html.Div(
                            className="equipment-availability-exceptions__r1--filter-title title",
                            children=["Functional area"],
                        ),
                        html.Div(
                            className="equipment-availability-exceptions__r1--filter-body p-10",
                            children=[
                                dcc.Dropdown(id="exc-failure-fa-dd", multi=True),
                            ],
                        )
                    ],
                ),
                html.Div(
                    className="equipment-availability-exceptions__r1--filter-2 h-filter w-6 p-4 bg-white",
                    children=[
                        html.Div(
                            className="equipment-availability-exceptions__r1--filter-title title",
                            children=["Equipment type"],
                        ),
                        html.Div(
                            className="equipment-availability-exceptions__r1--filter-body p-10",
                            children=[
                                dcc.Dropdown(id='exc-failure-device-dd', multi=True),
                            ],
                        )
                    ],
                ),
            ],
        ),

        dcc.Loading(
            id='memory-exc-failure-memory',
            type=ProjectCollections.loading_animation,
            children=[
                # STORAGE AREA
                dcc.Store(storage_type='memory', id='exc-failure-tablestore'),
                dcc.Store(storage_type='memory', id='exc-failure-equipmentstore'),
                ]
        ),
        # Row 2: Equipment and Failed assets against available charts
        html.Div(
            className="equipment-availability-exceptions__row-2 mb-16 h-with-filter-1-row-of-2 w-12",
            children=[
                html.Div(
                    className="equipment-availability-exceptions__g1 h-with-filter-1-row-of-2 w-8 bg-white mr-16",
                    children=[
                        html.Div(
                            className="equipment-availability-exceptions__title title m-4",
                            children=["Equipment"],
                        ),
                        html.Div(
                            className="equipment-availability-exceptions__body",
                            children=[
                                dcc.Loading(
                                    id='loading-exc-failure-datatable',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        html.Div(
                                        id="exc-failure-datatable",
                                        className="equipment-availability-exceptions__g1--body h-with-filter-1-row-of-2__body",
                                    )]),
                            ],
                        )
                    ]
                ),
                html.Div(
                    className="equipment-availability-exceptions__g4 h-with-filter-1-row-of-2 w-4 bg-white",
                    children=[
                        html.Div(
                            className="equipment-availability-exceptions__title title m-4",
                            children=["Failed assets against available"],
                        ),
                        html.Div(
                            className="equipment-availability-exceptions__body",
                            children=[
                                dcc.Loading(
                                    id='loading-exc-failure-piechart',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        dcc.Graph(
                                            id="exc-failure-piechart",
                                            className="equipment-availability-exceptions__g4--body h-with-filter-1-row-of-2__body",
                                        )]),
                            ],
                        ),
                    ],
                ),
            ]
        ),

        # Row 3: Days Unavailable chart
        html.Div(
            className="equipment-availability-exceptions__row-3 w-12",
            children=[
                html.Div(
                    className="equipment-availability-exceptions__g5 h-with-filter-1-row-of-2 w-12 bg-white",
                    children=[
                        html.Div(
                            className="equipment-availability-exceptions__title title m-4",
                            children=["Days failed"],
                        ),
                        html.Div(
                            className="equipment-availability-exceptions__body",
                            children=[
                                dcc.Loading(
                                    id='loading-exc-failure-barchart',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        dcc.Graph(
                                            id="exc-failure-barchart",
                                            className="equipment-availability-exceptions__g5--body h-with-filter-1-row-of-2__body",
                                        )]),
                            ],
                        ),
                    ],
                ),
            ]
        ),
    ]
)


@app.callback(
        output=Output('exc-failure-datatable-el', 'data'),
        inputs=[Input('exc-failure-datatable-el', "page_current"),
                Input('exc-failure-datatable-el', "page_size")],
        state=[State('exc-failure-tablestore', 'data')])
def exc_failure_navigate_readings_table(page_current, page_size, current_data):
    """

    Args:
        page_current (str): the current page
        page_size (int): the number of rows in a table page
        current_data (list): the currently held data

    Returns:

    """
    current_df = caching_object_fail.get('exc-failure-tablestore', current_data)
    return current_df.iloc[
           page_current * page_size:(page_current + 1) * page_size
           ].to_dict(orient='records')


@app.callback(output=[Output('exc-failure-fa-dd', "value"),
                      Output('exc-failure-fa-dd', 'options')],
              inputs=[
                      Input('nav-selected-installation', 'data'),
              ])
def update_exc_fail_fa_dd(site):
    conf = ProjectCollections.site_tag_config
    fas = list(conf.loc[conf.site_name == site, 'functional_area'].unique())

    fas = np.sort(fas)
    options = [
            {'label': val, 'value': val} for val in fas
    ]
    return fas, options


@app.callback(output=[Output('exc-failure-device-dd', "value"),
                      Output('exc-failure-device-dd', 'options')],
              inputs=[Input('nav-selected-installation', 'data'), ],
              state=[State("nav-data_filters", 'data')]
              )
def update_exc_fail_avail_devices_dd(site, data_filters):
    conf = ProjectCollections.site_tag_config
    # devices = list(conf.loc[conf.site_name == site, 'tag_category'].unique())
    devices = ["Pump", "OtherEquip"]
    options = [
            {'label': val, 'value': val} for val in devices
    ]
    if data_filters is not None:
        equip_type = data_filters.get("equipType")
    else:
        equip_type = None
    if equip_type is None:
        default_value = ['Pump']
    else:
        default_value = equip_type

    return default_value, options


@app.callback(output=Output('exc-failure-datatable', "children"),
              inputs=[
                      Input('exc-failure-tablestore', 'data'),
              ])
def update_fail_exc_datatable(input_data):
    df_store = caching_object_fail.get('exc-failure-datatable', input_data)
    # df_store = pd.DataFrame(df_store)
    if len(df_store) == 0:
        return make_table(pd.DataFrame(), 6, 'exc-failure-datatable-el', 'No data found')
    try:
        df_store.drop(['tag_short_description'], axis=1, inplace=True)

    except:
        pass
    style_data = []

    table_readings = make_table(df_store, 6, 'exc-failure-datatable-el', "Failure exceptions",
                                styledata_cond=style_data,
                                numeric_columns=['Days Failed'], decimals_displayed=3)

    return table_readings


@app.callback(output=Output('exc-failure-barchart', "figure"),
              inputs=[
                      Input('exc-failure-tablestore', 'data'),
              ],
              state=[State('nav-datetime', 'data')])
def update_fail_exc_bar_chart(current_data, current_datetime):
    df_store = caching_object_fail.get("exc-failure-barchart", current_data)
    # df_store = pd.DataFrame(df_store)
    if len(df_store) == 0:
        return go.Figure(layout={'title': 'No failed equipment found'})

    df_store = df_store.loc[df_store['Days Failed'] >= 0]

    bar_chart_avail = draw_bar_chart(df_store, 'tag_short_description', 'Days Failed')

    return bar_chart_avail


@app.callback(output=Output('exc-failure-piechart', "figure"),
              inputs=[
                      Input('exc-failure-tablestore', 'data'),

              ],
              state=[State('exc-failure-equipmentstore', 'data')])
def update_fail_exc_piechart(input_data, relevant_equipment):
    relevant_equipment = caching_object_fail.get('exc-failure-piechart', relevant_equipment)
    df_store = caching_object_fail.get('exc-failure-piechart', input_data)

    # df_store = pd.DataFrame(df_store)
    if len(df_store) == 0:
        return go.Figure(layout={'title': 'No data found'})
    num_unavail = len(df_store.loc[df_store['Days Failed'] >= 0])

    pie_chart_exc_avail = draw_pie_chart_donut([num_unavail, len(relevant_equipment)],
                                               ['Failed Equipment', 'Total Equipment'],
                                               args={'title': ""}, pct=False)
    return pie_chart_exc_avail


@app.callback(
        output=[Output('exc-failure-tablestore', 'data'),
                Output('exc-failure-equipmentstore', 'data')],
        inputs=[
                Input("nav-datetime", 'data'),
                Input('exc-failure-fa-dd', 'value'),
                Input('exc-failure-device-dd', 'value')
        ],
        state=[State('nav-selected-installation', 'data'),
               State('nav-live-switch', 'on')]
)
def exc_fail_update_datastore(current_datetime,
                              selected_func_areas,
                              selected_devices,
                              selected_installation,
                              live
                              ):
    site_conf = ProjectCollections.site_tag_config

    tag_subcat_filter = ["status", "fail status"]

    search_terms_fail = ["Failed", "FAILED"]
    selected_devices = tuple(selected_devices)
    selected_func_areas = tuple(selected_func_areas)

    equipment_list = list(site_conf.loc[(site_conf.site_name == selected_installation) &
                                        (site_conf.tag_category.isin(selected_devices)) &
                                        (site_conf.tag_subcategory.str.lower().isin(tag_subcat_filter)) &
                                        (site_conf.functional_area.isin(selected_func_areas)), 'tag_name'].unique())

    # data_collection = ProjectCollections.get_mongo_collection(collection='alarms',lookback=90)

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    ts_now_str = datetime.strftime(current_datetime, '%Y-%m-%d %H:%M')

    hash_code_table = hashlib.sha1(ts_now_str.encode()).hexdigest()
    hash_code_eq = hashlib.sha1(str(equipment_list).encode()).hexdigest()

    if len(selected_func_areas) > 0 and len(selected_devices) > 0:
        # df_merged = query_failed_unavail(equipment_list,current_datetime,
        #                                  search_terms_ok,search_terms_fail)
        df_merged = query_fail_unavail_new(equipment_list=equipment_list,
                                           search_terms_fail=search_terms_fail,
                                           current_datetime=current_datetime,
                                           live=live,
                                           failures=True)

        if len(df_merged) == 0:
            # return df_merged
            caching_object_fail.store('exc-failure-tablestore', hash_code_table, df_merged,
                                      time_created=current_datetime)
            caching_object_fail.store('exc-failure-equipmentstore', hash_code_eq, equipment_list,
                                      time_created=current_datetime)

            return hash_code_table, hash_code_eq

        df_merged.sort_values(by='Days Failed', ascending=False, inplace=True)


    else:
        df_merged = pd.DataFrame()
    try:
        df_merged.drop(["fabricated_key", 'site_name'], axis=1, inplace=True)
    except:
        pass

    df_merged = make_proper_datetime_formatting(df_merged)

    for col in df_merged.columns:
        df_merged[col] = df_merged[col].astype(str)

    df_merged['Days Failed'] = df_merged['Days Failed'].astype(int)

    caching_object_fail.store('exc-failure-tablestore', hash_code_table, df_merged, time_created=current_datetime)
    caching_object_fail.store('exc-failure-equipmentstore', hash_code_eq, equipment_list, time_created=current_datetime)

    return hash_code_table, hash_code_eq
