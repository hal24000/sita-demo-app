"""
code for making those triple/double button groups in the summary page
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import dash_html_components as html


def make_triple_button_group(ids, names):
        
    triple_button_group = html.Div(
            className="btn-group",
            role="group",
            children=[
                    html.Div(className="button-h__group",
                             children=[
                                     html.Div(
                                             id=ids[0],
                                             className="button-h",
                                             children=[
                                                     html.Div(names[0], className="button-h__title", ),
                                                     html.Div("0", id="{}-content".format(ids[0]),
                                                              className="button-h__value")
                                             ]
                                     ),
                                     html.Div(className="button-q__group",
                                              children=[
                                                      html.Div(
                                                              id=ids[1],
                                                              className="button-q button-q__left",
                                                              children=[
                                                                      html.Div(names[1], className="button-q__title"),
                                                                      html.Div("0", id="{}-content".format(ids[1]),
                                                                               className="button-q__value")
                                                              ]
                                                      ),
                                                      html.Div(
                                                              id=ids[2],
                                                              className="button-q button-q__right",
                                                              children=[
                                                                      html.Div(names[2], className="button-q__title"),
                                                                      html.Div("0", id="{}-content".format(ids[2]),
                                                                               className="button-q__value")
                                                              ]
                                                      )
                                              ]
                                              )
                             ]
                             )
            ])
    return triple_button_group


def make_horizontal_button_group(ids, names):
    horizontal_button_group = html.Div(
            className="btn-group-vertical",
            children=[
                    html.Div(
                            [
                                    html.Div(
                                            [
                                                    html.Div(
                                                            names[0],
                                                            id=ids[0],
                                                            className="btn btn-hrec-lg"
                                                    ),
                                            ],
                                            className='col'
                                    )
                            ],
                            className='row'),
                    html.Div(
                            [
                                    html.Div(
                                            [
                                                    html.Div(
                                                            names[1],
                                                            id=ids[1],
                                                            className="btn btn-hrec-lg"
                                                    )
                                            ],
                                            className='col'
                                    )
                            ],
                            className='row'
                    ),
            ])
    return horizontal_button_group
