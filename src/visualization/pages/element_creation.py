"""
Functions to create various custom elements
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===


"""

import dash_daq as daq
import dash_html_components as html
import dash_bootstrap_components as dbc


def create_nested_button_dropdown(config_dict: dict,
                                  class_name: str = '',
                                  dropdown_id: str = None):
    """
    Create a nested dropdown menu

    Args:
        config_dict (dict): configuration of the dropdown, {functional_area1: [list_of_metrics1], ...}
        class_name (str): class name for CSS styling
        dropdown_id (str): element name for callbacks

    Returns:
         DropdownMenu
    """

    element_list = []

    for title in config_dict:
        # add functional area header
        element_list.append(dbc.DropdownMenuItem(title, header=True))

        for metric in config_dict[title]:
            # create the content
            element_list.append(dbc.DropdownMenuItem(children=[
                    html.Div(children=[
                            html.Div([daq.BooleanSwitch(
                                    id='switch_{}'.format('_'.join(metric.split())),
                                    on=True,
                            )],
                                    className='w-1 p-4'),
                            html.Div(metric,
                                     className='w-1 p-4'),
                    ],
                            className='w-3')
            ])
            )
        # create a divider
        element_list.append(dbc.DropdownMenuItem(divider=True))

    # remove the final divider
    element_list = element_list[:-1]

    dropdown = dbc.DropdownMenu(
            children=element_list,
            className=class_name,
            # TODO: Remove hardcoded default id
            id=dropdown_id if dropdown_id is not None else 'wq_nested_dropdown',
            label='All metrics shown'
    )

    return dropdown
