"""
None Execution Maintenance plans (none_execution_maintenance_plans -> nemp)
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import logging
import numpy as np
import pandas as pd
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from plotly import colors
import plotly.graph_objects as go
from visualization.app_def import app
from data.pandas_cacher import BrainHolder, hash_query
from visualization.plot_drawing.plotly_plots import make_table, draw_pie_chart
from data.water_quality_utils import make_empty_plot, make_empty_table_dataframe
from data.project_loggers import dash_logger
from data.collections_object import ProjectCollections

logger = logging.getLogger(dash_logger)
site_tag_config = ProjectCollections.site_tag_config

nemp_store_cacher = BrainHolder(client_brains=['cleanup', 'load_nemp_data',
                                               'make_nemp_table',
                                               'make_maintenance_pie_chart'],
                                namespace="nemp-storage",
                                cleanupfreq=300)

content = html.Div(
    className="nemp__page",
    children=[
        # First Row: Functional area Filter
        html.Div(
            className="nemp__r1 h-filter p-4 bg-white w-12 mb-16",
            children=[
                dcc.Loading(
                    id='loading-query-store',
                    type=ProjectCollections.loading_animation,
                    children=[
                        dcc.Store(id='nemp-query-store', storage_type='memory')
                    ]
                ),
                html.Div(
                    className="nemp-filter__title title mb-4",
                    children=["Functional area"],
                ),
                html.Div(
                    className="nemp-filter__body",
                    children=[
                        dcc.Dropdown(
                            id="nemp-dd-func-areas",
                            className="nemp__r1--functional-filter",
                            multi=True
                        ),
                    ],
                ),
                html.Div(
                    className='nemp-filter__title title mb-4 mt-16',
                    children=['Maintenance type']
                ),
                html.Div(
                    className="nemp-filter__body",
                    children=[
                        dcc.RadioItems(
                            id='nemp-type-radio',
                            options=[
                                {'label': 'All maintenance', 'value': 'all'},
                                {'label': 'Central MEICA Maintenance', 'value': 'meica'},
                                {'label': 'Local Operational Maintenance', 'value': 'local'}
                            ],
                            value='all',
                            labelStyle={'display': 'inline-block'},
                            inputClassName="nemp__filter--radio"
                        )
                    ]
                )
            ],
        ),
        # Second Row: Planned Maintenance Over 365 Days - Process Area && Maintenance Orders by Status
        html.Div(
            className="nemp__r2 w-12",
            children=[
                html.Div(
                    className="nemp__r2--days w-9 h-with-filter-1-row-of-2 bg-white mr-16",
                    children=[
                        html.Div(
                                className="nemp-process__title title p-4",
                                children=["Maintenance status over the past 365 days"],
                        ),
                        dcc.Loading(
                            id='loading-none-execution-chart',
                            type=ProjectCollections.loading_animation,
                            children=[
                                html.Div(
                                    className="nemp-process__body h-with-filter-1-row-of-2__body",
                                    children=[
                                        dcc.Graph(
                                            id='nemp-bar-chart',
                                            className="none-execution-maintenance-plans__body",
                                            config={'displayModeBar': False}
                                        ),
                                    ],
                                )
                            ]
                        ),
                    ],
                ),
                html.Div(
                    className="nemp__r2--piechart w-3 h-with-filter-1-row-of-2 bg-white",
                    children=[
                        html.Div(
                            className="nemp-pie-chart__title title p-4",
                            children=["Maintenance Orders by Status"]
                        ),
                        dcc.Loading(
                            id='loading-nemp-piechart',
                            type=ProjectCollections.loading_animation,
                            children=[
                                dcc.Graph(
                                    id="nemp-pie-chart",
                                    className="nemp-pie-chart__body h-with-filter-1-row-of-2__body p-4",
                                    config={'displayModeBar': False}
                                )
                            ]
                        )
                    ]
                ),
            ],
        ),
        # Third Row: Planned Maintenance Over 365 Days - Asset
        html.Div(
            className="nemp__r3 w-12 bg-white h-with-filter-1-row-of-2 mt-16",
            children=[
                html.Div(
                    className="nemp__title title p-4",
                    children=["Maintenance data over the past 365 days"],
                ),
                html.Div(
                    className="nemp__body h-with-filter-1-row-of-2__body",
                    children=[
                        dcc.Loading(
                            id='loading-none-execution-maintenance-asset',
                            type=ProjectCollections.loading_animation,
                            children=[
                                html.Div(
                                    className="nemp__r3--body",
                                    id="nemp-table"
                                )]),
                    ],
                ),
            ],
        ),
    ]
)


@app.callback(output=Output("nemp-dd-func-areas", "options"),
              inputs=[Input("nav-selected-installation", "data")])
def update_nemp_dropdown(selected_installation):
    plant_df = site_tag_config[site_tag_config['site_name'] == selected_installation].copy()
    if plant_df.empty:
        return list()

    func_areas = sorted(list(plant_df['functional_area'].unique()))

    options = []
    for fa in func_areas:
        options.append({'label': fa, 'value': fa})

    return options


@app.callback(output=Output("nemp-dd-func-areas", "value"),
              inputs=[Input("nemp-dd-func-areas", "options")])
def update_nemp_dropdown(options):
    # pick all options
    func_areas = [fa['value'] for fa in options]

    return func_areas


@app.callback(output=Output('nemp-query-store', 'data'),
              inputs=[Input('nav-date', 'data')])
def load_nemp_data(active_date):
    """
    Load the data for the planned maintenance.
    """

    active_date = pd.to_datetime(active_date, format=ProjectCollections.dashboard_date_format)

    query = {'actual_release': {'$lte': active_date + pd.Timedelta(days=1),
                                '$gte': active_date - pd.Timedelta(days=364)}}
    projection = {'_id': 0}

    hashed_query = hash_query(query)

    nemp_data = nemp_store_cacher.get('make_nemp_table', hashed_query)

    if not nemp_data.empty:
        # we already have data
        return hashed_query

    cursor = ProjectCollections.maintenance_coll.find(query, projection)
    nemp_data = pd.DataFrame(list(cursor))

    if nemp_data.empty:
        nemp_store_cacher.store("load_nemp_data", hashed_query, nemp_data, active_date)

        return hashed_query

    ### DUPLICATES
    # TODO: think of a better aggregation! This ignores the work times
    order_counts = nemp_data['order_number'].value_counts().to_dict()

    # TODO: Column renaming
    completion_time = nemp_data.groupby('order_number')['planned_work_amount'].sum().to_dict()
    actual_time = nemp_data.groupby('order_number')['actual_work_amount'].sum().to_dict()

    nemp_data.drop_duplicates('order_number', keep='first', inplace=True)

    nemp_data['#tasks'] = nemp_data['order_number'].map(order_counts)
    nemp_data['planned_work_amount'] = nemp_data['order_number'].map(completion_time)
    nemp_data['actual_work_amount'] = nemp_data['order_number'].map(actual_time)

    ### DATA ENRICHMENT
    # functional area
    nemp_data = add_functional_area(nemp_data, site_tag_config)
    nemp_data = add_site_name(nemp_data, site_tag_config)

    # days worked
    nemp_data['active_duration'] = nemp_data.apply(lambda x: (min(active_date,
                                                                  x['technical_completion_date']) - x[
                                                                  'actual_release']).total_seconds() / 86400,
                                                   axis=1)

    # calculated status
    nemp_data.loc[(nemp_data['completion'] != 'Cancelled')
                  & (nemp_data['technical_completion_date'].notna()), 'completion'] = 'Completed'

    nemp_data.loc[nemp_data['technical_completion_date'] > active_date, 'completion'] = 'In-Progress'

    # OLD CALCULATION: uses frequency
    # nemp_data['expected_completion'] = nemp_data.apply(lambda x: x['actual_release'] \
    #                                                              + (pd.Timedelta(seconds=x['frequency_interval'])
    #                                                                 if not np.isnan(x['frequency_interval'])
    #                                                                 else pd.Timedelta(np.nan)),
    #                                                    axis=1)

    # NEW CALCULATION: Uses task duration. Pretty much everything will be overdue.
    nemp_data['expected_completion'] = nemp_data.apply(lambda x: x['actual_release'] \
                                                                 + (pd.Timedelta(minutes=x['planned_work_amount'])
                                                                    if not np.isnan(x['planned_work_amount'])
                                                                    else pd.Timedelta(np.nan)),
                                                       axis=1)

    nemp_data.loc[(nemp_data['completion'] == 'In-Progress')
                  & (nemp_data['expected_completion'].map(lambda x: x.ceil("D") < active_date)),
                  'completion'] = 'Overdue'
    nemp_data.loc[(nemp_data['completion'] == 'In-Progress')
                  & (nemp_data['expected_completion'].isnull()), 'completion'] = 'Unknown'
    nemp_data.loc[(nemp_data['completion'].map(lambda x: x in ['In-Progress', 'Overdue', 'Unknown']))
                  & (nemp_data['system_status'].map(lambda x: 'CRTD' in x)),
                  'completion'] = 'CRTD'

    nemp_data.drop('expected_completion', axis=1, inplace=True)

    nemp_store_cacher.store("load_nemp_data", hashed_query, nemp_data, active_date)

    return hashed_query


@app.callback(output=Output('nemp-table', 'children'),
              inputs=[Input('nemp-query-store', 'data'),
                      Input("nav-selected-installation", 'data'),
                      Input('nemp-type-radio', 'value'), ])
def make_nemp_table(hashed_query, location, maintenance_type):
    nemp_data = nemp_store_cacher.get('make_nemp_table', hashed_query).copy()

    if 'planned_work_amount' in nemp_data.columns:
        nemp_data.rename(columns={'planned_work_amount': 'Task duration (mins)'},
                         inplace=True)

    target_columns = ['functional_area', 'order_number', 'order_type', 'bsc_start',
                      'description', 'ecc_reference_number', 'functional_location_description',
                      'service_product_description', 'system_status', 'user_status', 'actual_release',
                      'actual_finish', 'Task duration (mins)', 'group_id', 'frequency_description',
                      'designation', 'completion', 'technical_completion_date', '#tasks']

    if nemp_data.empty:
        nemp_data = make_empty_table_dataframe(target_columns)

    else:
        if maintenance_type == 'all':
            nemp_data = nemp_data.loc[(nemp_data['site_name'] == location)
                                      & (nemp_data['completion'] != 'Completed'), target_columns].copy()
        elif maintenance_type == 'meica':
            nemp_data = nemp_data.loc[(nemp_data['site_name'] == location)
                                      & (nemp_data['group_id'].map(lambda x: str(x)[:3] != '111'))
                                      & (nemp_data['completion'] != 'Completed'), target_columns].copy()
        elif maintenance_type == 'local':
            nemp_data = nemp_data.loc[(nemp_data['site_name'] == location)
                                      & (nemp_data['group_id'].map(lambda x: str(x)[:3] == '111'))
                                      & (nemp_data['completion'] != 'Completed'), target_columns].copy()

    for time_col in ['bsc_start', 'actual_release', 'actual_finish', 'technical_completion_date']:
        nemp_data[time_col] = nemp_data[time_col].replace('', pd.NaT)
        nemp_data[time_col] = nemp_data[time_col].map(lambda x: x.strftime(ProjectCollections.dashboard_date_format) \
            if not pd.isnull(x) else '')

    # name the table something else than the parent element, duh!
    table_readings = make_table(nemp_data, 6, "nemp-dash-table", "Non Execution of Maintenance Plans",
                                numeric_columns=["order_number", '#tasks'],
                                decimals_displayed=3, page_action='native', filter_action='native')

    return table_readings


@app.callback(output=Output('nemp-pie-chart', 'figure'),
              inputs=[Input('nemp-query-store', 'data'),
                      Input("nav-selected-installation", 'data'),
                      Input("nemp-dd-func-areas", "value"),
                      Input("nemp-type-radio", 'value')])
def make_maintenance_pie_chart(hashed_query, location, functional_areas, maintenance_type):
    input_data = nemp_store_cacher.get('make_maintenance_pie_chart', hashed_query)
    if input_data.empty:
        fig = make_empty_plot(annotation='No data to report.')
        return fig

    if maintenance_type == 'meica':
        input_data = input_data.loc[input_data['group_id'].map(lambda x: str(x)[:3] != '111')]
    elif maintenance_type == 'local':
        input_data = input_data.loc[input_data['group_id'].map(lambda x: str(x)[:3] == '111')]

    input_data = pd.DataFrame(input_data.loc[(input_data['site_name'] == location)
                                             & (input_data['functional_area'].map(lambda x: x in functional_areas)),
                                             'completion'].value_counts()).reset_index()
    input_data.rename(columns={'index':      'Completion status',
                               'completion': 'Count'}, inplace=True)
    unique_statuses = list(input_data['Completion status'].unique())

    updated_pie_chart = draw_pie_chart(input_data, x='Completion status', y='Count',
                                       args=dict(color=unique_statuses,
                                                 color_discrete_map={'Completed':   'green',
                                                                     'Cancelled':   'red',
                                                                     'In-Progress': 'floralwhite',
                                                                     'Overdue':     'gold',
                                                                     'Unknown':     'grey',
                                                                     'CRTD':        'deepskyblue'},
                                                 hole=0.4))
    updated_pie_chart.update_layout(margin=dict(l=0, r=0, t=50, b=0, pad=0))

    updated_pie_chart.update_traces(
            hovertemplate='Completion status: %{label}<br>Occurences: %{value}<extra></extra>'
    )

    return updated_pie_chart


@app.callback(output=Output("nemp-bar-chart", "figure"),
              inputs=[Input('nemp-query-store', 'data'),
                      Input("nav-selected-installation", 'data'),
                      Input("nemp-dd-func-areas", "value"),
                      Input('nemp-type-radio', 'value')])
def make_maintenance_bar_chart(hashed_query, location, functional_areas, maintenance_type):
    input_data = nemp_store_cacher.get('make_maintenance_pie_chart', hashed_query)
    if input_data.empty:
        fig = make_empty_plot(annotation='No data to report.')
        return fig

    if maintenance_type == 'meica':
        input_data = input_data.loc[input_data['group_id'].map(lambda x: str(x)[:3] != '111')]
    elif maintenance_type == 'local':
        input_data = input_data.loc[input_data['group_id'].map(lambda x: str(x)[:3] == '111')]

    input_data = input_data[(input_data['site_name'] == location)
                            & (input_data['functional_area'].map(lambda x: x in functional_areas))]

    fig = make_stacked_grouped_bar_chart(input_data)

    return fig


# helper functions to be refactored out

def add_functional_area(maintenance_data: pd.DataFrame, site_tag_config: pd.DataFrame) -> pd.DataFrame:
    """
    Enrich the maintenance data by functional area.

    NOTE: currently uses SAI that is not unique and can span multiple FAs, take the most common one.

    TODO: figure out a better logic
    """

    # per site and
    fa_mapping = pd.DataFrame(site_tag_config.groupby('common_reference')['functional_area'].value_counts())
    fa_mapping.rename(columns={'functional_area': 'count'}, inplace=True)
    fa_mapping = fa_mapping.reset_index().drop_duplicates('common_reference', keep='first')
    fa_mapping = fa_mapping.set_index('common_reference')['functional_area'].to_dict()

    internal_data = maintenance_data.copy()
    internal_data['functional_area'] = internal_data['ecc_reference_number'].map(fa_mapping)

    return internal_data


def add_site_name(maintenance_data: pd.DataFrame, site_tag_config: pd.DataFrame) -> pd.DataFrame:
    """
    Enrich the maintenance data by functional area.

    NOTE: currently uses SAI that is not unique and can span multiple FAs, take the most common one.

    TODO: figure out a better logic
    """

    # per site and
    site_mapping = pd.DataFrame(site_tag_config.groupby('common_reference')['site_name'].value_counts())
    site_mapping.rename(columns={'site_name': 'count'}, inplace=True)
    site_mapping = site_mapping.reset_index().drop_duplicates('common_reference', keep='first')
    site_mapping = site_mapping.set_index('common_reference')['site_name'].to_dict()

    internal_data = maintenance_data.copy()
    internal_data['site_name'] = internal_data['ecc_reference_number'].map(site_mapping)

    return internal_data


def make_stacked_grouped_bar_chart(input_df: pd.DataFrame,
                                   fa_column: str = 'functional_area',
                                   des_column: str = 'designation',
                                   status_column: str = 'completion'):
    """
    Create a bar plot that combines stacked and non stacked bars.

    Args:
        input_df (pd.DataFrame): input data
        fa_column (str): name of the column with the functional areas (default: 'functional_area')
        des_column (str): name of the column with designation (default: 'designation')
        status_column (str): name of the column with the completion status (default: 'completion')

    Returns:
        plotly figure
    """

    if input_df.empty:
        return make_empty_plot(annotation='No data to report.')

    bar_df = input_df.groupby([fa_column, des_column, status_column])[
        'order_number'].count().reset_index().rename(columns={'order_number': 'count'})

    # bar_df = bar_df[bar_df[status_column] != 'Completed']
    bar_df = bar_df.sort_values(by=fa_column)

    sorted_fa = sorted(bar_df[fa_column].unique().tolist())
    sorted_des = sorted(bar_df[des_column].unique().tolist())
    sorted_st = sorted(bar_df[status_column].unique().tolist())

    # TODO: unhardcode colours
    des_cmap = {}

    for i, des in enumerate(sorted_des):
        des_cmap[des] = colors.qualitative.D3[i % len(colors.qualitative.D3)]

    data = []

    bar_totals = bar_df.groupby([fa_column, status_column])['count'].sum().unstack()

    for i, status in enumerate(sorted_st):
        data.append(
                go.Bar(
                        name='background_{}'.format(status),
                        x=bar_totals.index,
                        y=bar_totals[status],
                        offsetgroup=i,
                        marker_color='rgba(100,100,100,0.)',
                        showlegend=False,
                        hoverinfo='skip'
                )
        )

    known_des = set()

    for state in sorted_st:
        old_ys = pd.Series(0., index=sorted_fa)
        for des in sorted_des:
            tmp_df = bar_df.loc[(bar_df[des_column] == des)
                                & (bar_df[status_column] == state)].copy()
            if tmp_df.empty:
                continue

            tmp_df.set_index(fa_column, inplace=True)
            tmp_df.sort_index(inplace=True)

            xs = tmp_df.index
            ys = tmp_df['count'].fillna(0).values

            data.append(
                    go.Bar(
                            name=des,
                            x=xs,
                            y=ys,
                            offsetgroup=sorted_st.index(state),
                            base=old_ys.loc[xs],
                            meta=des,
                            hovertext=['{0} ({1}): {2:.0f}'.format(des, state, i) for i in ys],
                            showlegend=des not in known_des,
                            hovertemplate="%{hovertext}<extra></extra>",
                            marker_color=des_cmap[des]
                    )
            )

            old_ys = old_ys + tmp_df['count'].reindex_like(old_ys).fillna(0)
            known_des.add(des)

    for i, status in enumerate(sorted_st):
        data.append(
                go.Bar(
                        name='background_{}'.format(status),
                        x=sorted_fa,
                        y=[0] * len(sorted_fa),
                        offsetgroup=i,
                        text=status,
                        textposition='outside',
                        textangle=-90,
                        # textfont=dict(size=50),
                        marker_color='rgba(100,100,100,0.)',
                        showlegend=False,
                        hoverinfo='skip'
                )
        )

    fig = go.Figure(
            data=data
    )

    fig.update_layout(hovermode='x', margin=dict(l=0, r=0, t=0, b=25, pad=0),
                      height=271)

    return fig
