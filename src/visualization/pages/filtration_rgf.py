"""
filtration-rgf page
includes the callbacks and layout for the RGF page
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""

import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from visualization.app_def import app
from data.collections_object import ProjectCollections
from visualization.plot_drawing.plotly_plots import make_table
from data.summary_utils import (query_tags_conditional,
                                query_max_value)

from data.rgf_thresholds import (oos_thresholds,
                                 backwash_pressure_thresholds,
                                 backwash_thresholds,
                                 service_hour_thresholds,
                                 get_pflow_counter_risk_color,
                                 get_pflow_risk_color,
                                 uv254_roc_threshold,
                                 inlet_level_thresholds,
                                 get_queue_excess_thresholds,
                                 turbidity_thresholds,
                                 backwash_flow_tresholds,
                                 individual_thresholds
                                 )
# import data.RGFCollections

from data.rgf_collections import RGFCollections
from data.rgf_gac_utils import (get_value_diff_periods,
                                update_datastore_df,
                                thresh_func,
                                get_service_status)
from data.rgf_functions import (backwash_req_funcs,
                                excess_queue_funcs,
                                assign_filtering_funcs,
                                preprocess_outlet_flow_funcs,
                                preprocess_inlet_level_funcs,
                                preprocess_backwashing_funcs,
                                preprocess_backwash_pressure_funcs,
                                postprocess_df_funcs,
                                preprocess_backwashflow_funcs)
from data.load_data import colors_risk
import numpy as np
import pandas as pd
from datetime import datetime
import logging
from datetime import timedelta
from data.project_loggers import dash_logger
from data.pandas_cacher import BrainHolder
import hashlib

site_tag_config = ProjectCollections.site_tag_config

logger = logging.getLogger(dash_logger)

DECIM_S= RGFCollections.DECIM_S
DECIM_L = RGFCollections.DECIM_L

filtration_cacher = BrainHolder(client_brains=['filt-datastore',
                                               'filt-datastore-rocs',
                                               'filt-rgf-datatable',
                                               'filt-tablediv'],
                                namespace='filtration-rgf',
                                keep_period=1060)
button_availabilities = {
        "oos":['GOODW', 'HUBY', 'ELVINGTON'],
        'backwashing':['GOODW','HUBY','ELVINGTON'],
        'backwashdue': ['GOODW', 'ELVINGTON','HUBY'],
        'backwashrequired':['GOODW', 'ELVINGTON','HUBY'],
        'filterbedcond':['ELVINGTON'],
        'excessqueuebackwash':['GOODW', 'ELVINGTON'],
        'uv254':['GOODW', 'ELVINGTON'],
        'particleflow':['ELVINGTON'],
        'particlecounter':['ELVINGTON'],
        'outletflow':['ELVINGTON','HUBY','GOODW'],
        'intlevel':['ELVINGTON','HUBY','GOODW'],
        'intlevelb':['ELVINGTON'],
        'turbidity':['ELVINGTON','HUBY','GOODW'],
        'turbidityb':['ELVINGTON'],
        'backwashpressure':['ELVINGTON','HUBY'],
        'backwashpressureb':['ELVINGTON'],
        'backwashflow':['ELVINGTON','HUBY','GOODW']

}

content = html.Div(
    className="filtration-rgf__page",
    children=[
        # First Row: Filters
        dcc.Loading(id='filt-datastore-roc-loading', children=[dcc.Store(id='filt-datastore-rocs')]),
        dcc.Loading(id='filt-datastore-loading', children =[dcc.Store(id='filt-datastore')] ),
        html.Div(
            className="filtration-rgf__row-1 w-12 mb-16 h-filter",
            children=[
                html.Div(
                    className="filtration-rgf__g1 w-5 bg-white p-4 mr-16 h-filter",
                    children=[
                        html.Div(
                            className="filtration-rgf__title title",
                            children=["Status"],
                        ),
                        html.Div(
                            className="filtration-rgf__body filtration-rgf__g1--body",
                            children=[
                                dcc.Loading(id='rgf-updatestat-oos',
                                            children=html.Div(
                                                        id='filtration-rgf__g1--b1',
                                                        className="button-f mr-16",
                                                        children=[
                                                            html.Div("Out of service",
                                                                     className="button-f__title"),
                                                            html.Div("", className="button-f__value",
                                                                     id="filtration-rgf__g1--b1-value")]
                                            )
                                ),
                                dcc.Loading(id='rgf-updatestat-bwashing',
                                            children=html.Div(
                                                    id='filtration-rgf__g1--b2',
                                                    className="button-f mr-16",
                                                    children=[
                                                        html.Div("Backwashing", className="button-f__title"),
                                                        html.Div("", className="button-f__value",
                                                                 id="filtration-rgf__g1--b2-value")]
                                            )
                                ),
                                dcc.Loading(id='rgf-updatestat-bwdue',
                                            children=html.Div(
                                    id='filtration-rgf__g1--b3',
                                    className="button-f mr-16",
                                    children=[
                                        html.Div("Backwash due", className="button-f__title"),
                                        html.Div("", className="button-f__value", id="filtration-rgf__g1--b3-value")
                                    ])
                                ),
                                dcc.Loading(id='rgf-updatestat-bwreq',
                                            children=html.Div(
                                                        id='filtration-rgf__g1--b4',
                                                        className="button-f mr-16",
                                                        children=[
                                                            html.Div("Backwash required",
                                                                     className="button-f__title"),
                                                            html.Div("", className="button-f__value",
                                                                     id="filtration-rgf__g1--b4-value")]
                                            )
                                ),
                                dcc.Loading(id='rgf-updatestat-filterbed',
                                            children=html.Div(
                                                        id='filtration-rgf__g1--b5',
                                                        className="button-f mr-16",
                                                        children=[
                                                            html.Div("Filter bed condition change",
                                                                     className="button-f__title"),
                                                            html.Div("", className="button-f__value",
                                                                     id="filtration-rgf__g1--b5-value")]
                                            )
                                ),
                                dcc.Loading(id='rgf-updatestat-excqueue',
                                            children=html.Div(
                                                            id='filtration-rgf__g1--b6',
                                                            className="button-f mr-16",
                                                            children=[
                                                                html.Div("Excess que for backwash",
                                                                         className="button-f__title"),
                                                                html.Div("", className="button-f__value",
                                                                         id="filtration-rgf__g1--b6-value")]
                                            )
                                ),
                            ],
                        )
                    ]
                ),
                html.Div(
                    className="filtration-rgf__g2 w-7 bg-white p-4 h-filter",
                    children=[
                        html.Div(
                            className="filtration-rgf__title title",
                            children=["Performance"],
                        ),
                        html.Div(
                            className="filtration-rgf__body filtration-rgf__g2--body",
                            children=[
                                dcc.Loading(id='rgf-loadingstat-uv254',
                                            children=html.Div(
                                                            id='filtration-rgf__g2--b1',
                                                            className="button-f mr-16",
                                                            children=[
                                                                html.Div("UV254", className="button-f__title"),
                                                                html.Div("", className="button-f__value",
                                                                         id="filtration-rgf__g2--b1-value")]
                                            )
                                ),
                                dcc.Loading(id='rgf-loadingstat-flow',
                                            children=html.Div(
                                                                id='filtration-rgf__g2--b2',
                                                                className="button-f mr-16",
                                                                children=[
                                                                    html.Div("Particle flow",
                                                                             className="button-f__title"),
                                                                    html.Div("", className="button-f__value",
                                                                             id="filtration-rgf__g2--b2-value")]
                                            )
                                ),
                                dcc.Loading(id='rgf-updatestat-pcounter',
                                            children=html.Div(
                                                            id='filtration-rgf__g2--b3',
                                                            className="button-f mr-16",
                                                            children=[
                                                                html.Div("Particle counter", className="button-f__title"),
                                                                html.Div("", className="button-f__value",
                                                                         id="filtration-rgf__g2--b3-value")]
                                            )
                                ),
                                dcc.Loading(id='rgf-updatestat-outflow',
                                            children=html.Div(
                                                            id='filtration-rgf__g2--b4',
                                                            className="button-f mr-16",
                                                            children=[
                                                                html.Div("Outlet flow", className="button-f__title"),
                                                                html.Div("", className="button-f__value",
                                                                         id="filtration-rgf__g2--b4-value")],
                                            )
                                ),
                                html.Div(
                                    id='filtration-rgf__g2--b5',
                                    className="button-f__double mr-16",
                                    children=[
                                        dcc.Loading(id='rgf-updatestat-inletlvla',
                                                    children=html.Div(
                                                                    id='filtration-rgf__g2--b5t',
                                                                    className="button-h__double",
                                                                    children=[
                                                                        html.Div("Inlet Level A",
                                                                                 className="button-h__title"),
                                                                        html.Div("", className="button-h__value",
                                                                                 id="filtration-rgf__g2--b5t-value")]
                                                    )
                                        ),
                                        dcc.Loading(id='rgf-updatestat-inletlvlb',
                                                    children=html.Div(
                                                                id='filtration-rgf__g2--b5b',
                                                                className="button-h__double",
                                                                children=[
                                                                    html.Div("Inlet Level B",
                                                                             className="button-h__title"),
                                                                    html.Div("", className="button-h__value",
                                                                             id="filtration-rgf__g2--b5b-value")]
                                                    )
                                        ),
                                    ],
                                ),
                                html.Div(
                                    id='filtration-rgf__g2--b6',
                                    className="button-f__double mr-16",
                                    children=[
                                        dcc.Loading(id='rgf-updatestat-turbida',
                                                    children=html.Div(
                                                                    id='filtration-rgf__g2--b6t',
                                                                    className="button-h__double",
                                                                    children=[
                                                                        html.Div("Turbidity A",
                                                                                 className="button-h__title"),
                                                                        html.Div("", className="button-h__value",
                                                                                 id="filtration-rgf__g2--b6t-value")]
                                                    )
                                        ),
                                        dcc.Loading(id='rgf-updatestat-turbidb',
                                                    children=html.Div(
                                                                id='filtration-rgf__g2--b6b',
                                                                className="button-h__double",
                                                                children=[
                                                                    html.Div("Turbidity B",
                                                                             className="button-h__title"),
                                                                    html.Div("", className="button-h__value",
                                                                             id="filtration-rgf__g2--b6b-value")]
                                                    )
                                        ),
                                    ],
                                ),
                                html.Div(
                                    id='filtration-rgf__g2--b7',
                                    className="button-f__double mr-16",
                                    children=[
                                        dcc.Loading(id='rgf-updatestat-bwpressurea',
                                                    children=html.Div(
                                                                id='filtration-rgf__g2--b7t',
                                                                className="button-h__double",
                                                                children=[
                                                                    html.Div("Backwash pressure A",
                                                                             className="button-h__title"),
                                                                    html.Div("", className="button-h__value",
                                                                             id="filtration-rgf__g2--b7t-value")]
                                                    )
                                        ),
                                        dcc.Loading(id='rgf-updatestat-bwpressureb',
                                                    children=html.Div(
                                                                    id='filtration-rgf__g2--b7b',
                                                                    className="button-h__double",
                                                                    children=[
                                                                        html.Div("Backwash pressure B",
                                                                                 className="button-h__title"),
                                                                        html.Div("", className="button-h__value",
                                                                                 id="filtration-rgf__g2--b7b-value")]
                                                    )
                                        ),
                                    ],
                                ),
                                dcc.Loading(id='rgf-updatestat-bflow',
                                            children=html.Div(
                                                        id='filtration-rgf__g2--b8',
                                                        className="button-f mr-16",
                                                        children=[
                                                            html.Div("Backwash Flow", className="button-f__title"),
                                                            html.Div("", className="button-f__value",
                                                                     id="filtration-rgf__g2--b8-value")]
                                            )
                                ),
                            ],
                        )
                    ]
                ),
            ],
        ),

        # Second Row: Table
        html.Div(
            className="filtration-rgf__row-2 w-12 h-with-filter-1-row-of-1",
            children=[
                html.Div(
                    className="filtration-rgf__g3 w-12 bg-white h-with-filter-1-row-of-1",
                    children=[
                        html.Div(
                            className="filtration-rgf__title title p-4",
                            children=["RGFs with current performance and status details"],
                        ),
                        dcc.Loading( id='filt-loading-tablediv',
                            children= [html.Div(
                                className="filtration-rgf__body h-with-filter-1-row-of-1__body",
                                id='filt-tablediv',

                                children=[
                                # Graph or table please use the same class name, thanks
                                # html.Div(
                                #     id = 'filt-tablediv',
                                #     # className="filtration-rgf__g3--body h-with-filter-1-row-of-1__body",
                                # ),
                                ],
                            )]
                        )
                    ]
                ),
            ]
        ),
    ]
)


def get_value(selected_installation, tag_category, tag_subcat,collection='alarms'):
    """
    gets a single value for the given tags and installation
    """
    # current_datetime = pd.to_datetime(reference_time)
    site_tag_config = RGFCollections.site_tag_config
    tag_df = site_tag_config.loc[(site_tag_config.tag_category.str.lower() == tag_category) &
                                        (site_tag_config.tag_subcategory.str.lower() == tag_subcat) &
                                        (site_tag_config.site_name == selected_installation)]
    tags = list(tag_df['tag_name'].unique())
    if len(tags) == 0:
        val = 'n/a'
        logger.warning("get_value with no tags returning na".format(tags))

    res = query_tags_conditional(tags, collection=collection)

    if len(res) == 0:
        val = 'n/a'
        logger.warning("get_value with: {} tags found no values returning na".format(tags))
    else:
        val = float(res['measurement_value'].values.squeeze())


    return val


@app.callback(output=[Output('filtration-rgf__g1--b1-value', 'children'),
                      Output('filtration-rgf__g1--b1', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_update_oos(selected_installation,current_datetime):
    """
    updates the out of service status button
    GOODW logic: Total compliment of RGF Filters 'OUT OF SERVICE' (OOS)/Full compliment
        * tag_name: RGF_A_RGF1OUTSER
    HUBY logic: Total compliment of RGF Filters 'OUT OF SERVICE' (OOS)/Full compliment
        * tag_name: RGF1_OOS_INSERV_SP
    ELVINGTON logic: Total compliment of RGF Filters OOS/Full compliment (sum Tags:ERGFnnIOSR = 'OUT OF SERVICE'
        * tag_name : ERGF01IOSR

    """

    #try and get the tags from the project collections
    oos_tags = RGFCollections.oos_tags_dict.get(selected_installation, [])

    #if this installation has no tags then return empty
    if not oos_tags:
        return "no oos tags", {'background-color':'grey'}

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    site_tag_config = RGFCollections.site_tag_config

    n_assets = len(oos_tags)

    oos_vals = RGFCollections.OOS_VALS[selected_installation]

    is_alarm = site_tag_config.loc[site_tag_config.tag_name == oos_tags[0],'is_alarm'].values[0]
    if is_alarm:
        collection = 'alarms'
    else:
        collection = 'sensors'

    tags_res = query_tags_conditional(tag_list=oos_tags,
                                     reference_time= current_datetime,
                                     collection=collection, latest_only=True)

    if len(tags_res) == 0:
        return "data error", {'background-color':'grey'}
    oos_res = tags_res.loc[tags_res.measurement_value.isin(oos_vals)]

    n_unavail = len(oos_res)
    color = thresh_func(n_unavail,oos_thresholds[selected_installation])
    return_str = "{}/{}".format(n_unavail, n_assets)
    style = {'background-color':color}

    return return_str, style


@app.callback(output=[Output("filtration-rgf__g1--b2-value", 'children'),
                      Output('filtration-rgf__g1--b2', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_update_backwashing(selected_installation, current_datetime):
    """
    updates the backwashing button

    ACCOMB logic: tags in state 2 are queued to bashwash, 3-12  => bacwashing currently
        * tag_name: RGF_DW_RGF1_BWSTEPNUM
        RGFCollections.backwashing_tags
    HUBY logic: only 1 should be backwashing, raw value of current backwashing
        * tag_name: HUBYWTS1:RGF_CURRENT_WASHING
    ELVINGTON logic: individual rgf backwash status
        * tag_name:ERGF01CBS
        #NOTE: currently unused, sensor isnt being reported. Francis has requested

    """
    # individual_tag_df = RGFCollections.individual_tag_df
    # individual_tag_df_site = individual_tag_df.loc[individual_tag_df]
    backwash_tags = RGFCollections.backwashing_tags.get(selected_installation, [])
    n_assets = RGFCollections.num_rgfs[selected_installation]

    if not backwash_tags or selected_installation not in button_availabilities['backwashing']:
        return "", {'background-color':'grey'}

    backwash_vals = []
    for backwash_tag in backwash_tags:
        res = query_tags_conditional(tag_list=backwash_tag, latest_only=True,reference_time=current_datetime,
                                               collection='sensors')
        backwash_vals.append(res)
    backwash_vals= pd.concat(backwash_vals,axis=0)


    if len(backwash_vals) == 0:
        return "no data", {'background-color':'grey'}

    if selected_installation in preprocess_backwashing_funcs:
        backwash_val = preprocess_backwashing_funcs[selected_installation](backwash_vals)

    #currently no color for acomb, elvington or huby according to doc matrix v10
    if backwash_val is not None:
        color =backwash_thresholds(backwash_val)
        style = {'background-color':color}
        return_str = "{}/{}".format(backwash_val, n_assets)
    else:
        return_str = ""
        style = {'background-color':'grey'}

    return return_str, style


@app.callback(output=[Output('filtration-rgf__g1--b3-value', 'children'),
                      Output('filtration-rgf__g1--b3', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_update_backwash_due(selected_installation, current_datetime):
    """
    updates the backwash due button

    GOODW logic: servicehours >= thresholds -1h => servicehours >= 26-1
        * tag_name GOODWDA:RGFF_DA_RGF_1_HIF
    HUBY logic: service hours >= thresholds -h => servicehours >= 27-1
        * tag_name HUBYWTS1:RGF1_SERVICE_TIME_SI
    ELVINGTON logic: service hours >= thresholds -1h => servicehours >= 48-1
        * tag_name ELVGTNS1:ERGF01TIS

    """
    service_hour_tag_dict = RGFCollections.service_hour_tags

    if not selected_installation in service_hour_tag_dict:
        return "", {'background-color':'grey'}

    service_hour_tags = service_hour_tag_dict[selected_installation]
    #NOTE no thresholds needed for any site according to matrix v10
    res = query_tags_conditional(tag_list=service_hour_tags, latest_only=True,reference_time=current_datetime,
                                           collection='sensors')

    if len(res) == 0:
        return "no data", {'background-color':'grey'}


    try:
        service_df = get_service_status(oos_tags=RGFCollections.oos_tags_dict[selected_installation],
                                        reference_time=current_datetime,
                                        service_func=assign_filtering_funcs[selected_installation])

        res = res.sort_values(by='tag_name').reset_index(drop=True)
        res = pd.merge(res, service_df, how='left', left_index=True,right_index=True)

    except Exception as e:
        res['service_status'] = 'filtering'
        logger.warning('filt_update_backwash_due: failed checking the filtering status')

    n_assets = len(service_hour_tags)

    service_h_thresh = service_hour_thresholds[selected_installation]
    backwash_due = len(res.loc[(res.measurement_value >= service_h_thresh)&
                               (res.service_status == 'filtering')])

    return_str = "{}/{}".format(backwash_due, n_assets)

    return return_str, {}


@app.callback(output=[Output('filtration-rgf__g1--b4-value', 'children'),
                      Output('filtration-rgf__g1--b4', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_backwash_required(selected_installation, current_datetime):
    """
    updates the backwash required button

    GOODW rules: Look at headloss alarm tags with a High alarm value
        * tag_name: GOODWDA:RGFF_AV_RGF1HDLSSH
    HUBY rules:  Look at differential pressure 95% percentile >  => needs backwashing
        * tag_name: HUBYWTS1:RGF1_DP_SI
    ELVINGTON rules: filterbed cond >= 1.6
        * tag_name: ELVGTNS1:ERGF0107
    """

    if selected_installation not in button_availabilities['backwashrequired']:
        return "", {'background-color':'grey'}

    #NOTE no thresholds required for any site according to matrix v10
    n_required, n_assets = backwash_req_funcs[selected_installation](current_datetime)
    if np.isnan(n_required):
        return "no data", {'background-color':'grey'}
    return_str = "{}/{}".format(n_required, n_assets)
    return return_str, {}


@app.callback(output=[Output('filtration-rgf__g1--b5-value', 'children'),
                      Output('filtration-rgf__g1--b5', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_filterbed_condition(selected_installation, current_datetime ):
    """
    updates the backwash filterbed condition change button
    Filter Bed condition change. Number of RGFs in rapidly changing. Cat:RGF.
    SubCat: BedCond. Want to count based on ROC over an hour. Change     >=1.5 in an hour.
    No tag for Acomb or Huby. No alert colour.
    """

    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)

    if selected_installation not in button_availabilities['filterbedcond']:
        return "", {'background-color':'grey'}

    tags = RGFCollections.filter_bed_cond_tags[selected_installation]

    latest_values = query_tags_conditional(tag_list=tags,
                                         reference_time=current_datetime,
                                         time_span=20 / 60 / 24,
                                         latest_only=True)

    data_1h_ago = query_tags_conditional(tag_list=tags,
                                         reference_time=current_datetime - timedelta(hours=1),
                                         time_span=20 / 60 / 24,
                                         latest_only=True)
    if len(latest_values) == 0:
        return "data error", {'background-color':'grey'}

    latest_values = latest_values.sort_values(by='tag_name').reset_index(drop=True)


    try:
        service_df = get_service_status(oos_tags=RGFCollections.oos_tags_dict[selected_installation],
                                        reference_time=current_datetime,
                                        service_func=assign_filtering_funcs[selected_installation])
        latest_values = pd.merge(latest_values, service_df, how='left', left_index=True,right_index=True)
    except Exception as e:
        latest_values['service_status'] = 'filtering'
        logger.warning('filt_filterbed_condition: failed checking the filtering status')

    rocs_bad = []

    try:
        data_1h_ago = data_1h_ago.sort_values(by='tag_name').reset_index(drop=True)
        rocs = latest_values['measurement_value'] - data_1h_ago['measurement_value']
        rocs_bad += rocs.loc[(rocs>0.08)&
                             (latest_values['service_status']=='filtering')].index.values.tolist()
    except:
        logger.warning('filt_filterbed_condition: no roc data could be retrieved')

    abs_value_bad = latest_values.loc[(latest_values.measurement_value >=1.6)&
                                      (latest_values.service_status == 'filtering')].index.values.tolist()

    n_bad = len(set(abs_value_bad+rocs_bad))

    return "{}/{}".format(n_bad, len(tags)), {'background-color': 'white'}


@app.callback(output=[Output('filtration-rgf__g1--b6-value', 'children'),
                      Output('filtration-rgf__g1--b6', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_backwash_excessqueue(selected_installation, current_datetime):
    """
    updates the excess queue for backwash button
    Excess queue for backwash. Number of in alarm state for Backwash. Cat:RGF. SubCat: BackWashQueueExcess.
     Want to all RGFs with Text “ALARM”. No tag for Huby.
     Acomb might refer to value of “IN EXCESS” for Subcategory BackWashQueue – Check.
      Elvington Colour: >3 Red, =3 Amber. No colour rule for Acom?
    """
    site_tag_config = RGFCollections.site_tag_config

    if selected_installation not in button_availabilities['excessqueuebackwash']:
        return "", {'background-color':'grey'}

    exc_queue_tags = RGFCollections.excess_queue_tags[selected_installation]
    is_alarm = site_tag_config.loc[site_tag_config.tag_name.isin(exc_queue_tags), 'is_alarm'].unique()

    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)


    if is_alarm:
        collection = 'alarms'
    else:
        collection = 'sensors'

    res = query_tags_conditional(tag_list=exc_queue_tags,
                                 reference_time=current_datetime,
                                 collection=collection,
                                 latest_only=True)
    if len(res) == 0:
        return "no data", {'background-color':'grey'}
    n_assets = len(exc_queue_tags)
    try:
        service_df = get_service_status(oos_tags=RGFCollections.oos_tags_dict[selected_installation],
                                        reference_time=current_datetime,
                                        service_func=assign_filtering_funcs[selected_installation])
        res = res.sort_values(by='tag_name').reset_index(drop=True)
        res = pd.merge(res, service_df, how='left', left_index=True,right_index=True)
    except Exception as e:
        res['service_status'] = 'filtering'
        logger.warning('filt_backwash_excessqueue: failed checking the filtering status')


    n_excess = excess_queue_funcs[selected_installation](res)
    return_str = "{}/{}".format(n_excess, n_assets)

    color = get_queue_excess_thresholds(n_excess, selected_installation)
    return return_str, {'background-color':color}


@app.callback(output=[Output('filtration-rgf__g2--b1-value', 'children'),
                      Output('filtration-rgf__g2--b1', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_uv254(selected_installation, current_datetime):
    """
    updates the uv254 button

    ELVINGTON and GOODW only
    """

    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)

    site_tag_config = RGFCollections.site_tag_config

    if selected_installation not in button_availabilities['uv254']:
        return "", {'background-color':'grey'}

    # res = query_tags_conditional(uv254_tags, collection='sensors')
    uv254_tag = RGFCollections.uv254_tags[selected_installation]

    uom = site_tag_config.loc[site_tag_config.tag_name==uv254_tag,
                              "measurement_unit"].values[0]

    diff_res = get_value_diff_periods(reference_time=current_datetime,
                                      periods=3,
                                      tags=uv254_tag)

    if len(diff_res) == 0:
        return "no data", {'background-color':'grey'}

    #if not enough data to have 2 ROC periods
    #then don't calculate it

    diff_tag = "{}_diff".format(uv254_tag)

    color = colors_risk['neutral']
    uv_thresh =  uv254_roc_threshold[selected_installation]['amber']['lower']
    try:
        if diff_res.iloc[-2][diff_tag] >= uv_thresh and diff_res.iloc[-1][diff_tag] >= uv_thresh:
            color = colors_risk['amber']
    except:
        color = colors_risk['neutral']

    return_val = diff_res.iloc[-1][uv254_tag]

    return "{} {}".format( np.round(return_val, DECIM_S),uom), {'background-color': color}

@app.callback(output=[Output('filtration-rgf__g2--b2-value', 'children'),
                      Output('filtration-rgf__g2--b2', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_particleflow(selected_installation, current_datetime):
    """
    updates the particle flow button
    ELVINGTON ONLY
    """

    if selected_installation not in button_availabilities['particleflow']:
        return "", {'background-color':'grey'}

    pflow_tags =RGFCollections.particle_flow_tags[selected_installation]

    pflow_val = query_tags_conditional(tag_list=pflow_tags,reference_time=current_datetime,
                                       collection='sensors',
                                       latest_only=True)

    if len(pflow_val) == 0:
        return "no data" , {'background-color':'grey'}

    pflow_val = pflow_val['measurement_value'].values[0]

    return_str = "{} ml/min"
    color =get_pflow_risk_color(selected_installation,pflow_val)
    return return_str.format(np.round(pflow_val, DECIM_S)), {'background-color':color}

@app.callback(output=[Output('filtration-rgf__g2--b3-value', 'children'),
                      Output('filtration-rgf__g2--b3', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_particlecounter(selected_installation, current_datetime):
    """
    updates the particle counter button
    """
    if selected_installation not in button_availabilities['particlecounter']:
        return "", {'background-color':'grey'}

    pflow_tags =RGFCollections.particle_counter_tags[selected_installation]

    pflow_val = query_tags_conditional(tag_list=pflow_tags,reference_time=current_datetime,
                                       collection='sensors',
                                       latest_only=True)

    if len(pflow_val) == 0:
        return "no data" , {'background-color':'grey'}

    pflow_val = pflow_val['measurement_value'].values[0]

    return_str = "{} ml"
    color = get_pflow_counter_risk_color(selected_installation,pflow_val)
    return return_str.format(np.round(pflow_val,DECIM_S)), {'background-color':color}


@app.callback(output=[Output('filtration-rgf__g2--b4-value', 'children'),
                      Output('filtration-rgf__g2--b4', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_outlet_flow(selected_installation, current_datetime):
    """
    updates the outlet flow button
    """
    if selected_installation not in button_availabilities['outletflow']:
        return "", {'background-color': 'grey'}

    outlet_flow_tags = RGFCollections.outlet_flow_tags[selected_installation]

    val_df = query_tags_conditional(tag_list=outlet_flow_tags,
                                    reference_time=current_datetime,
                                    collection='sensors',
                                    latest_only=True)
    if len(val_df) == 0:
        return "no data", {'background-color': 'grey'}

    uom =val_df['measurement_unit'].values[0]

    val = val_df['measurement_value'].values.squeeze()

    if selected_installation in preprocess_outlet_flow_funcs:
        val = preprocess_outlet_flow_funcs[selected_installation](val)

    return_str = "{} {}".format(np.round(val,DECIM_S),uom)
    return return_str , {'background-color':'white'}


@app.callback(output=[Output('filtration-rgf__g2--b5t-value', 'children'),
                      Output('filtration-rgf__g2--b5t', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )


def filt_intlevel_a(selected_installation, current_datetime):
    """
    updates the Inlet Level A stream button

    elvington has two
    """

    if selected_installation not in button_availabilities['intlevel']:
        return "" , {'background-color': 'grey'}
    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)

    int_level_tag = RGFCollections.int_levela_tags[selected_installation]
    val_df = query_tags_conditional(tag_list=int_level_tag,
                                    reference_time=current_datetime,
                                    latest_only=True,
                                    collection='sensors')
    if len(val_df) == 0:
        return "data error", {'background-color':'grey'}

    val = val_df['measurement_value'].values.squeeze()
    if selected_installation in preprocess_inlet_level_funcs:
        val = preprocess_inlet_level_funcs[selected_installation](val)

    color = inlet_level_thresholds(val,selected_installation)
    uom =val_df['measurement_unit'].values.squeeze()

    return_str = "{} {}".format(np.round(val, DECIM_S), uom)

    return return_str, {'background-color':color}


@app.callback(output=[Output('filtration-rgf__g2--b6t-value', 'children'),
                      Output('filtration-rgf__g2--b6t', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_turbidity_a(selected_installation, current_datetime):
    """
    updates the turbidity stream a button
    """
    if selected_installation not in button_availabilities['turbidity']:
        return "", {'background-color': 'grey'}
    site_tag_config = RGFCollections.site_tag_config

    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)

    turbidity_tags = RGFCollections.turbiditya_tags[selected_installation]
    uom = site_tag_config.loc[site_tag_config.tag_name.isin(turbidity_tags), 'measurement_unit'].unique()[0]
    diff_res = get_value_diff_periods(tags=turbidity_tags,
                                      reference_time=current_datetime,
                                      periods=3)
    if len(diff_res) == 0:
        return "no data", {'background-color': 'grey'}

    try:
        thresh = turbidity_thresholds(selected_installation)
        if isinstance(turbidity_tags, list):
            diff_tag = "{}_diff".format(turbidity_tags[0])
        else:
            diff_tag = "{}_diff".format(turbidity_tags)

        if diff_res.iloc[-2][diff_tag] > thresh and diff_res.iloc[-1][diff_tag]> thresh:
            color = colors_risk['amber']
        else:
            color = colors_risk['neutral']
    except:
        color = colors_risk['neutral']

    val = diff_res.iloc[-1][turbidity_tags].values.squeeze()

    return_str = "{} {}".format(np.round(val, DECIM_S), uom)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-rgf__g2--b7t-value', 'children'),
                      Output('filtration-rgf__g2--b7t', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_backwash_pressure_a(selected_installation, current_datetime):
    """
    updates the backwash pressure stream a button
    """

    if selected_installation not in button_availabilities['backwashpressure']:
        return "" , {'background-color': 'grey'}
    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)

    backwash_pressure_a_tags = RGFCollections.backwash_pressure_a_tags[selected_installation]

    # val_df = query_tags_conditional(tag_list=backwash_pressure_a_tags,
    #                                 reference_time=current_datetime,
    #                                 latest_only=False,
    #                                 collection='sensors')
    val_df = query_max_value(tag_list=backwash_pressure_a_tags,
                             reference_time=current_datetime,
                             value_cap=1.,
                             lookback=24,
                             collection='sensors')

    if len(val_df) == 0:
        return "data error", {'background-color':'grey'}
    max_df = val_df.loc[val_df.measurement_value == val_df.measurement_value.max()]
    ts = max_df['measurement_timestamp'].dt.time.values[0].strftime("%H:%M")
    if selected_installation in preprocess_backwash_pressure_funcs:
        val = preprocess_backwash_pressure_funcs[selected_installation](max_df)

    site_tag_config = RGFCollections.site_tag_config
    uom = site_tag_config.loc[site_tag_config.tag_name.isin(backwash_pressure_a_tags),
                              'measurement_unit'].unique()[0].lower()

    return_str = "{}{}\n@{}".format(np.round(val,DECIM_S), uom, ts)
    color =  backwash_pressure_thresholds(val, selected_installation)

    return return_str, {'background-color':color}


@app.callback(output=[Output('filtration-rgf__g2--b5b-value', 'children'),
                      Output('filtration-rgf__g2--b5b', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_intlevel_b(selected_installation, current_datetime):
    """
    updates the intlevel b button
    """
    if selected_installation not in button_availabilities['intlevelb']:
        return "" , {'background-color': 'grey'}

    int_level_tag = RGFCollections.int_levelb_tags[selected_installation]
    val_df = query_tags_conditional(tag_list=int_level_tag,
                                    reference_time=current_datetime,
                                    latest_only=True,
                                    collection='sensors')
    if len(val_df) == 0:
        return "data error", {'background-color':'grey'}

    val = val_df['measurement_value'].values.squeeze()

    uom =val_df['measurement_unit'].values.squeeze()

    return_str = "{} {}".format(np.round(val,DECIM_S), uom)

    return return_str, {'background-color':'white'}


@app.callback(output=[Output('filtration-rgf__g2--b6b-value', 'children'),
                      Output('filtration-rgf__g2--b6b', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_turbidity_b(selected_installation, current_datetime):
    """
    updates the turbidity b button
    """
    if selected_installation not in button_availabilities['turbidityb']:
        return "", {'background-color': 'grey'}
    site_tag_config = RGFCollections.site_tag_config

    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)

    turbidity_tags = RGFCollections.turbidityb_tags[selected_installation]
    uom = site_tag_config.loc[site_tag_config.tag_name.isin(turbidity_tags), 'measurement_unit'].unique()[0]

    reference_times = [current_datetime,
                       current_datetime-timedelta(20),
                       current_datetime-timedelta(20)]
    spans = [20,20,20]
    diff_res = get_value_diff_periods(tags=turbidity_tags,
                                      reference_time=current_datetime,
                                      periods=3)
    if len(diff_res) == 0:
        return "data error", {'background-color': 'grey'}

    thresh = turbidity_thresholds(selected_installation)
    if isinstance(turbidity_tags, list):
        diff_tag = "{}_diff".format(turbidity_tags[0])
    else:
        diff_tag = "{}_diff".format(turbidity_tags)

    try:
        if diff_res.iloc[-2][diff_tag] > thresh and diff_res.iloc[-1][diff_tag]> thresh:
            color = colors_risk['amber']
        else:
            color = colors_risk['neutral']
    except:
        color = colors_risk['neutral']


    val = diff_res.iloc[-1][turbidity_tags].values.squeeze()
    return_str = "{} {}".format(np.round(val,DECIM_S), uom)

    return return_str, {'background-color': color}


@app.callback(output=[Output('filtration-rgf__g2--b7b-value', 'children'),
                      Output('filtration-rgf__g2--b7b', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_backwash_pressure_b(selected_installation, current_datetime):
    """
    updates the backwash pressure stream b button
    """

    if selected_installation not in button_availabilities['backwashpressureb']:
        return "" , {'background-color': 'grey'}
    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)

    backwash_pressure_b_tags = RGFCollections.backwash_pressure_b_tags[selected_installation]
    val_df = query_max_value(tag_list=backwash_pressure_b_tags,
                             reference_time=current_datetime,
                             value_cap=1.,
                             lookback=24,
                             collection='sensors')
    max_df = val_df.loc[val_df.measurement_value == val_df.measurement_value.max()]
    ts = max_df['measurement_timestamp'].dt.time.values[0].strftime("%H:%M")

    if len(val_df) == 0:
        return "data error", {'background-color': 'grey'}

    if selected_installation in preprocess_backwash_pressure_funcs:
        val = preprocess_backwash_pressure_funcs[selected_installation](max_df)

    site_tag_config = RGFCollections.site_tag_config
    uom = site_tag_config.loc[site_tag_config.tag_name.isin(backwash_pressure_b_tags),
                              'measurement_unit'].unique()[0].lower()

    color =  backwash_pressure_thresholds(val, selected_installation)

    return_str = "{}{}\n@{}".format(np.round(val, DECIM_S), uom ,ts)

    return return_str, {'background-color':color}


@app.callback(output=[Output('filtration-rgf__g2--b8-value', 'children'),
                      Output('filtration-rgf__g2--b8', 'style')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')]
              )
def filt_backwash_flow(selected_installation, current_datetime):
    if selected_installation not in button_availabilities['backwashflow']:
        return "" , {'background-color': 'grey'}
    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)

    backwash_flow_tags = RGFCollections.backwash_flow_tags[selected_installation]

    val_df = query_max_value(tag_list=backwash_flow_tags,
                             reference_time=current_datetime,
                             # value_cap=1.,
                             lookback=24,
                             collection='sensors')

    if len(val_df) == 0:
        return "data error", {'background-color': 'grey'}
    max_df = val_df.loc[val_df.measurement_value == val_df.measurement_value.max()]
    ts = max_df['measurement_timestamp'].dt.time.values[0].strftime("%H:%M")
    val = max_df['measurement_value'].values.squeeze()

    if selected_installation in preprocess_backwashflow_funcs:
        val  = preprocess_backwashflow_funcs[selected_installation](val)

    uom = val_df['measurement_unit'].values.squeeze()

    color = backwash_flow_tresholds(val,selected_installation)
    return_str = "{} {}\n@{}".format(np.round(val,DECIM_S), uom,ts)

    return return_str, {'background-color':color}


@app.callback(output=[Output('filt-datastore', 'data'),
                      Output('filt-datastore-rocs', 'data')],
              inputs=[Input('nav-selected-installation', 'data'),
                      Input('nav-datetime', 'data')])
def filt_update_datastore(selected_installation, current_datetime):
    """
    One row per RGF found for the site. Sequence by RGF number.
    Show one column for all possible RGF value (take superset from all sites) and populate where we can. Each cell coloured according to provided rules.

    """

    #need to fetch all the RGF assets for the selected installation
    current_datetime = pd.to_datetime(current_datetime,
                                      format=ProjectCollections.dashboard_datetime_format)
    new_df, rocs_df = update_datastore_df(collection_obj=RGFCollections,
                                          asset_type='RGF',
                                          selected_installation=selected_installation,
                                          reference_time=current_datetime,
                                          postprocess_df_funcs=postprocess_df_funcs)


    ts_now_str = datetime.strftime(current_datetime, '%Y-%m-%d %H:%M')

    hash_str_data = ts_now_str+selected_installation+'data'
    hash_str_rocs = ts_now_str+selected_installation+'rocs'

    hash_code_data = hashlib.sha1(hash_str_data.encode()).hexdigest()
    hash_code_rocs = hashlib.sha1(hash_str_rocs.encode()).hexdigest()

    filtration_cacher.store('filt-datastore-rocs',hash_code_rocs,rocs_df,current_datetime)
    filtration_cacher.store('filt-datastore',hash_code_data,new_df,current_datetime)

    return hash_code_data, hash_code_rocs


@app.callback(
    output=Output('filt-rgf-datatable', 'data'),
    inputs=[Input('filt-rgf-datatable', "page_current"),
     Input('filt-rgf-datatable', "page_size")],
    state=[State('filt-datastore', 'data')])
def filt_rgf__navigate_readings_table(page_current,page_size, current_data):
    data  = filtration_cacher.get('filt-rgf-datatable',current_data)

    if data.empty:
        return list()

    return data.iloc[
        page_current*page_size:(page_current+ 1)*page_size
    ].to_dict(orient='records')


@app.callback(output=Output('filt-tablediv', 'children'),
              inputs=[Input('filt-datastore', 'data'),
                      Input('filt-datastore-rocs','data')],
              state=[State("nav-selected-installation", 'data'),
                     State('nav-datetime', 'data')]
              )
def update_filt_rgf_datatable(input_key_data,input_key_rocs,
                              selected_installation,current_datetime):
    data = filtration_cacher.get('filt-tablediv',input_key_data)
    rocs_data = filtration_cacher.get('filt-tablediv',input_key_rocs)

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    if len(data) == 0:
        table = html.H2("No data found for specified time period")
    else:


        style_cond_queries, style_cond_header = individual_thresholds(installation=selected_installation,
                                                                      df=data,
                                                                      current_datetime=current_datetime,
                                                                      roc_df = rocs_data)

        table = make_table(df=data,
                           page_size=20,
                           style_cell={'font-size':'12px'},
                           id='filt-rgf-datatable',
                           styledata_cond = style_cond_queries,
                           style_header_cond=style_cond_header,
                           title=None,
                           custom_height_multiplier=60)
    return table