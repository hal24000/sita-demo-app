"""
PLC PLC Communication Exceptions 8.8
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from visualization.app_def import app
from data.summary_utils import query_tags_conditional
from data.collections_object import ProjectCollections
from visualization.plot_drawing.plotly_plots import make_table
import numpy as np
import pandas as pd
from datetime import timedelta, datetime


content = html.Div(
    className="plc-communication__page",
    children=[
        # First Row: Filter
        html.Div(
            className="plc-communication__row-1 h-filter mb-16",
            children=[
                html.Div(
                    className="plc-communication__g1 h-filter w-12 bg-white p-4",
                    children=[
                        html.Div(
                            className="plc-communication__title title",
                            children=["Functional area"],
                        ),
                        html.Div(
                            className="plc-communication__body",
                            children=[
                                html.Div(
                                    className="plc-communication__g1--body",
                                    children=[
                                        dcc.Dropdown(multi=True, id='plc-communication-fa-dd'),
                                    ],
                                ),
                            ],
                        )
                    ],
                ),
            ],
        ),
        dcc.Loading(
            id='loading-plc-communication-datastore',
            type=ProjectCollections.loading_animation,
            children=[
                dcc.Store(id='plc-communication-datastore'),
            ]
        ),
        # Second Row: Main grid
        html.Div(
            className="plc-communication__row-2",
            children=[
                html.Div(
                    className="plc-communication__g2 w-12 bg-white",
                    children=[
                        html.Div(
                            className="plc-communication__title title m-4",
                            children=["Equipment with communication issues in last week and last month"],
                        ),
                        html.Div(
                            className="plc-communication__body",
                            children=[
                                html.Div(
                                    id="plc-communication-datatable-div",
                                    className="plc-communication__g1--body  h-with-filter-1-row-of-1__body",
                                ),
                            ],
                        )
                    ],
                ),
            ]
        ),
    ]
)


@app.callback(
        output=Output('plc-communication-datatable', 'data'),
        inputs=[Input('plc-communication-datatable', "page_current"),
                Input('plc-communication-datatable', "page_size")],
        state=[State('plc-communication-datastore', 'data')])
def plc_comms_navigate_readings_table(page_current, page_size, current_data):
    return_data = current_data[
                  page_current * page_size:(page_current + 1) * page_size
                  ]
    return return_data


@app.callback(output=Output('plc-communication-datatable-div', 'children'),
              inputs=[Input('plc-communication-datastore', 'data')])
def update_plc_communications_datatable(data):
    data = pd.DataFrame(data)
    if len(data) == 0:
        table = html.H2(className="text-no-data", children=["No data found for specified input values"])
    else:
        table = make_table(df=data,
                           page_size=15,
                           id='plc-communication-datatable',
                           title=None)
    return table


@app.callback(output=[Output('plc-communication-fa-dd', 'options'),
                      Output('plc-communication-fa-dd', 'value')],
              inputs=[Input('nav-selected-installation', 'data')])
def update_plc_comms_fa_dd(selected_installation):
    conf = ProjectCollections.site_tag_config
    fas = list(conf.loc[conf.site_name == selected_installation, 'functional_area'].unique())

    fas = np.sort(fas)
    options = [
            {'label': val, 'value': val} for val in fas
    ]
    return options, fas


@app.callback(output=Output("plc-communication-datastore", 'data'),
              inputs=[Input('nav-datetime', 'data'),
                      Input('plc-communication-fa-dd', 'value'),
                      Input('nav-selected-installation', 'data')])
def update_comms_issues_datastore(current_datetime, selected_func_area, selected_installation):
    if selected_func_area is None:
        return []

    # tag_subcat_filter = "CommsStatus"
    search_terms = ["COMMS FAILED", 'comms failed', 'I/O Timeout']

    site_tag_config = ProjectCollections.site_tag_config

    current_datetime = datetime.strptime(current_datetime, ProjectCollections.dashboard_datetime_format)

    equipment_types = ['pump', 'otherequip', 'pumps']
    # equip_list = get_equipment_list_local(selected_installation,equipment_types ,selected_func_area)
    equip_list = list(site_tag_config.loc[(site_tag_config.tag_category.str.lower().isin(equipment_types)) &
                                          # (site_tag_config.tag_subcategory == tag_subcat_filter) &
                                          (site_tag_config.functional_area.isin(selected_func_area)) &
                                          (site_tag_config.site_name == selected_installation), 'tag_name'].unique())
    res = query_tags_conditional(tag_list=equip_list, tag_values=search_terms, reference_time=current_datetime,
                                 time_span=30, collection='alarms', retry=False)
    try:
        res.drop("fabricated_key", axis=1, inplace=True)
    except:
        pass

    if len(res) == 0:
        return []

    unique_tags = res.tag_name.unique()

    site_tag_config = ProjectCollections.site_tag_config
    unique_devices = site_tag_config.loc[site_tag_config.tag_name.isin(unique_tags), ['tag_name',
                                                                                      'tag_short_description',
                                                                                      'common_reference']]

    res = pd.merge(res, unique_devices, how='left', on='tag_name')

    # res_temp = res.groupby('common_reference')
    td_7 = timedelta(days=7)
    td_30 = timedelta(days=30)
    res_last_week = res.loc[(res.measurement_timestamp >= current_datetime - td_7) &
                            (res.measurement_timestamp <= current_datetime)]
    res_last_month = res.loc[(res.measurement_timestamp >= current_datetime - td_30) &
                             (res.measurement_timestamp <= current_datetime)]

    td_now = timedelta(minutes=15)
    res_now = res.loc[(res.measurement_timestamp >= current_datetime - td_now) &
                      (res.measurement_timestamp <= current_datetime)]

    res_now = res_now.groupby('tag_short_description')['low'].count().reset_index()
    res_now.rename({'low': 'CommsIssues'}, axis=1, inplace=True)

    res_last_week = res_last_week.groupby('tag_short_description')['low'].count().reset_index()
    res_last_week.rename({'low': 'CommsIssuesLastWeek'}, axis=1, inplace=True)
    res_last_month = res_last_month.groupby('tag_short_description')['low'].count().reset_index()
    res_last_month.rename({'low': 'CommsIssuesLastMonth'}, axis=1, inplace=True)

    res_final = pd.concat([res_now, res_last_week, res_last_month], axis=1)

    if len(res_final) < 10:
        # height=300
        for i in range(4):
            res_final = res_final.append({col: "" for col in res_final.columns}, ignore_index=True)

    return res_final.fillna(0).to_dict('records')
