"""
Contains various mongo collections, data collections and files to be used by the dashboard
Copyright 2020 HAL24K

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY===

"""
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import pandas as pd
import plotly.graph_objects as go
from datetime import datetime
from visualization.app_def import app
from dash.dependencies import Input, Output, State
from data.summary_utils import (count_equip_failures, count_equip_unavailable, count_temperature_failures,
                                count_comms_failures, count_data_failures, count_maintenance_status,
                                count_delayed_maintenance, get_significant_level_drops, get_color_button,
                                get_site_throughput_exceptions_nos)
from data.water_quality_utils import (get_wq_summary_data, get_wq_plot, make_empty_plot)
from data.pump_runtime_utils import get_pump_duty_status, gen_pump_dutycycle_chart
from visualization.plot_drawing.plotly_plots import draw_bar_chart, make_table
from visualization.plot_drawing.draw_schematic import draw_schematic_svg
from visualization.pages.make_buttons_groups import make_triple_button_group
import plotly.express as px
from data.collections_object import ProjectCollections
from data.pandas_cacher import BrainHolder
from data.summary_utils import MessageStatus

import logging
logger = logging.getLogger('dash-logger')
logger.setLevel('INFO')


# SETTINGS
MAX_ALLOWED_SERIES = 4

risk_store_reader = BrainHolder(client_brains=['schematic', 'flow'],
                                # keep_period=60,
                                namespace="risk-nav")
######################
content = html.Div(
    className='summary__page p-16',
    children=[
        # First row: Buttons
        html.Div(
            className="summary__buttons h-filter mb-16 w-12",
            children=[
                # Button Group 1
                html.Div(
                    className="summary__buttons-group--g1",
                    id="div-2buttons-g1",
                    children=[
                        html.Div(
                            className="summary__buttons-group--title title",
                            children=[
                                "Pump outages"]
                        ),
                        html.Div(
                            className="summary__buttons-group--body ",
                            children=[
                                # bttngrp1
                                html.Div(
                                    className="summary__buttons--g1 mr-8",
                                    id="summ-btngrp-1",
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-bttngrp-1',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                make_triple_button_group(
                                                    ids=[
                                                        'summ-bttn-pumpfailure',
                                                        'summ-bttn-fail-week',
                                                        'summ-bttn-fail-month'
                                                    ],
                                                    names=[
                                                        'Pumps in failure',
                                                        'Last week',
                                                        'Last month'
                                                    ]
                                                )]),
                                    ]
                                ),
                                # bttngrp2
                                html.Div(
                                    className="summary__buttons--g2",
                                    id="summ-btngrp-2",
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-btngrp-2',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                make_triple_button_group(
                                                    ids=[
                                                        "summ-bttn-unavailable",
                                                        'summ-bttn-unav-week',
                                                        'summ-bttn-unav-month'
                                                    ],
                                                    names=[
                                                        'Pumps unavailable',
                                                        'Last week',
                                                        'Last month'
                                                    ]
                                                )]),
                                    ]
                                ),
                            ]
                        )
                    ]
                ),

                # Button Group 2
                html.Div(
                    className='summary__buttons-group--g2',
                    children=[
                        html.Div(
                            className="summary__buttons-group--title title",
                            children=[
                                "Other equipment outages"]
                        ),
                        html.Div(
                            className='summary__buttons-group--body',
                            children=[
                                # btn group 3
                                html.Div(
                                    id="summ-btngrp-3",
                                    className='summary__buttons--g3 mr-8',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-btngrp-3',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                make_triple_button_group(
                                                    ids=[
                                                        "summ-bttn-otherfailure",
                                                        'summ-bttn-otherfail-week',
                                                        'summ-bttn-otherfail-month'
                                                    ],
                                                    names=[
                                                        'Equipment in failure',
                                                        'Last week',
                                                        'Last month'
                                                    ]
                                                )]),
                                    ]
                                ),
                                # btn group 4
                                html.Div(
                                    id="summ-btngrp-4",
                                    className='summary__buttons--g4 mr-8',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-btngrp-4',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                make_triple_button_group(
                                                    ids=[
                                                        "summ-bttn-otherunavail",
                                                        'summ-bttn-otherunavail-week',
                                                        'summ-bttn-otherunavail-month'
                                                    ],
                                                    names=[
                                                        'Equipment unavailable',
                                                        'Last week',
                                                        'Last month'
                                                    ]
                                                )]),
                                    ]
                                ),
                            ]
                        ),
                    ]
                ),
                
                # Button Group 3
                html.Div(
                    className='summary__buttons-group--g2',
                    children=[
                        html.Div(
                            className="summary__buttons-group--title title",
                            children=[
                                "Pump condition"]
                        ),
                        html.Div(
                            className='summary__buttons-group--body',
                            children=[
                                # btn group 5
                                html.Div(
                                    id="summ-btngrp-5",
                                    className='summary__buttons--g5 mr-8',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-btngrp-5',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                html.Div(
                                                    id='summ-bttn-other-motorot',
                                                    className="button-f",
                                                    children=[
                                                        html.Div(
                                                            "Motor over temperature", className="button-f__title"),
                                                        html.Div("0", className="button-f__value",
                                                                    id='summ-bttn-other-bearingot-content')
                                                    ]
                                                )]),
                                    ]
                                ),
                                # btn group 6
                                html.Div(
                                    id="summ-btngrp-4",
                                    className='summary__buttons--g6 mr-8',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-btngrp-4',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                html.Div(
                                                    className="button-f",
                                                    id='summ-bttn-other-bearingot',
                                                    children=[
                                                        html.Div(
                                                            "Bearing over temperature", className="button-f__title"),
                                                        html.Div("0", className="button-f__value",
                                                                    id='summ-bttn-other-motorot-content')
                                                    ]
                                                )]),
                                    ]
                                ),
                            ]
                        ),
                    ]
                ),

                # Button Group 4
                html.Div(
                    className='summary__buttons-group--g2',
                    children=[
                        html.Div(
                            className="summary__buttons-group--title title",
                            children=[
                                "Communications status"
                            ]
                        ),
                        html.Div(
                            className='summary__buttons-group--body',
                            children=[
                                # btn group 7
                                html.Div(
                                    id="summ-btngrp-7",
                                    className='summary__buttons--g7 mr-8',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-btngrp-7',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                make_triple_button_group(
                                                    ids=[
                                                        "summ-bttn-dataissues",
                                                        'summ-bttn-dataissues-week',
                                                        'summ-bttn-dataissues-month'
                                                    ],
                                                    names=[
                                                        'Data issues',
                                                        'Last week',
                                                        'Last month'
                                                    ]
                                                )]),
                                    ]
                                ),
                                # btn group 8
                                html.Div(
                                    id="summ-btngrp-8",
                                    className='summary__buttons--g8 mr-8',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-btngrp-8',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                make_triple_button_group(
                                                    ids=[
                                                        "summ-bttn-commsissues",
                                                        'summ-bttn-commsissues-week',
                                                        'summ-bttn-commsissues-month'
                                                    ],
                                                    names=[
                                                        'Comms issues',
                                                        'Last week',
                                                        'Last month'
                                                    ]
                                                )]),
                                    ]
                                ),
                            ]
                        ),
                    ]
                ),
                
                # Button Group 5
                html.Div(
                    className='summary__buttons-group--g2',
                    children=[
                        html.Div(
                            className="summary__buttons-group--title title",
                            children=[
                                "Maintenance"
                            ]
                        ),
                        html.Div(
                            className='summary__buttons-group--body',
                            children=[
                                # btn group 9
                                html.Div(
                                    id="summ-btngrp-9",
                                    className='summary__buttons--g9',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-btngrp-9',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                make_triple_button_group(
                                                    ids=[
                                                        "summ-bttn-other-maintexc",
                                                        'summ-bttn-othermaintexc-comp',
                                                        'summ-bttn-othermaintexc-canc'
                                                    ],
                                                    names=[
                                                        'Maintenance exceptions',
                                                        'Completed',
                                                        'Canceled'
                                                    ]
                                                )]),
                                    ]
                                ),
                            ]
                        ),
                    ]
                ),

                # Button Group 6
                html.Div(
                    className='summary__buttons-group--g3',
                    id='div-2buttons-g3',
                    children=[
                        html.Div(
                            className="summary__buttons-group--title title",
                            children=[
                                'Flow exceptions']
                        ),
                        html.Div(
                            className='summary__buttons-group--body',
                            children=[
                                html.Div(
                                    id="summ-btngrp-10",
                                    className='summary__buttons--g10 mr-8',
                                    children=[
                                        dcc.Loading(
                                            id='loading-sum-bttn-treatments-thr',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                html.Div(
                                                    className='button-f',
                                                    id='summ-bttn-treatments-thr',
                                                    children=[
                                                        html.Div(
                                                            'Site throughput exceptions', className="button-f__title"),
                                                        html.Div("0", className="button-f__value",
                                                                id='summ-bttn-treatments-thr-content')
                                                    ]
                                                ),
                                            ]
                                        ),
                                    ]
                                ),
                                html.Div(
                                    id="summ-btngrp-11",
                                    className='summary__buttons--g11',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-bttn-treatments-flow-exc',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                html.Div(
                                                    className='button-f',
                                                    id='summ-bttn-treatments-flow-exc',
                                                    children=[
                                                        html.Div(
                                                            "Sampling flow exceptions", className="button-f__title"),
                                                        html.Div("0", className="button-f__value",
                                                                id='summ-bttn-treatments-flow-exc-content')
                                                    ]
                                                ),
                                            ]
                                        ),
                                    ]
                                )
                            ]
                        )
                    ]
                ),
            ]
        ),
        # Second Row: Charts, Schematic and Table
        html.Div(
            className="summary__second-row h-with-filter-1-row-of-1 w-12",
            children=[
                # First column: drops and risks
                html.Div(
                    className='summary__charts mr-16 w-3',
                    children=[
                        # First chart - Risks
                        html.Div(
                            className='summary__charts--risks bg-white h-with-filter-1-row-of-2 mb-16',
                            children=[
                                html.Div(
                                    className="summary__risk-chart--title title p-4",
                                    children=[
                                        'Current pump duty cycles', ]
                                ),
                                html.Div(
                                    className="summary__risk-chart--sub-title",
                                    children=[
                                        'Equipment at risk at midnight', ]
                                ),
                                html.Div(
                                    className='summary__risk-chart--body',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-barh-pumps',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                dcc.Graph(
                                                    className='summary__risk-chart--graph',
                                                    id='summ-barh-pumps',
                                                )
                                            ]
                                        )
                                    ]
                                ),
                            ]
                        ),
                        # Second chart: Drops
                        html.Div(
                            className='summary__charts--drops bg-white h-with-filter-1-row-of-2',
                            children=[
                                html.Div(
                                    className="summary__drops-chart--title title p-4",
                                    children=[
                                        'Significant stock level drops']
                                ),
                                html.Div(
                                    className='summary__drops-chart--body',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-bar-drops',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                dcc.Graph(
                                                    className='summary__drops-chart--graph',
                                                    id='summ-bar-drops',
                                                    config={
                                                        'editable': True,
                                                        'modeBarButtonsToRemove':
                                                        [
                                                            'pan2d',
                                                            'lasso2d',
                                                        ],
                                                        'displaylogo': False
                                                    }
                                                )
                                            ]
                                        )
                                    ]
                                ),
                            ]
                        )
                    ]
                ),
                # Second column: Schematic
                html.Div(
                    className="summary__schematic h-with-filter-1-row-of-1 bg-white mr-16 w-3",
                    children=[
                        # Title
                        html.Div(
                            className="summary__schematic--title title m-4",
                            children=["Water treatment process and aggregated risk"]),
                        # Schematic body
                        html.Div(
                            className="summary__schematic--body h-with-filter-1-row-of-1__body",
                            children=[
                                dcc.Loading(
                                    id='loading-summ-schematic',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        dcc.Graph(
                                            id="summ-schematic",
                                            className="summary__schematic--graph",
                                            config={
                                                'displayModeBar': False
                                            }
                                        )
                                    ]
                                )
                            ]
                        ),
                    ]
                ),
                # Third column: Table and Graph
                html.Div(
                    className='summary__table h-with-filter-1-row-of-1 bg-white w-6',
                    children=[
                        # time series row
                        html.Div(
                            className='summary__table--graph',
                            children=[
                                html.Div(
                                    className="summary__table-graph--title title",
                                    children=['Water quality exceptions -  Last 24 hours',
                                        html.Div(
                                            className="summary__button-links",
                                            children=[
                                                html.Button(
                                                    'Exceptions',
                                                    id='bttn-wq-excep',
                                                    className='button-link mr-8'),
                                                html.Button(
                                                    'Readings',
                                                    id="bttn-wq-read",
                                                    className='button-link mr-8')
                                                ], 
                                            style={
                                                'float': 'right',
                                                'display': 'inline'
                                            }
                                        )
                                    ]
                                ),
                                html.Div(
                                    className="summary__table-graph--sub-title",
                                    children=[
                                        'Water quality exceptions', ]
                                ),
                                html.Div(
                                    className='summary__table-graph--body',
                                    children=[
                                        dcc.Loading(
                                            id='loading-summ-ts',
                                            type=ProjectCollections.loading_animation,
                                            children=[
                                                dcc.Graph(
                                                    className='summary__table-graph--graph h-with-filter-1-row-of-2__body',
                                                    id='summ-ts'
                                                )])
                                    ]
                                ),
                            ]
                        ),
                        dcc.Loading(
                            id='loading-summ-wq-readings-memory',
                            type=ProjectCollections.loading_animation,
                            children=[
                                # STORAGE AREA
                                dcc.Store(
                                    id='summ-wq-readings-ts-store',
                                    storage_type='memory'),
                                dcc.Store(
                                    id='summ-wq-readings-table-store',
                                    storage_type='memory'),
                            ]
                        ),
                        # table row
                        html.Div(
                            className="summary__table--grid h-with-filter-1-row-of-2__body",
                            children=[
                                dcc.Loading(
                                    id='loading-summ-readings-table-div',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        html.Div(
                                            id='summ-readings-table-div',
                                            className='summary__table-graph--grid h-with-filter-1-row-of-2__body',
                                        )
                                    ]
                                )
                            ]
                        )
                    ]
                )
            ]
        ),
    ]
)


@app.callback(
    output=Output(component_id='summ-readings-table-div',
                  component_property='children'),
    inputs=[Input(component_id='summ-wq-readings-table-store',
                  component_property='data')]
)
def update_wq_compliance_table(wq_data: dict):
    """

    Args:
            wq_data:

    Returns:

    """
    wq_summary = pd.DataFrame(wq_data)

    if wq_summary.shape[0] == 0:
        logger.info(
            'update_wq_compliance_table: Empty list of data received, returning empty data table')
        # TODO: create an empty table
        return dash_table.DataTable()
    if wq_summary.shape[0] < 3:
        for i in range(3):
            wq_summary.append(pd.Series(), ignore_index=True)

    logger.info(f'Table WQ: {wq_summary}')
    wq_summary_table = make_table(wq_summary, 10, "summ-readings-table", None)

    return wq_summary_table


@app.callback(
    output=Output(component_id='summ-ts', component_property='figure'),
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='summ-wq-readings-ts-store', component_property='data')]
)
def update_wq_timeseries(datetime_str: str, alert_tags: dict):
    """"""
    logger.info(f'WQ Alert tags are: {alert_tags}.')
    alert_tags = tuple(alert_tags.get('tags', []))
    fig = get_wq_plot(datetime_str, alert_tags, lookback_days=1)
    return fig


@app.callback(
    output=[Output('summ-wq-readings-table-store', 'data'),
            Output('summ-wq-readings-ts-store', 'data')],
    inputs=[Input('nav-datetime', 'data'),
            Input('nav-selected-installation', 'data')]
)
def update_wq_readings_store(datetime_str, selected_installation):
    """
    Updates the water readings storage page with real data. Activated upon changing time and selected installation.
    Need a WQ readings storage to properly navigate the WQ readings table.

    Args:
        datetime_str(str): the string formatted current datetime value
        selected_installation(str): the currently selected treatment plant
    Returns:
        df(list of dicts): the dataframe fetched for the specific range and entry

    """
    # datetime = pd.to_datetime(datetime_str)
    datetime_dt = datetime.strptime(datetime_str, ProjectCollections.dashboard_datetime_format)

    date = datetime_dt.strftime("%d/%m/%Y")
    time = datetime_dt.strftime("%H:%M")

    wq_summary, alert_tags = get_wq_summary_data(
        selected_installation, date=date, time=time)
    logger.info(f'UPDATE STORE: Alert tags are = {alert_tags}.')

    return wq_summary.to_dict(orient='records'), {'tags': list(alert_tags)}


@app.callback(
    output=Output('summ-readings-table', 'data'),
    inputs=[Input('summ-readings-table', "page_current"),
            Input('summ-readings-table', "page_size")],
    state=[State('summ-wq-readings-table-store', 'data')])
def summ_navigate_readings_table(page_current, page_size, current_data):
    return current_data[
        page_current*page_size:(page_current + 1)*page_size
    ]


@app.callback(
   Output(component_id='summ-schematic', component_property='figure'),
   [Input(component_id='nav-selected-installation', component_property='data'),
    Input(component_id='nav-risk-query', component_property='data'),
    Input(component_id='nav-flow-query', component_property='data')])
def update_schematic(selected_plant,hashed_query_risk, hashed_query_flow ):
    risk_df = risk_store_reader.get('schematic', hashed_query_risk)
    risk_df = risk_df[risk_df['site_name'] == selected_plant]
    risk_dict = risk_df.groupby('site_specific_schematic')['risk'].max().to_dict()

    flows = risk_store_reader.get('flow', hashed_query_flow)

    schematic = draw_schematic_svg(selected_plant, plant_risks=risk_dict,
                                   max_allowed_x = 400, max_allowed_y =603, flow_data=flows)

    logger.info(risk_dict)
    return schematic

# get_pump_duty_status, gen_pump_dutycycle_chart

@app.callback(
    output=[Output(component_id='summ-barh-pumps', component_property='figure'),
            Output(component_id='summ-barh-pumps', component_property='config')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation', component_property='data')])
def update_summ_pumps_barh(ref_datetime, plant):
    """

    Args:
        ref_datetime:
        plant:

    Returns:

    """
    ref_datetime = pd.to_datetime(ref_datetime, format=ProjectCollections.dashboard_datetime_format)
    pump_runtimes = get_pump_duty_status(site=plant, reference_datetime=ref_datetime)
    if pump_runtimes.shape[0] == 0:
        logger.warning("update_summ_pumps_barh: No pump runtimes found")
        return go.Figure(layout={'title': 'No data to estimate pump cycle run time'}), {}

    bar_chart_figure_pumps, config = gen_pump_dutycycle_chart(pump_runtimes)

    return bar_chart_figure_pumps, config


@app.callback(
    output=Output(component_id='summ-bar-drops', component_property='figure'),
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation', component_property='data')])
def update_summ_stockdrop_bar(datetime_str, plant):
    relevant_drops, msg = get_significant_level_drops(plant, datetime_str)

    if msg == MessageStatus.DATA_ERROR:
        logger.warning("update_summ_pumps_barh: No stock level data found")
        fig = make_empty_plot(annotation='Data error - no stock level data')
        return fig
    elif msg == MessageStatus.CRITERIA_NOT_MET:
        logger.info("update_summ_pumps_barh: No drops detected")
        fig = make_empty_plot(annotation='All stock levels above lower threshold')
        return fig

    #else drops were detected

    relevant_drops['tag_name'] = relevant_drops['tag_name'].apply(
        lambda x: x.split(":")[1])
    relevant_drops.sort_values(
        by='measurement_value', ascending=False, inplace=True)
    relevant_drops['measurement_value'] = relevant_drops['measurement_value'].round(
        3)
    bar_chart_figure_drops = draw_bar_chart(relevant_drops, x='tag_short_description',
                                            y='measurement_value',
                                            args={
                                                'labels': {
                                                        "tag_short_description": "",
                                                        "measurement_value":     "Stock level",
                                                },
                                                'color':                  'tag_short_description',
                                                'color_discrete_sequence':  px.colors.qualitative.Pastel1

                                            },
                                            layout_update={
                                                'yaxis':         {'visible':        True,
                                                                  'fixedrange': True,
                                                                  'range' : [0,
                                                                            max(relevant_drops['measurement_value'].max(),50)],
                                                                  "showticklabels": True,
                                                                  'showgrid':       False},
                                                'height': 250,

                                                'xaxis':         {'showgrid': False,
                                                                  'fixedrange': True,
                                                                  'tickangle': 60},
                                                'font': {
                                                    'family': "Lato, Courier New, monospace",
                                                    'size': 10,
                                                    'color': "Black"
                                                },
                                                'autosize':      True,
                                                'showlegend': False,
                                                'margin':        dict(l=0, r=0, t=0, b=100),
                                                'paper_bgcolor': "#FAFAF9",
                                                'plot_bgcolor':  "#FAFAF9",
                                                # 'font_color': 'white'
                                            }
                                            )
    return bar_chart_figure_drops


@app.callback(
    output=[Output(component_id='summ-bttn-pumpfailure-content', component_property='children'),
            Output(component_id='summ-bttn-pumpfailure', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-pumpfailure',
                 component_property='style')]
)
def update_summ_bttn_pumpfailure(datetime_str, plant, live, style):
    # print(datetime_str)
    # td = timedelta(hours=24)
    # n = get_devices_conditional(plant,datetime_str,td,"== 'PUMP'",["'FAIL'"] )
    n = count_equip_failures(site=plant, equip_type="Pump",
                             reference_time=datetime_str, time_span=None, live=live)

    color = get_color_button(n, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-fail-month-content', component_property='children'),
            Output(component_id='summ-bttn-fail-month', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-fail-month',
                 component_property='style')]
)
def update_summ_bttn_pumpfailures_month(datetime_str, plant, live, style):
    # td = timedelta(days=30)
    # n = get_devices_conditional(plant,datetime_str,td,"== 'PUMP'",["'FAIL'"] )
    n = count_equip_failures(site=plant, equip_type="Pump",
                             reference_time=datetime_str, time_span=30, live=live)

    color = get_color_button(n, 'monthly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-fail-week-content', component_property='children'),
            Output(component_id='summ-bttn-fail-week', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-fail-week',
                 component_property='style')]
)
def update_summ_bttn_pumpfailures_week(datetime_str, plant, live, style):
    # td = timedelta(days=7)
    # n = get_devices_conditional(plant,datetime_str,td,"== 'PUMP'",["'FAIL'"] )
    n = count_equip_failures(site=plant, equip_type="Pump",
                             reference_time=datetime_str, time_span=7, live=live)

    color = get_color_button(n, 'weekly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-unavailable-content', component_property='children'),
            Output(component_id='summ-bttn-unavailable', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-unavailable',
                 component_property='style')]
)
def update_summ_bttn_pumps_unav(datetime_str, plant, live, style):
    # td = timedelta(hours=24)
    # n = get_devices_conditional(plant,datetime_str,td,"== 'PUMP'",["'UNAVAIL'"] )
    n = count_equip_unavailable(site=plant, equip_type="Pump",
                                reference_time=datetime_str, time_span=None, live=live)

    color = get_color_button(n, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-unav-week-content', component_property='children'),
            Output(component_id='summ-bttn-unav-week', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-unav-week',
                 component_property='style')]
)
def update_summ_bttn_pumps_unav_week(datetime_str, plant, live, style):
    # td = timedelta(days=7)
    # n = get_devices_conditional(plant,datetime_str,td,"== 'PUMP'",["'UNAVAIL'"] )
    n = count_equip_unavailable(
        site=plant, equip_type="Pump", reference_time=datetime_str, time_span=7, live=live)

    color = get_color_button(n, 'weekly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-unav-month-content', component_property='children'),
            Output(component_id='summ-bttn-unav-month', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-unav-month',
                 component_property='style')]
)
def update_summ_bttn_pumps_unav_month(datetime_str, plant, live, style):
    # td = timedelta(days=30)
    # n = get_devices_conditional(plant,datetime_str,td,"== 'PUMP'",["'UNAVAIL'"] )
    n = count_equip_unavailable(
        site=plant, equip_type="Pump", reference_time=datetime_str, time_span=30, live=live)

    color = get_color_button(n, 'monthly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-otherfailure-content', component_property='children'),
            Output(component_id='summ-bttn-otherfailure', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-otherfailure',
                 component_property='style')]
)
def update_summ_bttn_otherfail(datetime_str, plant, live, style):
    # td = timedelta(hours=24)
    # n = get_devices_conditional(plant,datetime_str,td,"!= 'PUMP'",["'FAIL'"] )
    n = count_equip_failures(site=plant, equip_type="OtherEquip",
                             reference_time=datetime_str, time_span=None, live=live)

    color = get_color_button(n, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-otherfail-week-content', component_property='children'),
            Output(component_id='summ-bttn-otherfail-week', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-otherfail-week',
                 component_property='style')]
)
def update_summ_bttn_otherfail_week(datetime_str, plant, live, style):
    # td = timedelta(days=7)
    # n = get_devices_conditional(plant,datetime_str,td,"!= 'PUMP'",["'FAIL'"] )
    n = count_equip_failures(site=plant, equip_type="OtherEquip",
                             reference_time=datetime_str, time_span=7, live=live)

    color = get_color_button(n, 'weekly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-otherfail-month-content', component_property='children'),
            Output(component_id='summ-bttn-otherfail-month', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-otherfail-month',
                 component_property='style')]
)
def update_summ_bttn_otherfail_month(datetime_str, plant, live, style):
    # td = timedelta(days=30)
    # n = get_devices_conditional(plant,datetime_str,td,"!= 'PUMP'",["'UNAVAIL'"] )
    n = count_equip_failures(site=plant, equip_type="OtherEquip",
                             reference_time=datetime_str, time_span=30, live=live)

    color = get_color_button(n, 'monthly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-otherunavail-content', component_property='children'),
            Output(component_id='summ-bttn-otherunavail', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-otherunavail',
                 component_property='style')]
)
def update_bttn_otherunavail(datetime_str, plant, live, style):
    # td = timedelta(hours=24)
    # n = get_devices_conditional(plant,datetime_str,td,"!= 'PUMP'",["'UNAVAIL'"] )
    n = count_equip_unavailable(site=plant, equip_type="OtherEquip",
                                reference_time=datetime_str, time_span=None, live=live)

    color = get_color_button(n, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-otherunavail-week-content', component_property='children'),
            Output(component_id='summ-bttn-otherunavail-week', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-otherunavail-week',
                 component_property='style')]
)
def update_summ_bttn_otherunavail_week(datetime_str, plant, live, style):
    # td = timedelta(days=7)
    # n = get_devices_conditional(plant,datetime_str,td,"!= 'PUMP'",["'UNAVAIL'"] )
    n = count_equip_unavailable(site=plant, equip_type="OtherEquip",
                                reference_time=datetime_str, time_span=7, live=live)

    color = get_color_button(n, 'weekly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-otherunavail-month-content', component_property='children'),
            Output(component_id='summ-bttn-otherunavail-month', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-otherunavail-month',
                 component_property='style')]
)
def update_summ_bttn_otherunavail_month(datetime_str, plant, live, style):
    # td = timedelta(days=30)
    # n = get_devices_conditional(plant,datetime_str,td,"!= 'PUMP'",["'UNAVAIL'"] )
    n = count_equip_unavailable(site=plant, equip_type="OtherEquip",
                                reference_time=datetime_str, time_span=30, live=live)

    color = get_color_button(n, 'monthly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return n, style


@app.callback(
    output=[Output(component_id='summ-bttn-motorot-content', component_property='children'),
            Output(component_id='summ-bttn-motorot', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-motorot', component_property='style')]
)
def update_summ_bttn_other_motorot(datetime_str, plant, live, style):
    num_motor_over_temp = count_temperature_failures(site=plant, temperature_sensor="MOTOR",
                                                     reference_time=datetime_str, time_span=None, live=live)
    color = get_color_button(num_motor_over_temp, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_motor_over_temp, style


@app.callback(
    output=[Output(component_id='summ-bttn-bearingot-content', component_property='children'),
            Output(component_id='summ-bttn-bearingot', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-bearingot',
                 component_property='style')]
)
def update_summ_bttn_other_bearingot(datetime_str, plant, live, style):
    num_bearing_over_temp = count_temperature_failures(site=plant, temperature_sensor="BEARING",
                                                       reference_time=datetime_str, time_span=None, live=live)
    color = get_color_button(num_bearing_over_temp, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_bearing_over_temp, style


@app.callback(
    output=[Output(component_id='summ-bttn-dataissues-content', component_property='children'),
            Output(component_id='summ-bttn-dataissues', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-dataissues',
                 component_property='style')]
)
def update_summ_bttn_dataissues(datetime_str, plant, live, style):
    num_data_failures = count_data_failures(
        site=plant, reference_time=datetime_str, time_span=None, live=live)
    color = get_color_button(num_data_failures, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_data_failures, style


@app.callback(
    output=[Output(component_id='summ-bttn-dataissues-week-content', component_property='children'),
            Output(component_id='summ-bttn-dataissues-week', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-dataissues-week',
                 component_property='style')]
)
def update_summ_bttn_dataissues_week(datetime_str, plant, live, style):
    num_data_failures = count_data_failures(
        site=plant, reference_time=datetime_str, time_span=7, live=live)
    color = get_color_button(num_data_failures, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_data_failures, style


@app.callback(
    output=[Output(component_id='summ-bttn-dataissues-month-content', component_property='children'),
            Output(component_id='summ-bttn-dataissues-month', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-dataissues-month',
                 component_property='style')]
)
def update_summ_bttn_dataissues_month(datetime_str, plant, live, style):
    num_data_failures = count_data_failures(
        site=plant, reference_time=datetime_str, time_span=30, live=live)
    color = get_color_button(num_data_failures, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_data_failures, style


@app.callback(
    output=[Output(component_id='summ-bttn-commsissues-content', component_property='children'),
            Output(component_id='summ-bttn-commsissues', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-commsissues',
                 component_property='style')]
)
def update_summ_bttn_commsissues(datetime_str, plant, live, style):
    num_com_failures = count_comms_failures(
        site=plant, reference_time=datetime_str, time_span=None, live=live)
    color = get_color_button(num_com_failures, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_com_failures, style


@app.callback(
    output=[Output(component_id='summ-bttn-commsissues-month-content', component_property='children'),
            Output(component_id='summ-bttn-commsissues-month', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-commsissues-month',
                 component_property='style')]
)
def update_summ_bttn_commsissues_month(datetime_str, plant, live, style):
    num_com_failures = count_comms_failures(
        site=plant, reference_time=datetime_str, time_span=30, live=live)
    color = get_color_button(num_com_failures, 'monthly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_com_failures, style


@app.callback(
    output=[Output(component_id='summ-bttn-commsissues-week-content', component_property='children'),
            Output(component_id='summ-bttn-commsissues-week', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation',
                  component_property='data'),
            Input(component_id="nav-live-switch", component_property="on")],
    state=[State(component_id='summ-bttn-commsissues-week',
                 component_property='style')]
)
def update_summ_bttn_commsissues_week(datetime_str, plant, live, style):
    num_com_failures = count_comms_failures(
        site=plant, reference_time=datetime_str, time_span=7, live=live)
    color = get_color_button(num_com_failures, 'weekly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_com_failures, style


@app.callback(
    output=[Output(component_id='summ-bttn-other-maintexc-content', component_property='children'),
            Output(component_id='summ-bttn-other-maintexc', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation', component_property='data')],
    state=[State(component_id='summ-bttn-other-maintexc',
                 component_property='style')]
)
def update_summ_bttn_other_maintexc(datetime, plant, style):

    # return "", {'background-color':'grey'}

    num_overdue_maintenance = count_delayed_maintenance(
        site=plant, time_span=365)  # np.random.randint(0,35)
    color = get_color_button(num_overdue_maintenance, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_overdue_maintenance, style


@app.callback(
    output=[Output(component_id='summ-bttn-othermaintexc-comp-content', component_property='children'),
            Output(component_id='summ-bttn-othermaintexc-comp', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation', component_property='data')],
    state=[State(component_id='summ-bttn-othermaintexc-comp',
                 component_property='style')]
)
def update_summ_bttn_othermaintexc_comp(datetime, plant, style):
    # return "", {'background-color':'grey'}

    num_completed = count_maintenance_status(
        site=plant, status="Completed", time_span=365)
    # monthly because plain color
    color = get_color_button(num_completed, 'monthly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_completed, style


@app.callback(
    output=[Output(component_id='summ-bttn-othermaintexc-canc-content', component_property='children'),
            Output(component_id='summ-bttn-othermaintexc-canc', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation', component_property='data')],
    state=[State(component_id='summ-bttn-othermaintexc-canc',
                 component_property='style')]
)
def update_summ_bttn_othermaintexc_canc(datetime, plant, style):
    # return "", {'background-color':'grey'}

    num_cancelled = count_maintenance_status(
        site=plant, status="Cancelled", time_span=365)
    # monthly because color needs tobe plain
    color = get_color_button(num_cancelled, 'monthly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}

    return num_cancelled, style


@app.callback(
    output=[Output(component_id='summ-bttn-treatments-thr-content', component_property='children'),
            Output(component_id='summ-bttn-treatments-thr', component_property='style'),
            Output(component_id='summ-bttn-treatments-flow-exc-content', component_property='children'),
            Output(component_id='summ-bttn-treatments-flow-exc', component_property='style')],
    inputs=[Input(component_id='nav-datetime', component_property='data'),
            Input(component_id='nav-selected-installation', component_property='data')],
    state=[State(component_id='summ-bttn-treatments-thr', component_property='style'),
           State(component_id='summ-bttn-treatments-thr', component_property='style')]
)
def update_summ_bttn_treat_thr(datetime, plant, f_style, sf_style):
    date_time = pd.to_datetime(datetime)
    n_f, n_sf = get_site_throughput_exceptions_nos(date_time,plant)
    f_color = get_color_button(n_f, 'daily')
    sf_color = get_color_button(n_sf, 'daily')

    if f_style is not None:
        f_style['background-color'] = f_color
    else:
        f_style = {'background-color': f_color}

    if sf_style is not None:
        sf_style['background-color'] = sf_color
    else:
        sf_style = {'background-color': sf_color}

    return n_f, f_style, n_sf, sf_style
