"""
Copyright HAL24K 2020
This page is the overview page. It contains the navigation bar and navigation
functionalities. It should import and navigate to the other pages.

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY=== 


"""
# this script will be run twice, once when the index page is imported into runserver.py and once
# when the app.run is called. the second time around the working dir will be the root of the project for some reason
# need to set the working directory to the 'WRKDIR' environment variable set in the runserver.py (project/src)
# so that all scripts assume they are under src/

import os

# script_path = os.path.dirname(os.path.realpath(__file__))
# os.environ['WRKDIR'] = script_path
# os.chdir(os.environ['WRKDIR'])

import dash_core_components as dcc
from dash.dependencies import Input, Output, State
import dash_html_components as html
from visualization.pages import (map_page, summary_page, nav_header,
                                 failure_exceptions, availability_exceptions, water_quality_exceptions,
                                 water_quality_readings, plc_communication, plc_data,
                                 bearing_temperature_exceptions, motor_temperature_exceptions,
                                 # filtration_rgf,filtration_gac,
                                 none_execution_maintenance_plans,
                                 schematic_drilldown)
from visualization.app_def import app
import logging
from data.project_loggers import dash_logger

logger = logging.getLogger(dash_logger)

app.layout = html.Div([
        dcc.Location(id='url', refresh=False, pathname='yorkshire-water'),
        dcc.Store(id='stored-session-url', storage_type='session'),
        nav_header.content,
        html.Div(id='page-content', children=[map_page.content], className="page"),
])

pages = {
        'map':                              map_page.content,
        'summary':                          summary_page.content,
        'comms-issues':                     plc_communication.content,
        'data-issues':                      plc_data.content,
        'availability-exceptions':          availability_exceptions.content,
        'failure-exceptions':               failure_exceptions.content,
        'water-quality-exceptions':         water_quality_exceptions.content,
        'water-quality-readings':           water_quality_readings.content,
        'bearing-temperature-exceptions':   bearing_temperature_exceptions.content,
        'motor-temperature-exceptions':     motor_temperature_exceptions.content,
        # 'filtration-rgf':                   filtration_rgf.content,
        # 'filtration-gac':                   filtration_gac.content,
        'plc-communication':                plc_communication.content,
        'plc-data':                         plc_data.content,
        'none-execution-maintenance-plans': none_execution_maintenance_plans.content,
        'schematic-drilldown':              schematic_drilldown.content,
}


@app.callback(
        Output('page-content', 'children'),
        [Input("url", "pathname")],
        [State('stored-session-url', 'data')]
)
def change_page(url, stored_session_url):
    """
    Changes the pages content div to another
    Args:
        url (str): the input url to navigate to
        stored_session_url (str): this was attempted to be used for when refreshing

    Returns:
        content (html.Div): div containing the target content to display
    """
    logger.info("Entered change page code")
    if url == "/" or url is None:
        if stored_session_url is not None:
            content = pages.get(stored_session_url, map_page.content)
            return content
        else:
            logger.info("setting to map page")
            return map_page.content

    else:
        content = pages.get(url, map_page.content)
        return content
