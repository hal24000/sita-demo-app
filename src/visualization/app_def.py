"""
Copyright HAL24K 2020
App definition script, 'app' to be imported by all scripts that utilize it, e.g. scripts with callbacks.

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY=== 

"""
import dash
from os import environ

app = dash.Dash(__name__)
app.config.suppress_callback_exceptions = True

app.config.update({
        'routes_pathname_prefix':   environ.get("URL_PREFIX"),
        'requests_pathname_prefix': environ.get("URL_PREFIX")
})
