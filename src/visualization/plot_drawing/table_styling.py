"""
Copyright HAL24K 2020
Functions for the bars in the datables

===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY=== 

"""


def data_bars(df, column, n_bins=100):
    """
    gives a column a gradient of colors depending on the value it holds
    Args:
        df (pd.DataFrame): dataframe to be formatted
        column (str): column to be formatted
        n_bins (): number of color bins

    Returns:
        styles (list): conditional formatting queries

    """
    bounds = [i * (1.0 / n_bins) for i in range(n_bins + 1)]
    ranges = [
            ((df[column].max() - df[column].min()) * i) + df[column].min()
            for i in bounds
    ]
    styles = []
    for i in range(1, len(bounds)):
        min_bound = ranges[i - 1]
        max_bound = ranges[i]
        max_bound_percentage = bounds[i] * 100

        if i < 0.6 * len(bounds):
            color = '#91cf60'
        elif i < 0.85 * len(bounds):
            color = '#ffffbf'
        else:
            color = '#fc8d59'

        styles.append({
                'if':            {
                        'filter_query': (
                                                '{{{column}}} >= {min_bound}' +
                                                (' && {{{column}}} < {max_bound}' if (i < len(bounds) - 1) else '')
                                        ).format(column=column, min_bound=min_bound, max_bound=max_bound),
                        'column_id':    column
                },
                'background':    (
                        """
                            linear-gradient(90deg,
                            {color} 0%,
                            {color} {max_bound_percentage}%,
                            white {max_bound_percentage}%,
                            white 100%)
                        """.format(max_bound_percentage=max_bound_percentage,
                           color=color)
                ),
                'paddingBottom': 2,
                'paddingTop':    2
        })

    return styles


def data_bars_diverging(df, column, color_above='#3D9970', color_below='#FF4136'):
    """
    gives a column a gradient of diverging colors based on the value it holds . e.g. from red to blue

    Args:
        df (pd.DataFrame): the dataframe to be formatted
        column (str): the column the formatting is applied on
        color_above (str): the color to use above a threshold, hex code
        color_below (str): the color to use below a threshold, hex code

    Returns:
        styles (list): conditional formatting styles
    """
    n_bins = 100
    bounds = [i * (1.0 / n_bins) for i in range(n_bins + 1)]
    col_max = df[column].max()
    col_min = df[column].min()
    ranges = [
            ((col_max - col_min) * i) + col_min
            for i in bounds
    ]
    midpoint = (col_max + col_min) / 2.

    styles = []
    for i in range(1, len(bounds)):
        min_bound = ranges[i - 1]
        max_bound = ranges[i]
        min_bound_percentage = bounds[i - 1] * 100
        max_bound_percentage = bounds[i] * 100

        style = {
                'if':            {
                        'filter_query': (
                                                '{{{column}}} >= {min_bound}' +
                                                (' && {{{column}}} < {max_bound}' if (i < len(bounds) - 1) else '')
                                        ).format(column=column, min_bound=min_bound, max_bound=max_bound),
                        'column_id':    column
                },
                'paddingBottom': 2,
                'paddingTop':    2
        }
        if max_bound > midpoint:
            background = (
                    """
                        linear-gradient(90deg,
                        white 0%,
                        white 50%,
                        {color_above} 50%,
                        {color_above} {max_bound_percentage}%,
                        white {max_bound_percentage}%,
                        white 100%)
                    """.format(
                            max_bound_percentage=max_bound_percentage,
                            color_above=color_above
                    )
            )
        else:
            background = (
                    """
                        linear-gradient(90deg,
                        white 0%,
                        white {min_bound_percentage}%,
                        {color_below} {min_bound_percentage}%,
                        {color_below} 50%,
                        white 50%,
                        white 100%)
                    """.format(
                            min_bound_percentage=min_bound_percentage,
                            color_below=color_below
                    )
            )
        style['background'] = background
        styles.append(style)

    return styles
