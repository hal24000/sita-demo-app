"""
Copyright HAL24K 2020
Functions to draw schematic in plotly

 
===START OF COPY===
Copyright HAL24K WATER 2020 - All rights reserved
This file is supplied under licence, you are not free to copy or use this software in any other way.
===END OF COPY=== 


"""

import base64
import numpy as np
import pandas as pd
import plotly.graph_objects as go
from sympy import *
from data.collections_object import ProjectCollections as prj_collections
from data.load_data import colors_risk, risk_thresholds
import logging
from data.project_loggers import dash_logger

logger = logging.getLogger(dash_logger)


def draw_schematic_svg(plant, plant_risks: dict, max_allowed_x=400, max_allowed_y=600,
                       flows=True, flow_data=None):
    """
    Adapts the SVG schematic in the defined path so as to color code the risk on it
    then converts the schematic to png and draws it as a background to a plotly graph
    at the location of the shapes with ws.dimension.id properties, invisible points are scattered so as to
    enable interactivity through click_data events.

    Args:
        schematic_path(str): the path to the schematic to be loaded and drawn

    Returns:
        fig(plotly.graph_objects.Figure) The resulting schematic figure
    """

    def get_color(x):
        for key, value in risk_thresholds.items():
            if x < key[1] and x >= key[0]:
                return colors_risk[value]

    svg_mem_soup = prj_collections.schematics_dict_bs[plant]
    fig = go.Figure()
    img_width = int(float(svg_mem_soup.find("svg").get('width').strip('px')))
    img_height = int(float(svg_mem_soup.find("svg").get('height').strip('px')))

    x, y = var('x y')
    scale_y = solve_univariate_inequality(y * img_height <= max_allowed_y, y)
    scale_x = solve_univariate_inequality(x * img_width <= max_allowed_x, x)

    scale_y = float(scale_y.as_set().args[1])
    scale_x = float(scale_x.as_set().args[1])
    scale_factor = min([scale_x, scale_y])

    process_area_schematic = svg_mem_soup.find_all("", attrs={"data-SiteSpecificSchematic": True})

    names = [zone.get('data-SiteSpecificSchematic') for zone in process_area_schematic if zone is not None]
    process_areas = [zone.get('data-ProcessDesc') for zone in process_area_schematic if zone is not None]

    cnt = -1
    shapes = []
    for zone in process_area_schematic:
        if zone is not None:
            cnt += 1
        rects = zone.find_all('rect')

        if rects:
            # if you found rect shapes
            shapes += rects
            risk = plant_risks.get(names[cnt], 0.)
            if np.isnan(risk):
                risk = 0

            rect_color = get_color(risk)
            for rect in rects:
                if rect['style'] != "none":
                    style_split = rect['style'].split(';')
                    fill_split = style_split[0].split(":")

                    fill_split[1] = rect_color
                    fill = ":".join(fill_split)

                    style_split[0] = fill

                    style = ";".join(style_split)
                    rect['style'] = style

        else:
            # some objects are paths
            # draw a box roughly covering the text, it will be an dictionary object
            try:
                p = zone.find_all('path')[0]
            except IndexError:
                # no paths were found - add null shapes to preserve list sizes
                shapes.append(None)
                continue

            x, y = p.get('d').split()[1].split(',')
            x = float(x)
            y = float(y)

            width = 75 #img_width - x
            height = 5  # hardcoded

            # put the start at the bottom left(-ish) of the text
            y += height / 2

            shapes += [{'x':      x, 'y': y,
                        'width':  width,
                        'height': height}]

    if flows:
        nns = svg_mem_soup.find_all("", attrs={"data-label": "NN.N"})

        for bubble in nns:
            if not bubble.find_all("circle"):
                # no actual circle in the schematic
                continue

            process_area = bubble.get('data-ProcessDesc', 'not_available')
            functional_area = bubble.get('data-FunctionalArea', 'not_available')
            site_spec_schematic = bubble.get('data-SiteSpecificSchematic', 'not_available')
            # find text
            for b_text in bubble.find_all("text"):
                try:
                    flow_value = '{0:.1f}'.format(flow_data.loc[(flow_data['site_name'] == plant)
                                                                & (flow_data['functional_area'] == functional_area)
                                                                & (flow_data['process_area'] == process_area)
                                                                & (flow_data[
                                                                       'site_specific_schematic'] == site_spec_schematic), 'flow_value'].values[
                                                      0])
                except IndexError:
                    flow_value = 'NO TAG'

                if flow_value == 'nan':
                    flow_value = 'NO MEAS'

                b_text.string = flow_value

    svg_mem = str(svg_mem_soup)
    svg_mem = svg_mem.encode('utf-8')
    svg_raw = base64.b64encode(svg_mem)

    fig.add_layout_image(
            go.layout.Image(
                    x=0,
                    sizex=img_width,
                    y=img_height,
                    sizey=img_height,
                    xref="x",
                    yref="y",
                    opacity=1.0,
                    layer="below",
                    sizing="stretch",
                    source='data:image/svg+xml;base64,{}'.format(svg_raw.decode()))
    )

    for shape, name, process_area_set in zip(shapes, names, process_areas):
        if shape is None:
            continue

        # name = name_span.getText().lower()
        # if name not in avail_groups:
        height = int(float(shape.get('height')))
        width = int(float(shape.get('width')))

        x = int(float(shape.get('x')))
        y = (int(float(shape.get('y'))) * -1) + img_height

        x2 = x + width
        y2 = y - height
        xs = np.linspace(x, x2, 20)
        ys = np.linspace(y, y2, 20)
        xys = np.array(np.meshgrid(xs, ys)).T.reshape(-1, 2)

        fig.add_trace(go.Scatter(
                x=xys[:, 0],
                y=xys[:, 1],
                opacity=.0,
                text=name,
                hoverinfo='text',
                customdata=[{"submodule": name, "process_areas": process_area_set, "plant": plant} for z in
                            range(len(xys))]
        ))
    fig.update_layout(yaxis={'range':      [0, img_height],
                             'visible':    False,
                             'showgrid':   False,
                             'fixedrange': True,
                             },
                      xaxis={'range':      [0, img_width],
                             'visible':    False,
                             'fixedrange': True,
                             'showgrid':   False},
                      showlegend=False,
                      autosize=False,
                      hovermode='closest',
                      height=img_height * scale_factor,
                      width=img_width * scale_factor,

                      margin=dict(l=0, r=0, t=0, b=0))
    return fig


def draw_pill(x, y, width, height,
              fillcolor, linecolor) -> dict:
    """
    Create a dictionary to append to plotly data trace

    Args:
        x,y (num): coordinates of the center of the shape
        width, height (num): dimensions of the shapes
        fillcolor, linecolor (str): coloring

    Returns:
        dict
    """

    out_dict = {'type':       'path',
                'fillcolor':  fillcolor,
                'line_color': linecolor,
                'opacity':    0.7,
                'layer':      'below'
                }

    # 10% for the rouded edges
    ll_x = x - 0.5 * (width - height)
    ll_y = y - 0.5 * height

    lr_x = x + 0.5 * (width - height)
    lr_y = ll_y

    lrc_x = x + 0.5 * width
    lrc_y = ll_y

    mr_x = lrc_x
    mr_y = y

    urc_x = lrc_x
    urc_y = y + 0.5 * height

    ur_x = lr_x
    ur_y = urc_y

    ul_x = ll_x
    ul_y = ur_y

    ulc_x = x - 0.5 * width
    ulc_y = ur_y

    ml_x = x - 0.5 * width
    ml_y = y

    llc_x = ml_x
    llc_y = ll_y

    out_dict['path'] = "M {},{} L {},{} Q {},{} {},{} \
    Q {},{} {},{} L {},{} Q {},{} {},{} Q {},{} {},{} Z".format(ll_x,
                                                                ll_y,
                                                                lr_x,
                                                                lr_y,
                                                                lrc_x,
                                                                lrc_y,
                                                                mr_x,
                                                                mr_y,
                                                                urc_x,
                                                                urc_y,
                                                                ur_x,
                                                                ur_y,
                                                                ul_x,
                                                                ul_y,
                                                                ulc_x,
                                                                ulc_y,
                                                                ml_x,
                                                                ml_y,
                                                                llc_x,
                                                                llc_y,
                                                                ll_x,
                                                                ll_y)

    return out_dict


def draw_schematic(data: pd.DataFrame, width=3, height=1):
    """
    Based on the input dataframe draw the schematic.

    Args:
        data (pd.DataFrame): dataframe with the asset_name, risk_level and limit_risk

    Returns:
        plotly figure
    """

    machine_labels = []
    ml_xs = []
    ml_ys = []
    risks = []
    r_xs = []
    r_ys = []
    shapes = []

    connection_xs = []
    connection_ys = []

    for i, (ind, row) in enumerate(data.iterrows()):
        cent_x = 0
        cent_y = 2 * (len(data) - i * height)

        machine_labels.append(row['asset_name'])
        ml_xs.append(0)
        ml_ys.append(cent_y + height)

        risks.append(row['risk_level'])
        r_xs.append(0)
        r_ys.append(cent_y)

        shapes.append(draw_pill(cent_x, cent_y,
                                width=width, height=height,
                                fillcolor='blue' if row['risk_level'] <= row['limit_risk'] else 'red',
                                linecolor='blue' if row['risk_level'] <= row['limit_risk'] else 'red')
                      )

        if i + 1 < len(data):
            tmp_xs = [cent_x - width / 2, cent_x - (width / 2 + 1)]
            tmp_xs = tmp_xs + tmp_xs[::-1]
            tmp_ys = [cent_y, cent_y, cent_y - 2 * height, cent_y - 2 * height]

            connection_xs = connection_xs + tmp_xs
            connection_ys = connection_ys + tmp_ys
        else:
            continue

    fig = go.Figure()
    fig.add_trace(go.Scatter(
            x=ml_xs,
            y=ml_ys,
            text=machine_labels,
            mode='text',
            hoverinfo='skip'
    ))

    fig.add_trace(go.Scatter(
            x=r_xs,
            y=r_ys,
            text=risks,
            mode='text',
            hoverinfo='skip',
            textfont=dict(size=20, color='white')
    ))
    fig.add_trace(go.Scatter(
            mode='lines',
            x=connection_xs,
            y=connection_ys,
            line=dict(color='black'),
            hoverinfo='skip',
            textfont=dict(size=20)
    ))
    fig.update_layout(shapes=shapes,
                      showlegend=False,
                      xaxis={'showticklabels': False, 'showgrid': False,
                             'zeroline':       False, 'fixedrange': True},
                      yaxis={'showticklabels': False, 'showgrid': False,
                             'zeroline':       False, 'fixedrange': True})

    return fig
