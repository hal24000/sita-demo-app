water product
Situational awaress dashboard repository for [water generic product dashboard (jira board)](https://hal24k.atlassian.net/secure/RapidBoard.jspa?rapidView=145):

==============================
## Contents

1. How to setup environment
2. Interaction with css
3. Repository contents

### 1. How to setup the environment:


#### A. Setting up
1. Clone the project
2. Set up the dimension pypi server according to: https://hal24k.atlassian.net/wiki/spaces/CD/pages/200376360/PyPi+Server (you will need this to install the dimension package)
3. Put this file with your mongo creds under src/conf/mongo_conf_waterdemo.txt:

```
[mongoDB credentials]
username :
password :
database : cd_waterdemo
uri : 10.0.1.4:27017,10.0.1.5:27017,10.0.1.6:27017
```

username and password are the same as your dimension login creds, you need to be added to the waterdemo tenant first
You will also need VPN access, if running outside of the dimension network, e.g. your home computer.

### Τhe below only works on linux machines
#### B. Getting python, installing requirements, running
5. GO TO https://www.anaconda.com/products/individual
6. Download the appropriate anaconda for your system and install
7. After installation, navigate to the project root and create the environment using
`conda env create -f environment.yml`
8. After the environment has been created, activate the environment with:
`conda activate app_env`
9. If on Linux: 
        Set the environment variable URL_PREFIX=/ by running 
        ```export URL_PREFIX=/ ``` 
10. Run:  `plasma_store -m 1000000000 -s /tmp/plasma` OR run start.sh from next step and skip this
11. You should now be able to run the application with `python app.py` in `src/` OR `sh start.sh` once you copy `start.sh` in src 


#### Note for Windows users:
You may need to save requirements like GDAL, Fiona, Shapely, beautifulsoup etc. one by one to the application to be able to run it.

Brain_plasma has no Windows support yet. You may consider running with Docker, or installing Windows WSL2 or Ubuntu. 

#### C Running with docker

1. Get on the VPN
2. Run docker-compose up -b
3. Navigate to the URL the application indicates, should be something like 0.0.0.0:19888 or 127.0.0.1:19888

#### D Deploying the Dashboard
Make sure that you have version 0.13.2 of dimensionops (as of Jan 7 later versions don’t work)
Pip install dimensionops==0.13.2

Follow the instructions on this document from steps 4 and on
https://docs.google.com/document/d/1aKJ_YWU4RQbEkcAx4zUKhT5TKq2kOrkf/edit

#### E Building the documentation
1. install sphinx for python: `sudo apt-get install python3-sphinx`
2. navigate to the docs folder of the repo `cd docs`
3. if new files have been added to the project  run: `sphinx-apidoc -f -o source ../src`
4. run `make html`
5. You can find the new documentation html page in the build/html folder

### 2. Tweaking CSS:
The web pages for each page in the application are defined as below, in their respective page files:
For example the summary page is located under `src/visualization/pages/summary_page.py` and the web-contents of that page are defined in the variable `content`.

Python code:

```python
#example dummy content
import dash_core_components as dcc
import dash_html_components as html

#the content variable
content = html.Div([
        html.Div([
                html.Div([
                    dcc.Graph(figure=time_series_figure, #dcc.Graph is a plotly element, contents of plot unaffected by css
                              id='summ-tsfigure')
                ], className='col', id='tsfigure-div'), #classname and id of div element
                html.Div([
                    html.Button('refresh',
                                id='summ-bttn-refresh')
                ], className='col', id='refresh-bttn-div')
        ], classname='row')
], className='container')

```
You can place a css file containing CSS directives in a file under:
`src/visualization/assets/css/`
Plotly dash will automatically load the css and any changes and render them on the spot.
For example you could place this css under that folder, and it would update the above elements in the python code.

CSS CODE:

```css
#refresh-bttn-div {
  margin: auto;
}
```

If you want to move the DOM elements of the page altogether, that involves the process of restructuring the python file above.

### 3. Project Organization

The project uses the data science cookiecutter template, however only the `src/visualization` part of the template is actually used. So the rest has been ignored in the description below.

```bash
├── data
│   ├── external
│   ├── interim
│   ├── processed
│   └── raw
├── Dockerfile
├── docs
│   ├── commands.rst
│   ├── conf.py
│   ├── getting-started.rst
│   ├── index.rst
│   ├── make.bat
│   └── Makefile
├── Makefile
├── models
├── notebooks
│   ├── Access API.ipynb
│   ├── assets
│   │   ├── s1.css
│   │   └── scripts.js
│   └── table_playground.ipynb
├── README.md
├── references
├── reports
│   └── figures
├── requirements.txt
├── src
│   ├── conf
│   │   └── mongo_conf.txt
│   ├── data
│   │   ├── collections_object.py
│   │   ├── generate_data.py
│   │   ├── make_dataset.py
│   │   ├── project_loggers.py
│   │   ├── schematic_test.py
│   │   └── tmp_data
│   │       ├── dummy_sensor_data.pkl
│   │       ├── installation_metadata.pkl
│   │       └── relevant_alarm_sample.pkl
│   ├── features
│   │   ├── exception_notifier.py
│   ├── __init__.py
│   ├── Makefile
│   ├── runserver.py
│   ├── setup.py
│   └── visualization
│       ├── app_def.py
│       ├── assets
│       │   ├── css
│       │   │   ├── custom.css
│       │   │   ├── s1.css
│       │   │   └── ywBootstrapTheme.css
│       │   ├── images
│       │   │   ├── schematic_large.svg
│       │   │   ├── schematic_med.svg
│       │   │   ├── schematic_small.svg
│       │   │   └── Top Level Schematic Final.svg
│       │   └── scripts
│       ├── index.py <= The "main" page of the application,
│       ├── pages <=THe different pages of the app live here
│       │   ├── comms_data_issues.py
│       │   ├── exceptions_page.py
│       │   ├── flocc_daf_stream_risks.py
│       │   ├── gac_adsorber_risks.py
│       │   ├── make_buttons_groups.py
│       │   ├── map_page.py
│       │   ├── nav_header.py
│       │   ├── no_maintenance_page.py
│       │   ├── rapid_gravity_filter_risks.py
│       │   ├── summary_page_content.py
│       │   ├── summary_page.py
│       │   └── water_quality.py
│       ├── plot_drawing <= Plot drawing code
│       │   ├── draw_schematic.py  <= draw scehmatic code, pill and from SVG
│       │   ├── plotly_plots.py <= plotly plot code library
│       │   └── table_styling.py
├── test_environment.py
├── tox.ini
└── yorkshire-water-dash.yml <= the conda env file, used with conda create
```
